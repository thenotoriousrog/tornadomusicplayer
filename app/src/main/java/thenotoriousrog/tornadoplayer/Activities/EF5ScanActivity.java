package thenotoriousrog.tornadoplayer.Activities;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.widget.RelativeLayout;

import thenotoriousrog.tornadoplayer.Backend.Album;
import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.SerializeObject;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.Sorters;
import thenotoriousrog.tornadoplayer.Fragments.LoadingScreenFragment;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;


/**
 * Created by thenotoriousrog on 8/24/17.
 * This class is in charge of scanning all of the files within the phone itself and also in charge of populating and setting up the mainUIFragment and display all of users songs and info to the user.
 * This is going to be a pretty intense class and will be done primarily through an AsyncTask.
 *
 * **WARNING: This activity also takes over for the music player activity. So just know that all of the specialized functions in the music player will no longer work and we will have to implement them here,
 *
 * Or better yet, we can just have the InitializerActivity activity do the scanning for us and we will just send a special message telling it that the user wants to scan and there is no special path that we need to be using.
 * todo: we should have the music player activity take care of this for us actually to ensure that the app works as smooth as it can.
 */

public class EF5ScanActivity extends Activity implements Runnable {

    // TODO: The EF5 scan functionality doesn't work on a phone with an SD card. We need to get this fixed ASAP! Otherwise, it won't work!
    ArrayList<String> songPaths = new ArrayList<>(); // list to hold all of the song paths in the list.
    ArrayList<String> folders = new ArrayList<>(); // holds all of the folders in this list.
    ArrayList<SongInfo> songs = new ArrayList<>(); // holds the list of songInfos that the MainUIFragment is going to use.
    ArrayList<Artist> artists = new ArrayList<>(); // holds a list of artists.
    ArrayList<Album> albums = new ArrayList<>(); // holds the list of albums.
    private MainUIFragment mainUIFragment; // a public copy of the mainUIFragment so that we can control what happens when the user presses the back button.
    private final String internalMemoryPath = Environment.getExternalStorageDirectory().getPath() +"/"; // the path to main memory so that we can scan the entire phone in its entirety.
    private String sdMemoryPath = ""; // holds the memory path for the SD card, the program, should read both memory locations, that is very important!
    //final String Memory_Path = Environment.getExternalStorageDirectory().getPath() + "/"; // the path to main memory so that we can scan the entire phone in its entirety.

    final FragmentManager fragmentManager = getFragmentManager();
    final FragmentTransaction mainLayoutTransaction = fragmentManager.beginTransaction(); // start a FragmentTransaction for the mainlayout.
    final LoadingScreenFragment loadingScreenFragment = new LoadingScreenFragment();
    RelativeLayout startupLayout;

    private Runnable uiUpdater;
    private Thread backgroundThread;

    // need to also send a list of playlists, although we may want to modify that so that the MainUIFragment can take an empty list of playlists and be okay with that.
    // need to also send in the music folder file path, which may not be possible because we are searching the entire phone for songs and not necessarily a single list at a time.


    // This method will scan a file and determine if it is a directory or not. If it is a directory it will continue scanning until it hits a file that is not a directory and then decide if it is a song.
    // if it is a song, then the addSongToList method will also add the song and the associated directory into their proper lists.
    private void scanDirectory(File directory)
    {
        if(directory != null)
        {
            File[] filesList = directory.listFiles(); // list the files within that directory.

            // make sure that the list of files exists and also ensure that there is at least one item in that directory before trying to extract anything from it.
            if(filesList != null && filesList.length > 0)
            {
                // for each file in the filesList, check if it is a directory or if it is a file.
                for(File file: filesList)
                {
                    if(file.isDirectory())
                    {
                        scanDirectory(file); // recursively loop back to this method and take the new file and search for songs within it
                    }
                    else // file is not a directory meaning it could be a song. Send to the add SongToList method to determine if that is something that we want to do or not.
                    {
                        addSongToList(file, directory); // send in the current file, and also send in the last directory received by the method to add that directory to the list of folders.
                    }
                }
            }
        }
    }

    // tries to find the folder in the list of folders and reports if the folder exists or not in the list.
    private boolean isDirectoryInFolders(File directory)
    {
        for(int i = 0; i < folders.size(); i++)
        {
            //System.out.println("Directory name we are looking at = " + directory.getName());
            if(folders.get(i).equals(directory.getPath())); // the folder IS in the list of folders thus we need to add it.
            {
                System.out.println("folder found with name = " + directory.getName());
                return true; // this directory does exist in the list, report this to the calling method.
            }
        }

        return false; // the directory was not found in the folders list, report this to the calling method.
    }

    /*
       Looks for the file to end with in either MP3, M4A, FLAC, AAC, OGG, or WAV. If so, it is a valid file.
    */
    private boolean isValidSoundFile(File song)
    {
        String extension = song.getName().toLowerCase();



        // any of the below extensions are sufficient for the android system.
        if(extension.endsWith(".mp3") || extension.endsWith(".m4a") || extension.endsWith(".aac")
                || extension.endsWith(".flac") || extension.endsWith(".wav"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // This method will check to see if the file is a song or not. If the file is a song it will add the
    private void addSongToList(File song, File directory)
    {
        // check if the directory has been added to the list of folders if it has not, then add it otherwise skip adding it. Also, check if null, if null just check to see if it is a song then just add it.


        if(directory != null) // directory is not null meaning we have to check if the list exits and if it does then we want to add it into the list of songs.
        {
            //System.out.println("Directory checking for songs = " + directory.getName());

            if( isDirectoryInFolders(directory) == false ) // folder is new, check for song to be legit before adding to list of folders.
            {
                if(isValidSoundFile(song)) // this file is a song, add to the list of songs.
                {
                    songPaths.add(song.getPath()); // add the song path to the list.
                    folders.add(directory.getPath()); // add the directory path to the list of folders.
                }
            }
            else // folder already exists in the list, but we should double check that the specific file is a song and we should add that song to the list of songs.
            {
                if(isValidSoundFile(song)) // this file is a song, add to the list of songs.
                {
                    songPaths.add(song.getPath()); // add the song path to the list.
                    folders.add(directory.getPath()); // add the directory path to the list of folders.

                }
            }
        }
        else // directory is null, we should just check to see if this is a song and add it to the list of songs.
        {
            if(isValidSoundFile(song)) // check to see if the file is indeed a song.
            {
                songPaths.add(song.getPath()); // add the song path into the list of song paths.
            }
        }

        // add the song file to the list of song paths.
    }

    // This method begins searching for songs in the main memory and starts calling the helper functions to determine what is another directory and what is actually a song.
    private void searchForMusic(String memoryPath)
    {
        //System.out.println("The memory path is: " + memoryPath); // print the memory path that we are working with.

        // ** little piece of code I took from stack overflow by the author Harmeet Singh to start grabbing the songs out of the path. Thanks to him **.
        if (memoryPath != null) {
            File home = new File(memoryPath);
            File[] listFiles = home.listFiles();
            if (listFiles != null && listFiles.length > 0) {
                for (File file : listFiles) {
                    System.out.println(file.getAbsolutePath());
                    if (file.isDirectory()) {
                        scanDirectory(file);
                    } else {
                        addSongToList(file, null);
                    }
                }
            }
        }
    }

    // converts a list of songs and their paths into their SongInfo and returns a new list of SongInfo!
    public ArrayList<SongInfo> convertToSongInfoList(ArrayList<String> songsList)
    {
        //System.out.println("Am I failing here?");
        ArrayList<SongInfo> songsInfo = new ArrayList<SongInfo>(); // holds the songs and their info into this list.

        // iterate through the arraylist of song paths and convert each one into a SongInfo, and add to list of SongInfos.
        for(int i = 0; i < songsList.size(); i++)
        {
            SongInfo newSong = new SongInfo(); // create a new SongInfo object.
            newSong.getandSetSongInfo(songsList.get(i)); // get the song path and send it to SongInfo to be parsed into it's song details.

            // if null, no name, or the duration is less than 45 seconds do not add this song.
            if(newSong.getSongName() == null || newSong.getSongName().equalsIgnoreCase(" ")
                    || newSong.getSongName().isEmpty() || Long.parseLong(newSong.getSongDuration()) < 45000) {
                // do nothing this item is null and should not be included into the list.
            }
            else {
                // after removing spaces if the song name is empty then we do not add the song.

                    songsInfo.add(newSong); // add the new SongInfo into list of SongInfos
            }
        }

        // System.out.println("Did I finish grabbing the info?");
        return songsInfo; // return this list back to caller. All song information has been parsed successfully.
    }

    // Takes a list of SongInfo and alphabetizes them according to their song name
    public ArrayList<SongInfo> alphabetizeSongs(ArrayList<SongInfo> unorderedList)
    {
        ArrayList<SongInfo> orderedList = unorderedList; // holds the newly organized song ArrayList.

        // sorts the orderedList by songname
        Collections.sort(orderedList, new Comparator<SongInfo>() {

            // controls the behavior for how the list is organized which is based upon song name.
            @Override
            public int compare(SongInfo o1, SongInfo o2) {
                return o1.getSongName().compareTo(o2.getSongName());
            }
        });

        return orderedList; // return the alphabetized songinfo list.
    }

    // Checks for all storage locations and grabbs the storage locations and looks for songs and folders. Does work for most smartphones.
    private String getExternalSdCardPath()
    {
        String removableStoragePath = "";
        File fileList[] = new File("/storage/").listFiles();
        for (File file : fileList)
        {
            if(!file.getAbsolutePath().equalsIgnoreCase(Environment.getExternalStorageDirectory().getAbsolutePath()) && file.isDirectory() && file.canRead())
            {
                removableStoragePath = file.getAbsolutePath();
            }
        }

        return removableStoragePath;
    }

    // decides if the artist exists within our lists of artists or not.
    private boolean artistExists(String artistName)
    {
        if(artistName == null)
        {
            return false;
        }

        for(int i = 0; i < artists.size(); i++)
        {

            if(artistName.equals(artists.get(i).getArtistName()))
            {
                return true;
            }
        }

        return false;
    }

    // decides if the album exists within our lists of artists or not.
    private boolean albumExists(String albumName)
    {
        if(albumName == null)
        {
            return false;
        }

        for(int i = 0; i < albums.size(); i++)
        {
            if(albumName.equals(albums.get(i).getAlbumName()))
            {
                return true;
            }
        }

        return false;
    }

    // loops through all songs and creates new artists that don't exist and adds song into the new artists that are found
    private void findArtists(ArrayList<SongInfo> songs)
    {
        // loop through all songs.
        for(int i = 0; i < songs.size(); i++)
        {
            // this check is important otherwise we are hit with the memory out of error exception.
            if(songs.get(i).getArtistName() != null && songs.get(i).getArtistName().length() > 2)
            {
                if(artists.isEmpty()) // if list is empty add the the first song and first artist to the list of artists.
                {
                    Artist newArtist = new Artist(songs.get(i).getArtistName()); // create new artist.
                    newArtist.addSong(songs.get(i)); // add song to artists list.
                    artists.add(newArtist); // add the new artist to the list of artists.
                }
                else // determine whether the artist exists and whether the song exists in the artist list or not and update the list of artists
                {
                    // loop through all artists
                    for(int j = 0; j < artists.size(); j++)
                    {
                        SongInfo currSong = songs.get(i); // the current song we are working.
                        String currArtist = songs.get(i).getArtistName(); // get this songs artist name.

                        if(artistExists(currArtist)) // if artist exists in the list.
                        {
                            //System.out.println("Artist " + currArtist + " exists, adding song now");
                            artists.get(j).addSong(currSong);
                        }
                        else // artist does not exist, create the artist and add song to the artist.
                        {
                            ///System.out.println("Artist " + currArtist + " is a new artist, adding new artist into list now!");
                            Artist newArtist = new Artist(currArtist); // new artist is created.
                            newArtist.addSong(currSong); // add this song to the current artist.
                            artists.add(newArtist); // add new artist to the list of artists.
                        }
                    }
                }
            }
        }
    }

    // loops through all songs and creates new Albums that don't exist and adds songs into the new albums.
    private void findAlbums(ArrayList<SongInfo> songs)
    {
        // loop through all songs.
        for(int i = 0; i < songs.size(); i++)
        {
            // this check is very important without it we can the out of memory exception.
            if(songs.get(i).getAlbumName() != null && songs.get(i).getAlbumName().length() > 2)
            {
                System.out.println("album name = " + songs.get(i).getAlbumName());

                // todo: fix the albums problem thing.
                if(albums.isEmpty()) // if list is empty add the the first song and first artist to the list of artists.
                {
                    //albums.add(songs.get(i).getAlbumName());
                    Album newAlbum = new Album(songs.get(i).getAlbumName()); // create new artist.
                    newAlbum.addSong(songs.get(i)); // add song to artists list.
                    albums.add(newAlbum); // add the new artist to the list of artists.
                }
                else // determine whether the artist exists and whether the song exists in the artist list or not and update the list of artists
                {
                    // loop through all artists
                    for(int j = 0; j < albums.size(); j++)
                    {
                        SongInfo currSong = songs.get(i); // the current song we are working.
                        String currAlbum = songs.get(i).getAlbumName(); // get this songs artist name.

                        if(currAlbum != null && albumExists(currAlbum)) // if artist exists in the list.
                        {
                            System.out.println("adding song to album now!");
                            //System.out.println("Artist " + currArtist + " exists, adding song now");
                            albums.get(j).addSong(currSong);
                        }
                        else // artist does not exist, create the artist and add song to the artist.
                        {

                            //System.out.println("Artist " + currArtist + " is a new artist, adding new artist into list now!");
                            Album newAlbum = new Album(currAlbum); // new artist is created.
                            newAlbum.addSong(currSong); // add this song to the current artist.

                            System.out.println("creating a new album with name: " + newAlbum.getAlbumName());

                            albums.add(newAlbum); // add new artist to the list of artists.
                        }
                    }
                }
            }
        }
    }


    // First method called when this activity is created.
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startup_layout);

        final FragmentManager fragmentManager = getFragmentManager();
        final FragmentTransaction mainLayoutTransaction = fragmentManager.beginTransaction(); // start a FragmentTransaction for the mainlayout.
        final LoadingScreenFragment loadingScreenFragment = new LoadingScreenFragment();

        startupLayout = (RelativeLayout) findViewById(R.id.startup_layout); // grabs the startup layout.

        startupLayout.removeAllViews();
        startupLayout.setBackground(null);

        backgroundThread = new Thread(this);
        backgroundThread.start();
    }


    private void initiateUIUpdater()
    {
        uiUpdater = new Runnable() {
            @Override
            public void run()
            {
                System.out.println("Finished searching for songs!");
                // todo: make the startup layout to be a fragment so that we can get rid of this. This is horrible design principle, but this works for now anyway.
                RelativeLayout startuplayout = (RelativeLayout) findViewById(R.id.startup_layout); // get the startup layout here.
                startuplayout.removeAllViews(); // remove all the views from this layout.
                startuplayout.setBackground(null); // this completely removes the background of our startup layout. Which does not fix our activity.



                // start the Intent need to reload the music player here. Everything should be here, we should not have to reload this part of the fragment.
                Intent LoadMusicPlayerIntent = new Intent(EF5ScanActivity.this, InitializerActivity.class);
                LoadMusicPlayerIntent.putExtra("path", sdMemoryPath);
                LoadMusicPlayerIntent.putExtra("songs", songPaths);
                LoadMusicPlayerIntent.putExtra("folders", folders);

                //LoadMusicPlayerIntent.putExtra("playlistNames", playlistNames); // send in the playlist names for the list.
                startActivity(LoadMusicPlayerIntent); // start the InitializerActivity intent.


                Set<String> songSet = new HashSet<>(songPaths); // create a song out of all the songs that we want to use.
                Set<String> folderSet = new HashSet<>(folders); // create a folder of all the songs that want to use.


                // Write into main memory that the app has been started for the first time so that it will not show the main screen again.
                SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getBaseContext()).edit(); // edit the base preferences to help the app
                editor.putBoolean("FirstTimeStarting", true); // tell the app that the app has started loading for the first time.
                editor.putString("MusicDirectoryPath", sdMemoryPath); // set the path to be used for the app.
                editor.commit(); // commit the changes.
                editor.putStringSet("Songs", songSet);
                editor.putStringSet("Folders", folderSet);
                ArrayList<String> emptyList = new ArrayList<>(); // simply create an empty playlist for the app to handle.
                Set<String> emptySet = new HashSet<String>(emptyList);
                editor.putStringSet("playlistNames", emptySet);
                editor.putString("Path", sdMemoryPath);

                editor.commit(); // commit the changes to shared prefs.

                finish(); // finish the EF5_scan activity.
            }
        };
    }

    private void postUIUpdate()
    {
        //uiUpdater.run();
        startupLayout.post(uiUpdater);
        //getCurrentFocus().post(uiUpdater); // note: this may not work.
    }

    @Override
    public void run()
    {
        // TODO: tell the users that Torwald is searching for their songs now.

        startupLayout.removeAllViews();
        startupLayout.setBackground(null);
//
        // begin to show the spinning logo so users can see something while the app loads.
        mainLayoutTransaction.replace(R.id.startup_layout, loadingScreenFragment);
        mainLayoutTransaction.show(loadingScreenFragment);
        mainLayoutTransaction.commit(); // show this fragment while waiting for the songsInfo list to be set.

//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        getFragmentManager().executePendingTransactions();
//                    }
//                });


        sdMemoryPath = getExternalSdCardPath(); // gets the external memory path based on a few different memory path names.

        searchForMusic(internalMemoryPath); // begin searching for songs and folders in internal memory.
        searchForMusic(sdMemoryPath); // begin searching for songs and folders in external memory.

        //songs = convertToSongInfoList(songPaths); // convert all of the song paths into a songInfo list to display to the users. Very important to get this working correctly.

        songs = convertToSongInfoList(songPaths);
        Sorters.sortBySongName(songs);
        ArrayList<SongInfo> alphabeticalSongList = songs;//alphabetizeSongs(songs); // returns a new arraylist that has been alphabetized according to song name.

        System.out.println("Finding artists now...");

        findArtists(songs); // creates the artists list and updates the public variable for artists.
        System.out.println("Finding albums now...");
        findAlbums(songs); // creates the albums list and updates the public variables.

        // Write the arraylist object to main memory
        //String ser = SerializeObject.objectToString(alphabeticalSongInfoList); // make our alphabetized list the object we want to save to main memory.
        String ser = SerializeObject.objectToString(alphabeticalSongList); // make our alphabetized list the object we want to save to main memory.
        if(ser != null && !ser.equalsIgnoreCase(""))
        {
            SerializeObject.WriteSettings(getBaseContext(), ser, "SongInfoList.dat");
        }
        else
        {
            System.out.println("WE DID NOT WRITE THE LIST CORRECTLY, SOMETHING BAD HAPPENED.");
            SerializeObject.WriteSettings(getBaseContext(), "", "SongInfoList.dat"); // we should be getting this list if we are something bad has happened.
        }

        // Write the arraylist of Artist to main memory.
        String artistsSer = SerializeObject.objectToString(artists); // make our alphabetized list the object we want to save to main memory.
        if(artistsSer != null && !artistsSer.equalsIgnoreCase(""))
        {
            SerializeObject.WriteSettings(getBaseContext(), artistsSer, "ArtistsList.dat");
        }
        else
        {
            System.out.println("WE DID NOT WRITE THE LIST CORRECTLY, SOMETHING BAD HAPPENED WHEN GETTINGS ARTISTS!");
            SerializeObject.WriteSettings(getBaseContext(), "", "ArtistsList.dat"); // we should be getting this list if we are something bad has happened.
        }


        // Write the arraylist of Album to main memory.
        String albumsSer = SerializeObject.objectToString(albums); // make our alphabetized list the object we want to save to main memory.
        if(albumsSer != null && !albumsSer.equalsIgnoreCase(""))
        {
            SerializeObject.WriteSettings(getBaseContext(), albumsSer, "AlbumsList.dat");
        }
        else
        {
            System.out.println("WE DID NOT WRITE THE LIST CORRECTLY, SOMETHING BAD HAPPENED WHEN GETTINGS ARTISTS!");
            SerializeObject.WriteSettings(getBaseContext(), "", "AlbumsList.dat"); // we should be getting this list if we are something bad has happened.
        }

        initiateUIUpdater();
        postUIUpdate();
    }

}
