package thenotoriousrog.tornadoplayer.Activities;

import android.accounts.AuthenticatorException;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import thenotoriousrog.tornadoplayer.Backend.Album;
import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SerializeObject;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.Backend.Utils;
import thenotoriousrog.tornadoplayer.Fragments.AboutFragment;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Fragments.SettingsFragment;
import thenotoriousrog.tornadoplayer.NotificationManager.NotificationHandler;
import thenotoriousrog.tornadoplayer.R;
import thenotoriousrog.tornadoplayer.Services.PlayerService;

import java.util.ArrayList;

/**
 * Created by thenotoriousrog on 7/6/17.
 * This class is in charge of initializing the MainUIFragment with all of the proper details and ensures that the fragment behaves desired.
 */

public class InitializerActivity extends AppCompatActivity {

    private ArrayList<SongInfo> songInfos; // list of all the SongInfo used throughout the app.
    private MainUIFragment mainUIFragment; // a public copy of the mainUIFragment so that we can control what happens when the user presses the back button.
    private RelativeLayout mainLayout; // holds the main layout that the main ui fragment builds.
    private Intent serviceIntent; // this is set by notification handler that starts the foreground service. This behavior needs to be changed, but for now it works.
    PlayerService playerService; // an instance of the player service.
    SettingsFragment settingsFragment;
    AboutFragment aboutFragment; // the about fragment.
    boolean isBound = false; // tells the activity whether or not we are bound to the player service.
    MusicLibrary musicLibrary; // the music library that we are working with.

    // details if the class is bound to the service or not.
    public boolean isBoundToService()
    {
        return isBound;
    }

    // TODO: may have to remove this since it does not seem to be what I wanted it to be.
    // the service connection used to bind to our player service.
    private ServiceConnection playerServiceConnection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder)
        {
            PlayerService.ServiceBinder serviceBinder = (PlayerService.ServiceBinder) iBinder; // convert the binder to a ServiceBinder that connects to the player service.
            playerService = serviceBinder.getService(); // get's a copy of the player service and binds it to the activity.
            isBound = true;
            playerService.setSpeakWithUIState(true); // we may speak with the UI again

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName)
        {
            playerService.setSpeakWithUIState(false); // we may not speak with the UI
            playerService = null; // remove the instance of the player service.
            isBound = false; // we are no longer bound.
        }
    };

    // Starts the player service for this activity.
    public void startPlayerService()
    {
        NotificationHandler notificationHandler = new NotificationHandler(this);
        notificationHandler.startListening();

        serviceIntent = new Intent(this, PlayerService.class); // the intent that will start the PlayerService.
        serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) // only in oreo can we start a foreground service this way.
        {
            this.startForegroundService(serviceIntent); // starts the actual foreground service.
        }

        System.out.println("STARTING PLAYER SERVICE!");
        this.startService(serviceIntent); // start the service.
        bindService(serviceIntent, playerServiceConnection, Context.BIND_AUTO_CREATE); // allows us to bind a service and sends our activity to be started again if killed.

    }

    // causes a call to the main UI to invalidate and redraw the UI. This should only be called when the UI is updated in the backend like a rescan of directores.
    public void refreshMainUI(ArrayList<SongInfo> rescannedSongs, ArrayList<String> rescannedFolders)
    {
        mainUIFragment.setSongInfoList(rescannedSongs);
        mainUIFragment.setFolders(rescannedFolders);
        mainUIFragment.updateSongsListAdapter();
        mainUIFragment.updateFoldersListAdapter();
       // mainUIFragment.getMainView().invalidate();
    }

    // returns the MainUIView created by the mainUIFragment.
    public View getMainUIView()
    {
        return mainUIFragment.getMainView();
    }

    public void setSettingsFragment(SettingsFragment settingsFragment)
    {
        this.settingsFragment = settingsFragment;
    }

    public void setAboutFragment(AboutFragment aboutFragment)
    {
        this.aboutFragment = aboutFragment;
    }

    // Handles the behavior for when one of the items in the options menu is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getTitle().toString().equalsIgnoreCase("settings"))
        {
            //Intent launchSettings = new Intent(this, SettingsPreferenceActivity.class);
            //startActivity(launchSettings); // launch settings.
            // begin to display the fragment again.

            mainLayout = (RelativeLayout) findViewById(R.id.mainLayoutBottomSheet); // grab the startup layout so we can remove it when we are finished.
           // mainLayout.removeAllViews(); // remove all views from this view.
            // /mainLayout.setBackground(null); // ensure no background is present after we have loaded the mainUIFragment.

            //frameLayout.setVisibility(View.GONE);
           // frameLayout.removeAllViews();
//            FragmentTransaction transaction = getFragmentManager().beginTransaction();
//            settingsFragment = new SettingsFragment();
//
//            transaction.replace(R.id.mainFrameLayout, settingsFragment);
//
//            transaction.addToBackStack(null); // nothing to go back when the user hits the back button.
//            transaction.commit(); // commit the fragment to be shown.

        }

        return super.onOptionsItemSelected(item);
    }

//    // set's the behavior for when the options button is selected.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_ui_options, menu);

        return true;
    }

    // make a call to the main UI fragment to update the theme on the app itself.
    public void updateThemeToMainUI()
    {
        mainUIFragment.applyTheme();

        //getApplication().setTheme(R.style.AppTheme_AquaArmy);

        // todo: removed because it is not working in resetting the theme for the application. This is something that I am working on.
//        Intent intent = getIntent();
//        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//        finish();
//        overridePendingTransition(0, 0);
//        startActivity(intent);
//        overridePendingTransition(0, 0);
    }

    // First method called when this activity is created.
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        ThemeUtils.getInstance().setContext(getBaseContext()); // set the context of the ThemeUtils to be used throughout the application.

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String theme = preferences.getString("Theme", "OceanMarina");

        System.out.println("The theme we have selected: " + theme);
        //Utils.onActivityCreateSetTheme(this); // set the theme of the activity based on the actual theme that was set.

        setTheme(ThemeUtils.getInstance().getCurrentTheme()); // gets the theme whether dark or light.

//        if(theme.equalsIgnoreCase("OceanMarina"))
//        {
//
//        }
//        else if(theme.equalsIgnoreCase("ParadiseNightfall"))
//        {
//            setTheme(ThemeUtils.getInstance().getCurrentTheme()); // gets
//        }
//        else if(theme.equalsIgnoreCase("AquaArmy"))
//        {
//            setTheme(R.style.AppTheme_AquaArmy);
//        }

        super.onCreate(savedInstanceState);

        Intent intent = getIntent(); // get the intent that started this activity.



        //setTheme(R.style.AppTheme_OceanMarina);
        // todo: we may have to reset this layout as this is going to be called when the notification is called again. This is a problem.
        setContentView(R.layout.startup_layout); // only setting this for the purpose of resetting it later.
        startPlayerService(); // start the player service.

        //Toolbar actionBar = findViewById(R.id.toolbar); // todo: fix this, this is null, likely because I have a useless appbar right now.
        //actionBar.setOverflowIcon(getResources().getDrawable(R.drawable.options)); // white three dot options menu.
//        System.out.println("Is action bar null?" + actionBar);
        //setActionBar(actionBar); // sets the action bar
//        //setSupportActionBar(actionBar);

        //setSupportActionBar(actionBar);
        //getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setDisplayShowHomeEnabled(false);

//        PowerManager.WakeLock wakeLock;
//        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "System WakeLock");
//        wakeLock.acquire(); // TODO: I need to assure that I remove the wake lock when the service is killed however, the activity needs to not die in the background and take the wakelock with it. Very important!


        // TODO: Check the SharedPreferences to decide if we need to load the app or simply rebuild the MainUIFragment

        String path = intent.getStringExtra("path"); // get the path from the user.

        //SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean startupDisplayed = preferences.getBoolean("FirstTimeStarting", false);


        if(startupDisplayed == true)
        {

            // Need to grab the songs that we want to work with again. REMOVE THESE IF THE PROBLEM STILL PERSISTS.
            ArrayList<String> songList = new ArrayList<String>(preferences.getStringSet("Songs", null));
            ArrayList<String> folderList = new ArrayList<String>(preferences.getStringSet("Folders", null));
            ArrayList<String> playlistNames = new ArrayList<>(preferences.getStringSet("playlistNames", null));
            String musicPath = preferences.getString("MusicDirectoryPath", "");

            System.out.println("grabbing the songs now, but we need to display the items correctly!");

            // System.out.println("Startup screen has been displayed already. Should we show the mainDisplay only?");
            //displayMusicPlayer(songs, folders, path); // old way.
            // todo: send in the playlist to do be displayed to the main set up. Very important.
            displayMusicPlayer(songList, folderList, musicPath, playlistNames); // the main U.I. has already been loaded, I just need to extract and load the information again.


        }
        else // the startup screen has been displayed for the first time, load the app like normal.
        {
        }

    }


    // PopulateSongInfoList class calls this method.
    public void setSongInfoList(ArrayList<SongInfo> list)
    {
        songInfos = list;
    }



    // this method is called only when the list has been loaded and we have saved memory in the system. All this will do is simply send the items to the MainUIFragment to be use
    public void displayMusicPlayer(ArrayList<String> songs, ArrayList<String> folders, String path, ArrayList<String> playlistNames)
    {
        mainUIFragment = new MainUIFragment(); // create a copy of the main U.I. fragment here.

        System.out.println("Trying to display the music player, it has already been loaded.");


        System.out.println("The number of songs grabbed by the ef5_scan activity: " + songs.size());

        ArrayList<SongInfo> songInfoAlphabetized = new ArrayList<>(); // this is the list that we will want to fill.
        ArrayList<Artist> artistList = new ArrayList<>(); // this list holds all of the artists
        ArrayList<Album> albumList = new ArrayList<>(); // this list holds all of the albums

        // read what was wrote in MainMemory
        String ser = SerializeObject.ReadSettings(getBaseContext(), "SongInfoList.dat");
        if(ser != null && !ser.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(ser); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                songInfoAlphabetized = (ArrayList<SongInfo>) obj; // set our songInfo list.
            }
        }

        String artistsSer = SerializeObject.ReadSettings(getBaseContext(), "ArtistsList.dat");
        if(artistsSer != null && !artistsSer.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(artistsSer); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                artistList = (ArrayList<Artist>) obj; // set our songInfo list.
            }
        }

        String albumsSer = SerializeObject.ReadSettings(getBaseContext(), "AlbumsList.dat");
        if(albumsSer != null && !albumsSer.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(albumsSer); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                albumList = (ArrayList<Album>) obj; // set our songInfo list.
            }
        }

        // fixme: I think the problem lies in here.
        // send the arguments that will be used by the MainUIFragment.
        Bundle fragmentArgs = new Bundle(); // this will allow us to set arguments for the main layout.
        fragmentArgs.putStringArrayList("songs", songs); // send the arraylist of song paths.
        fragmentArgs.putStringArrayList("folders", folders); // send in the arraylist of folders.
        fragmentArgs.putParcelableArrayList("songInfoList", songInfoAlphabetized); // set our grabbed songIno list to send off to the MainUIFragment.
        fragmentArgs.putString("musicFolderPath",  path); // send in the music file path.
        fragmentArgs.putStringArrayList("playlistNames", playlistNames); // TODO: determine if this is causing the app to fail or not.
       // fragmentArgs.putParcelableArrayList("artists", artistList); // send in the artists to be created by the main UI activity.
        //fragmentArgs.putParcelableArrayList("albums", albumList);

        // READ the playlist data object from main memory if it exists.
        ArrayList<Playlist> playlists = new ArrayList<>();
        String ser1 = SerializeObject.ReadSettings(getBaseContext(), "playlists.dat"); // attempt to find this in main memory.
        if(ser1 != null && !ser1.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(ser1); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                playlists = (ArrayList<Playlist>) obj; // set our songInfo list.
                //Pair<String, ArrayList<SongInfo>> newPair = new Pair<>(playlistNames.get(i), playlistSongs); // create a new pair to be used by the list here.
                fragmentArgs.putParcelableArrayList("playlists", playlists); // send in the parcelable arraylist here.
            }
        }

        // test to see if the playlist list is null after reading from main memory.
        System.out.println("Is the playlist arraylist null after reading from main memory? " + playlists);

        mainUIFragment.setArguments(fragmentArgs); // set the arguments for the fragment.

        RelativeLayout startupLayout = (RelativeLayout) findViewById(R.id.startup_layout); // grab the startup layout so we can remove it when we are finished.
        startupLayout.removeAllViews(); // remove all views from this view.
        startupLayout.setBackground(null); // ensure no background is present after we have loaded the mainUIFragment.

        // begin to display the fragment again.
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        transaction.show(mainUIFragment);
        transaction.replace(R.id.startup_layout, mainUIFragment, "MainUIFragment"); // replace the loading screen with the main layout.
        transaction.addToBackStack("MainUIFragment");
        transaction.commit(); // commit the fragment to be shown.
    }

    @Override
    public void onBackPressed()
    {
       // mainUIFragment.closeFlowingDrawer(); // always close the flowing drawer no matter what.

        // if settingsFragment is null, then settings has not been opened in this run instance, thus, we can skip removing it.
        if(settingsFragment != null)
        {
            getFragmentManager().beginTransaction().remove(settingsFragment).commit(); // removes the settings button no matter what
        }

        if(aboutFragment != null)
        {
            getFragmentManager().beginTransaction().remove(aboutFragment).commit(); // removes the about fragment no matter what.
        }

        if(mainUIFragment.isSlidingPanelExpanded()) // have the panel fly down when the user hits the back button.
        {
            mainUIFragment.slidePanelDown(); // have the sliding layout fall down.
        }
        else // perform other back button behaviors.
        {
            if(mainUIFragment.getCurrentPageItem() == 0) // in the songs tab/page
            {
                // do nothing.
            }
            else if(mainUIFragment.getCurrentPageItem() == 1) // in the folders tab/page
            {
                mainUIFragment.closeFolder(); // close the playlist and show the folders again.
            }
            else if(mainUIFragment.getCurrentPageItem() == 2) // in the playlists tab/page.
            {
                mainUIFragment.closePlaylist(); // close the playlist and show list of playlists again.
            }
            else if(mainUIFragment.getCurrentPageItem() == 3) // close the artist and show list of artists again.
            {
                mainUIFragment.closeArtist();
            }
            else if(mainUIFragment.getCurrentPageItem() == 4)
            {
                mainUIFragment.closeAlbum();
            }

            // todo: add the work for when the user presses the back button closing the artists list and what not!
        }
    }

    @Override
    public void onResume()
    {
        startPlayerService();
        super.onResume();
    }

    @Override
    public void onPause()
    {
        System.out.println("we are in on pause! Saving UI state!");

        //StateSaver.getInstance().saveStateSaver(); // save state saver to local memory.

        super.onPause();
    }

    // Controls the behavior when the application is destroyed by Android.
    @Override
    public void onDestroy()
    {
        System.out.println("We are in on destroy!");
        playerService.setSpeakWithUIState(false); // activities are being destroyed, we cannot speak with the UI.
        //StateSaver.getInstance().setSpeakWithUIState(false); // we can no longer speak with the UI
        unbindService(playerServiceConnection); // unbind from service.

        super.onDestroy();
    }
}
