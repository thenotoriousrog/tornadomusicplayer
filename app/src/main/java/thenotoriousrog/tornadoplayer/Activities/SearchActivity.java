package thenotoriousrog.tornadoplayer.Activities;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.commonsware.cwac.merge.MergeAdapter;
import com.github.clans.fab.FloatingActionButton;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.Adapters.AlbumResultsAdapter;
import thenotoriousrog.tornadoplayer.Adapters.ArtistResultsAdapter;
import thenotoriousrog.tornadoplayer.Adapters.FolderAdapter;
import thenotoriousrog.tornadoplayer.Adapters.FolderResultsAdapter;
import thenotoriousrog.tornadoplayer.Adapters.PlaylistResultsAdater;
import thenotoriousrog.tornadoplayer.Adapters.SongResultsAdapter;
import thenotoriousrog.tornadoplayer.Backend.Album;
import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SerializeObject;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.BackgroundThreads.FolderSearchSelectedThread;
import thenotoriousrog.tornadoplayer.Handlers.ServiceHandler;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.R;
import thenotoriousrog.tornadoplayer.Services.PlayerService;
import thenotoriousrog.tornadoplayer.UI.FlyupPanelController;

public class SearchActivity extends Activity {

    // a collection of all of the lists in which I can perform the searches.
    private ArrayList<SongInfo> songs;
    private ArrayList<String> folders;
    private ArrayList<Artist> artists;
    private ArrayList<Album> albums;
    private ArrayList<Playlist> playlists;

    // all of the lists that our results should be going into.
    private ArrayList<SongInfo> songResults = new ArrayList<>();
    private ArrayList<String> folderResults = new ArrayList<>();
    private ArrayList<Artist> artistResults = new ArrayList<>();
    private ArrayList<Album> albumResults = new ArrayList<>();
    private ArrayList<Playlist> playlistResults = new ArrayList<>();

    private ArrayAdapter<SongInfo> songResultAdapter;
    private MergeAdapter mergeAdapter = new MergeAdapter(); // the adapter to combine all of the information into the app.
    private ListView searchResults; // the results that are displayed in the merge adapter and that should be set after all fields have been searched.
    private RelativeLayout relativeLayout;
    //private ServiceHandler serviceHandler; // the service handler to sends a response which communicates with the player service.

    // sets the result adapter as well as the listener.
    private void setSongResultAdapter()
    {
        //ArrayAdapter<SongInfo> adapter = new ArrayAdapter<>(this, R.layout.songlist, songResults);
        SongResultsAdapter adapter = new SongResultsAdapter(this, R.layout.songlist, songResults);
        adapter.notifyDataSetChanged();

        mergeAdapter.addAdapter(adapter); // merge adapter being added after the list.

        //songResultListView.setAdapter(adapter);
    }

    private void setFolderResultAdapter()
    {
        FolderResultsAdapter adapter = new FolderResultsAdapter(this, R.layout.folderlist, folderResults);
        adapter.notifyDataSetChanged();

        mergeAdapter.addAdapter(adapter);
    }

    private void setArtistResultAdapter()
    {
        ArtistResultsAdapter adapter = new ArtistResultsAdapter(this, R.layout.artistlist, artistResults);
        adapter.notifyDataSetChanged();

        mergeAdapter.addAdapter(adapter);
    }

    private void setAlbumResultAdapter()
    {
        AlbumResultsAdapter adapter = new AlbumResultsAdapter(this, R.layout.albumlist, albumResults);
        adapter.notifyDataSetChanged();

        mergeAdapter.addAdapter(adapter);
    }

    private void setPlaylistResultAdapter()
    {
        PlaylistResultsAdater adater = new PlaylistResultsAdater(this, R.layout.playlist_list, playlistResults);
        adater.notifyDataSetChanged();

        mergeAdapter.addAdapter(adater);
    }

    // performs a search on all of the lists Asynchronously.
    private void performSearch(String query)
    {
        songResults.clear();
        folderResults.clear();
        albumResults.clear();
        artistResults.clear();
        playlistResults.clear();
        mergeAdapter = null;
        mergeAdapter = new MergeAdapter();


        // first loop that iterates through all of the songs.
        for(int i = 0; i < songs.size(); i++)
        {
            // if the song contains the query, add is to the list of the song queries.
            if(songs.get(i).getSongName().toLowerCase().contains(query.toLowerCase()))
            {
                songResults.add(songs.get(i)); // add the song into the results.
            }
        }

        if(songResults.size() > 0)
        {
            TextView songHeader = new TextView(getBaseContext());
            songHeader.setText("Songs");
            songHeader.setAllCaps(true);

            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                songHeader.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.white));
            }
            else
            {
                songHeader.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.black));
            }


            songHeader.setTextSize(25.0f);
            songHeader.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            mergeAdapter.addView(songHeader);

            setSongResultAdapter();
        }

        // loop through all of the artists.
        for(int i = 0; i < artists.size(); i++)
        {
            // if the song contains the query, add is to the list of the song queries.
            if(artists.get(i).getArtistName().toLowerCase().contains(query.toLowerCase()))
            {
                artistResults.add(artists.get(i)); // add the song into the results.
            }
        }

        // if not empty, then we will set the artist results correctly.
        if(artistResults.size() > 0)
        {
            TextView artistHeader = new TextView(getBaseContext());
            artistHeader.setText("Artists");
            artistHeader.setAllCaps(true);

            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                artistHeader.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.white));
            }
            else {
                artistHeader.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.black));
            }


            artistHeader.setTextSize(25.0f);
            artistHeader.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            mergeAdapter.addView(artistHeader);
            setArtistResultAdapter();
        }


        // loop through all of the folders
        for(int i = 0; i < folders.size(); i++)
        {
            // if the song contains the query, add is to the list of the song queries.
            if(folders.get(i).toLowerCase().contains(query.toLowerCase()))
            {
                folderResults.add(folders.get(i)); // add the song into the results.
            }
        }

        if(folderResults.size() > 0)
        {
            TextView folderHeader = new TextView(getBaseContext());
            folderHeader.setText("Folders");
            folderHeader.setAllCaps(true);

            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                folderHeader.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.white));
            }
            else
            {
                folderHeader.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.black));
            }


            folderHeader.setTextSize(25.0f);
            folderHeader.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            mergeAdapter.addView(folderHeader);

            setFolderResultAdapter();
        }

        // loop through all of the albums
        for(int i = 0; i < albums.size(); i++)
        {
            // if the song contains the query, add is to the list of the song queries.
            if(albums.get(i).getAlbumName().toLowerCase().contains(query.toLowerCase()))
            {
                albumResults.add(albums.get(i)); // add the song into the results.
            }
        }

        if(albumResults.size() > 0)
        {
            TextView albumHeader = new TextView(getBaseContext());
            albumHeader.setText("Albums");
            albumHeader.setAllCaps(true);

            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                albumHeader.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.white));
            }
            else {
                albumHeader.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.black));
            }


            albumHeader.setTextSize(25.0f);
            albumHeader.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            mergeAdapter.addView(albumHeader);

            setAlbumResultAdapter();
        }

        if(playlists != null)
        {
            // loop through all of the playlists
            for(int i = 0; i < playlists.size(); i++)
            {
                // if the song contains the query, add is to the list of the song queries.
                if(playlists.get(i).name().toLowerCase().contains(query.toLowerCase()))
                {
                    playlistResults.add(playlists.get(i)); // add the song into the results.
                }
            }
        }

        if(playlistResults.size() > 0)
        {
            TextView playlistHeader = new TextView(getBaseContext());
            playlistHeader.setText("Playlists");
            playlistHeader.setAllCaps(true);

            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                playlistHeader.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.white));
            }
            else {
                playlistHeader.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.black));
            }


            playlistHeader.setTextSize(25.0f);
            playlistHeader.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            mergeAdapter.addView(playlistHeader);

            setPlaylistResultAdapter();
        }

        mergeAdapter.notifyDataSetChanged();
        mergeAdapter.notifyDataSetInvalidated();

        searchResults.setAdapter(mergeAdapter); // sets the adapter to be used within the merge adapter.
    }

    // reads the song info list from memory and sets the arraylist.
    private void readSongInfoListFromMemory()
    {
        // read what was wrote in MainMemory
        String ser = SerializeObject.ReadSettings(getBaseContext(), "SongInfoList.dat");
        if(ser != null && !ser.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(ser); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                songs = (ArrayList<SongInfo>) obj; // set our songInfo list.
            }
        }
    }

    // reads the artist list from memory and sets the arraylist.
    private void readArtistListFromMemory()
    {
        String artistsSer = SerializeObject.ReadSettings(getBaseContext(), "ArtistsList.dat");
        if(artistsSer != null && !artistsSer.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(artistsSer); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                artists = (ArrayList<Artist>) obj; // set our songInfo list.
            }
        }
    }

    // reads the album list from memory and sets the arraylist.
    private void readAlbumListFromMemory()
    {
        String albumsSer = SerializeObject.ReadSettings(getBaseContext(), "AlbumsList.dat");
        if(albumsSer != null && !albumsSer.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(albumsSer); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                albums = (ArrayList<Album>) obj; // set our songInfo list.
            }
        }
    }

    // reads the playlist from memory and sets the arraylist.
    private void readPlaylistFromMemory()
    {
        String ser1 = SerializeObject.ReadSettings(getBaseContext(), "playlists.dat"); // attempt to find this in main memory.
        if(ser1 != null && !ser1.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(ser1); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                playlists = (ArrayList<Playlist>) obj; // set our songInfo list.
            }
        }
    }


    public void sendSearchSelectedMessage()
    {
        Message msg = ServiceHandler.getInstance().obtainMessage();
        msg.arg1 = Constants.COMMAND.SEARCH_SELECTED; // allow for message to send an alert that a song was selected and is ready to be played.
        ServiceHandler.getInstance().sendMessage(msg); // send the message to the service Handler
    }

    // albums have to account for songs, artists and folders ahead of it.
    private int findAlbumItemActualPosition(int position)
    {
        int temp = 0;
        if(songResults.size() > 0) {
            temp = songResults.size()+1; // always an additional one  because of the header
        }
        if(folderResults.size() > 0) {
            temp += folderResults.size() + 1; // one extra for the header.
        }
        if(artistResults.size() > 0) {
            temp += artistResults.size() + 1;
        }

        int actualItemPos = position - temp -1; // minus an additional one because of the header.

        return actualItemPos;
    }

    // Artists only have to account for songs ahead of it.
    private int findArtistItemActualPosition(int position)
    {
        int temp = 0;
        if(songResults.size() > 0) {
            temp = songResults.size()+1; // always an additional one  because of the header
        }
        int actualItemPos = position - temp -1; // minus an additional one because of the header.

        return actualItemPos;
    }

    // only has to account for songs and artists
    private int findFolderItemActualPosition(int position)
    {
        int temp = 0;
        if(songResults.size() > 0) {
            temp += songResults.size()+1; // always an additional one  because of the header
        }

        if(artistResults.size() > 0) {
            temp += artistResults.size() + 1;
        }

        int actualItemPos = position - temp -1; // minus an additional one because of the header.

        System.out.println("position = " + position);
        System.out.println("temp = " + temp);
        System.out.println("actualItemPos = " + actualItemPos);

        if(actualItemPos < 0 )
        {
            actualItemPos *= -1;
        }

        return actualItemPos;
    }

    // playlists has to account for everything ahead of it.
    private int findPlaylistItemActualPosition(int position)
    {
        int temp = 0;
        if(songResults.size() > 0) {
            temp = songResults.size()+1; // always an additional one  because of the header
        }
        if(folderResults.size() > 0) {
            temp += folderResults.size() + 1; // one extra for the header.
        }
        if(albumResults.size() > 0) {
            temp += albumResults.size() + 1;
        }
        if(artistResults.size() > 0) {
            temp += artistResults.size() + 1;
        }

        int actualItemPos = position - temp -1; // minus an additional one because of the header.

        return actualItemPos;
    }

    // this method will find songs within the directory passed into it and return with a File[] full of .mp3 files.
    private File[] getSongsInDirectory(File dir)
    {
        // filteredSongFiles holds all of the mp3 files that are found
        File[] filteredSongFiles = dir.listFiles(new FilenameFilter() {

            // This will search for songs and ensure that whatever is retrieved is an mp3 and nothing else.
            @Override
            public boolean accept(File dir, String songName) {
                return songName.contains(".mp3");
            }
        });

        return filteredSongFiles; // returns the File[] of filtered songs. It can return 0 if no songs were found.
    }

    // converts a list of songs and their paths into their SongInfo and returns a new list of SongInfo!
    private ArrayList<SongInfo> convertToSongInfoList(ArrayList<String> songsList)
    {
        //System.out.println("Am I failing here?");
        ArrayList<SongInfo> songsInfo = new ArrayList<SongInfo>(); // holds the songs and their info into this list.

        // iterate through the arraylist of song paths and convert each one into a SongInfo, and add to list of SongInfos.
        for(int i = 0; i < songsList.size(); i++)
        {
            SongInfo newSong = new SongInfo(); // create a new SongInfo object.
            newSong.getandSetSongInfo(songsList.get(i)); // get the song path and send it to SongInfo to be parsed into it's song details.

            if(newSong.getSongName() == null || newSong.getSongName().equalsIgnoreCase("")) {
                // do nothing this item is null and should not be included into the list.
            }
            else {
                songsInfo.add(newSong); // add the new SongInfo into list of SongInfos.
            }
        }

        // System.out.println("Did I finish grabbing the info?");
        return songsInfo; // return this list back to caller. All song information has been parsed successfully.
    }

    // This method will extract the songs out of the Folder and convert them into an Arraylist<SongInfo> to be added to the playlist.
    private ArrayList<SongInfo> extractSongsFromFolder(String folderPath)
    {
        ArrayList<String> rawSongs = new ArrayList<>(); // Holds all of the song filepaths.
        ArrayList<SongInfo> folderSongs; // holds all of the songs after being converted into a list of SongInfo objects.

        File folderDirectory = new File(folderPath); // convert the foldername into a file that we can use to open up and get the songs in that folder.
        File[] songs; // will hold all the .mp3 files in this folder.

        if(folderDirectory.isDirectory()) // check to make sure that this is a valid directory before we go searching through it.
        {
            // get the songs in this directory and add it to the arrayList of folderSongs
            songs = getSongsInDirectory(folderDirectory); // get all of the songs in the folder.

            // TODO: check to make sure that their songs in the folder before trying to add them all or the app will crash. I know it works in mine, but it may not for other users.

            // add songs into the arrayList.
            for(int i = 0; i < songs.length; i++)
            {
                rawSongs.add(songs[i].toString()); // adds all the song names into the arraylist of folder songs.
            }
        }
        else
        {
            // TODO: create a toast to let the user know that selecting this folder has failed. Also check if it exists, if it doesn't tell the user that the folder no longer exists.
        }

        folderSongs = convertToSongInfoList(rawSongs); // convert all of the raw songs into an arraylist of SongInfo objects.
        return  folderSongs; // send the arraylist of songs back to the calling function.
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {

        setTheme(ThemeUtils.getInstance().getCurrentTheme()); // sets the current theme be it light or dark.

//        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext()); // gets the default shared preferences
//        String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.
//
//        if(theme.equalsIgnoreCase("OceanMarina"))
//        {
//            setTheme(R.style.AppTheme_OceanMarina);
//        }
//        else if(theme.equalsIgnoreCase("ParadiseNightfall"))
//        {
//            setTheme(R.style.AppTheme_ParadiseNightfall);
//        }
//        else if(theme.equalsIgnoreCase("AquaArmy"))
//        {
//            setTheme(R.style.AppTheme_AquaArmy);
//        }

        super.onCreate(savedInstanceState);

        setContentView(R.layout.search);

        relativeLayout = findViewById(R.id.searchRelativeLayout);
        RelativeLayout fieldLayout = findViewById(R.id.searchLayout); // the exact field itself.




       // serviceHandler = new ServiceHandler(getMainLooper());

        Bundle args = getIntent().getExtras(); // get's the bundle extra from the activity.
        folders = (ArrayList<String>) args.getStringArrayList("folders");

        // sets the remainder of the listeners up.
        readSongInfoListFromMemory();
        readArtistListFromMemory();
        readAlbumListFromMemory();
        readPlaylistFromMemory();


        searchResults = findViewById(R.id.searchResults); // holds the listview where all of the results will be displayed.

        final FloatingActionButton backButton = findViewById(R.id.backButton); // holds the back button.
        backButton.setVisibility(View.VISIBLE);
        backButton.show(true); // show the animation when loaded and display the animation

        // simple click listener to finish this activity to go back to the main UI.
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                //SearchActivity.this.finish();
                onBackPressed(); // calls the on backpressed option which will kill the activity.
            }
        });


        // grabs the colors from the ThemeUtils
        int colorPrimary = ThemeUtils.getInstance().getCurrentThemePrimaryColor();
        int colorAccent = ThemeUtils.getInstance().getCurrentThemeAccentColor();

        fieldLayout.setBackgroundColor(ContextCompat.getColor(getBaseContext(), colorPrimary));
        backButton.setColorNormal(ContextCompat.getColor(getBaseContext(), colorAccent));
        backButton.setColorPressed(ContextCompat.getColor(getBaseContext(), colorAccent));


        // todo: most of the work here should be in a background thread to allow for the UI to behave better.
        searchResults.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            // if something is clicked we must hide the search button.
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                String itemTag = (String) view.getTag(); // gets the tag name for the song item.
                backButton.hide(true); // hide the back button with an animation.

                // if null, we need to show the back button again.
                if(itemTag == null)
                {
                    System.out.println("The item tag is null");
                    backButton.show(true); // show the animation for the back button.
                    return; // force a return out of the listener.
                }

                if(itemTag.equals("SONG"))
                {
                    System.out.println("A SONG WAS SELECTED IN THE SEARCH VIEW");

                    SongInfo currentSong = songResults.get(position-1); // minus one to account for the song header.
                    ArrayList<SongInfo>  songList = new ArrayList<>();
                    songList.add(currentSong);

                    MusicLibrary.setCurrSong(currentSong);
                    MusicLibrary.setCurrentSongList(songList, currentSong); // set the current song list in the music library
                    MusicLibrary.setCurrentSongNumber(0); // set the song position in the music library

                    if(MusicLibrary.isShuffleOn())
                    {
                        System.out.println("SHUFFLE IS ON");
                        MusicLibrary.resetQueue();
                        MusicLibrary.resetPlayedSongs();
                        MusicLibrary.shuffle();
                    }

                    sendSearchSelectedMessage(); // sends the message which forces the UI to update.

                    finish(); // kill the activity after selecting the song.
                }
                else if(itemTag.equals("FOLDER"))
                {
                    int actualItemPos = findFolderItemActualPosition(position); // find the folder's actual position.
                    String folderPath = folderResults.get(actualItemPos);

                    FolderSearchSelectedThread backgroundThread = new FolderSearchSelectedThread(SearchActivity.this, folderPath);
                    backgroundThread.startBackgroundThread(); // starts the background thread which will handle the killing of the activity and whatnot.

                }
                else if(itemTag.equals("ALBUM"))
                {
                    // todo: fix this ASAP


                    int actualItemPos = findAlbumItemActualPosition(position); // find the album's actual position


                    ArrayList<SongInfo> songs = new ArrayList<>();
                    songs.addAll(albumResults.get(actualItemPos).getSongs());

                    SongInfo currentSong = songs.get(0); // get the first song in the list.


                    MusicLibrary.setCurrSong(currentSong);
                    // Note: only in the song selection threads are we allowed to make calls to the music library. No where else in the code should this happen besides player service.
                    MusicLibrary.setCurrentSongList(songs, currentSong); // set the current song list in the music library
                    MusicLibrary.setCurrentSongNumber(0); // set the song position in the music library

                    if(MusicLibrary.isShuffleOn())
                    {
                        System.out.println("SHUFFLE IS ON");
                        MusicLibrary.resetQueue();
                        MusicLibrary.resetPlayedSongs();
                        MusicLibrary.shuffle();
                    }

                    sendSearchSelectedMessage(); // sends the message which forces the UI to update.
                    finish(); // kill the activity.
                }
                else if(itemTag.equals("ARTIST"))
                {
                    int actualItemPos = findArtistItemActualPosition(position);


                    ArrayList<SongInfo> songs = new ArrayList<>();

                    songs.addAll(artistResults.get(actualItemPos).getSongs());

                    SongInfo currentSong = songs.get(0); // get the first song in the list.


                    MusicLibrary.setCurrSong(currentSong);
                    // Note: only in the song selection threads are we allowed to make calls to the music library. No where else in the code should this happen besides player service.
                    MusicLibrary.setCurrentSongList(songs, currentSong); // set the current song list in the music library
                    MusicLibrary.setCurrentSongNumber(0); // set the song position in the music library

                    if(MusicLibrary.isShuffleOn())
                    {
                        System.out.println("SHUFFLE IS ON");
                        MusicLibrary.resetQueue();
                        MusicLibrary.resetPlayedSongs();
                        MusicLibrary.shuffle();
                    }

                    sendSearchSelectedMessage(); // sends the message which forces the UI to update.
                    finish(); // kill the activity.
                }
                else if(itemTag.equals("PLAYLIST"))
                {
                    int actualItemPos = findPlaylistItemActualPosition(position);

                    ArrayList<SongInfo> songs = playlistResults.get(actualItemPos).songs();
                    SongInfo currentSong = playlistResults.get(actualItemPos).songs().get(0); // get the first song in the list.

                    MusicLibrary.setCurrSong(currentSong);
                    // Note: only in the song selection threads are we allowed to make calls to the music library. No where else in the code should this happen besides player service.
                    MusicLibrary.setCurrentSongList(songs, currentSong); // set the current song list in the music library
                    MusicLibrary.setCurrentSongNumber(0); // set the song position in the music library

                    if(MusicLibrary.isShuffleOn())
                    {
                        System.out.println("SHUFFLE IS ON");
                        MusicLibrary.resetQueue();
                        MusicLibrary.resetPlayedSongs();
                        MusicLibrary.shuffle();
                    }

                    sendSearchSelectedMessage(); // sends the message which forces the UI to update.
                    finish(); // kill the activity.
                }
                else { // if this is reached and for some reason no option was allowed, we need the back button to show up again.
                    backButton.show(true);
                }



                // TODO: Set the other items for the tags right here to allow us to be able to do the rest action within the search view
            }
        });

        //SearchView searchView = mainView.findViewById(R.id.searchView); // holds the search vie where all of the results will be displayed.

        ImageView searchIcon = findViewById(R.id.searchIcon); // gets the search icon.
        final EditText searchField = findViewById(R.id.searchField); // gets the edit text field
        final ImageView clearTextIcon = findViewById(R.id.clearTextIcon); // gets the clear icon.


        // allows for listening for some actions in the list.
        searchField.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent)
            {
                if(actionId == EditorInfo.IME_ACTION_SEARCH) // when the user hits the "enter" or "search" button in the software keyboard.
                {
                    // todo: need to perform a search of some sort.
                    performSearch(textView.getText().toString()); // perform a search when the user hits enter
                    return true;
                }
                return false;
            }
        });

        searchField.addTextChangedListener(new TextWatcher() {

            // TODO: auto generated method stub, not needed.
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }


            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

                // if no text, remove the clear icon otherwise  show the clear icon.
                if(charSequence.toString().trim().length() == 0)
                {
                    clearTextIcon.setVisibility(View.INVISIBLE);
                }
                else {
                    clearTextIcon.setVisibility(View.VISIBLE);
                }
            }

            // TODO: auto generated method stub, not needed
            @Override
            public void afterTextChanged(Editable editable) { }
        });

        // when clicked simply remove all of the text from the editor
        clearTextIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchField.setText(null);

                // refreshes the results to show nothing.
                searchResults.setAdapter(null);
                mergeAdapter = null; // nullify the adapter.
                mergeAdapter = new MergeAdapter(); // create a new merge adapter
                mergeAdapter.notifyDataSetChanged();
                mergeAdapter.notifyDataSetInvalidated();
                searchResults.setAdapter(mergeAdapter);
            }
        });

        searchField.setFocusable(true);
        searchField.setFocusableInTouchMode(true);
        searchField.requestFocus();

        //searchResults.setAdapter(mergeAdapter);


    }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish(); // kills the activity.
    }

}
