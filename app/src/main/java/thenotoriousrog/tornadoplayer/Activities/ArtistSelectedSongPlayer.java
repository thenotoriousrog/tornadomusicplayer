package thenotoriousrog.tornadoplayer.Activities;

import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.UI.FlyupPanelController;

public class ArtistSelectedSongPlayer extends Activity implements AdapterView.OnItemClickListener{

    private ArrayList<SongInfo> songsToPick;
    private FlyupPanelController flyupPanelController;
    private FlyupPanelListener flyupPanelListener;

    public ArtistSelectedSongPlayer(ArrayList<SongInfo> songsToPick, FlyupPanelController flyupPanelController, FlyupPanelListener flyupPanelListener)
    {
        this.songsToPick = songsToPick;
        this.flyupPanelController = flyupPanelController;
        this.flyupPanelListener = flyupPanelListener;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
    {
        flyupPanelController.flyPanelUp();
        SongInfo song = songsToPick.get(position);
        flyupPanelListener.loadInBackground(songsToPick, song, position);
    }
}
