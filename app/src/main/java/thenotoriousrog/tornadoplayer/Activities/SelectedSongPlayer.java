package thenotoriousrog.tornadoplayer.Activities;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;

import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.UI.CountdownTimer;
import thenotoriousrog.tornadoplayer.UI.FlyupPanelController;

import java.util.ArrayList;

/**
 * Created by thenotoriousrog on 5/25/17.
 * This class is in charge of playing the songs that are selected by the user and setting up a media player to play those songs!
 */
public class SelectedSongPlayer extends Activity implements AdapterView.OnItemClickListener {

    private MediaPlayer mediaPlayer; // mediaPlayer that we are using to play our songs.
    private static ArrayList<SongInfo> songPaths = null; // paths to different songs, this will be different depending on where the user chooses song be it in all songs or folders.
    private FlyupPanelController flyupPanelController; // a controller for the flyup panel layout.
    private FlyupPanelListener flyupPanelListener; // holds the panel sliding listener to control specific functions for the sliding U.I.
    private SongInfo info;
    private CountdownTimer songTimer; // used to ensure only one instance of the song timer can be active at a time.
    private boolean musicControlsListenersSet = false; // this boolean will tell us weather or not the app has set the music control listeners yet.
    AsyncTask<Void, Void, Boolean> setupInBackground = null; // the async task that we need to work on to ensure that all of the work is done in the background thread.

    // the boolean ShufflingNow will help us determine whether or not the SelectedSongPlayer was set to be shuffling or not.
    public SelectedSongPlayer(ArrayList<SongInfo> songToPick, ArrayList<String> songpaths, FlyupPanelController flyupPanelController, InitializerActivity activity, FlyupPanelListener psl, MainUIFragment fragment)
    {
        songPaths = songToPick; // set the ArrayList up.
        this.flyupPanelController = flyupPanelController;
        flyupPanelListener = psl; // set the FlyupPanelListener.
    }

    // todo: decide if this needs to be removed or not. I don't believe it is being used or is needed
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo contextInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle("Select Action");
        menu.add(Menu.NONE, v.getId(), 0, "Edit Tags");
        menu.add(Menu.NONE, v.getId(), 0, "Add to Playlist");
    }

    public void onItemClick(AdapterView<?> parent, View view, final int position, long id)
    {
        flyupPanelController.flyPanelUp();
        info = songPaths.get(position);
        flyupPanelListener.loadInBackground(songPaths, info, position); // have the FlyupPanelListener do the work in the background!
    }
}
