package thenotoriousrog.tornadoplayer.Activities;

import android.app.Activity;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.UI.CountdownTimer;
import thenotoriousrog.tornadoplayer.UI.FlyupPanelController;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by thenotoriousrog on 8/4/17.
 * This class will behave much like the SelectedSongPlayer and the FolderSelectedSongPlayer and will simply play the song that was selected.
 */

public class PlaylistSelectedSongPlayer extends Activity implements AdapterView.OnItemClickListener{

    private MediaPlayer mediaPlayer; // mediaPlayer that we are using to play our songs.
    private static ArrayList<SongInfo> songPaths = null; // paths to different songs, this will be different depending on where the user chooses song be it in all songs or folders.
    private FlyupPanelController flyupPanelController; // the flyup panel controller used to control the flyup panel.
    private FlyupPanelListener FlyupPanelListener; // holds the panel sliding listener to control specific functions for the sliding U.I.
    private SongInfo info;
    private CountdownTimer songTimer; // used to ensure only one instance of the song timer can be active at a time.
    private boolean musicControlsListenersSet = false; // this boolean will tell us weather or not the app has set the music control listeners yet.

    // the boolean ShufflingNow will help us determine whether or not the SelectedSongPlayer was set to be shuffling or not.
    public PlaylistSelectedSongPlayer(ArrayList<SongInfo> songToPick, FlyupPanelController flyupPanelController, FlyupPanelListener psl)
    {
        songPaths = songToPick; // set the ArrayList up.
        this.flyupPanelController = flyupPanelController;
        FlyupPanelListener = psl; // set the FlyupPanelListener.
    }

    // When a song is clicked it is sent to the sliding listener which overwrites the list that it is working with.
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        flyupPanelController.flyPanelUp();
        info = songPaths.get(position);
        FlyupPanelListener.loadInBackground(songPaths, info, position); // have the FlyupPanelListener do the work in the background!
    }
}
