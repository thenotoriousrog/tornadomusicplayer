package thenotoriousrog.tornadoplayer.Activities;

import android.app.Activity;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.AdapterView;

import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.UI.CountdownTimer;
import thenotoriousrog.tornadoplayer.UI.FlyupPanelController;

import java.util.ArrayList;

/**
 * Created by thenotoriousrog on 7/17/17.
 * This class is an exact copy of the SelectedSongPlayer except that it takes in a list from the folder that was selected by the user. This should create separation and prevent the lists from getting mixed up.
 */
public class FolderSelectedSongPlayer extends Activity implements AdapterView.OnItemClickListener {

    private ArrayList<SongInfo> songPaths = null; // paths to different songs, this will be different depending on where the user chooses song be it in all songs or folders.
    private FlyupPanelController flyupPanelController; // the flyupPanelController
    private FlyupPanelListener FlyupPanelListener; // holds the panel sliding listener to control specific functions for the sliding U.I.
    private SongInfo info;
    private CountdownTimer songTimer; // used to ensure only one instance of the song timer can be active at a time.
    private boolean musicControlsListenersSet = false; // this boolean will tell us weather or not the app has set the music control listeners yet.

    // // FIXME: 8/4/17 Remove some of the items sent into this constructor and do the same for the normal SelectedSongPlayer because these items are not needed and only consume memory.

    // the boolean ShufflingNow will help us determine whether or not the SelectedSongPlayer was set to be shuffling or not.
    public FolderSelectedSongPlayer(ArrayList<SongInfo> songToPick, ArrayList<String> songpaths, FlyupPanelController flyupPanelController, InitializerActivity activity, FlyupPanelListener psl, MainUIFragment fragment)
    {
        songPaths = songToPick; // set the ArrayList up.
        this.flyupPanelController = flyupPanelController;
        FlyupPanelListener = psl; // set the FlyupPanelListener.
    }


    // controls the behavior whenever the user selects a song in that particular list.
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        flyupPanelController.flyPanelUp();
        info = songPaths.get(position);

        System.out.println("SONG THAT WAS SELECTED: " + info.getSongName());

        FlyupPanelListener.loadInBackground(songPaths, info, position); // have the FlyupPanelListener do the work in the background!
    }
}
