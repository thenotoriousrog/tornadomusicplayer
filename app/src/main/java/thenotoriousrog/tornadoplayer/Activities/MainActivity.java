package thenotoriousrog.tornadoplayer.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.R;

/**
 * Created by thenotoriousrog on 3/3/18.
 * MainActivity is the very first class that's called and is in charge of getting the app running for the first time and also in charge of ensuring that the app displays properly.
 */

public class MainActivity extends Activity {

    private boolean startupScreenDisplayed = false; // tells us if the app has started up for the first time or not.
    private boolean appStarted = false; // tells the app whether or not the user has begun searching for songs and allowed the app to display properly.
    public final int REQUEST_READ_STORAGE_PERMISSION = 4000;
    public final int REQUEST_WRITE_STORAGE_PERMISSION = 4001;
    public final int DIRECTORY_CHOSEN = 201; // result code that tells that the users have chosen to select the directory that their music exists in.

    public final int EF5_SCAN = 400; // result code that tells us that the users have chosen to scan their phone automatically.
    private ArrayList<String> songs = null;
    private ArrayList<String> folders = null;
    private String path = ""; // this is the path that the user has entered showing there music directory.
    private File musicDirectory; // this holds the music directory itself.


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.startup_layout);

        Intent intent = getIntent(); // get the intent that started this activity.
        boolean loadInBackground = intent.getBooleanExtra("load_in_background", true); // default is false so only when true will the activity start in background.

        if(loadInBackground)
        {
            moveTaskToBack(true); // sets the activity to run in the background.
        }
        else {
            moveTaskToBack(false); // run activity in foreground.
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        startupScreenDisplayed  = preferences.getBoolean("FirstTimeStarting", false); // grab the boolean and see if the screen has already been displayed or not.
        appStarted = preferences.getBoolean("AppStarted", false); // false by default.


        // Check if the permission to read the internal memory has been granted have been granted by the user.
        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            System.out.println("Permission has not yet been granted.");
            // Should we show an explanation here?
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_EXTERNAL_STORAGE))
            {
                // Not exactly sure that this does here.
                System.out.println("Should something be shown to the user while trying to get storage read permissions?");
                //ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_READ_STORAGE_PERMISSION);
            }
            else // no explanation needed, just request the permission.
            {
                System.out.println("requesting permissions for read storage now");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE }, REQUEST_READ_STORAGE_PERMISSION);

            }
        }
        else // if permissions are indeed granted.
        {
            if(!appStarted)
            {
                Intent startUp = new Intent(MainActivity.this, startUpScreen.class);
                startActivityForResult(startUp, DIRECTORY_CHOSEN);
            }
        }
    }

    // This is the method that controls what happens after a user decides on a permission at runtime.
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode)
        {
            case REQUEST_READ_STORAGE_PERMISSION:
            {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {

                    System.out.println("ldfasldkhvlaskdfhvlsjdflskjdflksjdflskjdflskjdflskjdflksjdflksdjflskdjf");

                    // permission was granted, yay! We can use the app.
                    if(startupScreenDisplayed == false)
                    {
                        setContentView(R.layout.startup_layout);

                        Intent startUp = new Intent(MainActivity.this, startUpScreen.class);
                        startActivityForResult(startUp, DIRECTORY_CHOSEN);
                        System.out.println("HAVE I REACHED THIS AFTER DOING AN ACTIVITY FOR RESULT??");
                    }
                    else // The app is has been started already display the current layout
                    {

                        //TODO: The app is already started, I need to display the main application here, we cannot have the application be running again, that is very very important!

                        System.out.println("startup screen is already displayed.");

                        // start the Intent need to reload the music player here. Everything should be here, we should not have to reload this part of the fragment.
                        Intent LoadMusicPlayerIntent = new Intent(this, InitializerActivity.class);
                        LoadMusicPlayerIntent.putExtra("path", path);
                        LoadMusicPlayerIntent.putExtra("songs", songs);
                        LoadMusicPlayerIntent.putExtra("folders", folders);
                        LoadMusicPlayerIntent.putExtra("load_in_background", false); // this will force the activity to load in the foreground not the background

                        startActivity(LoadMusicPlayerIntent); // start the activity
                    }

                }
                else {
                    // permission denied, boo! the app is going to crash now. Toast message tells the user that this permission is needed for the app to run.

                    // crying face.
                    Toast.makeText(getApplicationContext(), "I need this permission to play your music! " + ("\ud83d\ude22"), Toast.LENGTH_LONG).show();
                    finish(); // end the activity.
                }
                return;
            }
            case REQUEST_WRITE_STORAGE_PERMISSION: // may be able to remove this decide if this permission is even needed.
            {
                // If request is cancelled, the result arrays are empty.
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    // permission was granted, yay! we can use the app.
                }
                else
                {
                    // permission denied, the app will not work, users should be aware of this.
                }

                return;
                // other 'case' lines to check for other
                // permissions this app might request
            }

        }
    }

    // sets the music directory given by the user.
    public void setMusicDirectory(File dir)
    {
        musicDirectory = dir;
    }

    // sets the music folder path indicated by the user.
    public void setUserPath(String str)
    {
        path = str;
    }


    // sets the arrayList of songs
    public synchronized void setSongsList(ArrayList<String> list)
    {
        songs = list;
    }

    // sets the arrayList of folders.
    public synchronized void setFoldersList(ArrayList<String> list)
    {
        folders = list;
    }

    // This method is in charge of retrieving any result from an activity. Different codes tell us where the result is coming from!
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intentResult)
    {
        System.out.println("we are in activity result now");
        super.onActivityResult(requestCode, resultCode, intentResult);

        if(requestCode == EF5_SCAN)
        {
            System.out.println("We have received input from the EF5_Scan intent trying to start the activity now... ");
            Intent EF5ScanIntent = new Intent(this, EF5ScanActivity.class); // create the activity. We do not need to have any response back from this activity, it is going to scan the phone and make the mainUIFragment.
            startActivity(EF5ScanIntent); // start the activity now.
        }
        else // this will become an else-if that will allow us to look for different request codes depending on the code that we need. We need this to get the app to perform different things.
        {
            System.out.println("result code was grabbed incorrectly");
            System.out.println("the requestCode = " + requestCode);
            System.out.println("the resultCode = " + resultCode);
        }

    }

    protected void onResume()
    {
        super.onResume();

        System.out.println("We are on in onResume in the mainactivity. Attempting to start and load the music player right now...");


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean startupScreenDisplayed  = preferences.getBoolean("FirstTimeStarting", false); // grab the boolean and see if the screen has already been displayed or not.
        appStarted = preferences.getBoolean("AppStarted", false); // false by default.



        if(startupScreenDisplayed == true) // start up screen has not been loaded yet.
        {
            System.out.println("The loading screen has already been displayed, attempting to display the information now.");
            // todo: Save the above information to main memory so that we can display the songs correctly.

            // if app started is not true then we need to restart the startup activity.
            ArrayList<String> songList = new ArrayList<String>(preferences.getStringSet("Songs", null));
            ArrayList<String> folderList = new ArrayList<String>(preferences.getStringSet("Folders", null));
            ArrayList<String> playlistNames;

            if(preferences.getStringSet("playlistNames", null) == null)
            {
                playlistNames = new ArrayList<>();
            }
            else
            {
                playlistNames = new ArrayList<>(preferences.getStringSet("playlistNames", null));
            }

            String path = preferences.getString("Path", null); // grab the path that we want to use.


            // start the Intent need to reload the music player here. Everything should be here, we should not have to reload this part of the fragment.
            Intent LoadMusicPlayerIntent = new Intent(this, InitializerActivity.class);
            LoadMusicPlayerIntent.putExtra("path", path);
            LoadMusicPlayerIntent.putExtra("songs", songList);
            LoadMusicPlayerIntent.putExtra("folders", folderList);
            LoadMusicPlayerIntent.putExtra("playlistNames", playlistNames); // send in the playlist names for the list.

            // for testing.
            System.out.println("is songs null?: " + songList);
            System.out.println("is folders null?: " + folderList);
            System.out.println("is the path null?: " + path);
            System.out.println("Is the playlistNames null?: " + playlistNames);

            startActivity(LoadMusicPlayerIntent); // start the activity

        }
        else {

//            Intent startUp = new Intent(MainActivity.this, startUpScreen.class);
//            startActivityForResult(startUp, DIRECTORY_CHOSEN);

        } // do nothing, the loading display has not been displayed yet.



    }

}
