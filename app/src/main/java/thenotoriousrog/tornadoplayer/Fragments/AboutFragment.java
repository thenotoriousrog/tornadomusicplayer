package thenotoriousrog.tornadoplayer.Fragments;

import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.R;

public class AboutFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedIntanceState)
    {
        super.onCreate(savedIntanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mainView = inflater.inflate(R.layout.about, container, false); // load the main view.

        int backgroundcolor = ContextCompat.getColor(getActivity().getBaseContext(), R.color.white); // the default color.

        if(ThemeUtils.getInstance().usingDarkTheme())
        {
            backgroundcolor = ContextCompat.getColor(getActivity().getBaseContext(), R.color.md_dark_background); // the default color.
        }
        else
        {
            backgroundcolor = ContextCompat.getColor(getActivity().getBaseContext(), R.color.white); // the default color.
        }

        mainView.setBackgroundColor(backgroundcolor);

        RelativeLayout devRecognitionLayout = mainView.findViewById(R.id.DeveloperRecognitionLayout); // the layout itself.
        devRecognitionLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Snackbar.make(view, "He's my maker! The so-called \"Master Mind\" " + ("\ud83d\ude02"), Snackbar.LENGTH_LONG).show(); // laughing
            }
        });

        TextView devRecogText = mainView.findViewById(R.id.DeveloperRecognition);
        String recognition = "Developer: Roger Hannagan\nEmail: roger.hannagan113@gmail.com";
        devRecogText.setText(recognition);

        RelativeLayout artistRecognitionLayout = mainView.findViewById(R.id.artistRecognitionLayout); // the artist recognition layout itself.
        artistRecognitionLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Snackbar.make(view, "She made me! The beautiful ME! " + ("\ud83d\udc95"), Snackbar.LENGTH_LONG).show(); // hearts
            }
        });

        TextView artistRecogText = mainView.findViewById(R.id.ArtistRecognition);
        String recog = "Graphic Artist: Kaitlin Crump";
        artistRecogText.setText(recog);

        RelativeLayout specialThanksLayout = mainView.findViewById(R.id.specialThanksLayout); // the special thanks layout itself.
        specialThanksLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                String shuttleAppURL = "https://play.google.com/store/apps/details?id=another.music.player"; // the url to my app.
                Intent rateIntent = new Intent(Intent.ACTION_VIEW);
                rateIntent.setData(Uri.parse(shuttleAppURL)); // set the app url that we need.
                try{
                    startActivity(rateIntent);
                } catch (ActivityNotFoundException ex) {
                    ex.printStackTrace();
                    Toast.makeText(getActivity(), "Try again later", Toast.LENGTH_SHORT).show();
                }

            }
        });

        TextView specialThanks = mainView.findViewById(R.id.SpecialThanksRecognition);
        String specialThanksText = "Special thanks to Tim Malseed, the creator of Shuttle Music Player\nCheck out Shuttle!";
        specialThanks.setText(specialThanksText);


//        FloatingActionButton backButton = mainView.findViewById(R.id.backButton);
//        backButton.setColorNormal(ContextCompat.getColor(getActivity(), R.color.SteelBlue));
//        backButton.setColorPressed(ContextCompat.getColor(getActivity(), R.color.SteelBlue));
//        backButton.setColorRipple(ContextCompat.getColor(getActivity(), R.color.white));
//        backButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view)
//            {
//                getActivity().onBackPressed();
//            }
//        });

        return mainView;
    }

}
