package thenotoriousrog.tornadoplayer.Fragments;

import android.animation.Animator;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.transition.ChangeBounds;
import android.transition.Slide;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.widget.Toast;

import com.bumptech.glide.util.Util;

import thenotoriousrog.tornadoplayer.Activities.InitializerActivity;
import thenotoriousrog.tornadoplayer.Backend.SoundSpoutSpeech;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.Backend.Utils;
import thenotoriousrog.tornadoplayer.BackgroundThreads.RescanDirectories;
import thenotoriousrog.tornadoplayer.R;

/*
    This class is in charge of creating the Settings for when the user wants to modify certain items within the music player itself.
 */
public class SettingsFragment extends PreferenceFragment {


    @Override
    public void onCreate(Bundle savedInstanState)
    {
        super.onCreate(savedInstanState);
        addPreferencesFromResource(R.xml.preference_screen);

//        String settings = getArguments().getString("settings");
//        if ("notifications".equals(settings)) {
//            addPreferencesFromResource(R.xml.preference_screen);
//        } else if ("sync".equals(settings)) {
//            addPreferencesFromResource(R.xml.settings_rescan);
//        }

        // set rescan directories preference and it's associated click listener





    }

    // instantiates and sets the preference click listener for Rescan Directories.
    private void setRescanDirectoriesPreferenceBehavior(final View preferenceView)
    {
        Preference rescanDirectories = findPreference("RescanDirectories");
        rescanDirectories.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference)
            {
                InitializerActivity initializerActivity = (InitializerActivity) getActivity(); // cast our generic activity to be that of the initializer activity.
                RescanDirectories rescanThread = new RescanDirectories(getActivity(), initializerActivity);
                rescanThread.startBackgroundThread(); // starts the background thread.

                Snackbar.make(preferenceView, SoundSpoutSpeech.getRescanMessage(), Snackbar.LENGTH_SHORT).show(); // display message to users
                return false;
            }
        });
    }

    // instantiates and sets the preference click listener for themes.
    private void setThemesPreferenceBehavior(final View preferenceView)
    {
        Preference themes = findPreference("Themes");
        themes.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference)
            {
                //Snackbar.make(preferenceView, "Coming soon to an app near you! " + ("\ud83e\udd2b"), Snackbar.LENGTH_SHORT).show(); // display message to users

                // todo: implement in v4.0.0
                return false;
            }
        });
    }

//    // instantiates and sets the preference click listener for Unknown
//    private void setUnknownPreferenceBehavior(final View preferenceView)
//    {
//        Preference unknown = findPreference("Unknown");
//        unknown.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
//            @Override
//            public boolean onPreferenceClick(Preference preference)
//            {
//                Snackbar.make(preferenceView, "I don't even know what this is yet " + ("\ud83e\udd25"), Snackbar.LENGTH_SHORT).show(); // display message to users
//                // todo: this is just something that I'm throwing in if we ever need other things to add to the app in the settings.
//                return false;
//            }
//        });
//    }

    // instantiates and sets the preference click listener for SoundSpout's button!
    private void setSoundSpoutButtonPreferenceBehavior(final View preferenceView)
    {
        Preference soundSpoutsButton = findPreference("SoundSpoutsButton");
        soundSpoutsButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference)
            {
                Snackbar.make(preferenceView, SoundSpoutSpeech.getSoundSpoutsButtonMessage(), Snackbar.LENGTH_LONG).show(); // display message to users, long since SoundSpout says longer things.
                return false;
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()); // gets the default shared preferences
        boolean usingDarkTheme = sharedPreferences.getBoolean("UseDarkTheme", false); // the default is false.

        if(usingDarkTheme)
        {
            view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.md_dark_background)); // ensures that the preferences can be shown by setting the background white
        }
        else
        {
            view.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.md_white_1000)); // ensures that the preferences can be shown by setting the background white

        }





        // set preference behaviors
        setRescanDirectoriesPreferenceBehavior(view);
        setThemesPreferenceBehavior(view);
        //setUnknownPreferenceBehavior(view);
        setSoundSpoutButtonPreferenceBehavior(view);

        final SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();

        getPreferenceScreen().findPreference("UseDarkTheme").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference)
            {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()); // gets the default shared preferences
                boolean checked = sharedPreferences.getBoolean("UseDarkTheme", false); // default value is false.

                if(checked) // if not checked, then we are activating the dark theme.
                {
                    editor.putBoolean("UseDarkTheme", true).commit(); // turn off the dark theme

                    String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme.

                    if(theme.equalsIgnoreCase("ParadiseNightfall"))
                    {
                        Utils.changeToTheme(getActivity(), R.style.AppTheme_ParadiseNightfallDark);
                    }
                    else if(theme.equalsIgnoreCase("AquaArmy"))
                    {
                        Utils.changeToTheme(getActivity(), R.style.AppTheme_AquaArmyDark);
                    }
                    else{
                        Utils.changeToTheme(getActivity(), R.style.AppTheme_OceanMarinaDark);
                    }

                }
                else { // turning off the dark theme
                    editor.putBoolean("UseDarkTheme", false).commit(); // turn on dark theme.

                    String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme.

                    if(theme.equalsIgnoreCase("ParadiseNightfall"))
                    {
                        Utils.changeToTheme(getActivity(), R.style.AppTheme_ParadiseNightfall);
                    }
                    else if(theme.equalsIgnoreCase("AquaArmy"))
                    {
                        Utils.changeToTheme(getActivity(), R.style.AppTheme_AquaArmy);
                    }
                    else {
                        Utils.changeToTheme(getActivity(), R.style.AppTheme_OceanMarina);
                    }
                }

                return true;
            }
        });

        getPreferenceScreen().findPreference("OceanMarina").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()); // gets the default shared preferences
                boolean usingDarkTheme = sharedPreferences.getBoolean("UseDarkTheme", false); // the default is false.

                System.out.println("Ocean Marina theme selected");

                // note: if this is not seen by the main ui fragment we need to use commit to force the shared preferences to be written immediately.
                editor.putString("Theme", "OceanMarina").commit(); // sets the theme within the shared preferences to be used to rebuild the app theme on startup.
                // make a call to the initializer activity to update the theme that is being used

                if (usingDarkTheme)
                {
                    Utils.changeToTheme(getActivity(), R.style.AppTheme_OceanMarinaDark);

                    InitializerActivity activity = (InitializerActivity) getActivity();
                    activity.setTheme(R.style.AppTheme_OceanMarinaDark);
                    activity.updateThemeToMainUI();
                }
                else
                {
                    Utils.changeToTheme(getActivity(), R.style.AppTheme_OceanMarina);

                    InitializerActivity activity = (InitializerActivity) getActivity();
                    activity.setTheme(R.style.AppTheme_OceanMarina);
                    activity.updateThemeToMainUI();
                }



                return false;
            }
        });

        getPreferenceScreen().findPreference("ParadiseNightfall").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()); // gets the default shared preferences
                boolean usingDarkTheme = sharedPreferences.getBoolean("UseDarkTheme", false); // the default is false.

                System.out.println("Paradise Nightfall theme selected");
                editor.putString("Theme", "ParadiseNightfall").commit();

                if(usingDarkTheme)
                {
                    Utils.changeToTheme(getActivity(), R.style.AppTheme_ParadiseNightfallDark);

                    InitializerActivity activity = (InitializerActivity) getActivity();
                    activity.setTheme(R.style.AppTheme_ParadiseNightfallDark);
                    activity.updateThemeToMainUI();
                }
                else // not using dark theme
                {
                    Utils.changeToTheme(getActivity(), R.style.AppTheme_ParadiseNightfall);

                    InitializerActivity activity = (InitializerActivity) getActivity();
                    activity.setTheme(R.style.AppTheme_ParadiseNightfall);
                    activity.updateThemeToMainUI();
                }

                return false;
            }
        });

        getPreferenceScreen().findPreference("AquaArmy").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()); // gets the default shared preferences
                boolean usingDarkTheme = sharedPreferences.getBoolean("UseDarkTheme", false); // the default is false.

                System.out.println("Paradise Nightfall theme selected");
                editor.putString("Theme", "AquaArmy").commit();


                if(usingDarkTheme)
                {
                    Utils.changeToTheme(getActivity(), R.style.AppTheme_AquaArmyDark);


                    InitializerActivity activity = (InitializerActivity) getActivity();
                    activity.setTheme(R.style.AppTheme_AquaArmyDark);
                    activity.updateThemeToMainUI();
                }
                else
                {
                    Utils.changeToTheme(getActivity(), R.style.AppTheme_AquaArmy);


                    InitializerActivity activity = (InitializerActivity) getActivity();
                    activity.setTheme(R.style.AppTheme_AquaArmy);
                    activity.updateThemeToMainUI();
                }

                return false;
            }
        });

        getPreferenceScreen().findPreference("Aftermath").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()); // gets the default shared preferences
                boolean usingDarkTheme = sharedPreferences.getBoolean("UseDarkTheme", false); // the default is false.

                System.out.println("Paradise Nightfall theme selected");
                editor.putString("Theme", "Aftermath").commit();


                if(usingDarkTheme)
                {
                    Utils.changeToTheme(getActivity(), R.style.AppTheme_AftermathDark);


                    InitializerActivity activity = (InitializerActivity) getActivity();
                    activity.setTheme(R.style.AppTheme_AftermathDark);
                    activity.updateThemeToMainUI();
                }
                else
                {
                    Utils.changeToTheme(getActivity(), R.style.AppTheme_Aftermath);


                    InitializerActivity activity = (InitializerActivity) getActivity();
                    activity.setTheme(R.style.AppTheme_Aftermath);
                    activity.updateThemeToMainUI();
                }

                return false;
            }
        });

        getPreferenceScreen().findPreference("FlowerPetal").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()); // gets the default shared preferences
                boolean usingDarkTheme = sharedPreferences.getBoolean("UseDarkTheme", false); // the default is false.

                System.out.println("Paradise Nightfall theme selected");
                editor.putString("Theme", "FlowerPetal").commit();


                if(usingDarkTheme)
                {
                    Utils.changeToTheme(getActivity(), R.style.AppTheme_FlowerPetalDark);


                    InitializerActivity activity = (InitializerActivity) getActivity();
                    activity.setTheme(R.style.AppTheme_FlowerPetalDark);
                    activity.updateThemeToMainUI();
                }
                else
                {
                    Utils.changeToTheme(getActivity(), R.style.AppTheme_FlowerPetal);


                    InitializerActivity activity = (InitializerActivity) getActivity();
                    activity.setTheme(R.style.AppTheme_FlowerPetal);
                    activity.updateThemeToMainUI();
                }

                return false;
            }
        });


        getPreferenceScreen().findPreference("RateButton").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference)
            {
                String myAppURL = "https://play.google.com/store/apps/details?id=thenotoriousrog.tornadoplayer"; // the url to my app.
                Intent rateIntent = new Intent(Intent.ACTION_VIEW);
                rateIntent.setData(Uri.parse(myAppURL)); // set the app url that we need.

                try {
                    startActivity(rateIntent);
                } catch (ActivityNotFoundException ex){
                    ex.printStackTrace();
                    Toast.makeText(getActivity(), "Try again later", Toast.LENGTH_SHORT).show();
                }

                return true;
            }
        });

        getPreferenceScreen().findPreference("About").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference)
            {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                //SearchFragment searchFragment = new SearchFragment();
                AboutFragment aboutFragment = new AboutFragment();

                InitializerActivity initializerActivity = (InitializerActivity) getActivity();
                initializerActivity.setAboutFragment(aboutFragment);

                ChangeBounds bounds = new ChangeBounds();
                bounds.setDuration(500);

                Slide slide = new Slide();
                slide.setSlideEdge(Gravity.RIGHT);
                slide.setDuration(350);

                aboutFragment.setEnterTransition(slide);

                transaction.replace(R.id.mainFrameLayout, aboutFragment);

                transaction.addToBackStack(null); // nothing to go back when the user hits the back button.
                transaction.commit(); // commit the fragment to be shown.

                return true;
            }
        });

        getPreferenceScreen().findPreference("NotificationUpNext").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference)
            {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getBaseContext()); // gets the default shared preferences
                SharedPreferences.Editor editor = sharedPreferences.edit();

//                CheckBoxPreference checkBoxPreference = (CheckBoxPreference) preference;
//
                boolean checked = sharedPreferences.getBoolean("NotificationUpNext", true); // default value is true.

                //checkBoxPreference.setChecked(checked);
                if(!checked) // if clicked we must set the boolean to false.
                {
                    System.out.println("SETTING THE BOOLEAN FOR THE CHECK TO BE FALSE NOW!");
                    editor.putBoolean("NotificationUpNext", false).commit(); // set false meaning don't show on notifications.
                }
                else { // false set the boolean to true.
                    editor.putBoolean("NotificationUpNext", true).commit(); // set true, show up next in the notifications.
                }

                return true;
            }
        });

        getPreferenceScreen().findPreference("AlbumUpNext").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
        {
            @Override
            public boolean onPreferenceClick(Preference preference)
            {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity()); // gets the default shared preferences
                SharedPreferences.Editor editor = sharedPreferences.edit();

//                CheckBoxPreference checkBoxPreference = (CheckBoxPreference) preference; // set the check box preference.
//
                boolean checked = sharedPreferences.getBoolean("AlbumUpNext", true); // default value is true.


                if(!checked)
                {
                    editor.putBoolean("AlbumUpNext", false).commit(); // stop showing up next in the album section of stereos and what not
                }
                else
                {
                    editor.putBoolean("AlbumUpNext", true).commit(); // start showing up next in the album sections of stereos.
                }


                return true;
            }
        });


        getPreferenceManager().findPreference("BTDisconnectPause").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference)
            {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity()); // gets the default shared preferences
                SharedPreferences.Editor editor = sharedPreferences.edit();
                boolean checked = sharedPreferences.getBoolean("BTDisconnectPause", true); // default value is true.

                if(!checked)
                {
                    editor.putBoolean("BTDisconnectPause", false).commit(); // stop pausing on bluetooth disconnect.
                }
                else
                {
                    editor.putBoolean("BTDisconnectPause", true).commit(); // start pausing on bluetooth disconnect.
                }

                return true;
            }
        });

        getPreferenceManager().findPreference("BTReconnectPlay").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference)
            {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity()); // gets the default shared preferences
                SharedPreferences.Editor editor = sharedPreferences.edit();
                boolean checked = sharedPreferences.getBoolean("BTReconnectPlay", false); // default value is false.

                if(!checked)
                {
                    editor.putBoolean("BTReconnectPlay", true).commit(); // start playing on bluetooth connect.
                }
                else
                {
                    editor.putBoolean("BTReconnectPlay", false); // stop playing on bluetooth connect.
                }

                return true;
            }
        });


        return view;
    }
}
