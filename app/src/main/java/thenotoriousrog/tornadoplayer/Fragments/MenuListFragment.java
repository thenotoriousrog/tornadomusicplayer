package thenotoriousrog.tornadoplayer.Fragments;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.ViewUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import thenotoriousrog.tornadoplayer.R;
import thenotoriousrog.tornadoplayer.UI.CircularFragReveal;

public class MenuListFragment extends Fragment {

    private MainUIFragment mainUIFragment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setMainUIFragment(MainUIFragment mainUIFragment)
    {
        this.mainUIFragment = mainUIFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       final View view = inflater.inflate(R.layout.fragment_menu, container,
                false);
        //ivMenuUserProfilePhoto = (ImageView) view.findViewById(R.id.ivMenuUserProfilePhoto);
        final NavigationView vNavigation = (NavigationView) view.findViewById(R.id.vNavigation);

        vNavigation.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                if(menuItem.getTitle().toString().equalsIgnoreCase("Settings"))
                {
                    mainUIFragment.openSettingsFragment();
                    mainUIFragment.closeFlowingDrawer();

                }

                return false;
            }
        });

        return  view;
    }

//    @Override
//    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//
//
//        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                reveal(view);
//
//
//            }
//        });
//
//    }
//
//    void reveal(View view)
//    {
//
////        int finalRadius = Math.max(view.getWidth(), view.getHeight());
////
////        Animator animator =
////                ViewAnimationUtils.createCircularReveal(view, 0, 0, 0, finalRadius);
////        animator.setInterpolator(new AccelerateInterpolator());
////        animator.setDuration(3500);
////        animator.start();
//    }


}
