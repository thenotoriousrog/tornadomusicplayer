package thenotoriousrog.tornadoplayer.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.commonsware.cwac.merge.MergeAdapter;

import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.Backend.Album;
import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.R;

public class SearchFragment extends Fragment {

    private MainUIFragment mainUIFragment;

    // a collection of all of the lists in which I can perform the searches.
    private ArrayList<SongInfo> songs;
    private ArrayList<String> folders;
    private ArrayList<Artist> artists;
    private ArrayList<Album> albums;
    private ArrayList<Playlist> playlists;

    // all of the lists that our results should be going into.
    private ArrayList<SongInfo> songResults = new ArrayList<>();
    private ArrayList<String> folderResults = new ArrayList<>();
    private ArrayList<Artist> artistResults = new ArrayList<>();
    private ArrayList<Album> albumResults = new ArrayList<>();
    private ArrayList<Playlist> playlistResults = new ArrayList<>();

    MergeAdapter mergeAdapter = new MergeAdapter(); // the adapter to combine all of the information into the app.

    public void setMainUIFragment(MainUIFragment mainUIFragment)
    {
        this.mainUIFragment = mainUIFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        songs = mainUIFragment.getSongInfoList();
        folders = mainUIFragment.getFolders();
        albums = mainUIFragment.getAlbums();
        artists = mainUIFragment.getArtists();
        playlists = mainUIFragment.getPlaylists();

        // create all of the text view headers
        TextView songHeader = new TextView(getActivity());
        TextView artistHeader = new TextView(getActivity());
        TextView albumHeader = new TextView(getActivity());
        TextView folderHeader = new TextView(getActivity());
        TextView playlistHeader = new TextView(getActivity());

        // add all of the text view headers to the merge adapter.
        mergeAdapter.addView(songHeader);
        mergeAdapter.addView(artistHeader);
        mergeAdapter.addView(albumHeader);
        mergeAdapter.addView(folderHeader);
        mergeAdapter.addView(playlistHeader);
    }

    // performs a search on all of the lists Asynchronously.
    private void performSearch(String query)
    {
        // first loop that iterates through all of the songs.
        for(int i = 0; i < songs.size(); i++)
        {
            // if the song contains the query, add is to the list of the song queries.
            if(songs.get(i).getSongName().contains(query))
            {
                songResults.add(songs.get(i)); // add the song into the results.
            }
        }

        // loop through all of the artists.
        for(int i = 0; i < artists.size(); i++)
        {
            // if the song contains the query, add is to the list of the song queries.
            if(artists.get(i).getArtistName().contains(query))
            {
                artistResults.add(artists.get(i)); // add the song into the results.
            }
        }

        // loop through all of the folders
        for(int i = 0; i < folders.size(); i++)
        {
            // if the song contains the query, add is to the list of the song queries.
            if(folders.get(i).contains(query))
            {
                folderResults.add(folders.get(i)); // add the song into the results.
            }
        }

        // loop through all of the albums
        for(int i = 0; i < albums.size(); i++)
        {
            // if the song contains the query, add is to the list of the song queries.
            if(albums.get(i).getAlbumName().contains(query))
            {
                albumResults.add(albums.get(i)); // add the song into the results.
            }
        }

        // loop through all of the playlists
        for(int i = 0; i < playlists.size(); i++)
        {
            // if the song contains the query, add is to the list of the song queries.
            if(playlists.get(i).name().contains(query))
            {
                playlistResults.add(playlists.get(i)); // add the song into the results.
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mainView = inflater.inflate(R.layout.search, container, false); // holds the UI for the main search area.

        mainView.setFocusable(true);
        mainView.setFocusableInTouchMode(true);
        mainView.requestFocus();

        ListView searchResults = mainView.findViewById(R.id.searchResults); // holds the listview where all of the results will be displayed.
        //SearchView searchView = mainView.findViewById(R.id.searchView); // holds the search vie where all of the results will be displayed.

        ImageView searchIcon = mainView.findViewById(R.id.searchIcon); // gets the search icon.
        final EditText searchField = mainView.findViewById(R.id.searchField); // gets the edit text field
        final ImageView clearTextIcon = mainView.findViewById(R.id.clearTextIcon); // gets the clear icon.


        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        // when clicks we need to begin allowing the user to enter some text.
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

            }
        });

        // allows for listening for some actions in the list.
        searchField.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent)
            {
                if(actionId == EditorInfo.IME_ACTION_SEARCH) // when the user hits the "enter" or "search" button in the software keyboard.
                {
                    // todo: need to perform a search of some sort.
                    performSearch(textView.getText().toString()); // perform a search when the user hits enter
                    return true;
                }
                return false;
            }
        });

        searchField.addTextChangedListener(new TextWatcher() {

            // TODO: auto generated method stub, not needed.
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }


            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

                // if no text, remove the clear icon otherwise  show the clear icon.
                if(charSequence.toString().trim().length() == 0)
                {
                    clearTextIcon.setVisibility(View.GONE);
                }
                else {
                    clearTextIcon.setVisibility(View.VISIBLE);
                }
            }

            // TODO: auto generated method stub, not needed
            @Override
            public void afterTextChanged(Editable editable) { }
        });

        // when clicked simply remove all of the text from the editor
        clearTextIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchField.setText(null);
            }
        });

        searchField.setFocusable(true);
        searchField.setFocusableInTouchMode(true);
        searchField.requestFocus();

        searchResults.setAdapter(mergeAdapter);


        return mainView;

    }

}
