package thenotoriousrog.tornadoplayer.Fragments;

import android.app.ActivityOptions;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Process;
import android.preference.PreferenceManager;;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TabLayout;
import android.support.transition.Transition;
import android.support.transition.TransitionValues;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.view.ViewPager;
import android.text.method.ScrollingMovementMethod;
import android.transition.ChangeBounds;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.util.Util;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.mxn.soul.flowingdrawer_core.ElasticDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingDrawer;
import com.mxn.soul.flowingdrawer_core.FlowingMenuLayout;

import thenotoriousrog.tornadoplayer.Activities.FolderSelectedSongPlayer;
import thenotoriousrog.tornadoplayer.Activities.InitializerActivity;
import thenotoriousrog.tornadoplayer.Activities.SearchActivity;
import thenotoriousrog.tornadoplayer.Adapters.FolderAdapter;
import thenotoriousrog.tornadoplayer.Adapters.MainUIPagerAdapter;
import thenotoriousrog.tornadoplayer.Adapters.MusicAdapter;
import thenotoriousrog.tornadoplayer.Adapters.PlayListAdapter;
import thenotoriousrog.tornadoplayer.Adapters.PlaylistMusicAdapter;
import thenotoriousrog.tornadoplayer.Backend.Album;
import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SerializeObject;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.Sorters;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.Backend.Utils;
import thenotoriousrog.tornadoplayer.BackgroundThreads.MainUIActivityResultThread;
import thenotoriousrog.tornadoplayer.BackgroundThreads.MainUiInitializationThread;
import thenotoriousrog.tornadoplayer.BackgroundThreads.MainUiStartupThread;
import thenotoriousrog.tornadoplayer.BluetoothSupport.MediaSessionControlCenter;
import thenotoriousrog.tornadoplayer.Handlers.ServiceHandler;
import thenotoriousrog.tornadoplayer.Listeners.AddPlaylistButtonClickListener;
import thenotoriousrog.tornadoplayer.Listeners.FilterListPopupMenuClickListener;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.Listeners.ImportPlaylistClickListener;
import thenotoriousrog.tornadoplayer.Listeners.LongSelectedSongListener;
import thenotoriousrog.tornadoplayer.Listeners.ModifyAllPlaylistsButtonClickListener;
import thenotoriousrog.tornadoplayer.Listeners.MusicControlListener;
import thenotoriousrog.tornadoplayer.R;
import thenotoriousrog.tornadoplayer.UI.CircularFragReveal;
import thenotoriousrog.tornadoplayer.UI.FlyupPanelController;

import java.util.ArrayList;


/**
 * Created by thenotoriousrog on 7/1/17.
 * This class is the most important. This class is in charge of creating the main UI for the music player. It will allow the app to load as well as display a loading screen while the app does work.
 * This class removes most of the work away from MainActivity so that the only thing that the MainActivity has to do is simply show this fragment using a Fragment Transaction.
 */

public class MainUIFragment extends Fragment {

    private static ArrayList<String> songs;  // holds all the raw song paths.
    private ArrayList<String> folders; // holds all the folders.
    private ArrayList<SongInfo> songInfoList = null; // this list holds our songInfo list holds all of the attributes of a song file.
   // private MediaPlayer mediaPlayer = new MediaPlayer(); // this class will play the songs we want, but we can only have ONE of this class otherwise multiple songs will get played.
    private String playListName = ""; // holds the name of a playlist, but this can be changed multiple times.
    private PlayListAdapter playListAdapter = null; // creates a public playlist adapter so that it is only created once, preventing multiple copies from being created.
    private PlaylistMusicAdapter playlistMusicAdapter = null; // this will allow us to set the playlistMusicAdapter and allow it to be updated once the lists have been saved.
    private ArrayList<Playlist> PlayLists = new ArrayList<>();
    private ArrayList<String> folderSongs = null;//new ArrayList<String>(); // this will hold the songs within the folder itself.
    private ArrayList<String> playlistNames = null; // holds the name of all the playlists.
    private ArrayList<Artist> artists = null; // holds all of the artists in the list.
    private ArrayList<Album> albums = null; // holds all of the ablums.
    private MainUIPagerAdapter mainUIPagerAdapter; // this holds the UI pager that we need to be sure that all of these items are updated correctly.
    private ViewPager viewPager; // the main view pager that holds all of the items in the music player.
    private com.github.clans.fab.FloatingActionMenu addPlayListMenu; // the playlist button that controls all of the actions by the users.
    private FlowingDrawer flowingDrawer; // the flowing drawer layout for the app itself.

    private MediaSessionControlCenter mediaSessionControlCenter; // a copy of the media control center to control the media session on the smartwatch.
    private MediaSessionCompat mediaSessionCompat; // a copy of the media sessin that will control events from a smartwatch.

    private FlyupPanelListener FlyupPanelListener;
    private FlyupPanelController flyupPanelController; // allows for control and respones from the flyupPanelController.
    private TabLayout tabLayout;

    // These variables are used when the user selects a song from the folder itself to help prevent the app from greatly slowing down.
    private FolderSelectedSongPlayer folderSelectedSongPlayer; // this SelectedSongPlayer is only used for when users select a song from within the folder itself. Overwriting the older folder that was used.
    private LongSelectedSongListener folderLongSelectedSongListener; // this LongSelectedSongListener is only used when a user selects a song from within the folder itself.

    public final int OPEN_FOLDER_INTENT = 100; // this is the result code that we want to pass back to the intent that way we know to look for specific intent codes.
    public final int CREATE_PLAYLIST_INTENT = 200; // result code we want to send to the open playlist intent.
    public final int MODIFY_PLAYLIST_INTENT = 300; // the result we want to receive after modifying one or all playlists.

    private boolean sortBySongName = true; // tells the app to sort songs by the song name.
    private boolean sortByArtistName = false; // tells us if the user wants the songs to be sorted by Artist name instead.
    private boolean sortByDuration = false; // tells the app to sort songs by the duration of the songs.

    private View mainView;

    MainUIActivityResultThread activityResultThread; // the background thread for all activity result actions.

    private ListView songsList; // holds the view for songs in the directory passed by the user.
    private ListView foldersList; // holds the view for songs in the directory passed by the user.
    private ListView folderSongsList; // holds all the songs inside a folder.
    private ListView playListList; // holds all of the playlists that a user creates.
    private ListView artistsList; // holds all of the artists in a folder like format.
    private ListView artistSongList; // holds all of the song lists.
    private ListView albumsList; // holds the albums listview.
    private ListView albumSongList; // holds all of the songs in the album.
    //private SeekBarCompat seekBar; // the seekbar that is used to ensure that the songs that are being played are shown and able to be controlled via a seek option.
    private SeekBar seekBar; // the seekbar that is used to ensure that the songs that are being played are shown and able to be controlled via a seek option.

    FloatingActionButton addPlaylistButton;
    FloatingActionButton modifyAllPlaylists;
    FloatingActionButton importPlaylist;

    private ServiceHandler serviceHandler; // holds a copy of the service handler
    private HandlerThread handlerThread;

    // Tells us the the way the user wants to sort their song lists.
    public String getSortByFilter()
    {
        if(sortBySongName == true)
        {
            return "SongName"; // tells the system to sort by songname.
        }
        else if(sortByArtistName == true)
        {
            return "ArtistName"; // tells the system to sort by name of artists.
        }
        else if(sortByDuration == true)
        {
            return "Duration"; // tells the system to sort by duration of songs.
        }

        return null; // this should never happen but if it does then we want to send this with null and handle the error that occurs when null is received.
    }

    // this method will sort the songs list passed in based what the sort filter is set as.
    public void sort(ArrayList<SongInfo> songsToSort)
    {
        String filter = getSortByFilter(); // gets the sort by filter that was set by the users.

        if(filter.equalsIgnoreCase("SongName"))
        {
            Sorters.sortBySongName(songsToSort); // sort the songs by song name.
        }
        else if(filter.equalsIgnoreCase("ArtistName"))
        {
            Sorters.sortByArtistName(songsToSort); // sort the songs by artist name.
        }
        else if(filter.equalsIgnoreCase("Duration"))
        {
            Sorters.sortByDuration(songsToSort); // sort the songs by duration.
        }
    }


    private void setMainView(View mainView)
    {
        this.mainView = mainView;
    }

    public View getMainView()
    {
        return mainView;
    }

    public void setSongs(ArrayList<String> songs)
    {
        this.songs = songs;
    }

    public void setFolders(ArrayList<String> folders)
    {

        this.folders = folders;
        System.out.println("Size of folders after resetting the folders: " + folders.size());

    }

    public void setArtists(ArrayList<Artist> artists)
    {
        this.artists = artists;
    }

    public void setAlbums(ArrayList<Album> albums)
    {
        this.albums = albums;
    }

    public void setPlaylistNames(ArrayList<String> playlistNames)
    {
        this.playlistNames = playlistNames;
    }

    public ArrayList<String> getSongs()
    {
        return songs;
    }

    public ArrayList<String> getFolders()
    {
        return folders;
    }

    public ArrayList<Artist> getArtists()
    {
        return artists;
    }

    public ArrayList<Album> getAlbums()
    {
        return albums;
    }

    public void setSongInfoList(ArrayList<SongInfo> songInfoList)
    {
        this.songInfoList = songInfoList;
    }

    public ArrayList<SongInfo> getSongInfoList()
    {
        return songInfoList;
    }

    public FlyupPanelController getFlyupPanelController()
    {
        return flyupPanelController;
    }

    public FlyupPanelListener getFlyupPanelListener()
    {
        return FlyupPanelListener;
    }

    // sets the correct sort filters and makes sure that one of them is at least true. Otherwise we are going to have bigger problems. We have to make sure that one of them is at least true.
    public void setSortFilters(boolean songName, boolean artistName, boolean duration)
    {
        if(songName == false && artistName == false && duration == false)
        {
            System.out.println("All three filters are false here this should not happen! Figure out where the system has all 3 of these filters as false and why!");
        }

        sortBySongName = songName;
        sortByArtistName = artistName;
        sortByDuration = duration;
    }

    public void setPlaylists(ArrayList<Playlist> playlists)
    {
        PlayLists = playlists;
    }

    public ArrayList<Playlist> getPlaylists()
    {
        return PlayLists;
    }

    // this method literally resets the songsListAdapter and hopefully will show the songs being updated in the viewpager as well.
    public void updateSongsListAdapter()
    {
        MusicAdapter musicAdapter = new MusicAdapter(getActivity(), R.layout.songlist, songInfoList); // used to just be songInfoList
       // musicAdapter.setMainUIFragment(MainUIFragment.this); // set the playlists in the class to be used by the popup menu.
        songsList.setAdapter(musicAdapter); // set out current view.
        musicAdapter.notifyDataSetChanged(); // let the music adapter know that the list is updated and ready to go.

        //updateViewPager(viewPager.getCurrentItem());
    }

    // resets the adapter and updates it for the ui to have the updated information.
    public void updateFoldersListAdapter()
    {

        FolderAdapter folderAdapter = new FolderAdapter(getActivity(), R.layout.folderlist, folders);
        folderAdapter.setMainUIFragment(this); // set the MainUIFragment that we need for the application.
        foldersList.setAdapter(folderAdapter); // set the new adapter.
        folderAdapter.notifyDataSetChanged();

       // updateViewPager(viewPager.getCurrentItem());
    }

    // sets the playlistMusicAdapter to be modified later after the app has information that is truly saved. Very important.
    public void setPlaylistMusicAdapter(PlaylistMusicAdapter pma)
    {
        pma.setMainUIFragment(MainUIFragment.this);
        playlistMusicAdapter = pma;
    }

    public void setMainUIPagerAdapter(MainUIPagerAdapter mainUIPagerAdapter)
    {
        this.mainUIPagerAdapter = mainUIPagerAdapter;
    }

    public MainUIPagerAdapter getMainUIPagerAdapter()
    {
        return mainUIPagerAdapter;
    }

    public ViewPager getViewPager()
    {
        return viewPager;
    }

    public TabLayout getTabLayout()
    {
        return tabLayout;
    }

    public void setSongsList(ListView songsList)
    {
        this.songsList = songsList;
    }

    public void setFoldersList(ListView foldersList)
    {
        this.foldersList = foldersList;
    }

    public ListView getFolderSongsList()
    {
        return folderSongsList;
    }

    public void setFolderSongsList(ListView folderSongsList)
    {
        this.folderSongsList = folderSongsList;
    }

    public void setFolderSongs(ArrayList<String> folderSongs)
    {
        this.folderSongs = folderSongs;
    }

    public ArrayList<String> getFolderSongs()
    {
        return folderSongs;
    }

    public void setPlayListList(ListView playListList)
    {
        this.playListList = playListList;
    }

    public void setArtistsList(ListView artistsList)
    {
        this.artistsList = artistsList;
    }

    public void setArtistSongList(ListView artistSongList)
    {
        this.artistSongList = artistSongList;
    }

    public void setAlbumsList(ListView albumsList)
    {
        this.albumsList = albumsList;
    }

    public void setAlbumSongList(ListView albumSongList)
    {
        this.albumSongList = albumSongList;
    }

    public void setFolderSelectedSongPlayer(FolderSelectedSongPlayer folderSelectedSongPlayer)
    {
        this.folderSelectedSongPlayer = folderSelectedSongPlayer;
    }

    public com.github.clans.fab.FloatingActionMenu getAddPlayListMenu()
    {
        return addPlayListMenu;
    }

    // this will allow us to close the flowing drawer when the user hits the back button.
    public void closeFlowingDrawer()
    {
        flowingDrawer.closeMenu(true);
    }

    // simply tells the playlistMusicAdapter to be updated whenever the data has been changed.
    public void refreshPlaylistMusicAdapter()
    {
        if(playlistMusicAdapter != null)
        {
            playlistMusicAdapter.notifyDataSetChanged();
        }

    }

    // This method will extract the sort filters out of main memory and restore them in the class to ensure that the lists are being displayed properly.
    public void extractAndSetSortFilters()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity()); // get the context that we need in order to sort these lists.
        boolean songNameSort = preferences.getBoolean("sortBySongName", true); // If someone does not get grabbed by main memory, we are defaulting this to be the sort that we want to use which is by songName
        boolean artistNameSort = preferences.getBoolean("sortByArtistName", false);
        boolean durationSort = preferences.getBoolean("sortByDuration", false);

        setSortFilters(songNameSort, artistNameSort, durationSort); // set the sort filters chosen by the users.
    }


    // This is the first method that is called when the app is ready to start loading the music UI.
    // This method will take in items from the class and begin loading the songs and preparing the adapters and listeners to be set.
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        MainUiStartupThread startupThread = new MainUiStartupThread(this);
        startupThread.startBackgroundThread(); // start the background thread.

        // Note: this makes no sense, but at the time when I was creating the main ui fragment I put all the onCreate stuff in a thread, this caused random crashes and the only way to fix it is to wait for
        // it to finish so that the MainUiInitializationThread can initialize everything in the app correctly.
        while(startupThread.isBackgroundThreadAlive()) {} // do nothing until the startup thread finishes.

       // mediaSessionControlCenter = new MediaSessionControlCenter(getActivity());

        handlerThread = new HandlerThread(Constants.HANDLER.MUSIC_PLAYER_HANDLER, Process.THREAD_PRIORITY_BACKGROUND);
        handlerThread.start();

        serviceHandler = new ServiceHandler(handlerThread.getLooper()); // create our copy of the service handler to be used throughout the application.

    }

    // starts the music control listeners after the sliding layout has been created.
    public void startMusicControlListeners(FlyupPanelListener FlyupPanelListener)
    {
        System.out.println("Creating Music Control listeners.....");

        // Image buttons.
        ImageView pauseButton;
        ImageView playButton;
        ImageView skipButton;
        ImageView prevButton;
        ImageView shuffleOnButton;
        ImageView shuffleOffButton;
        ImageView repeatOnButton;
        ImageView repeatOffButton;

        // Action codes.
        String pauseCode = "pause";
        String playCode = "play";
        String skipCode = "skip";
        String replayCode = "replay"; // the back button was pressed.
        String prevCode = "prev"; // a long click was was pressed on the back button.
        String shuffleOn = "shuffleOn";
        String shuffleOff = "shuffleOff";
        String repeatOn = "repeatOn";
        String repeatOff = "repeatOff";

        // todo: set these images buttons when we create the sliding layout in MainUIFragment
        // Image buttons.
        pauseButton = (ImageView) flyupPanelController.getFlyupPanel().findViewById(R.id.pauseButton);
        playButton = (ImageView) flyupPanelController.getFlyupPanel().findViewById(R.id.playButton);
        skipButton = (ImageView) flyupPanelController.getFlyupPanel().findViewById(R.id.skipButton);
        prevButton = (ImageView) flyupPanelController.getFlyupPanel().findViewById(R.id.prevButton); // normal press == replay, longpress == previous.
        shuffleOnButton = (ImageView) flyupPanelController.getFlyupPanel().findViewById(R.id.shuffleOn);
        shuffleOffButton = (ImageView) flyupPanelController.getFlyupPanel().findViewById(R.id.shuffleOff);
        repeatOnButton = (ImageView) flyupPanelController.getFlyupPanel().findViewById(R.id.repeatOn);
        repeatOffButton = (ImageView) flyupPanelController.getFlyupPanel().findViewById(R.id.repeatOff);


        // todo: may be able to set these OnClickListeners using the FlyupPanelListener class since the only need to have the SelectedSongPlayer class is to tell us the info for the song during actions.
        // todo: (cont.) investigate the purpose of sending in the SelectedSongPlayer as we may be able to just use FlyupPanelListener instead! This is very important!
        // click listeners for each button.
        pauseButton.setOnClickListener(new MusicControlListener(pauseButton, pauseCode, flyupPanelController.getFlyupPanel(), FlyupPanelListener, serviceHandler));
        playButton.setOnClickListener(new MusicControlListener(playButton, playCode, flyupPanelController.getFlyupPanel(), FlyupPanelListener, serviceHandler));
        skipButton.setOnClickListener(new MusicControlListener(skipButton, skipCode, flyupPanelController.getFlyupPanel(), FlyupPanelListener, serviceHandler));
        prevButton.setOnClickListener(new MusicControlListener(prevButton, replayCode, flyupPanelController.getFlyupPanel(), FlyupPanelListener, serviceHandler));
        prevButton.setOnLongClickListener(new MusicControlListener(prevButton, prevCode, flyupPanelController.getFlyupPanel(), FlyupPanelListener, serviceHandler));
        shuffleOnButton.setOnClickListener(new MusicControlListener(shuffleOnButton, shuffleOn, flyupPanelController.getFlyupPanel(), FlyupPanelListener, serviceHandler));
        shuffleOffButton.setOnClickListener(new MusicControlListener(shuffleOffButton, shuffleOff, flyupPanelController.getFlyupPanel(), FlyupPanelListener, serviceHandler));
        repeatOnButton.setOnClickListener(new MusicControlListener(repeatOnButton, repeatOn, flyupPanelController.getFlyupPanel(), FlyupPanelListener, serviceHandler));
        repeatOffButton.setOnClickListener(new MusicControlListener(repeatOffButton, repeatOff, flyupPanelController.getFlyupPanel(), FlyupPanelListener, serviceHandler));
    }


    // opens the settings fragment.
    public void openSettingsFragment()
    {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        SettingsFragment settingsFragment = new SettingsFragment();

        InitializerActivity initializerActivity = (InitializerActivity) getActivity();
        initializerActivity.setSettingsFragment(settingsFragment);

        ChangeBounds bounds = new ChangeBounds();
        bounds.setDuration(500);

        Slide slide = new Slide();
        slide.setSlideEdge(Gravity.RIGHT);
        slide.setDuration(350);

        settingsFragment.setEnterTransition(slide);

        transaction.replace(R.id.mainFrameLayout, settingsFragment);

        transaction.addToBackStack(null); // nothing to go back when the user hits the back button.
        transaction.commit(); // commit the fragment to be shown.
    }


    // applies the theme based on what the user has selected
    // todo: make sure that create view also calls this in the future for when the user changes the theme so that the interface is correcly applied.
    public void applyTheme()
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity()); // gets the default shared preferences
        String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.

        AppBarLayout appBar = mainView.findViewById(R.id.appbar); // the main app bar of the app itself.
        TabLayout tabs = mainView.findViewById(R.id.tabs);
        RelativeLayout mainLayoutBottomSheet = mainView.findViewById(R.id.mainLayoutBottomSheet); // the main layout bottom sheet.
        RelativeLayout topBar = mainView.findViewById(R.id.topBar); // the top bar of the peek view of the flyup panel.
        RelativeLayout songInfoLayout = mainView.findViewById(R.id.SongInfoBar); // the song info bar that holds all of the song stuff.
        RelativeLayout upNextTextBar = mainView.findViewById(R.id.upNextTextBar); // the bar with the up next items that are showing.
        RelativeLayout musicControlBar = mainView.findViewById(R.id.MusicControlBar); // the music control bar that holds all of the music control items.
        FloatingActionMenu fam = mainView.findViewById(R.id.addPlaylistButton); // the add playlist button itself.

        mainLayoutBottomSheet.invalidate();
        mainLayoutBottomSheet.getRootView().invalidate();
        //getView().invalidate();
        //getView().getRootView().invalidate();

        // todo: I need to figure out how to set the color of the other themes in the app.
        //eekBar = mainView.findViewById(R.id.seekBar); // the seek bar within the layout itself.


        System.out.println("the theme is: " + theme);

        int currColorPrimary = ThemeUtils.getInstance().getCurrentThemePrimaryColor();
        int currColorAccent = ThemeUtils.getInstance().getCurrentThemeAccentColor();
        int currSeekbarThumb = ThemeUtils.getInstance().getCurrentSeekbarThumb();
        int currSeekbarProgressDrawable = ThemeUtils.getInstance().getCurrentSeekbarProgressDrawable();

       // seekBar.set
        //seekBar.setProgressColor(ContextCompat.getColor(getActivity(), currColorPrimary));
       // seekBar.setThumbColor(ContextCompat.getColor(getActivity(), currColorAccent));
        seekBar.setThumb(getActivity().getDrawable(currSeekbarThumb));
        //seekBar.setBackground(getActivity().getDrawable(R.drawable.seekbar_ocean_marina));
        seekBar.setProgressDrawable(getActivity().getDrawable(currSeekbarProgressDrawable));

        // changes the color of the status bar.
        Window window = getActivity().getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(getActivity(), currColorPrimary));

        System.out.println("Working on applying the ocean marina theme");
        appBar.setBackgroundColor(ContextCompat.getColor(getActivity(), currColorPrimary));
        appBar.invalidate();
        topBar.setBackgroundColor(ContextCompat.getColor(getActivity(), currColorPrimary));
        topBar.invalidate();
        songInfoLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), currColorPrimary));
        songInfoLayout.invalidate();
        upNextTextBar.setBackgroundColor(ContextCompat.getColor(getActivity(), currColorPrimary));
        upNextTextBar.invalidate();
        musicControlBar.setBackgroundColor(ContextCompat.getColor(getActivity(), currColorPrimary));
        musicControlBar.invalidate();

        tabs.setSelectedTabIndicatorColor(ContextCompat.getColor(getActivity(), currColorAccent));
        fam.setMenuButtonColorNormal(ContextCompat.getColor(getActivity(), currColorAccent));
        fam.setMenuButtonColorPressed(ContextCompat.getColor(getActivity(), currColorAccent));
        addPlaylistButton.setColorNormal(ContextCompat.getColor(getActivity(), currColorAccent));
        addPlaylistButton.setColorPressed(ContextCompat.getColor(getActivity(), currColorAccent));
        modifyAllPlaylists.setColorNormal(ContextCompat.getColor(getActivity(), currColorAccent));
        modifyAllPlaylists.setColorPressed(ContextCompat.getColor(getActivity(), currColorAccent));
        importPlaylist.setColorNormal(ContextCompat.getColor(getActivity(), currColorAccent));
        importPlaylist.setColorPressed(ContextCompat.getColor(getActivity(), currColorAccent));

        mainView.invalidate();
        mainLayoutBottomSheet.invalidate();
        mainView.getRootView().invalidate();
    }

    // This method is in charge of creating as well as setting all the adapters and listeners of the main U.I. itself.
    // This method is what will make the U.I. become displayed. Essentially when MainActivity creates this fragment, everything will be already loaded and ready to be displayed. Awesome!
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        int currentTheme = Utils.getCurrentTheme(getActivity());

        //final Context contextThemeWrapper = new ContextThemeWrapper(getActivity(), currentTheme);
        //LayoutInflater localInflater = getActivity().getLayoutInflater().cloneInContext(contextThemeWrapper);

        final View mainView = inflater.inflate(R.layout.activity_main, container, false); // inflate the main layout.
        setMainView(mainView); // set the main view to be used by background threads.

        // implement the flyup panel here.
        final View flyupPanel = mainView.findViewById(R.id.flyupPanel); // get the flyup panel

        final BottomSheetBehavior flyupPanelBehavior = BottomSheetBehavior.from(flyupPanel); // set the bottom sheet behavior for the flyup panel.
        flyupPanelController = new FlyupPanelController(flyupPanel, flyupPanelBehavior); // instantiate the flyup panel controller.
        flyupPanelController.hideFlyupPanel(); // hide the flyup panel for when the app is loading.

        //setupFlowingDrawer(mainView); // sets up the flowing drawer.


        RelativeLayout topbar = mainView.findViewById(R.id.topBar);
        topbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                flyupPanelController.flyPanelUp();
            }
        });


        TextView nowPlayingText = (TextView) flyupPanelController.getFlyupPanel().findViewById(R.id.nowPlaying); // the view that actually shows the Now Playing... message
        TextView songText = (TextView) flyupPanelController.getFlyupPanel().findViewById(R.id.songText); // the view in the slide that actually shows the song title.
        TextView artistText = (TextView) flyupPanelController.getFlyupPanel().findViewById(R.id.artistText); // the view in the slide that shows the artist of the song.
        ImageView albumArt = (ImageView) flyupPanelController.getFlyupPanel().findViewById(R.id.AlbumArt); // album art on the sliding layout.

        TextView upNext = (TextView) flyupPanelController.getFlyupPanel().findViewById(R.id.upNextText); // upnext text on the sliding layout.
        final TextView timerText = (TextView) flyupPanelController.getFlyupPanel().findViewById(R.id.songTimeText); // timer on the sliding layout.

        final ImageView searchOptions = mainView.findViewById(R.id.search);
        searchOptions.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {

                // The intent is necessary due to the edit text problems.
                Intent searchIntent = new Intent(getActivity(), SearchActivity.class);
                  Bundle bundle = new Bundle();
                  bundle.putStringArrayList("folders", getFolders());
                searchIntent.putExtras(bundle); // place the arguments for the activity.

                startActivity(searchIntent);
            }
        });

        final ImageView filterOptions = (ImageView) mainView.findViewById(R.id.filterOptions); // gets the filter list image. We will need to create a popup menu for this list when this happens.
        filterOptions.setOnClickListener(new View.OnClickListener() {

            // When clicked open up the options menu and make sure that the menu is in fact working correctly.
            @Override
            public void onClick(View v)
            {
                PopupMenu filterOptions = new PopupMenu(getActivity(), v); // grab the main view that we want to be modifying here.
                MenuInflater inflater = filterOptions.getMenuInflater();
                inflater.inflate(R.menu.filter_list_options, filterOptions.getMenu()); // inflate the menu we are using for the filter_list options.

                // TODO: create a menuClickListener for the filter_options area and make sure that the entire list is able to be searched. This is very important!!!!
                FilterListPopupMenuClickListener filterPopupMenuClkListener = new FilterListPopupMenuClickListener(getActivity(), MainUIFragment.this, songInfoList); // the click listener that is needed for this here.
                filterOptions.setOnMenuItemClickListener(filterPopupMenuClkListener); // set the click listener the filter options popup.

                filterOptions.show();
            }
        });

        final ImageView options = (ImageView) mainView.findViewById(R.id.overflowOptions); // grab the extra options menu on the actual U.I.
        options.setOnClickListener(new View.OnClickListener() {

            // When clicked open up the popup menu and listen for actions after the menu is opened.
            @Override
            public void onClick(View v)
            {
                PopupMenu extraOptions = new PopupMenu(getActivity(), v); // grab the main view that we want to be modifying here.
                MenuInflater inflater = extraOptions.getMenuInflater();
                inflater.inflate(R.menu.main_ui_options, extraOptions.getMenu()); // inflate the extra options menu, this is very important.

                // TODO: create a menuClick listener for the extra options area and create a fragment that will show the fragment that we are trying to write over.
                extraOptions.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        if(menuItem.getTitle().toString().equalsIgnoreCase("settings"))
                        {
                            openSettingsFragment();
                        }

                        return true;
                    }
                });

                extraOptions.show();
            }
        });

       // todo: implement search here!

        playListAdapter = new PlayListAdapter(getActivity(), R.layout.playlist_list, PlayLists);
        addPlayListMenu = mainView.findViewById(R.id.addPlaylistButton); // grab in our main activity.
        addPlayListMenu.setClosedOnTouchOutside(true);
        addPlayListMenu.setMenuButtonColorRipple(ContextCompat.getColor(getActivity(), R.color.white));
        addPlayListMenu.hideMenuButton(true);
        addPlayListMenu.close(true); // ensure that the add playlist menu is closed on startup.

        addPlaylistButton = new FloatingActionButton(getActivity());
        addPlaylistButton.setButtonSize(FloatingActionButton.SIZE_MINI);
        addPlaylistButton.setImageResource(R.drawable.edit_white_15dp);
        addPlaylistButton.setColorRipple(ContextCompat.getColor(getActivity(), R.color.white));
        addPlaylistButton.setColorNormal(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        addPlaylistButton.setColorPressed(ContextCompat.getColor(getActivity(), R.color.colorAccent));

        // create a click listener for the button lists.
        AddPlaylistButtonClickListener addPlaylistButtonClickListener = new AddPlaylistButtonClickListener(this, getPlaylists(), getSongs(),
                getSongInfoList(), getFolders());
        addPlaylistButton.setOnClickListener(addPlaylistButtonClickListener); // the click listener for the app playlist button.

      //  addPlaylistButton.setColorNormal(ContextCompat.getColor(getActivity(), R.color.Red)); // make the button red.
        addPlaylistButton.setLabelText("Create Playlist");
        //addPlaylistButton.setLabelColors(ContextCompat.getColor(getActivity(), R.color.white), ContextCompat.getColor(getActivity(), R.color.white), ContextCompat.getColor(getActivity(), R.color.transparent_black) );

        modifyAllPlaylists = new FloatingActionButton(getActivity());
        modifyAllPlaylists.setButtonSize(FloatingActionButton.SIZE_MINI);
        modifyAllPlaylists.setImageResource(R.drawable.edit_white_15dp);
        modifyAllPlaylists.setColorNormal(ContextCompat.getColor(getActivity(), R.color.colorAccent));
        modifyAllPlaylists.setColorRipple(ContextCompat.getColor(getActivity(), R.color.white));
        modifyAllPlaylists.setColorPressed(ContextCompat.getColor(getActivity(), R.color.colorAccent));

        // create a click listener for the button lists.
        ModifyAllPlaylistsButtonClickListener modifyAllPlaylistsButtonClickListener = new ModifyAllPlaylistsButtonClickListener(this, getPlaylists(), getSongs(),
                getSongInfoList(), getFolders());
        modifyAllPlaylists.setOnClickListener(modifyAllPlaylistsButtonClickListener);

        //modifyAllPlaylists.setColorNormal(ContextCompat.getColor(getActivity(), R.color.Red));
        modifyAllPlaylists.setLabelText("Modify All Playlists");
        //modifyAllPlaylists.setLabelColors(ContextCompat.getColor(getActivity(), R.color.white), ContextCompat.getColor(getActivity(), R.color.white), ContextCompat.getColor(getActivity(), R.color.transparent_black));

        importPlaylist = new FloatingActionButton(getActivity());
        importPlaylist.setButtonSize(FloatingActionButton.SIZE_MINI);
        importPlaylist.setImageResource(R.drawable.down_arrow_white);
        importPlaylist.setLabelText("Import Playlist");

        ImportPlaylistClickListener importPlaylistClickListener = new ImportPlaylistClickListener(this);
        importPlaylist.setOnClickListener(importPlaylistClickListener);

        // add the buttons within the menu!
        addPlayListMenu.addMenuButton(addPlaylistButton);
        addPlayListMenu.addMenuButton(modifyAllPlaylists);
        addPlayListMenu.addMenuButton(importPlaylist);

        // TODO: grab the view pager that we want to work with and update the fields within the view pager itself.
        viewPager = (ViewPager) mainView.findViewById(R.id.tabsViewPager); // grab the view pager that we want to work with. This is very important.

        seekBar = mainView.findViewById(R.id.seekBar); // grab the seek bar that is being used by the sliding layout.

        FlyupPanelListener = new FlyupPanelListener(nowPlayingText, songText, artistText, albumArt, upNext, timerText, getActivity(), seekBar, flyupPanelController, this); // set the panel sliding listener here.
        flyupPanelController.setFlyupPanelListener(FlyupPanelListener); // set the panel listener for the flyupPanelController.
        //flyupPanel.addPanelSlideListener(FlyupPanelListener); // set the listener that we are using for the sliding layout. There is only one listener, another will not be created.

        tabLayout = (TabLayout) mainView.findViewById(R.id.tabs);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabTextColors(ContextCompat.getColor(getActivity(), R.color.background_grey), ContextCompat.getColor(getActivity(), R.color.white) );

        MainUiInitializationThread initializationThread = new MainUiInitializationThread(this, addPlayListMenu, mainView);
        initializationThread.startBackgroundThread(); // start the background thread now!

        //mediaSessionControlCenter.setFlyupPanelListener(FlyupPanelListener); // set the flyup panel listener to ensure the control center can control the application!
        //showStartUpDialog(mainView); // show the startup dialog. TODO: remove this once the app is in beta texting!!
        applyTheme();
        return mainView;
    }

    // this method will simply begin the transaction to add songs to a single playlist very important.
    public void startAddSongsToSinglePlaylistActivity(Playlist playlistChosen)
    {
        int chosenPlaylistPos = 0;
        String playlistName = "";
        for(int i = 0; i < PlayLists.size(); i++)
        {
            // TODO: if this breaks we need to fix this to only search for playlist name since that is the most important thing here.
            if(PlayLists.get(i).name().equals(playlistChosen.name()))
            {
                playlistName = PlayLists.get(i).name(); // grab the name of the playlist.
            }
        }

        Intent addSongsToPlaylistActivity = new Intent(getActivity(), AddSongsToSinglePlaylistFragmentActivity.class); // activity for adding songs to a single playlist.
        Bundle playlistIntentBundle = new Bundle();
        playlistIntentBundle.putStringArrayList("songs", songs); // send in the songs to be used by the modifyAllPlaylistsFragmentActivity
        playlistIntentBundle.putStringArrayList("playlistNames", getPlaylistNames()); // send in the playlist names.
        playlistIntentBundle.putParcelableArrayList("songInfos", songInfoList); // send in the songInfoList
        playlistIntentBundle.putStringArrayList("folders", folders); // send in the folders.
        playlistIntentBundle.putParcelable("PlaylistChosen", playlistChosen); // the playlist that we are listening too
        playlistIntentBundle.putParcelableArrayList("playlists", PlayLists); // send in the playlists NOTE: remove this if we cannot do it this way as it could be causing problems.
        //playlistIntentBundle.putInt("PlaylistPosition", chosenPlaylistPos); // send in the last playlist because that is the one we just added muahaha
        playlistIntentBundle.putString("ChosenPlaylistName", playlistName); // send in the name of the playlist that we want to work with.
        addSongsToPlaylistActivity.putExtra("args", playlistIntentBundle); // send the arguments to the fragmentActivity.
        startActivityForResult(addSongsToPlaylistActivity, MODIFY_PLAYLIST_INTENT); // start the activity for the result of the songs in the playlist.

        refreshPlaylistAdapter();
        updatePlaylistAdapter(); // this forces a sort making the list behave much better.

        // Warning: if issues arise just uncomment the line below and pass in the viewPager via the constructor of this class from the mainUIFragment.
        // mainUIFragment.updateViewPager(viewPager.getCurrentItem()); // update the view pager after the playlist was updated.

        updateViewPager(getCurrentPageItem()); // this should work if not see the warning comment 3 lines up.
    }

    // This will read whatever is written into main memory and grab our playlists arraylist.
    private ArrayList<Playlist> readPlaylistsFromMainMemory()
    {
        // READ the playlist data object from main memory if it exists.
        ArrayList<Playlist> playlists = new ArrayList<>();
        String ser1 = SerializeObject.ReadSettings(getActivity().getBaseContext(), "playlists.dat"); // attempt to find this in main memory.
        if(ser1 != null && !ser1.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(ser1); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                playlists = (ArrayList<Playlist>) obj; // set our songInfo list.
            }
        }

        Sorters.sortPlaylistsName(playlists); // sorts the playlists by their name to keep the app looking uniform.
        return playlists; // todo: make sure that this is not null
    }

    // tells the caller of this method if the slidinglayout is expanded or not.
    public boolean isSlidingPanelExpanded()
    {

        // todo: create a class that checks that for us! Very important!
        if(flyupPanelController.getFlyupPanelState() == BottomSheetBehavior.STATE_EXPANDED)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // forces the panel to fly down!
    public void slidePanelDown()
    {
        flyupPanelController.flyPanelDown();
    }

    // this method returns the current item from the view pager which helps with deciding what to do for the music player.
    public int getCurrentPageItem()
    {
        return viewPager.getCurrentItem();
    }

    // Opens but the folder when the user wishes to do so.
    public void openFolder(ListView folderSongsList)
    {
        int tempListItem = viewPager.getCurrentItem();
        mainUIPagerAdapter.openFolder(folderSongsList, viewPager.getCurrentItem()); // open the folder of the current view that we are working with.
        mainUIPagerAdapter.notifyDataSetChanged();

        viewPager.setAdapter(mainUIPagerAdapter);
        viewPager.setCurrentItem(tempListItem);

        folderSongsList.startLayoutAnimation(); // start the fading animation
    }

    // closes the folder and shows the list of folders to begin with which is a pretty big deal.
    public void closeFolder()
    {
        int tempListItem = viewPager.getCurrentItem();

        mainUIPagerAdapter.closeFolder(tempListItem); // send in the original folderlist to reset the view pager.
        mainUIPagerAdapter.notifyDataSetChanged();

        viewPager.setAdapter(mainUIPagerAdapter);
        viewPager.setCurrentItem(tempListItem);
        foldersList.startLayoutAnimation(); // start the fading animation.
    }

    // this method tells the mainUIPager to open the playlist and display the songs within it.
    public void openPlaylist(ListView playListList)
    {
        playListList.setVerticalScrollBarEnabled(false); // removes the vertical scrollbar for listview.
        int tempListItem = viewPager.getCurrentItem();
        mainUIPagerAdapter.openPlaylist(playListList, viewPager.getCurrentItem()); // open the folder of the current view that we are working with.
        mainUIPagerAdapter.notifyDataSetChanged();

        viewPager.setAdapter(mainUIPagerAdapter);
        viewPager.setCurrentItem(tempListItem);

        playListList.startLayoutAnimation(); // start the fading animation.
    }

    // this method will close the folder in the playlist and update the items within that playlist. Very important.
    public void closePlaylist()
    {
        int tempListItem = viewPager.getCurrentItem();
        mainUIPagerAdapter.closePlaylist(tempListItem); // send in the original folderlist to reset the view pager.
        mainUIPagerAdapter.notifyDataSetChanged();

        viewPager.setAdapter(mainUIPagerAdapter);
        viewPager.setCurrentItem(tempListItem);

        playListList.startLayoutAnimation(); // start the fading animation.
    }

    public void openArtist(ListView artistSongList)
    {
        int tempListItem = viewPager.getCurrentItem();
        mainUIPagerAdapter.openArtist(artistSongList, viewPager.getCurrentItem());
        mainUIPagerAdapter.notifyDataSetChanged();

        viewPager.setAdapter(mainUIPagerAdapter);
        viewPager.setCurrentItem(tempListItem);

        artistSongList.startLayoutAnimation(); // start the fading animation.
    }

    public void closeArtist()
    {
        int tempListItem = viewPager.getCurrentItem();
        mainUIPagerAdapter.closeArtist(tempListItem); // send in the original folderlist to reset the view pager.
        mainUIPagerAdapter.notifyDataSetChanged();

        viewPager.setAdapter(mainUIPagerAdapter);
        viewPager.setCurrentItem(tempListItem);

        artistsList.startLayoutAnimation(); // start the fading animation.
    }

    public void openAlbum(ListView albumSongList)
    {
        int tempListItem = viewPager.getCurrentItem();
        mainUIPagerAdapter.openAlbum(albumSongList, viewPager.getCurrentItem());
        mainUIPagerAdapter.notifyDataSetChanged();

        viewPager.setAdapter(mainUIPagerAdapter);
        viewPager.setCurrentItem(tempListItem);

        albumSongList.startLayoutAnimation(); // start the fading animation.
    }

    public void closeAlbum()
    {
        int tempListItem = viewPager.getCurrentItem();
        mainUIPagerAdapter.closeAlbum(tempListItem); // send in the original folderlist to reset the view pager.
        mainUIPagerAdapter.notifyDataSetChanged();

        viewPager.setAdapter(mainUIPagerAdapter);
        viewPager.setCurrentItem(tempListItem);

        albumsList.startLayoutAnimation(); // start the fading animation.
    }

    // this causes the view pager to be updated and snap automatically to the page that was modified on.
    public void updateViewPager(int currentPageItem)
    {
        mainUIPagerAdapter.notifyDataSetChanged();
        viewPager.setAdapter(mainUIPagerAdapter);
        viewPager.setCurrentItem(currentPageItem);
    }

    // This method is in charge of retrieving any result from an activity. Different codes tell us where the result is coming from!
    // Essentially the only time that this item is going to be called is when the user grabs a folder, this will let us open the folder and grab all of the songs and put them into a folder.
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intentResult) {
        System.out.println("we are in activity result now");
        super.onActivityResult(requestCode, resultCode, intentResult);

        if(activityResultThread != null && !activityResultThread.isBackgroundThreadAlive()) // thread is not alive but is instantiated.
        {
            activityResultThread = new MainUIActivityResultThread(this, requestCode, intentResult);
            activityResultThread.startBackgroundThread();
        }
        else if(activityResultThread != null && activityResultThread.isBackgroundThreadAlive()) // thread is alive and instantiated
        {
            activityResultThread.stopThread();
            activityResultThread = new MainUIActivityResultThread(this, requestCode, intentResult);
            activityResultThread.startBackgroundThread();
        }
        else // thread was never instantiated.
        {
            activityResultThread = new MainUIActivityResultThread(this, requestCode, intentResult);
            activityResultThread.startBackgroundThread();
        }



    }

    // refreshes the playlist adapter showing any changes made to the playlists.
    public void refreshPlaylistAdapter()
    {
        Sorters.sortPlaylistsName(PlayLists); // resorts the playlists according to their names
        playListAdapter.notifyDataSetChanged();
    }

    // resets the playListList to ensure that the adapter is accurate. Called only by after the user has created a new playlist and saved it.
    public void updatePlaylistAdapter()
    {
        Sorters.sortPlaylistsName(PlayLists); // resorts the playlists according to their names
        playListAdapter = new PlayListAdapter(getActivity(), R.layout.playlist_list, PlayLists);
        playListAdapter.setMainUIFragment(MainUIFragment.this); // very important and greatly needed.
        playListList.setAdapter(playListAdapter);
        playListAdapter.notifyDataSetChanged();

    }


    // Adds an empty playlist to the list of our playlists.
    public void addEmptyPlaylist()
    {
        String playlistName = getPlayListName(); // get the name of the newly created playlist.
        ArrayList<SongInfo> emptySongList = new ArrayList<SongInfo>(); // an empty playlist that has no songs in it.
        //Pair<String, ArrayList<SongInfo>> newPair = new Pair<String, ArrayList<SongInfo>>(playlistName, emptySongList); // create a new empty pair. old version
        Playlist emptyPlaylist = new Playlist(playlistName, emptySongList);
        PlayLists.add(emptyPlaylist);
    }

    // returns the names of all playlists
    public ArrayList<String> getPlaylistNames()
    {
        ArrayList<String> playlistNames = new ArrayList<>();
        for(int i = 0; i < PlayLists.size(); i++)
        {
            //playlistNames.add(PlayLists.get(i).getName()); // add the name into the arraylist. old version
            playlistNames.add(PlayLists.get(i).name());
        }

        return playlistNames;
    }

    public void setPlayListName(String name)
    {
        playListName = name;
    }

    public String getPlayListName()
    {
        return playListName;
    }
}
