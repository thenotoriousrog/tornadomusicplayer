package thenotoriousrog.tornadoplayer.Fragments;

import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import thenotoriousrog.tornadoplayer.Backend.BitmapWorkshop;
import thenotoriousrog.tornadoplayer.R;

/**
 * Created by thenotoriousrog on 7/1/17.
 * This class is simply responsible for displaying and showing the loading screen and that's about it. It does not do any work to the app except keep the user occupied.
 */

public class LoadingScreenFragment extends Fragment {

    TextView mainTextView;

    // called when this fragment is started. This will also start the spinning performance of the image view.
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    // simply sets text for the user to look at
    public void updateLoadingText(String textToDisplay)
    {
        System.out.println("is main text view null? " + mainTextView);
        mainTextView.setText(textToDisplay);
    }

    // creates the view for us, no real work is needed however.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.loading_screen, container, false); // inflate the loading screen. Nothing too fancy here.
        mainTextView = view.findViewById(R.id.loadingScreenText); // this holds the text of the loading fragment.

        System.out.println("is main text view null in on create?" + mainTextView);

        // get and set the spinning tornado for the startup screen.
        ImageView spinningTornado = view.findViewById(R.id.tornado);
        Bitmap bm = BitmapWorkshop.decodeSampledBitmapFromResource(getResources(), R.drawable.soundspout_ocean_marina, 400,400);
        spinningTornado.setImageBitmap(bm);

        // use object animator to flip image while we are loading.
        ObjectAnimator spin = ObjectAnimator.ofFloat(spinningTornado, "rotationY", 0.0f, 720.0f); // spin the tornado image for two full revolutions.
        spin.setDuration(1200); // the higher the number the slower the spinning animation
        spin.setRepeatCount(ObjectAnimator.INFINITE); // repeat constantly
        spin.reverse(); // reverse the spinning animation
        spin.start(); // start the animation.

        TextView funMessage = view.findViewById(R.id.funMessage); // the fun message for the loading screen

        int haloFaceUnicode = 0x1F607;
        String haloEmojie = new String(Character.toChars(haloFaceUnicode)); // the halo face emojie.
        funMessage.setText("Tornadoes are scary! But I'm not " + haloEmojie);

        return view; // return the created view.
    }
}
