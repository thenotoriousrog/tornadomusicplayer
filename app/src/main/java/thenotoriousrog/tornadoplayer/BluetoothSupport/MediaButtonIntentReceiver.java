package thenotoriousrog.tornadoplayer.BluetoothSupport;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;

/**
 * Created by thenotoriousrog on 3/18/18.
 */

public class MediaButtonIntentReceiver extends BroadcastReceiver {

    private int clickCounter = 0; // the click counter that's needed.
    private long clickDuration = 0; // the duration since the last click.
    private long firstClickTime = 0; // the system time since the very first click.
    private final int ONE_SECOND = 1000; // one thousand milliseconds = 1 second.

    // handles all keyevents.
    private void handleKeyEvent(KeyEvent event, String action)
    {
        if(event.getAction() != KeyEvent.ACTION_DOWN) return;

        System.out.println(event.getAction() + " " + event.getKeyCode() + " " + event.getCharacters());

        if(event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)
        {
            BluetoothControlCenter.playPause(); // triggers a playpause event.
        }
        else if(event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY)
        {
            System.out.println("Detected play");
            BluetoothControlCenter.play();
        }
        else if(event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PAUSE)
        {
            System.out.println("detected pause");
            BluetoothControlCenter.pause();
        }
        else if(event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_NEXT)
        {
            System.out.println("next/skip was detected");
            BluetoothControlCenter.skip();
        }
        else if(event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PREVIOUS)
        {
            System.out.println("previous was detected!");

            // if the song is less than 2 seconds in just go back to the last song.
            if(MusicLibrary.getCurrentSongTimePosition() < 2000)
            {
                BluetoothControlCenter.prev();
            }
            else {
                BluetoothControlCenter.replay();
            }

        }
    }

    // handles all bluetooth events
    private void handleBluetoothEvent(BluetoothDevice device, String action)
    {
        if(BluetoothDevice.ACTION_ACL_CONNECTED.equalsIgnoreCase(action)) // device was connected. Play music
        {
            BluetoothControlCenter.play();
        }
        else if(BluetoothDevice.ACTION_ACL_DISCONNECTED.equalsIgnoreCase(action)) // device disconnected. Pause music
        {
            BluetoothControlCenter.pause();
        }
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        String action = intent.getAction();

        final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE); // get the intent of the bluetooth device
        if(device != null)
        {
            handleBluetoothEvent(device, action);
        }

        final KeyEvent event = (KeyEvent) intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
        if(event != null)
        {
            handleKeyEvent(event, action);
        }


    }
}
