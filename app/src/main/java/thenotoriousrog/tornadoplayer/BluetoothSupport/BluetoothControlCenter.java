package thenotoriousrog.tornadoplayer.BluetoothSupport;

import android.app.Activity;
import android.app.PendingIntent;
import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaMetadata;
import android.media.MediaPlayer;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Parcel;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v4.media.session.MediaButtonReceiver;
import android.view.KeyEvent;
import android.widget.Toast;

import java.nio.file.ClosedFileSystemException;
import java.security.Key;
import java.util.Set;

import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.Services.PlayerService;

/**
 * Created by thenotoriousrog on 3/12/18.
 *
 * This class is in control of all bluetooth commands. In control of paying attention to connecting and disconnecting the bluetooth
 */

public class BluetoothControlCenter extends BroadcastReceiver implements BluetoothProfile.ServiceListener {

    private BluetoothDevice bluetoothDevice;
    private BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothAdapter bluetoothHeadsetAdapter = BluetoothAdapter.getDefaultAdapter();
    private BluetoothA2dp bluetoothA2dpService; // allows for streaming high quality audio
    private BluetoothHeadset bluetoothHeadset; // a bluetooth headset connection.
    private AudioManager audioManager; // allows for controlling audio.
   // private static FlyupPanelListener flyupPanelListener; // controls the flyup panel.
    private Activity currentActivity;
    //private static MediaPlayer mediaPlayer;
    //MediaSession mediaSession;
    private boolean isConnected = false; // if true, the bluetooth control center is connected to some bluetooth device.
    private static PlayerService playerService; // the player service that we are using to communicate actions with.
    // TODO: any actions on play, pause, skip, or prev should be sent to the appropriate class to ensure that its properly handled.

    private int clickCounter = 0; // the click counter that's needed.
    private long clickDuration = 0; // the duration since the last click.
    private long firstClickTime = 0; // the system time since the very first click.
    private final int ONE_SECOND = 1000; // one thousand milliseconds = 1 second.
    SharedPreferences sharedPreferences;


    public BluetoothControlCenter(PlayerService playerService)
    {
       // this.flyupPanelListener = flyupPanelListener;
        //this.currentActivity = currentActivity;
        //this.mediaPlayer = mediaPlayer;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(playerService.getBaseContext()); // gets the default shared preferences

        this.playerService = playerService;

        bluetoothAdapter.getProfileProxy(playerService, this, BluetoothProfile.A2DP);
        bluetoothHeadsetAdapter.getProfileProxy(playerService, this, BluetoothProfile.HEADSET);
        audioManager = (AudioManager) playerService.getSystemService(Context.AUDIO_SERVICE);
        //mediaSession = new MediaSession(currentActivity.getApplicationContext(), currentActivity.getPackageName());
    }


    // most bluetooth players don't use this, but if they do, we are prepared.
    public static void playPause()
    {
        System.out.println("PLAY/PAUSE WAS DETECTED!!!");
//        if(mediaPlayer.isPlaying())
//        {
//            flyupPanelListener.clickPause();
//        }
//        else
//        {
//            flyupPanelListener.clickPlay();
//        }
    }

    // sends a play event to the flyup panel
    public static void play()
    {
        playerService.play();
        //flyupPanelListener.clickPlay();
    }

    // sends a pause event to the flyup panel
    public static void pause()
    {
        playerService.pause();
        //flyupPanelListener.clickPause();
    }

    // sends a skip event to flyup panel
    public static void skip()
    {
        playerService.skip();
        //flyupPanelListener.clickSkip();
    }

    // sends single click event to the flyup panel.
    public static void replay()
    {
        playerService.replaySong();
        //flyupPanelListener.clickPrev(); // triggers replay.
    }

    public static void prev()
    {
        playerService.prev();
       // flyupPanelListener.longClickPrev(); // triggers prev action.
    }

    // tells the caller if the user is connected to a bluetooth device, if so, we should be sending updates to the bluetooth device.
    public boolean isCurrentlyConnected()
    {
        return isConnected;
    }

    // notifies bluetooth that the song was changed and sends the correct information.
    public void notifySongChange(SongInfo currSong, int currSongPos, int numOfSongs, SongInfo nextSong)
    {
        Intent bluetoothUpdateIntent = new Intent("com.android.music.metachanged");
        bluetoothUpdateIntent.putExtra("track", currSong.getSongTime());
        bluetoothUpdateIntent.putExtra("artist", currSong.getArtistName());
        bluetoothUpdateIntent.putExtra("duration", currSong.getSongDuration());
        bluetoothUpdateIntent.putExtra("position", currSongPos);
        bluetoothUpdateIntent.putExtra("ListSize", numOfSongs);

    }

    // behavior for when a connection is received, usually a connection or disconnection.
    @Override
    public void onReceive(Context context, Intent intent)
    {
        String action = intent.getAction();

        System.out.println("Action received: " + action);

        // This needs to handle options for whenever the device is disconnected from the bluetooth device! Very important!
        if(action.equals(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED))
        {
            int state = intent.getIntExtra(BluetoothA2dp.EXTRA_STATE, BluetoothA2dp.STATE_DISCONNECTED);
            if(state == BluetoothA2dp.STATE_CONNECTED)
            {
                System.out.println("Connected to device!");
                // todo: make a setting to allow for users to be able to change the behavior of when a bluetooth service is connected.
                //playerService.play(); // initiate a play when connected to bluetooh.
                //flyupPanelListener.clickPlay();
                //Toast.makeText(context, "CONNECTED TO DEVICE", Toast.LENGTH_SHORT).show();
            }
            else if(state == BluetoothA2dp.STATE_DISCONNECTED)
            {
                System.out.println("Disconnected from device!!");
                // TODO: stop playing music here!
                if(MusicLibrary.isActivelyPlaying())
                {
                    playerService.pause();
                }

            }
        }
        else if(action.equals(BluetoothDevice.ACTION_ACL_CONNECTED))
        {
            boolean btReconnectPlay = sharedPreferences.getBoolean("BTReconnectPlay", false); // do not play by default.
            if(MusicLibrary.isMusicPaused() && btReconnectPlay) // if music is paused and bt reconnect play is true, play music
            {
                playerService.play(); // start playing music again.
            }
        }
        else if(action.equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)) // it's this action code that checks if a device was disconnected to bluetooth or not.
        {
            boolean btDisconnectPause = sharedPreferences.getBoolean("BTDisconnectPause", true); // pause on default.

            System.out.println("a bluetooth device was disconnected. ");
            if(MusicLibrary.isActivelyPlaying() && btDisconnectPause)
            {
                playerService.pause();
            }
        }
        else if(action.equals(BluetoothHeadset.ACTION_CONNECTION_STATE_CHANGED))
        {
            int state = intent.getIntExtra(BluetoothHeadset.EXTRA_STATE, BluetoothHeadset.STATE_AUDIO_DISCONNECTED); // get the state, state disconnect is default.

            if(state == BluetoothHeadset.STATE_AUDIO_CONNECTED)
            {
                System.out.println("A BLUETOOTH DEVICE WAS CONNECTED!");
                // todo: add an option in settings to play music.
            }
            else if(state == BluetoothHeadset.STATE_AUDIO_DISCONNECTED)
            {
                System.out.println("A BLUETOOTH DEVICE WAS DISCONNECTED");
                if(MusicLibrary.isActivelyPlaying()) // if playing music pause.
                {
                    playerService.pause();
                }
            }
        }

        // now we can begin to listen to key events.

        final KeyEvent event = (KeyEvent) intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);

        if(event == null) {
            return;
        }

        if(event.getAction() != KeyEvent.ACTION_DOWN ) {return;}




        //System.out.println(event.getAction() + " " + event.getKeyCode() + " " + event.getCharacters());

        if(event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE)
        {
            System.out.println("PLAY/PAUSE WAS DETECTED!!!");
        }
        else if(event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PLAY)
        {
            System.out.println("Detected play");
            playerService.play();
            //flyupPanelListener.clickPlay();
        }
        else if(event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PAUSE)
        {
            System.out.println("detected pause");
            playerService.pause();
            //flyupPanelListener.clickPause();
        }
        else if(event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_NEXT)
        {
            System.out.println("next/skip was detected");
            playerService.skip();
            //flyupPanelListener.clickSkip();
        }
        else if(event.getKeyCode() == KeyEvent.KEYCODE_MEDIA_PREVIOUS) {
            System.out.println("previous was detected!");

            // if the song is less than 2 seconds in just go back to the last song.
            if (MusicLibrary.getCurrentSongTimePosition() < 2000) {
                prev();
            } else {
                replay();
            }
        }



    }

    @Override
    public void onServiceConnected(int profile, BluetoothProfile bluetoothProfile)
    {
        if(profile == BluetoothProfile.A2DP)
        {

            bluetoothA2dpService = (BluetoothA2dp) bluetoothProfile; // set the bluetoothA2dpService

            if(audioManager.isBluetoothA2dpOn() )
            {
                System.out.println("A SERVICE IS CONNECTED!");
                // TODO: play music!
                isConnected = true; // finally connected to a service.
                //Toast.makeText(, "CONNECTED TO DEVICE", Toast.LENGTH_SHORT).show();

            }
        }
        else if(profile == BluetoothProfile.HEADSET)
        {
            bluetoothHeadset = (BluetoothHeadset) bluetoothProfile; // set the bluetooth headset.
        }
    }

    @Override
    public void onServiceDisconnected(int profile)
    {
        System.out.println("A service was disconnected!");

        // pause music if music is actively playing, no matter what.
        if(MusicLibrary.isActivelyPlaying())
        {
            playerService.pause();
        }

        if(profile == BluetoothProfile.HEADSET)
        {
            bluetoothHeadsetAdapter.closeProfileProxy(profile, bluetoothHeadset);
        }
        else if(profile == BluetoothProfile.A2DP)
        {
            bluetoothAdapter.closeProfileProxy(profile, bluetoothA2dpService);
        }
        //playerService.pause(); // pause the music player when the bluetooth service is disconnected.

        isConnected = false; // set state to no longer be connected.
        //flyupPanelListener.clickPause(); // force the music player to pause automatically whenever a bluetooth device is connected.
    }
}
