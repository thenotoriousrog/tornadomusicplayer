package thenotoriousrog.tornadoplayer.BluetoothSupport;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaMetadata;
import android.media.MediaMetadataRetriever;
import android.media.session.MediaSession;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.widget.RemoteViews;

import thenotoriousrog.tornadoplayer.Activities.InitializerActivity;
import thenotoriousrog.tornadoplayer.Backend.BitmapWorkshop;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.NotificationManager.NotificationReceiver;
import thenotoriousrog.tornadoplayer.NotificationManager.WearActionReceiver;
import thenotoriousrog.tornadoplayer.R;
import thenotoriousrog.tornadoplayer.Services.PlayerService;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by thenotoriousrog on 3/17/18.
 * This allows for creating the media session as well as updating the notifications when a song changes.
 */

public class MediaSessionControlCenter extends MediaSessionCompat.Callback {

    private Context context;
    private MediaSessionCompat mediaSessionCompat;
    private MediaMetadataCompat metadata; // metadata here is important for ensuring that the notifications on android wear sees the correct information, the same for cars and stuff.
    private PlaybackStateCompat playbackStateCompat; // controls the playback state and allows for us to be able to work on everything
    //private FlyupPanelListener flyupPanelListener; // controls the music player programmatically.
    private PlaybackStateCompat.Builder stateBuilder;
    private RemoteViews notificationLayout; // the standard expanded notification
    private RemoteViews notificationLayoutExpanded; // the large notification when expanded.
    private NotificationManager notificationManager; // controls the building of the notifications.
    private NotificationCompat.Builder builder; // builder for the notification compat.
    private Handler uiHandler = new Handler(Looper.getMainLooper()); // creates a handler and listens for messages.

    private SongInfo currSong;
    private SongInfo nextSong;
    private Bitmap currAlbArt;

    // notification actions.
    private NotificationCompat.Action actionPause;
    private NotificationCompat.Action actionSkip;
    private NotificationCompat.Action actionPrev;
    private NotificationCompat.Action actionPlay;

    private int clickCounter = 0; // the click counter that's needed.
    private long clickDuration = 0; // the duration since the last click.
    private long firstClickTime = 0; // the system time since the very first click.
    private final int ONE_SECOND = 1000; // one thousand milliseconds = 1 second.

    private PlayerService playerService; // player service to handle actions from media session icons.

    // contructor that initiates the mediasession to do some work for the class
    public MediaSessionControlCenter(Context context, PlayerService playerService)
    {
        this.context = context;
        ComponentName componentName = new ComponentName(context.getPackageName(), MediaButtonIntentReceiver.class.getName());
        mediaSessionCompat = new MediaSessionCompat(context, "PlayerService", componentName, null);
        mediaSessionCompat.setCallback(this); // MediaSessionControlCenter is in of itself a callback.

        stateBuilder = new PlaybackStateCompat.Builder();

        playbackStateCompat = stateBuilder // builds the state in which we will be listening to for the media session.
                .setState(PlaybackStateCompat.STATE_PLAYING, 0, 1.0f, 0)
                .setActions(PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_PAUSE | PlaybackStateCompat.ACTION_SKIP_TO_NEXT | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)
                .build();

        initializeNotifications(); // initializes the notifications.
        mediaSessionCompat.setPlaybackState(playbackStateCompat); // sets the playback state for the
        mediaSessionCompat.setActive(false); // do not activate until the user has decided to play a song.

        this.playerService = playerService;
        startUIHandler();

    }

    public void startUIHandler()
    {
        System.out.println("is handler message null? " + uiHandler.obtainMessage());
    }

    public MediaSessionCompat getMediaSession()
    {
        return mediaSessionCompat;
    }

    // starts the media session.
    public void activateSession()
    {
        mediaSessionCompat.setActive(true);
        MusicLibrary.setActivelyPlayingStatus(true); // the service is now actively playing and has started.
    }

    // deactivates media session.
    public void deactivateSession()
    {
        mediaSessionCompat.setActive(false);
        MusicLibrary.setActivelyPlayingStatus(false); // we are no longer actively playing.
    }

    // releases the media session.
    public void releaseSession()
    {
        mediaSessionCompat.release();
    }

    public void cancelNotification()
    {
        notificationManager.cancelAll();
    }

    // updates the notifications with the information from the songs.
    private void updateNotifications(SongInfo currSong, SongInfo nextSong, Bitmap notifAlbumArt)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context); // gets the default shared preferences
        boolean upNextInNotifications = sharedPreferences.getBoolean("NotificationUpNext", true); // default value is true.

        // if the current song has no song name just leave the notifications immediately.
        if(currSong == null)
        {
            notificationManager.cancelAll();
            return;
        }

        this.currSong = currSong;
        this.nextSong = nextSong;
        this.currAlbArt = notifAlbumArt;

        // don't show up next in notifications.
        if(!upNextInNotifications)
        {
            builder.setContentTitle(currSong.getSongName())
                    .setContentText(currSong.getArtistName())
                    .setLargeIcon(notifAlbumArt)
                    .setSubText("\ud83c\udfb6" + "\ud83c\udf2A"); // music notes and a tornado
        }
        else // show up next in notifications.
        {
            builder.setContentTitle(currSong.getSongName())
                    .setContentText(currSong.getArtistName() + " - Up next: " + nextSong.getSongName())
                    .setLargeIcon(notifAlbumArt)
                    .setSubText("\ud83c\udfb6" + "\ud83c\udf2A"); // music notes and a tornado
        }





        System.out.println("We are displaying notifications now!!");
        notificationManager.notify(0001, builder.build());



    }

    // returns the notification.
    public Notification getNotification()
    {
        return builder.build();
    }

    // Need to start the notifications right in here
    private void initializeNotifications()
    {
        notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) // only in oreo is there notification controls to worry about.
        {

            String description = "Controls music through notifications";
            NotificationChannel channel = new NotificationChannel("Music Notification", "Music Notification", NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(description);
            channel.enableLights(false);
            channel.enableVibration(false);

            // Register the channel with the system
            notificationManager.createNotificationChannel(channel);
        }

        Intent openAppIntent = new Intent(context, InitializerActivity.class);
        openAppIntent.setAction("Main");
        openAppIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP ); // new flags.

        Intent deleteNotificationIntent = new Intent(context, PlayerService.class);
        deleteNotificationIntent.setAction("Stop"); // tells the app that we need to stop the notification.

        Intent playIntent = new Intent(context, PlayerService.class);
        playIntent.setAction("Play");
        playIntent.putExtra(NotificationReceiver.NOTIFICATION_ID_STRING, 0001);
        //playIntent.putExtra(NotificationReceiver.ACTIONS, NotificationReceiver.PLAY);

        Intent pauseIntent = new Intent(context, PlayerService.class);
        pauseIntent.setAction("Pause");
        pauseIntent.putExtra(NotificationReceiver.NOTIFICATION_ID_STRING, 0001);
        //pauseIntent.putExtra(NotificationReceiver.ACTIONS, NotificationReceiver.PAUSE);

        Intent skipIntent = new Intent(context, PlayerService.class);
        skipIntent.setAction("Skip");
        skipIntent.putExtra(NotificationReceiver.NOTIFICATION_ID_STRING, 0001);
        //skipIntent.putExtra(NotificationReceiver.ACTIONS, NotificationReceiver.SKIP);

        Intent prevIntent = new Intent(context, PlayerService.class);
        prevIntent.setAction("Prev");
        prevIntent.putExtra(NotificationReceiver.NOTIFICATION_ID_STRING, 0001);
        //prevIntent.putExtra(NotificationReceiver.ACTIONS, NotificationReceiver.PREV);

        PendingIntent pendingOpenAppIntent = PendingIntent.getActivity(context, 0, openAppIntent,0); // this intent controls how the app is opened.
        PendingIntent pendingDeleteIntent = PendingIntent.getService(context, 4545, deleteNotificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        PendingIntent pendingPauseIntent = PendingIntent.getService(context, 4545, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingSkipIntent = PendingIntent.getService(context, 4545, skipIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingPrevIntent = PendingIntent.getService(context, 4545, prevIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pendingPlayIntent = PendingIntent.getService(context, 4545, playIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        actionPause = new NotificationCompat.Action.Builder(R.drawable.pause, "pause", pendingPauseIntent).build();
        actionSkip = new NotificationCompat.Action.Builder(R.drawable.skip, "skip", pendingSkipIntent).build();
        actionPrev = new NotificationCompat.Action.Builder(R.drawable.prev, "prev", pendingPrevIntent).build();
        actionPlay = new NotificationCompat.Action.Builder(R.drawable.play, "play", pendingPlayIntent).build();

       // notificationLayout = new RemoteViews(context.getPackageName(), R.layout.notification_layout);
        //notificationLayoutExpanded = new RemoteViews(context.getPackageName(), R.layout.notification_layout_big);

         builder = new NotificationCompat.Builder(context, "Music Notification")
                        .setSmallIcon(R.drawable.app_icon_simple)
                        .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle().setShowActionsInCompactView(0,1,2).setMediaSession(mediaSessionCompat.getSessionToken()))
                        .setOnlyAlertOnce(true)
                        .addAction(actionPrev)
                        .addAction(actionPause)
                        .addAction(actionSkip)
                        .setOngoing(true)
                        .setContentIntent(pendingOpenAppIntent) // intent for opening the app again.
                        .setDeleteIntent(pendingDeleteIntent) // intent for when the notification is deleted.
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);


         // TODO: I need to decide if I want to leave the notification with letting android 8.0 decide the colors of the application itself. This makes the tornado guy disappear in a very cool way. Not sure yet.


        notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
    }

    // This method is called whenever there is a song change.
    public void updateMediaSession(SongInfo currSong, SongInfo nextSong, int songPos, Bitmap albumArt)
    {
        Bitmap wearAlbumArt = BitmapWorkshop.extractAlbumArtForAndroidWear(context, currSong);
       // albumArt = BitmapWorkshop.decodeSampledBitmapFromResource(context.getResources(), R.drawable.logoclipped, 200, 200);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context); // gets the default shared preferences
        boolean albumUpNext = sharedPreferences.getBoolean("AlbumUpNext", true); // default value is true.

        if(albumUpNext) // show up next instead album name
        {
            metadata = new MediaMetadataCompat.Builder()
                    .putString(MediaMetadata.METADATA_KEY_TITLE, currSong.getSongName())
                    .putString(MediaMetadata.METADATA_KEY_ARTIST, currSong.getArtistName())
                    .putString(MediaMetadata.METADATA_KEY_ALBUM, "Up Next: " + nextSong.getSongName()) // TODO: have an option to show album name instead of the Up Next: feature.
                    .putLong(MediaMetadata.METADATA_KEY_DURATION, Long.parseLong(currSong.getSongDuration()))
                    .putLong(MediaMetadata.METADATA_KEY_TRACK_NUMBER, songPos)
                    .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, wearAlbumArt)
                    .build();
        }
        else // show the album up next text
        {
            metadata = new MediaMetadataCompat.Builder()
                    .putString(MediaMetadata.METADATA_KEY_TITLE, currSong.getSongName())
                    .putString(MediaMetadata.METADATA_KEY_ARTIST, currSong.getArtistName())
                    .putString(MediaMetadata.METADATA_KEY_ALBUM, currSong.getAlbumName()) // shows the album name instead of up next.
                    .putLong(MediaMetadata.METADATA_KEY_DURATION, Long.parseLong(currSong.getSongDuration()))
                    .putLong(MediaMetadata.METADATA_KEY_TRACK_NUMBER, songPos)
                    .putBitmap(MediaMetadataCompat.METADATA_KEY_ALBUM_ART, wearAlbumArt)
                    .build();
        }



        mediaSessionCompat.setMetadata(metadata); // set the metadata for the song.
        updateNotifications(currSong, nextSong, albumArt); // update the notification.

    }

    // refreshes the notification display based on the state passed... mainly used for pause and play.
    public void refreshNotifications(final long state)
    {
        Intent deleteNotificationIntent = new Intent(context, PlayerService.class);
        deleteNotificationIntent.setAction("Stop"); // tells the app that we need to stop the notification.

        PendingIntent pendingDeleteIntent = PendingIntent.getService(context, 4545, deleteNotificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Intent openAppIntent = new Intent(context, InitializerActivity.class);
        openAppIntent.setAction("Main");
        openAppIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP ); // new flags.
        PendingIntent pendingOpenAppIntent = PendingIntent.getActivity(context, 0, openAppIntent,0); // this intent controls how the app is opened.

        if(state == PlaybackStateCompat.ACTION_PLAY) // refresh the notification to show the play button.
        {
            System.out.println("Setting the action to be play");

            builder = new NotificationCompat.Builder(context, "Music Notification")
                    .setSmallIcon(R.drawable.app_icon_simple)
                    .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle().setShowActionsInCompactView(0,1,2).setMediaSession(mediaSessionCompat.getSessionToken()))
                    .setOnlyAlertOnce(true)
                    .addAction(actionPrev)
                    .addAction(actionPause)
                    .addAction(actionSkip)
                    .setOngoing(true) // prevents the notification from being swiped away.
                    .setContentIntent(pendingOpenAppIntent) // opens the app when notification is clicked.
                    .setDeleteIntent(pendingDeleteIntent) // stops the music player in the background when the music player is clicked.
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

            // set a new playback state which sets the state to be paused on the mediasession.
            playbackStateCompat = stateBuilder // builds the state in which we will be listening to for the media session.
                    .setState(PlaybackStateCompat.STATE_PLAYING, 0, 1.0f, 0)
                    .setActions(PlaybackStateCompat.ACTION_PAUSE | PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_SKIP_TO_NEXT | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)
                    .build();

            mediaSessionCompat.setPlaybackState(playbackStateCompat); // set the updated playback state.
            updateNotifications(currSong, nextSong, currAlbArt); // update and display the notifications with the current information.

        }
        else if(state == playbackStateCompat.ACTION_PAUSE) // refresh the notification to show the pause button.
        {
            builder = new NotificationCompat.Builder(context, "Music Notification")
                    .setSmallIcon(R.drawable.app_icon_simple)
                    .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle().setShowActionsInCompactView(0,1,2).setMediaSession(mediaSessionCompat.getSessionToken()))
                    .setOnlyAlertOnce(true)
                    .addAction(actionPrev)
                    .addAction(actionPlay)
                    .addAction(actionSkip)
                    .setOngoing(false) // allows for the notification to be swiped away.
                    .setContentIntent(pendingOpenAppIntent) // opens the app when notification is clicked.
                    .setDeleteIntent(pendingDeleteIntent) // stops the music player in the background when the music player is clicked.
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

            // set a new playback state which sets the state to be paused on the mediasession.
            playbackStateCompat = stateBuilder // builds the state in which we will be listening to for the media session.
                    .setState(PlaybackStateCompat.STATE_PAUSED, 0, 1.0f, 0)
                    .setActions(PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_PAUSE | PlaybackStateCompat.ACTION_SKIP_TO_NEXT | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)
                    .build();

            mediaSessionCompat.setPlaybackState(playbackStateCompat); // set the updated playback state.
            updateNotifications(currSong, nextSong, currAlbArt); // update and display the notifications with the current information.
        }
        else
        {
            System.out.println("Some other state was called with state: " + state);
        }
    }

    // simply initiates pause on the media session.
    public void pauseMediaSession()
    {
        refreshNotifications(PlaybackStateCompat.ACTION_PAUSE);
    }

    public void playMediaSession()
    {
        refreshNotifications(PlaybackStateCompat.ACTION_PLAY); // forces the action to play.
    }

    public boolean isSessionActive()
    {
        if(mediaSessionCompat.isActive())
        {
            return true;
        }
        else {
            return false;
        }
    }

    // Behavior for when the play button is pressed.
    @Override
    public void onPlay()
    {
        System.out.println("PLAY WAS PRESSED IN THE MEDIA SESSION CALLBACK");
        playerService.play();
        refreshNotifications(PlaybackStateCompat.ACTION_PLAY);
        super.onPlay();
    }

    // Behavior for when the pause button is pressed
    @Override
    public void onPause()
    {
        System.out.println("PAUSE WAS PRESSED IN THE MEDIA SESSION CALLBACK");
        playerService.pause();
        refreshNotifications(PlaybackStateCompat.ACTION_PAUSE);
        super.onPause();
    }

    // Behavior for when the skip button is pressed.
    @Override
    public void onSkipToNext()
    {
        System.out.println("SKIP WAS PRESSED IN THE MEDIA SESSION CALLBACK");

        // TODO: need to setup the behavior for skip when we move the song behavior out of the flyup panel listener
        playerService.skip();

        // if the playback state is not play, then we need to set the action to be play to ensure that the notifications are correctly set.
        if(playbackStateCompat.getState() != PlaybackStateCompat.ACTION_PLAY)
        {
            refreshNotifications(PlaybackStateCompat.ACTION_PLAY);
        }

        super.onSkipToNext();
    }

    // Behavior for when the previous button is pressed.
    @Override
    public void onSkipToPrevious()
    {
        // if the song is less than 2 seconds in just go back to the last song.
        if(MusicLibrary.getCurrentSongTimePosition() < 2000)
        {
            playerService.prev();
        }
        else {
            playerService.replaySong();
        }

        // if the playback state is not play, then we need to set the action to be play to ensure that the notifications are correctly set.
        if(playbackStateCompat.getState() != PlaybackStateCompat.ACTION_PLAY)
        {
            refreshNotifications(PlaybackStateCompat.ACTION_PLAY);
        }

        super.onSkipToPrevious();
    }

    // todo: I need to somehow get this started so that I can stop the media session.
    @Override
    public void onStop()
    {
        System.out.println("The media session is STOPPING!");
        super.onStop();
    }

}
