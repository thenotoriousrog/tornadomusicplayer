package thenotoriousrog.tornadoplayer.BluetoothSupport;

import android.support.v4.media.session.MediaSessionCompat;

/**
 * Created by thenotoriousrog on 3/18/18.
 * This class is responsible for handling the callback functions from the media session.
 */

public class MediaSessionCallback extends MediaSessionCompat.Callback  {

    // Behavior for when the play button is pressed.
    @Override
    public void onPlay()
    {
        System.out.println("PLAY WAS PRESSED IN THE MEDIA SESSION CALLBACK");
        super.onPlay();
    }

    // Behavior for when the pause button is pressed
    @Override
    public void onPause()
    {
        System.out.println("PAUSE WAS PRESSED IN THE MEDIA SESSION CALLBACK");
        super.onPause();
    }

    // Behavior for when the skip button is pressed.
    @Override
    public void onSkipToNext()
    {
        System.out.println("SKIP WAS PRESSED IN THE MEDIA SESSION CALLBACK");
        super.onSkipToNext();
    }

    // Behavior for when the previous button is pressed.
    @Override
    public void onSkipToPrevious()
    {
        System.out.println("PREV WAS PRESSED IN THE MEDIA SESSION CALLBACK");
        super.onSkipToPrevious();
    }

}
