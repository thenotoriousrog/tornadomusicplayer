package thenotoriousrog.tornadoplayer.UI;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.view.View;
import android.widget.ImageView;

import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.R;

/**
 * Created by thenotoriousrog on 3/4/18.
 * This class controls the state and behavior of the flyupPanel that is used throughout the application.
 * The flyup panel and Bottom sheet behavior is created in the MainUIFragment and is maintained there.
 * This class ensures that only one is created whenever this is class is used.
 */

public class FlyupPanelController extends BottomSheetBehavior.BottomSheetCallback {

    private final View flyupPanel; // the flyup panel that we are using.
    private static BottomSheetBehavior flyupPanelBehavior; // allows us to read and maintain control the state of the behavior of the flyupPanel
    private FlyupPanelListener flyupPanelListener; // a copy of the flyup panel listener that we are using.
    private ImageView rotatingArrow; // holds the view for the rotating arrow.
    private ImageView albumArtHeader; // holds the album art in the peek view of the flyup panel.

    public FlyupPanelController(View flyupPanel, BottomSheetBehavior flyupPanelBehavior)
    {
        this.flyupPanel = flyupPanel;
        this.flyupPanelBehavior = flyupPanelBehavior;


        rotatingArrow = flyupPanel.findViewById(R.id.rotatingArrow); // the rotating arrow that we are working with.
        albumArtHeader = flyupPanel.findViewById(R.id.albumArtHeader); // the album art of the header itself to give the user's something to look at!
        flyupPanelBehavior.setBottomSheetCallback(this); // sets this to be the new call back listener that we need to have!
    }

    // set the listener for the panel listener.
    public void setFlyupPanelListener(FlyupPanelListener flyupPanelListener)
    {
        this.flyupPanelListener = flyupPanelListener;
    }

    public View getFlyupPanel() {
        return flyupPanel;
    }

    public BottomSheetBehavior getFlyupPanelBehavior() {
        return flyupPanelBehavior;
    }

    // forces the flyup panel to expand.
    public static void flyPanelUp()
    {
        flyupPanelBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    // hides the flyup panel.
    public static void flyPanelHiden()
    {
        flyupPanelBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    // forces the flyup panel to collapse
    public void flyPanelDown()
    {
        flyupPanelBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    // hides the flyup Panel.
    public void hideFlyupPanel()
    {
        flyupPanelBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    // shows the flyup panel, but it its collapsed form.
    public void showFlyUpPanel()
    {
        flyupPanelBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    // returns the state of the flyup panel.
    public int getFlyupPanelState()
    {
        return flyupPanelBehavior.getState();
    }

    // causes the panel to be updated.
    public void updatePanelHeader()
    {
        // if collapsed we can update the information otherwise don't update the information.
        if(flyupPanelBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED)
        {
            flyupPanelListener.setNowPlayingText(MusicLibrary.getCurrSong().getSongName()); // gets the song name and sets it when the flying panel is collapsed.
        }

        albumArtHeader.setImageBitmap(flyupPanelListener.getCurrAlbumArt()); // sets the current album art for the song that the user is listening too.
    }

    @Override
    public void onStateChanged(@NonNull View bottomSheet, int newState)
    {
        System.out.println("a new state was detected!!!");
        if(newState == BottomSheetBehavior.STATE_COLLAPSED) // if collapsed need to change the text to be now playing.
        {
            albumArtHeader.setVisibility(View.VISIBLE);
            flyupPanelListener.setNowPlayingText(MusicLibrary.getCurrSong().getSongName()); // gets the song name and sets it when the flying panel is collapsed.
            albumArtHeader.setImageBitmap(flyupPanelListener.getCurrAlbumArt()); // sets the current album art for the song that the user is listening too.
           // albumArtHeader.animate().alpha(1.0f); // make the album art fade back

            flyupPanelListener.refreshSliderLayout();

            int tabPos = flyupPanelListener.getMainUIFragment().getTabLayout().getSelectedTabPosition();

            // if we are on the playlists tab, open the playlist menu
            if(tabPos == 2)
            {
                flyupPanelListener.getMainUIFragment().getAddPlayListMenu().showMenuButton(true);
                //flyupPanelListener.getMainUIFragment().getAddPlayListMenu().open(true); // animate the floating action button.
            }


        }
        else if(newState == BottomSheetBehavior.STATE_EXPANDED)
        {
            flyupPanelListener.setNowPlayingText("Now Playing...");
            //albumArtHeader.setVisibility(View.INVISIBLE);
           // albumArtHeader.animate().alpha(0.0f); // make the album art fade away.

            int tabPos = flyupPanelListener.getMainUIFragment().getTabLayout().getSelectedTabPosition();

            if(tabPos == 2)
            {
                flyupPanelListener.getMainUIFragment().getAddPlayListMenu().hideMenuButton(true);

                //flyupPanelListener.getMainUIFragment().getAddPlayListMenu().close(true); // animate the floating action button.
            }

        }
    }

    // controls the behavior for the panel as it is sliding.
    @Override
    public void onSlide(@NonNull View bottomSheet, float slideOffset)
    {
        rotatingArrow.setRotation(slideOffset * 180);
        albumArtHeader.setAlpha(1.0f - slideOffset);
    }


    // TODO: I need to set a callback feature for intercepting swipe events on the sheet! Very important!

}
