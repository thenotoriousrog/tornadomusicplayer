package thenotoriousrog.tornadoplayer.Services;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.mikepenz.materialize.color.Material;

import java.io.IOException;
import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.Backend.BitmapWorkshop;
import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SerializeObject;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.BluetoothSupport.BluetoothControlCenter;
import thenotoriousrog.tornadoplayer.BluetoothSupport.MediaButtonIntentReceiver;
import thenotoriousrog.tornadoplayer.BluetoothSupport.MediaSessionControlCenter;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Handlers.PlayerServiceHandler;
import thenotoriousrog.tornadoplayer.Handlers.ServiceHandler;
import thenotoriousrog.tornadoplayer.Listeners.OnCompletionListener;

/**
 * Created by thenotoriousrog on 3/18/18.
 *
 * This is the service that runs in the background and allows the app to keep running even if the app is removed by Android OS.
 */

public class PlayerService extends Service {

    //private final IBinder mIBinder = new LocalBinder(); // this will allow us to set our handler.
    private Handler msgHandler = null; // handler to send messages to the SelectedSongPlayer.
    private Context context;
    private MainUIFragment mainUIFragment;

    private int clickCounter = 0; // the click counter that's needed.
    private long clickDuration = 0; // the duration since the last click.
    private long firstClickTime = 0; // the system time since the very first click.
    private final int ONE_SECOND = 1000; // one thousand milliseconds = 1 second.

    private static MediaPlayer mediaPlayer; // the media player used to control music.
    private MediaSessionControlCenter mediaSessionControlCenter; // the control center to control media actions
    private MediaPlayerControlCenter mediaPlayerControlCenter;
    private HandlerThread playerServiceHandlerThread; // the thread that handles messages through the handler in the background
    private Handler mainHandler; // the handler that will be speaking with the MainLooper as well as the MainUIThread for controlling certain actions.
    private PlayerServiceHandler playerServiceHandler; // the PlayerServiceHandler
    private boolean foregroundStarted = false; // tells the media session control center whether or not the notifications have been started in foreground or not yet.
    private static ServiceHandler serviceHandler; // service handler that communicates to and from the UI

    private IBinder serviceBinder = new ServiceBinder(); // an example of the service binder.
    private boolean speakWithUI = true; // tells the system whether or not we speak with the UI
    private Intent restartActivityIntent; // the intent that is passed into the binder when the initializer activity binds to this service.
    private BluetoothControlCenter bluetoothControlCenter; // controls app through bluetooth controls
    private MediaButtonIntentReceiver intentReceiver = new MediaButtonIntentReceiver();
    private boolean isBounded = true; // tells the system if there is an element bound to the service and if it is, then we must ensure that we do not restart the PlayerService.
    private boolean pausedByCall = false; // tells the system that a phone call has caused the pause. If this is the case we may be able to start playing music.

    private PhoneStateListener phoneStateListener; // the phone state listener used to listen for incoming phone calls.
    private int currentVolume; // holds the current volume of the device when the user is starting the music player.

    // this creates a one time use for the MusicLibrary within the app and allows the MusicLibrary to be used through out the app life cycle
    private void startMusicLibrary()
    {

        ArrayList<Playlist> playlists = new ArrayList<>();

        // READ the playlist data object from main memory if it exists.
        String ser1 = SerializeObject.ReadSettings(getBaseContext(), "playlists.dat"); // attempt to find this in main memory.
        if(ser1 != null && !ser1.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(ser1); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                playlists = (ArrayList<Playlist>) obj; // set our songInfo list.
            }
        }

        ArrayList<SongInfo> songInfosAlphabetized = new ArrayList<>(); // this is what is going to be passed to the Player Service.

        // READ SongInfo list from main memory.
        String ser = SerializeObject.ReadSettings(getBaseContext(), "SongInfoList.dat");
        if(ser != null && !ser.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(ser); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                songInfosAlphabetized = (ArrayList<SongInfo>) obj; // set our songInfo list.
            }
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        ArrayList<String> songPathsList = new ArrayList<String>(preferences.getStringSet("Songs", null));
        ArrayList<String> folders = new ArrayList<String>(preferences.getStringSet("Folders", null));

        MusicLibrary musicLibrary = new MusicLibrary(songPathsList, songInfosAlphabetized, folders, playlists); // creates our music library to be used throughout the app.
    }

    public static MediaPlayer getMediaPlayer()
    {
        return mediaPlayer;
    }

    // used only in dire circumstances. This is when the service is killed but we need to revive the media player.
    public static void setMediaPlayer(MediaPlayer player)
    {
        mediaPlayer = player;
    }

    // starts the bluetooth control center and prevents additional creations of the bluetooth control center
    private void registerBluetoothControlCenterBroadcast()
    {


        // TODO: I absolutely cannot have the bluetooth controls get started here, absolutely cannot not okay! I need my own class for this
        if(bluetoothControlCenter == null)
        {
            // sets the media button intent receiver to listen for certain connection requests.
            IntentFilter filter = new IntentFilter();
            filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
            filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
            filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
            // this.currentActivity.registerReceiver(intentReceiver, filter);


            bluetoothControlCenter = new BluetoothControlCenter(this); // create the bluetooth control center to control the songs
            // TODO: I cannot have the code be doing this inside of here. I need to restructure my code.
            IntentFilter mediaFilter = new IntentFilter();
            mediaFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
            mediaFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
            mediaFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
            mediaFilter.setPriority(2147483647);  //TODO: this is bad I need to change this immediately
            registerReceiver(bluetoothControlCenter, mediaFilter); // registers the receiver to listen for media button controls.
            //bluetoothControlCenter.notifySongChange(MusicLibrary.getCurrSong(), MusicLibrary.getCurrentSongNumber(), MusicLibrary.getCurrentSongList().size(), MusicLibrary.peekNextSong());
        }
        else { // the bluetooth connection is valid, just update the song through the bluetooth controls
           // bluetoothControlCenter.notifySongChange(MusicLibrary.getCurrSong(), MusicLibrary.getCurrentSongNumber(), MusicLibrary.getCurrentSongList().size(), MusicLibrary.peekNextSong());
        }
        // only start the bluetooth control center if it's null!

    }

    private void setOnCompletionListener()
    {
        mediaPlayer.setOnCompletionListener(null); // nulls the first one.
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer player) {
                try
                {
                    SongInfo newSong = MusicLibrary.getNextSong(); // grab the current song that is playing.

                    MusicLibrary.setCurrSong(newSong); // the current song is the new song now

                    if(isSpeakingWithUI()) // if speaking with the UI, send update to UI.
                    {
                        Message msg = new Message();
                        msg.arg1 = Constants.UI.UI_UPDATE_SLIDER_INFO;
                        Bundle updateInfo = new Bundle();
                        updateInfo.putString("CurrSongPath", newSong.getSongPath()); // in this case, the new song is the current song we want to play.
                        updateInfo.putString("NextSongPath", MusicLibrary.peekNextSong().getSongPath());
                        updateInfo.putBoolean("restartTimer", true); // timer needs to be restarted.
                        msg.setData(updateInfo); // set the update information
                        serviceHandler.sendMessage(msg);
                    }


                    if(mediaPlayer != null)
                    {
                        mediaPlayer.release();
                        mediaPlayer = null; // nullify the media player.
                        mediaPlayer = new MediaPlayer(); // create a new media player.
                        setOnCompletionListener();
                        setOnPreparedListener();
                        MusicLibrary.setMediaPlayer(mediaPlayer); // reset the new media player in music library.
                    }

                    mediaPlayer.setDataSource(newSong.getSongPath()); // pass in the song path for when this thing has finished correctly.
                    mediaPlayer.prepareAsync();

                    //CountdownTimer t = new CountdownTimer(nextSong.getSongDuration(), flyupPanelController.getFlyupPanel());

                    //flyupPanelListener.cancelAndFinishTimer(); // finish the current timer.
                    //flyupPanelListener.setTimer(t); // set the new timer.
                    //flyupPanelListener.startTimer(); // start the new timer.

                    //player.start(); // start playing the song.
                    updateNotification(); // update the notification once the song starts playing.

                    //flyupPanelListener.updateSliderData(newSong, nextSong);
                    //flyupPanelListener.refreshSliderLayout(); // force the app to refresh when the next song is playing.
                    //flyupPanelListener.reportSongChangeToMediaSession(); // report a song change to the media session when the song ends.
                }
                catch (IOException  ex)
                {
                    System.out.println("Something happened during the on completion listener");
                    ex.printStackTrace();
                }
                catch(IllegalStateException ex)
                {
                    System.out.println("Illegal state exception has occured!");
                    ex.printStackTrace();
                }
            }
        });
    }

    // sets the on prepared listener for the media player.
    private void setOnPreparedListener()
    {
        mediaPlayer.setOnPreparedListener(null); // nullify the old one first.
        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer player) {
                System.out.println("The media player is prepared and ready to work!");
                mediaPlayer.start();
            }
        });
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        System.out.println("INSIDE PLAYER SERVICE ONCREATE!");

        // TODO: I will need to make sure that I have this thing working and fixed
        final AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC); // set the state of the current volume for when the user is listening to music.
        audioManager.requestAudioFocus(new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange)
            {
                switch (focusChange)
                {
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                        currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                        int maxVol = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                        float percent = 0.40f; // 40% volume when ducking.
                        int fourtyPercent = (int) (maxVol * percent); // converts to 40% volume.
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, fourtyPercent, 0);
                        System.out.println("TEST1");
                        break;
                    case AudioManager.AUDIOFOCUS_GAIN:
                        System.out.println("TEST2");
                        if(MusicLibrary.isMusicPaused()) {
                            play();
                        }
                        else {
                            // raise the volume to normal levels.
                            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, currentVolume, 0); // set the current volume to be the volume of the system whenever ducking last occurred.
                        }
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS:
                        pause(); // pause the playback immediately.
                        System.out.println("TEST3");
                        break;
                    case AudioManager.AUDIOFOCUS_REQUEST_FAILED: // audio focus was not granted. The music player must not play music.
                        pause();
                        break;
                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                        pause(); // pause the music player, we lost audio focus.
                        break;

                    // nothing for the default cause.
                    default:
                        break;
                }
            }
        }, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        context = getApplicationContext();

        mediaPlayerControlCenter = new MediaPlayerControlCenter();
        mediaPlayer = new MediaPlayer(); // creates a new MediaPlayer

        startMusicLibrary(); // start the music library now.
        setOnCompletionListener(); // sets the oncompletion listener for the media player.
        setOnPreparedListener();

        // Note: we don't need to set a new media player in on create since a new one is created in

        MusicLibrary.setMediaPlayer(mediaPlayer);

        //startMusicLibrary(); // starts the music library to used throughout the whole application.
        //MusicLibrary.setMediaPlayer(mediaPlayer); // set the media player to be used for very limited actions in the application.
        mediaSessionControlCenter = new MediaSessionControlCenter(context, this);

        // TODO: I need to fix the unregistering of this bluetooth broadcast receiver. I'm getting a service connection leak when onDestroy is called and that cannot be allowed to happen!
        registerBluetoothControlCenterBroadcast(); // registers the bluetooth controls.

        playerServiceHandlerThread = new HandlerThread(Constants.HANDLER.PLAYER_SERVICE_HANDLER, Process.THREAD_PRIORITY_BACKGROUND); // set's the thread to be in the background.
        playerServiceHandlerThread.start(); // start the playerService Handler Thread

        playerServiceHandler = new PlayerServiceHandler(this, playerServiceHandlerThread.getLooper());

        serviceHandler = new ServiceHandler(getMainLooper());

        mainHandler = new Handler(Looper.getMainLooper()); // gets the main looper to communicate throughout the application.

        // initialize the phone state listener here.
        phoneStateListener = new PhoneStateListener()
        {
            // the actual state of the phone call. This will allow the music to stop playing when a phone call is started.
            @Override
            public void onCallStateChanged(int state, String incomingNumber)
            {
                System.out.println("CALL STATE CHANGED WITH STATE: " + state);
                // a call is coming. Pause music.
                if(state == TelephonyManager.CALL_STATE_RINGING) {

                    System.out.println("A PHONE CALL IS INCOMING");
                    pausedByCall = true; // tells the system that a pause by phone call is true.
                    pause(); // begin pausing the music.
                }
                else if(state == TelephonyManager.CALL_STATE_IDLE) { // no longer in a call. Begin playing music.
                    if(pausedByCall)
                    {
                        play(); // begin playing again.
                        pausedByCall = false; // reset the state of the system.
                    }
                }
                else if(state == TelephonyManager.CALL_STATE_OFFHOOK) { // call is no longer active or is
                    pausedByCall = true;
                    pause();
                }

                super.onCallStateChanged(state, incomingNumber);
            }
        };
        TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE); // get the telephony service to determine when we should stop playing music.
        if(manager != null)
        {
            manager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE); // start listening for the phone state listener.
        }

    }

    // sends updates to the BluetoothControlCenter
    public void updateBluetoothInfo()
    {
        if(bluetoothControlCenter != null && bluetoothControlCenter.isCurrentlyConnected()) // not null and currently connected to a device, send updates.
        {
            bluetoothControlCenter.notifySongChange(MusicLibrary.getCurrSong(), MusicLibrary.getCurrentSongNumber(), MusicLibrary.getCurrentSongList().size(), MusicLibrary.peekNextSong());
        }
    }

    // tells whether the player service is speaking with the UI or not. Primarily used by the OnCompletionListener
    public boolean isSpeakingWithUI()
    {
        return speakWithUI;
    }

    // sets alerts to whether or not we can speak with the UI
    public void setSpeakWithUIState(boolean state)
    {
        this.speakWithUI = state;
    }

    public void updateNotification()
    {
        Bitmap albumArt = BitmapWorkshop.extractAlbumArtForNotifications(getApplicationContext(), MusicLibrary.getCurrSong(), 200,200);
        mediaSessionControlCenter.updateMediaSession(MusicLibrary.getCurrSong(), MusicLibrary.peekNextSong(), MusicLibrary.getCurrentSongNumber(), albumArt);
    }

    // starts the foreground service.
    private void startForegroundNotifications()
    {
        if(!foregroundStarted) // only starts foreground notifications if they haven't been yet.
        {
            mediaSessionControlCenter.activateSession();
            foregroundStarted = true; // set the state to be true.
            startForeground(1, mediaSessionControlCenter.getNotification()); // start the foreground service.


            if(mediaPlayer != null)
            {
                mediaPlayer.setOnCompletionListener(new OnCompletionListener(mediaPlayer, serviceHandler, this)); // custom on completion listener started.
            }

        }
    }

    // replays the current song
    public void replaySong()
    {
        // media player must not be null in order to prevent exceptions.
        if(mediaPlayer != null)
        {
            mediaPlayer.pause();
            mediaPlayer.seekTo(0);
            mediaPlayer.start();
        }


        updateBluetoothInfo();

    }

    // plays a song
    public void playPrevSong(String songToPlay)
    {
        startForegroundNotifications(); // starts foreground notifications if they are not started yet.

        SongInfo playedSong = new SongInfo();
        playedSong.getandSetSongInfo(songToPlay);

        try
        {
            if(mediaPlayer != null)
            {
                // mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null; // nullify the media player.
                mediaPlayer = new MediaPlayer(); // create a new media player.
                setOnCompletionListener();
                setOnPreparedListener();
                MusicLibrary.setMediaPlayer(mediaPlayer); // reset the new media player in music library.
            }

            mediaPlayer.setDataSource(songToPlay);
            mediaPlayer.prepareAsync(); // TODO: may need to do prepare async instead.
            //mediaPlayer.start();
        }
        catch (IOException e)
        {
            System.out.println("Error occurred trying to play prev song within Player Service");
            e.printStackTrace();
        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }

        updateBluetoothInfo();
        updateNotification();
    }

    // plays a song
    public void playSong(String songToPlay)
    {
        startForegroundNotifications(); // starts foreground notifications if they are not started yet.

        SongInfo playedSong = new SongInfo();
        playedSong.getandSetSongInfo(songToPlay);

        MusicLibrary.addPlayedSong(playedSong); // add the played song to the list of songs that were played.

        try
        {

            if(mediaPlayer != null)
            {
               // mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null; // nullify the media player.
                mediaPlayer = new MediaPlayer(); // create a new media player.
                setOnCompletionListener();
                setOnPreparedListener();
                MusicLibrary.setMediaPlayer(mediaPlayer); // reset the new media player in music library.
                mediaPlayer.setDataSource(songToPlay);
                mediaPlayer.prepareAsync(); // TODO: may need to do prepare async instead.
            }

            //mediaPlayer.start();

        }
        catch (IOException e)
        {
            System.out.println("Error occurred trying to play song within Player Service");
            e.printStackTrace();
        }
        catch (IllegalStateException ex)
        {
            System.out.println("Illegal state was caught in play song in player service");
            ex.printStackTrace();
        } catch (NullPointerException ex)
        {
            ex.printStackTrace();
        }

        updateBluetoothInfo();
        updateNotification();
    }

    public void pause()
    {
        //mediaPlayer.pause();

        if(mediaPlayer != null)
        {
            mediaPlayer.pause();
        }

        mediaSessionControlCenter.pauseMediaSession(); // sets the media session to pause.
        stopForeground(false); //
        foregroundStarted = false; // we have temporarily stopped the foreground and thus we will have to reenable it


        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = Constants.UI.UI_PAUSE;
        serviceHandler.sendMessage(msg); // send message to tell the ui to display pause button.
       // mediaSessionControlCenter.
    }

    public void play()
    {
        // nothing can occur if the media player is null.
        if(mediaPlayer != null)
        {
            int timePausedAt = mediaPlayer.getCurrentPosition(); // gets the current position of the media player.
            mediaPlayer.seekTo(timePausedAt); // go to the time that was paused at.
            mediaPlayer.start(); // restart the song at the current position.
        }



        mediaSessionControlCenter.playMediaSession(); // sets the media session to play.
        startForegroundNotifications(); // this will re-instate the start foregroundedStarted boolean and reinstate the notification again.

        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = Constants.UI.UI_PLAY;
        serviceHandler.sendMessage(msg); // send message to tell the ui to display the play button
    }

    // skips to the next song.
    public void skip()
    {
        SongInfo currSong = MusicLibrary.getNextSong(); // the next song is the current song now.
        MusicLibrary.setCurrSong(currSong); // set the new current song.
        playSong(currSong.getSongPath()); // get the next song that is going to play.

        if(MusicLibrary.isShuffleOn())
        {
            int nextSongPosition = MusicLibrary.getCurrentSongNumber()+1; // grabs the current song number and adds 1 to it. This is needed to keep the current song number in the list accurate.
            MusicLibrary.setCurrentSongNumber(nextSongPosition); // set the updated song position.
        }


        //playSong(currSong.getSongPath()); // have the new current song be played.
       // MusicLibrary.addPlayedSong(currSong);
        mediaSessionControlCenter.playMediaSession(); // so matter what, play the mediasession control to show the pause button on the notifications.
        updateNotification();

        System.out.println("speaking with UI = " + speakWithUI);

        if(speakWithUI) // if true, we may speak with the UI
        {
            // send message to alert the UI to update its information.
            Message msg = serviceHandler.obtainMessage();
            msg.arg1 = Constants.UI.UI_UPDATE_SLIDER_INFO;
            Bundle updateInfo = new Bundle();
            updateInfo.putString("CurrSongPath", MusicLibrary.getCurrSong().getSongPath());
            updateInfo.putString("NextSongPath", MusicLibrary.peekNextSong().getSongPath());
            updateInfo.putBoolean("restartTimer", true); // timer needs to be restarted.
            msg.setData(updateInfo); // set the update information
            serviceHandler.sendMessage(msg);
        }

    }

    // FixMe: prev is currently broken for some reason. This needs to be fixed.
    public void prev()
    {
        // todo: this needs to be updated to take pressure away from the UI, very important.
        MusicLibrary.setCurrentSongNumber(MusicLibrary.getCurrentSongNumber() - 1);
        SongInfo prevSong = MusicLibrary.getPrevSong(); // get the previous song.

        if(prevSong == null) // if the previous song is null, there are no songs to return to previous too.. so replay song.
        {
            replaySong();
            return;
        }

        SongInfo currSong = MusicLibrary.getCurrSong(); // the prev song is the current song now.
        MusicLibrary.setCurrSong(prevSong); // the prev song is now the current song.
        playPrevSong(prevSong.getSongPath()); // get the next song that is going to play.
        mediaSessionControlCenter.playMediaSession();
        updateNotification();

        System.out.println("Prev song we are playing: " + prevSong.getSongName());

        if(speakWithUI) // speak with UI if true.
        {
            // send message to alert the UI to update its information.
            Message msg = serviceHandler.obtainMessage();
            msg.arg1 = Constants.UI.UI_UPDATE_SLIDER_INFO;
            Bundle updateInfo = new Bundle();
            updateInfo.putString("PrevSongPath", prevSong.getSongPath());
            updateInfo.putString("CurrSongPath", currSong.getSongPath());
            updateInfo.putString("NextSongPath", MusicLibrary.peekNextSong().getSongPath());
            updateInfo.putBoolean("restartTimer", true); // timer needs to be restarted.
            msg.setData(updateInfo); // set the update information
            serviceHandler.sendMessage(msg);
        }
    }

    // this completely stops everything.
    public void stop()
    {
        mediaSessionControlCenter.deactivateSession(); // deactivate the media session.

        if(mediaPlayer != null)
        {
            mediaPlayer.stop(); // need to kill the media player!
            mediaPlayer.release(); // release the media player to allow for the service to be killed!
            mediaPlayer = null;
        }

        // nulling the media player in the music library may be causing problems.
       // MusicLibrary.setMediaPlayer(null); // nullify the media player in the music library.
        stopSelf();

        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = Constants.UI.UI_STOP;
        serviceHandler.sendMessage(msg);
    }

    // starts shuffling the songs.
    // the current song really is the current song that's playing in this method.
    public void startShuffling()
    {
        MusicLibrary.shuffle(); // starts shuffling the songs.
        //MusicLibrary.resetPlayedSongs(); // reset the played queue.
        MusicLibrary.setShuffleState(true); // tells the music library that we are shuffling.

        MusicLibrary.setCurrentSongNumber(0); // Note: if there is a problem with numbering, we can remove this line. It is only for displaying on a car stereo.

        updateNotification();

        if(speakWithUI) // speak with the UI if true.
        {
            Message msg = serviceHandler.obtainMessage();
            msg.arg1 = Constants.UI.UI_UPDATE_SLIDER_INFO;
            Bundle updateInfo = new Bundle();
            updateInfo.putString("CurrSongPath", MusicLibrary.getCurrSong().getSongPath());
            updateInfo.putString("NextSongPath", MusicLibrary.peekNextSong().getSongPath());
            updateInfo.putBoolean("restartTimer", false); // timer does not need to be restarted
            msg.setData(updateInfo); // set the update information
            serviceHandler.sendMessage(msg);
        }

    }

    // stops shuffling the songs.
    public void stopShuffling()
    {
        MusicLibrary.resetQueue();
        MusicLibrary.resetPlayedSongs();
        MusicLibrary.setShuffleState(false);



        int currentSongNum = 0; // holds the exact position of the song currently playing.

        // get the current song and find the current song number of the song that is playing.
        for(int i = 0; i < MusicLibrary.getCurrentSongList().size(); i++)
        {
            if(MusicLibrary.getCurrSong().getSongName().equalsIgnoreCase(MusicLibrary.getCurrentSongList().get(i).getSongName())) // if the songs have the same name
            {
                currentSongNum = i; // grab the position of the song playing in the list.
            }
        }

        System.out.println("current song number after shuffle off = " + currentSongNum);
        MusicLibrary.setCurrentSongNumber(currentSongNum); // resets the current song number to ensure that songs are correctly playing.

        SongInfo nextSong = MusicLibrary.getNextSong(); // get the next song from the normal list.
        MusicLibrary.setCurrentSongNumber(MusicLibrary.getCurrentSongNumber() - 1); // decrement by once to ensure that the next song grabbed plays correctly.

        updateNotification();

        if(speakWithUI) // speak with the UI if true
        {
            Message msg = serviceHandler.obtainMessage();
            msg.arg1 = Constants.UI.UI_UPDATE_SLIDER_INFO;
            Bundle updateInfo = new Bundle();
            updateInfo.putString("CurrSongPath", MusicLibrary.getCurrSong().getSongPath());
            updateInfo.putString("NextSongPath", nextSong.getSongPath());
            updateInfo.putBoolean("restartTimer", false); // timer does not need to be restarted.
            msg.setData(updateInfo); // set the update information
            serviceHandler.sendMessage(msg);
        }
    }

    // starts repeating the current song.
    public void startRepeating()
    {
        mediaPlayer.setLooping(true);
    }

    // stops repeating the current song.
    public void stopRepeating()
    {
        mediaPlayer.setLooping(false);
    }

    // awknoledges that a new list is now present in the MusicLibrary. and reshuffles songs if need be.
    public void applyNewList()
    {
        // if shuffle is on we need to reshuffle the list, otherwise we do nothing.
        if(MusicLibrary.isShuffleOn()) {
            stopShuffling(); // clears the shuffle queue
            startRepeating(); // restarts shuffling.
        }
    }

    // this behavior will simply just call the song to play quickly
    public void searchSelected()
    {
        playSong(MusicLibrary.getCurrSong().getSongPath());

        updateNotification();
        updateBluetoothInfo();

       // if(speakWithUI) // if true, we may speak with the UI
        //{
            // send message to alert the UI to update its information.
            Message msg = serviceHandler.obtainMessage();
            msg.arg1 = Constants.UI.UI_UPDATE_SLIDER_INFO;
            Bundle updateInfo = new Bundle();
            updateInfo.putString("CurrSongPath", MusicLibrary.getCurrSong().getSongPath());
            updateInfo.putString("NextSongPath", MusicLibrary.peekNextSong().getSongPath());
            updateInfo.putBoolean("restartTimer", true); // timer needs to be restarted.
            updateInfo.putBoolean("flyPanelUp", true); // tells the system to fly the panel up.
            msg.setData(updateInfo); // set the update information
            serviceHandler.sendMessage(msg);
        //}
    }

    // plays the selected from search used in static so that the activity can force the update. This is ok since the UI will have to be up for the user to use search.
    public static void sendSearchSelectedMessage(SongInfo newSong, ArrayList<SongInfo> newList)
    {
        MusicLibrary.setCurrSong(newSong);
        MusicLibrary.setCurrentSongList(newList, newSong); // sets the current song.
        MusicLibrary.setCurrentSongNumber(0);

        if(MusicLibrary.isShuffleOn())
        {
            System.out.println("SHUFFLE IS ON");
            MusicLibrary.resetQueue();
            MusicLibrary.resetPlayedSongs();
            MusicLibrary.shuffle();
        }

        // need to send a message to the service handler that a random message was selected and the player service thus needs to be able to handle things accordingly.
        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = Constants.COMMAND.SEARCH_SELECTED;
        serviceHandler.sendMessage(msg);



    }

    // sends the replay message to be handled.
    private void sendReplayMessage()
    {
        Bundle b;

        Message msg = serviceHandler.obtainMessage();
        b = new Bundle();
        b.putString("msg", "Replay");
        msg.setData(b);
        //msgHandler.sendMessage(msg); // sending this message
        replaySong();
    }

    // sends the message that issues replay.
    private void sendPrevMessage()
    {

        Bundle b;

        Message msg = serviceHandler.obtainMessage();
        b = new Bundle();
        b.putString("msg", "Prev");
        msg.setData(b);
        prev();
        //msgHandler.sendMessage(msg); // sending this message
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        System.out.println("Inside on start command for Player Service!");

        String action = "";//intent.getAction();

        if(intent == null) // if the intent is null, something went wrong and we need to stop the music player asap.
        {
            speakWithUI = false; // activities are likely killed, stop speaking with the UI.

            // if music library is not actively playing we need to kill the service. Likely Android or the user has killed the app and the app is paused.
//            if(!MusicLibrary.isActivelyPlaying())
//            {
//                stop();
//            }

            System.out.println("startId = " + startId);
            System.out.println("Is intent null? " + intent);
            System.out.println("Is restart intent null? " + restartActivityIntent);
        }
        else // if not null, we can do something with the action.
        {
            action = intent.getAction();
        }


        Message msg;
        Bundle b;


        switch (action)
        {
            case "Pause":
                pause();
                break;

            case "Skip":
                skip();
                break;

            case "Prev":

                clickCounter++; // increment click counter.

                if(clickCounter == 1)
                {
                    System.out.println("SENDING REPLAY");
                    firstClickTime = System.currentTimeMillis(); // get the system time in milliseconds.
                    sendReplayMessage(); // tells the system to start a replay event.
                }
                else if(clickCounter == 2)
                {
                    System.out.println("SENDING PREV");
                    clickDuration = System.currentTimeMillis() - firstClickTime;
                    if(clickDuration <= ONE_SECOND)
                    {
                        sendPrevMessage(); // tells the system to go back to previous song.
                        clickDuration = 0;
                        clickCounter = 0;
                        // firstClickTime = 0; removed for now.
                    }
                    else
                    {
                        clickCounter = 1;
                        firstClickTime = System.currentTimeMillis();
                        sendReplayMessage(); // send a replay since the double tap didn't happen in the amount of time that was needed.
                    }
                }

                break;

            case "Play":
                //msg = new Message();
                //b = new Bundle();
                //b.putString("msg", "Play");
                //msg.setData(b);
                //msgHandler.sendMessage(msg); // sending this message
                play(); // have the service continuing playing.
                break;

                // todo: likely this case below can be removed from the system.
            case "Stop":
                System.out.println("Starting shutdown of Service now!");
                stop();
                break;
        }

        return START_NOT_STICKY; // starting not sticky removes the notification from reappearing when nothing exists which is super incredibly helpful.

    }

    // This method is called when a service is disconnected.
    @Override
    public boolean onUnbind(Intent intent)
    {
        System.out.println("Unbind was called, we need to stop speaking with the UI!");
        this.speakWithUI = false; // can no longer speak with the UI
        this.isBounded = false; // tell the system not too show the
        return false; // returning false means we do not want the onRebind method to be called.
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        // todo: we should determine if I should be interacting with the UI or not yet!
        System.out.println("Did we reach the binder?");
        this.speakWithUI = true; // activities are back we can speak with the UI.
        restartActivityIntent = intent; // holds this intent to start the activity.
        System.out.println("SERVICE WAS BOUNDED!! WE NEED TO SPEAK WITH THE UI NOW!");
        return serviceBinder;
    }

    // todo: this will likely be able to be removed!
    public void setMsgHandler(Handler handler)
    {
        System.out.println("did my message handler get set? " + handler);
        msgHandler = handler;
    }


    // Service Binder class to allow to make a call to the new binder.
    public class ServiceBinder extends Binder
    {
        public PlayerService getService()
        {
            return PlayerService.this; // retrieve the instance of this service.
        }
    }


    @Override
    public void onDestroy() {
       // Toast.makeText(context, "SERVICE DESTROYED", Toast.LENGTH_SHORT).show();
        System.out.println("Service has been destroyed!");
        unregisterReceiver(bluetoothControlCenter);
        stopSelf();
        mediaSessionControlCenter.cancelNotification();
        mediaSessionControlCenter.releaseSession(); // release the media session when PlayerService is destroyed.
        TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE); // get the telephony service to stop music.
        if(manager != null)
        {
            manager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE); // stop listening for the phone state listener.
        }

        phoneStateListener = null;

        super.onDestroy();
    }
}
