package thenotoriousrog.tornadoplayer.Services;

import android.media.MediaPlayer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Process;

/**todo: delete this class
 * Created by thenotoriousrog on 3/26/18.
 * This class is in charge of the media player. This involves starting, stopping and any other actions that need to take place.
 */

public class MediaPlayerControlCenter extends HandlerThread {

    private MediaPlayer mediaPlayer; // an instance of the media player
    private HandlerThread handlerThread; // the thread that handles messages through the handler in the background
    private Handler mainHandler; // the handler that will be speaking with the MainLooper as well as the MainUIThread for controlling certain actions.


    public MediaPlayerControlCenter()
    {
        super("MusicServiceHandler", Process.THREAD_PRIORITY_BACKGROUND);
        this.start(); // start the MediaPlayerControlCenter thread.

        //handlerThread = new HandlerThread("MusicPlayerHandler", Process.THREAD_PRIORITY_BACKGROUND); // set's the thread to be in the background.
        //handlerThread.start(); // start the thread

        mainHandler = new Handler(Looper.getMainLooper()); // gets the main looper to communicate throughout the application.
    }

}
