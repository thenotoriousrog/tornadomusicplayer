package thenotoriousrog.tornadoplayer.Backend;

/**
 * Created by thenotoriousrog on 6/12/17.
 * Originally from the same website as the code that is used in NotificationService.java. Modified to fit my app.
 */


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import thenotoriousrog.tornadoplayer.R;

public class Constants
{
    // commands to speak to the differerent handlers
    public interface COMMAND
    {
        public static int PLAY_SELECTED = 0; // tells the player service to play a song that was was selected, likely a song path is passed in.
        public static int PLAY = 1; // tells the player service to play a song, passed in through a method call.
        public static int PAUSE = 2; // tells the player service to pause the song.
        public static int REPLAY = 3; // tells the player service to replay the current song that's playing.
        public static int PREV = 4; // tells the player to go back one song.
        public static int SKIP = 5; // tells the player service to initiate a skip and update the Music Library.
        public static int SHUFFLE_ON = 6; // tells the player to start shuffling.
        public static int SHUFFLE_OFF = 7; // tells the player to stop shuffling.
        public static int REPEAT_ON = 8; // tells the player to start repeating.
        public static int REPEAT_OFF = 9; // tells the player to stop repeating.
        public static int NEW_LIST = 10; // tells the player that the user has selected a song from a new list.
        public static int SEARCH_SELECTED = 11; // tells the player that the user has selected a song from the search list.
    }

    // commands that are carried out by the UI
    public interface UI
    {
        public static int UI_PLAY = 1000; // tells the app that the Play button was pressed on the UI
        public static int UI_PAUSE = 2000; // tells the app that the pause button was pressed on the UI
        public static int UI_UPDATE_SLIDER_INFO = 3000; // tells the app that the slider UI needs to be updated.
        public static int UI_SHUFFLE_OFF = 4000; // tells the app that the user has turned shuffle off.
        public static int UI_SHUFFLE_ON = 5000; // tells the app that the user has turned shuffle on.
        public static int UI_STOP = 6000; // this command issues all background threads to be killed to allow service to fully stop.
    }

    // actions for controlling the music player.
    public interface ACTION
    {

        public static String PREV = "PREV";
        public static String REPLAY = "REPLAY";
        public static String PLAY = "PLAY";
        public static String SKIP = "SKIP";
        public static String STARTFOREGROUND_ACTION = "START_FOREGROUND_ACTION";
        public static String STOPFOREGROUND_ACTION = "STOP_FOREGROUND_ACTION";
        public static String PLAY_SONG = "PLAY_SONG";
        public static String SEARCH_SELECTED = "SEARCH_SELECTED";
    }

    // handlers to know what handler is listening to where.
    public interface HANDLER
    {
        public static String MUSIC_PLAYER_HANDLER = "MusicPlayerHandler"; // tells a handler what handler we are communicating with the MusicPlayerHandler
        public static String PLAYER_SERVICE_HANDLER = "PlayerServiceHandler"; // tells a handler that we are communicating with the player service handler
        public static String UI_HANDLER = "UIHandler"; // holds onto the handler that speaks with the remainder of the UI Handler.
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }

    public interface INTENT
    {
        public static int PICK_FILE = 1111; // tells the system that the intent we want is to grab a file, likely to import a playlist.
        public static int GAIN_STORAGE_ACCESS = 2222; // this allows the user to be able to handle to make changes to their strage framework. Very important.
    }

    public static Bitmap getDefaultAlbumArt(Context context)
    {
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        try {
            bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher, options); // todo: change this to be one of the logo's that I have. Not the gif if we can help it.
        } catch (Error ee) {
            // do nothing for this error.
            System.out.println("Constants.java received this error: " + ee.getMessage());
        } catch (Exception e) {
            System.out.println("Constants.java received exception: " + e.getMessage());
        }
        return bm;
    }
}
