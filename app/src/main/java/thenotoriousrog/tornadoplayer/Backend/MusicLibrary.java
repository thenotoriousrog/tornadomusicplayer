package thenotoriousrog.tornadoplayer.Backend;

import android.media.MediaPlayer;
import android.os.Looper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import java.util.logging.Handler;

import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Services.PlayerService;

/**
 * Created by thenotoriousrog on 3/30/18.
 *
 * This class is in charge of maintaining access to all of the songs, folders, and playlists throughout the entirety of the application. This is crucial because all parts of the app should be able to have access
 * to songs as well as the folders
 */

public class MusicLibrary {

    private static ArrayList<String> songPaths;
    private static ArrayList<SongInfo> songs;
    private static ArrayList<String> folders;
    private static ArrayList<Playlist> playlists;
    private static ShuffleQueue shuffleQueue; // the shuffle queue that will maintain the current items that we are watching over.
    private static ArrayList<SongInfo> currentSongList; // the list that the user is currently listening too.
    private static boolean shuffleState = false; // tells the library if we are shuffling or not.
    private static int currentSongNumber = 0; // the current song number we are in in the application.
    private static SongInfo currSong; // the current song that the user is listening to.
    private static Stack<SongInfo> playedSongs = new Stack<>(); // this stack holds the items in the list to allow for LIFO operation
    private static boolean activelyPlaying = false; // if actively playing, that means the media session is active and thus, the service is running, UI updates should be taking place.
    private static MediaPlayer mediaPlayer; // this is a copy of the media player. This must only be used to determine the time of a currently playing song and nothing else.
    private static SongInfo prevPeek; // this is a copy of the song to peek whenever previous was selected. It only holds one copy and is always rendered null.
    private static boolean actionWasPrev = false; // when peeking the next song, if the user presses prev, we need this boolean to help determine if we should peek the songs out of the played songs list.
    private static int prevCounter = 0; // this counter should tell us how many times the user has pressed the prev button.
    private static int tempCurrTime = 0; // holds the temp current time of the song.

    public MusicLibrary(ArrayList<String> songPaths, ArrayList<SongInfo> songs, ArrayList<String> folders, ArrayList<Playlist> playlists)
    {
        this.songPaths = songPaths;
        this.songs = songs;
        this.folders = folders;
        this.playlists = playlists;

        currentSongList = songs; // Note: On startup the very first list is that of all songs, until the user selects a new song list!
    }

    public static void setMediaPlayer(MediaPlayer player)
    {
        mediaPlayer = null; // nullify the mediaplayer first.
        mediaPlayer = player; // reset the media player.
    }

    // Note: The only time we are allowed to statically set the media player is in this method where the one case where the user swipes the notification away kills the player. This revives it and gives it to player service.
    private static void reviveMediaPlayer()
    {
        try
        {
            if(mediaPlayer != null) // if media player is not null then we need to refresh it.
            {
                mediaPlayer.release();
                mediaPlayer = null;
                mediaPlayer = new MediaPlayer();
            }
            else // media player is null, thus we need to reset the media player.
            {
                mediaPlayer = new MediaPlayer();
            }

            mediaPlayer.setDataSource(currSong.getSongPath());
            mediaPlayer.prepare();
            mediaPlayer.start();

            PlayerService.setMediaPlayer(mediaPlayer); // this is the only circumstance that this is ok. The service needs a jump start with the media player used to play music again.
        } catch (IOException e)
        {
            e.printStackTrace();

        } catch (IllegalStateException ex) { // this is likely to occur when we select a song before the player service truly is ready. Likely on startup of the app.
            ex.printStackTrace();
        }
    }

    // This method will allow for the media player to seek to a certain position.
    public static void seekMediaPlayerTo(int progress) {
        mediaPlayer.seekTo(progress);
        tempCurrTime = progress; // set the temp curr time for the user for where they have seeked to
    }

    // gets the duration of the media according to the media player.
    public static int getMediaDuration()
    {
        try
        {
            // potential random crash fix.
            if(mediaPlayer == null) {
                return 0;
            }

            return mediaPlayer.getDuration();
        }
        catch (IllegalStateException ex) {
            System.out.println("Caught illegal state when getting media duration in MusicLibrary");
            ex.printStackTrace();
        }
        catch (NullPointerException ex) { // this is likely because the media session was killed in the background. Catching this should greatly reduce system crashes.
            ex.printStackTrace();
            reviveMediaPlayer();
        }

        return 0; // this obviously cannot happen.
    }

    public static boolean isMusicPaused()
    {
        try {
            if(mediaPlayer.isPlaying())
            {
                return false;
            }
            else {
                return true;
            }
        } catch (IllegalStateException ex)
        {
            System.out.println("caught an illegal state exception while checking if paused in Music library");
            ex.printStackTrace();
        }
        catch (NullPointerException ex) { // this is likely because the media session was killed in the background. Catching this should greatly reduce system crashes.
            System.out.println("The media player was killed, we need to refresh the media player so that we can listen to music in the background");
            //reviveMediaPlayer();
            ex.printStackTrace();
        }

        return true; // if here then no media is playing so we must assume it's true.

    }

    // returns a song's current time if the media player isn't null. If however, the time is null, then we must return -1 to signal an error.
    public static long getCurrentSongTimePosition()
    {
        try
        {
            if(mediaPlayer != null && mediaPlayer.isPlaying())
            {
                tempCurrTime = mediaPlayer.getCurrentPosition();
                return mediaPlayer.getCurrentPosition();
            }

        }
        catch (IllegalStateException ex)
        {
            System.out.println("ILLEGAL STATE EXCEPTION WHEN GRABBING CURRENT POSITION!");
            //reviveMediaPlayer();
            // todo: note: the reviving the media player every time an illegalstateexception occurs causes the phantom music player to play music even when the notification is killed. This is not good.
            if(PlayerService.getMediaPlayer() == null)
            {
               // reviveMediaPlayer();
            }
            ex.printStackTrace();
        }
        catch (NullPointerException ex){
            reviveMediaPlayer();
            ex.printStackTrace();
        }

        return tempCurrTime; // returning -1 is an error that we shouldn't display any song time at all perhaps.
    }

    public static void setActivelyPlayingStatus(boolean status)
    {
        activelyPlaying = status;
    }

    public static boolean isActivelyPlaying()
    {
        return activelyPlaying;
    }

    public static boolean isMediaPlayerNull()
    {
        if(mediaPlayer == null)
        {
            return true;
        }
        else return false;
    }

    public static ArrayList<String> getAllSongPaths()
    {
        return songPaths;
    }

    public static ArrayList<SongInfo> getAllSongs()
    {
        return songs;
    }

    public static ArrayList<String> getFolders()
    {
        return folders;
    }

    // sets the new playlists.
    public static void setPlaylists(ArrayList<Playlist> newPlaylists)
    {
        playlists = newPlaylists;
    }

    public static ArrayList<Playlist> getPlaylists()
    {
        return playlists;
    }

    // returns the current song that the user is listening to.
    public static SongInfo getCurrSong()
    {
        return currSong;
    }

    // sets the new curr song.
    public static void setCurrSong(SongInfo newCurrSong)
    {
        currSong = newCurrSong;
        tempCurrTime = 0;
    }

    // sets the current song list being used within the app at the moment.
    // Note: when setting the current song list we need to look for a media player being dead and if so, we need to revive the music player!
    public static void setCurrentSongList(ArrayList<SongInfo> newCurrentSongList, SongInfo currentSong)
    {
        // if the player service is null, this is likely because the user or the android OS has killed the service. Thus, we need to revive it because the user has selected a song to play.
        if(PlayerService.getMediaPlayer() == null)
        {
            reviveMediaPlayer();
        }
        currentSongList = newCurrentSongList; // the list of songs that we are currently listening to.
        currSong = currentSong;
        //RelativeLayout draggedView = (RelativeLayout) flyupPanel.findViewById(R.id.draggedView);
        //draggedView.setVisibility(View.VISIBLE);
        prevCounter = 0; // settings a new list resets all previous songs.
        resetPlayedSongs(); // reset the played songs to be zero as well.
        playedSongs.push(currentSong); // a selected song is automatically added into the list of played songs.

        //This is causing the fly up panel to act all choppy instead of flying up smoothely. Thus, we have to do this only after the list
        if(shuffleState == true) // user has shuffle still turned on.
        {
            resetQueue(); // clear out the old queue.
            shuffle(); // restart the shuffling queue.

            // after redoing the shuffle queue update the data that is being displayed to correctly show the song that is upnext.
            //updateSliderData(currSong, shuffleQueue.peekNxtSong()); // keep the same song information, but simply peek the next song that is going to be played.
            //refreshSliderLayout(); // force the slider information to update.
        }
        else {} // do nothing, shuffle is not on proceed as normal.
    }


    public static ArrayList<SongInfo> getCurrentSongList()
    {
        return currentSongList;
    }

    public static boolean isShuffleOn()
    {
        return shuffleState;
    }

    public static void setShuffleState(boolean state)
    {
        shuffleState = state;
    }

    // shuffles our songs.
    public static void shuffle()
    {
        shuffleQueue = new ShuffleQueue(currentSongList);
    }

    // gets the current song number
    public static int getCurrentSongNumber()
    {
        return currentSongNumber;
    }

    public static void setCurrentSongNumber(int num)
    {
        currentSongNumber = num;
    }

    // this will literally return the next song in the list or queue if shuffle is on.
    public static SongInfo getNextSong()
    {


        System.out.println("GETTING NEXT SONG IN FLYUPPANELLISTENER!");
        if(shuffleState == false) // shuffle is not active
        {

            // check to see if the song is at the end of the list or not.
            if(currentSongNumber == currentSongList.size()-1) // used to be plus 1, but something isn't working right for whatever reason.
            {
                System.out.println("song paths size = " + songPaths.size());
                System.out.println("Grabbing the first song in the list"); // for testing.
                currentSongNumber = 0;

                return currentSongList.get(currentSongNumber);
            }
            else // we are not at the end of the list.
            {
                currentSongNumber++; // increment the current song number.
                System.out.println("song paths size = " + songPaths.size());
                System.out.println("Current song number in music player = " + currentSongNumber);
                return currentSongList.get(currentSongNumber); // grab the next song in the list.
            }
        }
        else // shuffle is active, ensure that prev is handle appropriately.
        {
            return shuffleQueue.popNextSong(); // get the next song in the queue.
        }
    }

    public static SongInfo peekNextSong()
    {
        if(shuffleState == false) // shuffle is not active.
        {
            System.out.println("IS CURRENT SONG LIST NULL?" + currentSongList);


            if(currentSongNumber == currentSongList.size()-1)
            {
                return currentSongList.get(0); // grab the very first item in the list.
            }
            else
            {
                return currentSongList.get(currentSongNumber + 1); // return the next song in the list without actually changing the song number.
            }

        }
        else // shuffle is active, need to ensure that for x amount of previous entries selected that x are popped from the played songs list before proceeding into the rest of the list.
        {
            if(prevCounter > 0 && !playedSongs.isEmpty()) // if prev counter is greater than 0 than we need to ensure that we are returning the correct song to ensure the correct thing is shown to the user.
            {
                System.out.println("SHOWING PREV PEEK INSTEAD OF NORMAL PEEK! With song name: " + playedSongs.peek().getSongName());
                //return prevPeek();

                System.out.println("The current song playing is: " + currSong.getSongName());
                // if the curr song and next song are the same, we need to pop it out of the queue.
                if(currSong.getSongName().equals(shuffleQueue.peekNxtSong().getSongName()))
                {
                    shuffleQueue.popNextSong();
                }

                return shuffleQueue.peekNxtSong();
                //return playedSongs.peek();
            }

            SongInfo nextSong = shuffleQueue.peekNxtSong();

            // if next song is null, then we have reached the end of the list. We need to reshuffle and clear the lists and display the newly peeked song.
            if(nextSong == null)
            {
                // reset the shuffle queue behavior for the app.
                resetPlayedSongs();
                resetQueue();
                addPlayedSong(currSong); // the current song playing must be added to the current song list.
                shuffle();

            }

            return shuffleQueue.peekNxtSong(); // simply look at the next song without actually changing anything.
        }
    }

    // returns true if the song was played.
    public static boolean isPlayed(SongInfo songToCheck)
    {
        for(int i = 0; i < playedSongs.size(); i++)
        {
            SongInfo song = playedSongs.get(i);

            // only if the song name and artist name are the same can we determine that a song was played.
            if(song.getSongName().equals(songToCheck.getArtistName()) &&
                    song.getArtistName().equals(songToCheck.getArtistName()))
            {
                return true;
            }
        }

        return false;
    }

    public static void resetPlayedSongs()
    {
        prevCounter = 0;
        playedSongs.clear();
    }

    // adds a song to the played song list.
    public static void addPlayedSong(SongInfo playedSong)
    {

        if(isShuffleOn())
        {
            if(!isPlayed(playedSong)) // if this song is not in the played songs currently, then we need to add it otherwise skip so we don't have duplicates.
            {
                System.out.println("Adding to played songs!");

                //playedSongs.add(playedSong);
                playedSongs.push(playedSong); // push the played song into the list of songs.
                System.out.println("song after pushing to the stack: " + playedSongs.peek().getSongName());
            }

            System.out.println("Printing all songs in the played songs stack...");
            for(int i = 0; i < playedSongs.size(); i++)
            {
                System.out.println("Song " + i + " = " + playedSongs.get(i).getSongName());
            }
            System.out.println();
            System.out.println();
        }

    }

    public static boolean isPlayedSongsEmpty()
    {
        return playedSongs.isEmpty();
    }

    // this is called when we call previous generally when not in shuffle.
    public static SongInfo prevPeek()
    {
        return playedSongs.peek();
    }

    // this method removes the top item from the queue of played songs.
    public static SongInfo getPrevSong()
    {
        //return currentSongList.get(currentSongNumber);

        if(!isShuffleOn())
        {
            if(currSong.getSongName().equals(currentSongList.get(0).getSongName()) &&
                    currSong.getArtistName().equals(currentSongList.get(0).getArtistName()))
            {
                currentSongNumber = currentSongList.size()-1; // set the current song number to be of the very last song.
                return currentSongList.get(currentSongList.size()-1);
            }

            else
            {
                return currentSongList.get(currentSongNumber);
            }
        }
        else // shuffle is on and thus previous needs to be handled accordingly.
        {
            if(playedSongs.isEmpty())
            {
                return null;
            }

            if(prevCounter == 0) // previous is being started from the start thus we need to skip the very first song.
            {
                playedSongs.pop(); // an initial extra pop is needed to ensure that the previous song is selected correctly.
            }

            // fixme: the peeking problem lies here

            System.out.println("The song peeked in the played songs queue: " + playedSongs.peek().getSongName());
            //shuffleQueue.addToStack(playedSongs.peek());

            shuffleQueue.addToStack(currSong); // add the current song that's playing to the played songs stack.

            prevCounter++; // count the number of times that the user is hitting previous.
            return playedSongs.pop();
        }

        //return null;



//        prevCounter++; // increment the prev counter.
//        System.out.println("number of previous songs is: " + playedSongs.size());
//        if(playedSongs == null || playedSongs.isEmpty() || playedSongs.size() <= 1)
//        {
//            return currSong; // simply replay the current song if the played song are null
//        }
//        else
//        {
//            //SongInfo prevSong = playedSongs.get(playedSongs.size()-2); // pop the last item added in the stack.
//
//            if(isShuffleOn())
//            {
//                //SongInfo toPeek = playedSongs.get(playedSongs.size()-1);
//                //shuffleQueue.addToStack(toPeek); // push the previous song back to the song stack.
//                shuffleQueue.addToStack(playedSongs.peek()); // add the song to stack again, by peeking so we don't remove it from the played songs stack.
//            }
//
//            //playedSongs.remove(playedSongs.size()-2);
//
//
//
//            //return prevSong;
//            //playedSongs.pop();
//            return playedSongs.pop();
//        }
    }

    public static void resetQueue()
    {
        shuffleQueue.clear();
        //prevCounter = 0; // nothing to go previous to
    }
}
