package thenotoriousrog.tornadoplayer.Backend;



import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/*
    This class is responsible for two things: 1) holding the name of an artist and 2.) all of the songs associated with that artist.
    The class is parcelable and Serializable to allow for some things to be able to write the data to main memory.
 */
public class Artist implements Parcelable, Serializable {

    private String name = ""; // name of the artist name
   // private ArrayList<SongInfo> songs = new ArrayList<>(); // holds all of the songs by that artist.
    private ArrayList<SongInfo> songPaths = new ArrayList<>(); // holds all of the songs by this artist in their song path form

    // this is the contructor called when made by the user i.e. dev.
    public Artist(String name) {
        this.name = name;
    }

    // this is the constructor called when the list is made from memory.
    private Artist(Parcel in) {
        this.name = in.readString(); // read in the name of the artist.

        in.readList(songPaths, null); // reads the lists and stores the results inside the songs list. Very important

//        // go through each of the song paths and reset the list and do it again.
//        for(int i = 0; i < songPaths.size(); i++)
//        {
//            SongInfo song = new SongInfo();
//            song.getandSetSongInfo(songPaths.get(i));
//
//            songs.add(song); // add the song into the song path list. Very important!
//        }
    }

    private boolean exists(SongInfo song)
    {
        for(int i = 0; i < songPaths.size(); i++)
        {
            // check to see if the song matches.
            if(song.getSongPath().equals(songPaths.get(i).getSongPath()))
            {
                return true; // match exists return true.
            }
        }

        return false; // if reached here, we did not find a song and thus does not exist.
    }

    // adds a song into the list of songs for this artist. Automatically will not add duplicates.
    public void addSong(SongInfo song)
    {
        if(!exists(song)) // check to see if the song exists in the list or not.
        {
            //System.out.println("is song null? " + song);

            if(song.getArtistName() == null)
            {
                return;
            }

            // only add the song if the artist name matches the name of the artist.
            if(song != null && song.getArtistName().equals(name) && name.length() > 2 )
            {
                //songs.add(song);
                songPaths.add(song);
            }

        }
    }

    // returns the number of songs for this user.
    public int numOfSongs()
    {
        return songPaths.size();
    }

    public ArrayList<SongInfo> getSongs()
    {
        return songPaths;
    }

    public String getArtistName()
    {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(name); // writes the name of the artist
        parcel.writeList(songPaths); // writes the songs in the list.
        //parcel.writeTypedList(songs); // writes the songs in the list.
    }

    public static final Creator<Artist> CREATOR = new Creator<Artist>() {
        @Override
        public Artist createFromParcel(Parcel parcel) {
            return new Artist(parcel);
        }

        @Override
        public Artist[] newArray(int i) {
            return new Artist[i];
        }
    };
}
