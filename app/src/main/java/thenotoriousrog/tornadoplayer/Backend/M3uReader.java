package thenotoriousrog.tornadoplayer.Backend;

import android.content.Context;
import android.view.View;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;

/*
    This class creates and saves the playlist based on the data read from the m3u file.
 */
public class M3uReader {

    private MainUIFragment mainUIFragment;
    private File m3uFile; // path of the m3u file.

    public M3uReader(MainUIFragment mainUIFragment, File m3uFile)
    {
        this.mainUIFragment = mainUIFragment;
        this.m3uFile = m3uFile;
    }

    private boolean isValidM3U()
    {
        String path = m3uFile.getAbsolutePath();
        String extension = path.substring(path.length() - 3, path.length()); // should get the extension of the file.
        System.out.println("m3u extention is: " +  extension);
        if(!extension.equalsIgnoreCase("m3u"))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // extracts the name of the playlist.
    private String getPlaylistName()
    {
        String path = m3uFile.getAbsolutePath();
        String[] temp = path.split("/"); // split based on /'s

        String lastIndex = temp[temp.length-1];
        String name = lastIndex.replace(".m3u", ""); // remove the .m3u

        return name;
    }

    // starts reading and creating the playlist.
    public Playlist createPlaylist()
    {
        if(!isValidM3U()) { // returning null will allow us to tell the user that we could not create the playlist.
            System.out.println("M3U is not valid!!!!");
            return null;
        }

        ArrayList<SongInfo> songs = new ArrayList<>(); // the songs in the playlist.
        String playlistName = getPlaylistName(); // gets the name of the playlist.

        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(m3uFile));
            String line;
            while( (line = reader.readLine()) != null ) // loop through all lines adding songs as we go!
            {
                if(line.charAt(0) == '/') // this char means that we have found the path of the song in the playlist.
                {
                    SongInfo song = new SongInfo();
                    song.getandSetSongInfo(line); // parse the line to convert into a new SongInfo

                    songs.add(song); // add the song to the list of songs.
                }
            }

            Playlist importedPlaylist = new Playlist(playlistName, songs); // create the new playlist.
            return importedPlaylist; // send playlist back to the main ui fragment.

        }catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return null; // tell the system that the playlist could not be created.
        }catch (IOException ex) {
            System.out.println("received IO exception when trying to read lines in M3UReader!");
            ex.printStackTrace();
        }


        return null; // if we reach here something back has happened and the playlist could not be created, tell the user.


    }

}
