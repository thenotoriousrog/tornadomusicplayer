package thenotoriousrog.tornadoplayer.Backend;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

/*
    This class is responsible for two things: 1) holding the name of an album and 2.) all of the songs associated with that album
    The class is parcelable and Serializable to allow for some things to be able to write the data to main memory.
 */
public class Album implements Parcelable, Serializable {

    private String name = ""; // the name of this album
    //private ArrayList<SongInfo> songs = new ArrayList<>();
    private ArrayList<SongInfo> songPaths = new ArrayList<>(); // holds the paths of all the songs in the album.

    // this is the contructor called when made by the user i.e. dev.
    public Album(String name) {
        this.name = name;
    }

    // this is the constructor called when the list is made from memory.
    private Album(Parcel in) {
        this.name = in.readString(); // read in the name of the artist.

        in.readList(songPaths, null); // reads the lists and stores the results inside the songs list. Very important
    }

    private boolean exists(SongInfo song)
    {
       // SongInfo songToCheck;
        for(int i = 0; i < songPaths.size(); i++)
        {
            //songToCheck = new SongInfo();
            //songToCheck.getandSetSongInfo(songPaths.get(i));
            // check to see if the song matches.
            if(song.getSongPath().equals(songPaths.get(i)))
            {
                return true; // match exists return true.
            }
        }

        return false; // if reached here, we did not find a song and thus does not exist.
    }

    // adds a song into the list of songs for this artist. Automatically will not add duplicates.
    public void addSong(SongInfo song)
    {
        if(!exists(song)) // check to see if the song exists in the list or not.
        {
            if(song.getAlbumName() == null)
            {
                return;
            }

            // only add the song if the artist name matches the name of the artist.
            if(song.getAlbumName().equals(name) && name.length() > 2)
            {
                // songs.add(song);
                songPaths.add(song);
            }

        }
    }

    // returns the number of songs for this user.
    public int numOfSongs()
    {
        return songPaths.size();
    }

    public ArrayList<SongInfo> getSongs()
    {
        return songPaths;
    }

//    public ArrayList<String> getSongPaths()
//    {
//        return songPaths;
//    }

    public String getAlbumName()
    {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(name); // writes the name of the artist
        parcel.writeList(songPaths); // writes the songs in the list.
        //parcel.writeTypedList(songs); // writes the songs in the list.
    }

    public static final Creator<Album> CREATOR = new Creator<Album>() {
        @Override
        public Album createFromParcel(Parcel parcel) {
            return new Album(parcel);
        }

        @Override
        public Album[] newArray(int i) {
            return new Album[i];
        }
    };
}
