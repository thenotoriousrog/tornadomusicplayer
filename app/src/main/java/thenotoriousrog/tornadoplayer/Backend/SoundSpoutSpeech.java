package thenotoriousrog.tornadoplayer.Backend;

import java.util.Random;

/*
    This class is used to retrieve a saying to be used by soundspout. Very useful in retreivieng randomized sayings for the user to get a kick out of.
    All responses have about 10 different variations to them, but more can be added, we just have to watch for the randomizer to ensure that the randomizer can hit all of the saying randomly.
 */
public class SoundSpoutSpeech {

    // generates a random number and loads up a random message to be displayed to the user when the app is loaded.
    public static String grabWelcomeMessage()
    {
        Random rand = new Random();
        int num = rand.nextInt(10 - 0) + 1; // generates a random number between 1 and 10 to allowing between 1 of 10 different options to choose from.

        // WARNING: I've never seen the last message saying "WOOOOOOOOOOSH" This is very odd indeed. Need to figure out how why this is.

        // TODO: add more welcome messages for the user to see, this will keep the app refreshing!
        switch (num)
        {
            case 1:
                return "I'm so happy to see you! " + ("\ud83d\ude01"); // super happy face
            case 2:
                return "Play me your favorite song! " + ("\ud83c\udfb6"); // multiple music notes
            case 3:
                return "What shall we listen to today hmmm?";
            case 4:
                return "You survived! I'm a small tornado " + ("\ud83d\ude1d") + ("\ud83c\udf2A"); // tongue out face, tornado
            case 5:
                return "Music can never be too loud!";
            case 6:
                return "My name is SoundSpout! Have we met before?";
            case 7:
                return "Turn up the volume! " + ("\ud83d\udd0a"); // speaker
            case 8:
                return "I love music sooooooo much.... and you! " + ("\ud83d\udc95"); // hearts
            case 9:
                return "Your songs are fire " + ("\ud83d\udd25"); // fire
            case 10:
                return "You have a great taste in music! " + ("\ud83d\ude0a"); // smiling face
            case 11:
                return "Wooooooooooooooosh!";
        }

        return ""; // this message should never, ever be displayed because the randomizer will ensure that this message is never reached. We can leave it blank.
    }

    public static String getSoundSpoutsButtonMessage()
    {
        final Random rand = new Random();
        final int num = rand.nextInt(10) + 1; // generates a random number between 1 and 10 to allowing between 1 of 10 different options to choose from.

        switch (num)
        {
            case 1:
                return "OUCH! " + ("\ud83e\udd15"); // head with bandage
            case 2:
                return "That button is mine! MINE! " + ("\ud83d\ude29"); // weary face
            case 3:
                return "Hey! I don't want to share this button! " + ("\ud83d\ude41"); // frowning face
            case 4:
                return "Owie, don't press that so hard! " + ("\ud83e\udd15") + ("\ud83d\ude2d"); // head with bandage and loud crying face
            case 5:
                return "Nooooo don't push that! " + ("\ud83d\ude20"); // angry face
            case 6:
                return "Why do you push that so hard!? " + ("\ud83d\ude22"); // crying face
            case 7:
                return "Tornados can never have anything nice... " + ("\ud83d\ude22"); // crying face
            case 8:
                return "One day... I'm going to push YOUR buttons " + ("\ud83d\ude24"); // steam coming out of nose angry face.
            case 9:
                return "Don't make me turn into an F5! " + ("\ud83e\udd2a") + ("\ud83c\udf2A"); // zany face with tornado
            case 10:
                return "My maker said this is MY button " + ("\ud83d\ude0a"); // smiling face with smiling eyes

        }
        return ""; // this should never be called.
    }

    public static String getRescanMessage()
    {
        final Random rand = new Random();
        final int num = rand.nextInt(10) + 1; // generates a random number between 1 and 10 to allowing between 1 of 10 different options to choose from.

        switch (num)
        {
            case 1:
                return "Oh yeah, I'm on it! " + ("\ud83d\udcaa") + ("\ud83c\udf2A"); // flexed biceps with tornado symbol.
            case 2:
                return "More songs to play eh? " + ("\ud83d\ude0f"); // smirking face
            case 3:
                return "SoundSpout is on the job! ";
            case 4:
                return "Scanning for your music now! ";
            case 5:
                return "Just when I thought you didn't have more music to play " + ("\ud83e\udd29"); // starstruck face
            case 6:
                return "You are just full of surprises arn't you? " + ("\ud83d\ude43"); // upside down smiley face
            case 7:
                return "You can never have too much music! " + ("\ud83c\udfb6"); // music notes.
            case 8:
                return "I'll have this done is a flash! " + ("\ud83c\udfc3") + ("\ud83c\udf2A"); // running with tornado symbol
            case 9:
                return "Your music is my music " + ("\ud83d\ude01"); // smiling with eyes closed.
            case 10:
                return "Working on it now!";
        }

        return ""; // this should never be called
    }

    // returns a message to be displayed when a user turns shuffle on
    public static String getShuffleOnMessage()
    {
        final Random rand = new Random();
        final int num = rand.nextInt(10) + 1; // generates a random number between 1 and 10 to allowing between 1 of 10 different options to choose from.

        switch (num)
        {
            case 1:
                return "Mixing it up!";
            case 2:
                return "Oooooooo I ALSO like to live dangerously " + ("\ud83d\ude09"); // winking face.
            case 3:
                return "See what's up next!"; // maybe put a down arrow here!
            case 4:
                return "Randomizing. pssp pssp pssp pssp";
            case 5:
                return "Got it! Shuffling now!";
            case 6:
                return "Psst, I know what songs will play before you " + ("\ud83d\ude0f"); // smirking face.
            case 7:
                return "SoundSpout ready for action! " + ("\ud83d\udcaa") + ("\ud83c\udf2A"); // flexed biceps with tornado symbol
            case 8:
                return "Being a tornado makes shuffling easy!";
            case 9:
                return "Did you see that?? It's what's up next";
            case 10:
                return "Is your volume all the way up? 'Cuz I can't hear you!";
            case 11:
                return "You made the shuffle button change colors... AWESOME!";
        }

        return ""; // this will never be used. The randomizer won't allow it
    }

    // returns a message to be displayed when a user turns shuffle off.
    public static String getShuffleOffMessage()
    {
        final Random rand = new Random();
        final int num = rand.nextInt(10) + 1; // generates a random number between 1 and 10 to allowing between 1 of 10 different options to choose from.

        switch(num)
        {
            case 1:
                return "Going down the line!";
            case 2:
                return "Phew, that was hard mixing the songs up like that";
            case 3:
                return "The next song is by ummm Taylor Swift huh?";
            case 4:
                return "woosh woosh wooosh";
            case 5:
                return "Hey, look how the shuffle button is white now " + ("\ud83d\ude02"); // laughing face with tears.
            case 6:
                return "You just want to know what songs are going to play before me... " + ("\ud83d\ude2d"); // crying face.
            case 7:
                return "RedBull may give you wings, but not like a Tornado can! " + ("\ud83d\udcaf"); // the one hundred symbol
            case 8:
                return "I shuffle by juggling but... I can't juggle";
            case 9:
                return "Made you look!";
            case 10:
                return "Turn it down! Now.... Turn it up!";
            case 11:
                return "I love listening to music with you! " + ("\ud83d\ude01"); // super happy face.
        }

        return ""; // This will never be used, the randomizer won't allow it.
    }

    // returns a message to be displayed when a user turns repeat on.
    public static String getRepeatOnMessage()
    {
        final Random rand = new Random();
        final int num = rand.nextInt(10) + 1; // generates a random number between 1 and 10 to allowing between 1 of 10 different options to choose from.

        switch(num)
        {
            case 1:
                return "I'm becoming an F5!";
            case 2:
                return "'round and 'round we go... like a tornado" + "\ud83c\udf2A"; // tornado
            case 3:
                return "I love this song! Let's listen to it forever!";
            case 4:
                return "Repeating song forever! No, Five-ever!";
            case 5:
                return "Repeating current song now";
            case 6:
                return "I like the repeat button because it represents spinning heehee " + ("\ud83c\udf2A"); // tornado
            case 7:
                return "I love Taylor Swift too! No I mean Katy Perry? Ah, I give up";
            case 8:
                return "Repeat this song? That's easy! " + ("\ud83d\ude0e"); // sunglasses face.
            case 9:
                return "Hey the repeat button changed colors!";
            case 10:
                return "Repeat on. Repeat On. Repeat on. Repea...";
            case 11:
                return "Let's tell everyone how awesome this song is!";
        }

        return ""; // This will never be used, the randomizer won't allow it.
    }

    // returns a message to be displayed when a user turns repeat off.
    public static String getRepeatOffMessage()
    {
        final Random rand = new Random();
        final int num = rand.nextInt(10) + 1; // generates a random number between 1 and 10 to allowing between 1 of 10 different options to choose from.

        switch(num)
        {
            case 1:
                return "Aha! I knew you had more songs to play!";
            case 2:
                return "I could listen to that one forever " + ("\ud83d\ude0a"); // smiling face.
            case 3:
                return "Woah! You made the repeat button turn white again!";
            case 4:
                return "Going back through the list again, how exciting!";
            case 5:
                return "Repeat is off!";
            case 6:
                return "That was one of my favorite songs";
            case 7:
                return "The repeat and I are good friends. We are always spinning!";
            case 8:
                return "Why repeat when you can shuffle right? " + ("\ud83d\ude02"); // laughing with tears.
            case 9:
                return "See what's next! Oh, you already know about that.";
            case 10:
                return "Have no fear, SoundSpout is here!";
            case 11:
                return "Mr. Repeat can be annoying sometimes I know... he just repeats himself " + ("\ud83d\ude12"); // annoyed face.
        }

        return ""; // This will never be used, the randomizer won't allow it.
    }

}
