package thenotoriousrog.tornadoplayer.Backend;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by thenotoriousrog on 3/18/18.
 * This class holds the arrange of sorters that sort a variety of ways.
 */

public class Sorters {

    // sorts the songs passed in by songName
    public static void sortBySongName(ArrayList<SongInfo> songs)
    {
        // sorts the orderedList by songname
        Collections.sort(songs, new Comparator<SongInfo>() {

            // sorts the list.
            @Override
            public int compare(SongInfo o1, SongInfo o2)
            {
                if(o1.getSongName() == null || o2.getSongName() == null)
                {
                    return -1;
                }

                return o1.getSongName().compareTo(o2.getSongName());
            }
        });
    }

    // sorts the songs passed in by artist name.
    public static void sortByArtistName(ArrayList<SongInfo> songs)
    {
        System.out.println("are the songs when sorting by artist null? " + songs);
        // sorts the orderedList by songname
        Collections.sort(songs, new Comparator<SongInfo>() {

            // sorts the list.
            @Override
            public int compare(SongInfo o1, SongInfo o2)
            {
                if(o1.getArtistName() == null || o2.getArtistName() == null)
                {
                    return -1;
                }

                return o1.getArtistName().compareTo(o2.getArtistName());
            }
        });
    }

    // sorts the songs passed in by duration.
    public static void sortByDuration(ArrayList<SongInfo> songs)
    {
        // sorts the orderedList by songname
        Collections.sort(songs, new Comparator<SongInfo>() {

            // sorts the songs by duration.
            @Override
            public int compare(SongInfo o1, SongInfo o2) {
                return o1.getSongDuration().compareTo(o2.getSongDuration());
            }
        });
    }

    // Sorts folders according to their names.
    public static void sortFolders(ArrayList<String> folders)
    {

        // sorts the orderedList by songname
        Collections.sort(folders, new Comparator<String>() {

            // sorts the folders
            @Override
            public int compare(String f1, String f2)
            {
                String f1temp = f1;
                String[] f1Arr = f1temp.split("/"); // split the string by /'s

                int f1Length = f1Arr.length; // get the length of the split string.
                String f1Name = f1Arr[f1Length -1]; // get the last split which should hold our folder name.

                String f2temp = f2;
                String[] f2Arr = f2temp.split("/"); // split the string by /'s

                int length = f2Arr.length; // get the length of the split string.
                String f2Name = f2Arr[length -1]; // get the last split which should hold our folder name.

                return f1Name.compareToIgnoreCase(f2Name);
            }
        });
    }

    // sorts playlists according to their names.
    public static void sortPlaylistsName(ArrayList<Playlist> Playlists)
    {
        // sorts the playlists.
        Collections.sort(Playlists, new Comparator<Playlist>() {

            // sorts the list.
            @Override
            public int compare(Playlist p1, Playlist p2) {
                return p1.name().compareTo(p2.name());
            }
        });
    }


    public static void sortAlbums(ArrayList<Album> albums)
    {
        Collections.sort(albums, new Comparator<Album>() {
            @Override
            public int compare(Album a1, Album a2) {
                return a1.getAlbumName().compareTo(a2.getAlbumName());
            }
        });
    }

    public static void sortArtists(ArrayList<Artist> artists)
    {
        Collections.sort(artists, new Comparator<Artist>() {
            @Override
            public int compare(Artist a1, Artist a2) {
                return a1.getArtistName().compareTo(a2.getArtistName());
            }
        });
    }

}
