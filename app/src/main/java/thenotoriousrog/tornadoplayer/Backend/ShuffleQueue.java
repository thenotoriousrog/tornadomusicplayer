package thenotoriousrog.tornadoplayer.Backend;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;

/**
 * Created by thenotoriousrog on 7/17/17.
 * This class is a queue that simply holds two songs at any given time that way when shuffling is going on the user can see the next song that is playing.
 * This queue will automatically fill itself whenever a user pops a song out of the queue. The user can peek to the next song as well.
 */

public class ShuffleQueue {

    private final ArrayList<SongInfo> songs; // once this list is set it will never be set again until a new creation of the Queue that we wish to use.
    private Stack<SongInfo> stack = new Stack<>(); // The queue that will be used for shuffling of the songs.

    private boolean isInStack(SongInfo songToCheck)
    {
        while(stack.iterator().hasNext())
        {
            SongInfo song = stack.iterator().next();

            // if the song and artist name are the same then this song is indeed in the list.
            if(song.getSongName().equals(songToCheck.getSongName()) &&
                    song.getArtistName().equals(songToCheck.getArtistName()))
            {
                return true; // this song is in the queue.
            }
        }

        return false; // the song was not found.
    }

    // When the class is made, the Queue is automatically filled.
    public ShuffleQueue(ArrayList<SongInfo> currentSongList)
    {
        songs = new ArrayList<>(currentSongList); // create a brand new copy of this list so that we can clear it and have the Garbage collector can clean it on its pass.
        // fill the queue with two random songs out of the song list.

        // the below two lines are the old way of doing things
        //queue.add(songs.get(randomSongPos())); // get random song 1.
        //queue.add(songs.get(randomSongPos())); // get random song 2.

        Collections.shuffle(songs); // shuffle the songs getting random items.

        //queue.addAll(songs); // add the freshly shuffled songs into the queue

        for(int i = 0; i < songs.size(); i++) // loop through all songs getting random songs.
        {
            // if not played, we may add it to the queue.
            if(!MusicLibrary.isPlayed(songs.get(i)))
            {
                stack.add(songs.get(i));
            }
        }
    }

    public void addToStack(SongInfo songToAdd)
    {
        stack.push(songToAdd);
    }

    // this method is used to grab the next song in the queue, also triggering the queue to fill the queue with another song.
    public SongInfo popNextSong()
    {
        SongInfo nextSong = stack.pop(); // grabs the first song in the list.

        //queue.add(songs.get(randomSongPos())); // grab a random song and fill the queue one more time.

        return nextSong; // return the next song in the list.
    }

    // This method is used to grab the song name of the next song to be used by the main title displays and notifications.
    public SongInfo peekNxtSong()
    {
        if(stack.isEmpty())
        {
            return null;
        }

        return stack.peek(); // grab the next song and extract the song name and return that to the calling class.
    }

    // clears every list to hold as little data as possible.
    public void clear()
    {
        stack.clear(); // removed all songs from the queue.
        songs.clear(); // removes all songs from the list of songs.
    }


}
