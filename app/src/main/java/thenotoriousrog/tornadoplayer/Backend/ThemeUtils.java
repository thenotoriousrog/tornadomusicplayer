package thenotoriousrog.tornadoplayer.Backend;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import thenotoriousrog.tornadoplayer.R;

/*
    -This class is responsible for returning the correct theme'd icons and colors as well as other objects throughout the application that require the theme to be more accurate.
    -This class is started directly at startup of the app to ensure that the ThemeUtils can be used throughout the rest of the application.
    -The idea is that all things that need to check for the current theme should use this class. The app is a bit scattered on how this is done right now and this class should hold all that logic to make the rest of the app
    cleaner in terms of deciding what the theme of the layout should be.
 */
public class ThemeUtils {

    private Context context; // the context within the app.
    private static ThemeUtils instance; // the instance of the theme utils.
    private SharedPreferences sharedPreferences; // the preferences to be used throughout the application.
    private String theme; // holds the current theme that is used throughout the applciation.

    // this should only be called if our context is set otherwise we will have a null pointer exception if calling any other of the methods within the app.
    public static ThemeUtils getInstance()
    {
        if(instance == null) { // if null instantiate the instance of theme Utils
            instance = new ThemeUtils();
        }

        return instance;
    }

    public void setContext(Context context)
    {
        this.context = context;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context); // gets the default shared preferences
        theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.
    }

    // holds the current theme that the user is working with. Primarily this is the theme colors that will go in the layouts.
    public int getCurrentThemePrimaryColor()
    {

        if(theme.equalsIgnoreCase("ParadiseNightfall"))
        {
            return R.color.ParadiseNightfallPrimary;
        }
        else if(theme.equalsIgnoreCase("AquaArmy"))
        {
            return R.color.AquaArmyPrimary;
        }
        else if(theme.equalsIgnoreCase("Aftermath"))
        {
            return R.color.AftermathPrimary;
        }
        else if(theme.equalsIgnoreCase("FlowerPetal"))
        {
            return R.color.FlowerPetalPrimary;
        }
        else // if none of the options, we need to use the default color.
        {
            return R.color.OceanMarinaPrimary;
        }
    }

    public int getCurrentOptionsMenu()
    {
        if(theme.equalsIgnoreCase("ParadiseNightfall"))
        {
            return R.drawable.options_paradise_nightfall;
        }
        else if(theme.equalsIgnoreCase("AquaArmy"))
        {
            return R.drawable.options_aqua_army;
        }
        else if(theme.equalsIgnoreCase("Aftermath"))
        {
            return R.drawable.options_aftermath;
        }
        else if(theme.equalsIgnoreCase("FlowerPetal"))
        {
            return R.drawable.options_flower_petal;
        }
        else // if none of the options, we need to use the default color.
        {
            return R.drawable.options_ocean_marina;
        }
    }

    public int getCurrentFolderColor()
    {
        if(theme.equalsIgnoreCase("ParadiseNightfall"))
        {
            return R.drawable.folder_paradise_nightfall;
        }
        else if(theme.equalsIgnoreCase("AquaArmy"))
        {
            return R.drawable.folder_aqua_army;
        }
        else if(theme.equalsIgnoreCase("Aftermath"))
        {
            return R.drawable.folder_aftermath;
        }
        else if(theme.equalsIgnoreCase("FlowerPetal"))
        {
            return R.drawable.folder_flower_petal;
        }
        else // if none of the options, we need to use the default color.
        {
            return R.drawable.folder_ocean_marina;
        }
    }

    public int getCurrentAlbumColor()
    {
        if(theme.equalsIgnoreCase("ParadiseNightfall"))
        {
            return R.drawable.album_paradise_nightfall;
        }
        else if(theme.equalsIgnoreCase("AquaArmy"))
        {
            return R.drawable.album_aqua_army;
        }
        else if(theme.equalsIgnoreCase("Aftermath"))
        {
            return R.drawable.album_aftermath;
        }
        else if(theme.equalsIgnoreCase("FlowerPetal"))
        {
            return R.drawable.album_flower_petal;
        }
        else // if none of the options, we need to use the default color.
        {
            return R.drawable.album_ocean_marina;
        }
    }

    public int getCurrentSongIcon()
    {
        if(theme.equalsIgnoreCase("ParadiseNightfall"))
        {
            return R.drawable.music_note_paradise_nightfall_24dp;
        }
        else if(theme.equalsIgnoreCase("AquaArmy"))
        {
            return R.drawable.music_note_aqua_army_24dp;
        }
        else if(theme.equalsIgnoreCase("Aftermath"))
        {
            return R.drawable.music_note_aftermath;
        }
        else if(theme.equalsIgnoreCase("FlowerPetal"))
        {
            return R.drawable.music_note_flower_petal;
        }
        else // if none of the options, we need to use the default color.
        {
            return R.drawable.music_note_ocean_marina_24dp;
        }
    }


    // holds the accent color of the current theme that we are working with.
    public int getCurrentThemeAccentColor()
    {
       // boolean usingDarkTheme = sharedPreferences.getBoolean("UseDarkTheme", false); // default is false.

        if(theme.equalsIgnoreCase("ParadiseNightfall"))
        {

            return R.color.ParadiseNightfallAccent;
        }
        else if(theme.equalsIgnoreCase("AquaArmy"))
        {
            return R.color.AquaArmyAccent;
        }
        else if(theme.equalsIgnoreCase("Aftermath"))
        {
            return R.color.AftermathAccent;
        }
        else if(theme.equalsIgnoreCase("FlowerPetal"))
        {
            return R.color.FlowerPetalAccent;
        }
        else // if none of the options, we need to use the default color.
        {
            //if(theme.equalsIgnoreCase(""))
            return R.color.OceanMarinaAccent;
        }
    }

    // tells the rest of the app if we are using the dark theme or not.
    public boolean usingDarkTheme()
    {
        boolean darkTheme = sharedPreferences.getBoolean("UseDarkTheme", false); // default is false.
        if(darkTheme)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // gets the actual theme that we want to set for the app itself.
    public int getCurrentTheme()
    {
        boolean usingDarkTheme = sharedPreferences.getBoolean("UseDarkTheme", false); // default is false.

        if(!usingDarkTheme)
        {
            if(theme.equalsIgnoreCase("ParadiseNightfall"))
            {
                return R.style.AppTheme_ParadiseNightfall;
            }
            else if(theme.equalsIgnoreCase("AquaArmy"))
            {
                return R.style.AppTheme_AquaArmy;
            }
            else if(theme.equalsIgnoreCase("Aftermath"))
            {
                return R.style.AppTheme_Aftermath;
            }
            else if(theme.equalsIgnoreCase("FlowerPetal"))
            {
                return R.style.AppTheme_FlowerPetal;
            }
            else
            {
                return R.style.AppTheme_OceanMarina;
            }
        }
        else
        {
            if(theme.equalsIgnoreCase("ParadiseNightfall"))
            {
                return R.style.AppTheme_ParadiseNightfallDark;
            }
            else if(theme.equalsIgnoreCase("AquaArmy"))
            {
                return R.style.AppTheme_AquaArmyDark;
            }
            else if(theme.equalsIgnoreCase("Aftermath"))
            {
                return R.style.AppTheme_AftermathDark;
            }
            else if(theme.equalsIgnoreCase("FlowerPetal"))
            {
                return R.style.AppTheme_FlowerPetalDark;
            }
            else // default dark theme.
            {
                return R.style.AppTheme_OceanMarinaDark;
            }
        }


    }

    // gets the color of the thumb image that we want to work with.
    public int getCurrentSeekbarThumb() {
        if (theme.equalsIgnoreCase("ParadiseNightfall")) {
            return R.drawable.paradise_nightfall_thumb;
        } else if (theme.equalsIgnoreCase("AquaArmy")) {
            return R.drawable.aqua_army_thumb;
        }else if(theme.equalsIgnoreCase("Aftermath")){
            return R.drawable.aftermath_thumb;
        } else if(theme.equalsIgnoreCase("FlowerPetal")) {
            return R.drawable.flower_petal_thumb;
        }
        else {
            return R.drawable.ocean_marina_thumb;
        }
    }

    public int getCurrentSeekbarProgressDrawable() {
        if (theme.equalsIgnoreCase("ParadiseNightfall")) {
            return R.drawable.seekbar_paradise_nightfall;
        } else if (theme.equalsIgnoreCase("AquaArmy")) {
            return R.drawable.seekbar_aqua_army;
        } else if(theme.equalsIgnoreCase("Aftermath")) {
            return R.drawable.seekbar_aftermath;
        } else if(theme.equalsIgnoreCase("FlowerPetal")) {
            return R.drawable.seekbar_flower_petal;
        }
        else {
            return R.drawable.seekbar_ocean_marina;
        }
    }

    public int getCurrentTouchIcon() {
        if (theme.equalsIgnoreCase("ParadiseNightfall")) {
            return R.drawable.touch_paradise_nightfall;
        } else if (theme.equalsIgnoreCase("AquaArmy")) {
            return R.drawable.touch_aqua_army;
        } else if(theme.equalsIgnoreCase("Aftermath")) {
            return R.drawable.touch_aftermath;
        } else if(theme.equalsIgnoreCase("FlowerPetal")) {
            return R.drawable.touch_flower_petal;
        }
        else {
            return R.drawable.touch_ocean_marina;
        }
    }

    public int getCurrentShuffleIcon()
    {
        if (theme.equalsIgnoreCase("ParadiseNightfall")) {
            return R.drawable.shuffle_paradise_nightfall;
        } else if (theme.equalsIgnoreCase("AquaArmy")) {
            return R.drawable.shuffle_aqua_army;
        } else if(theme.equalsIgnoreCase("Aftermath")) {
            return R.drawable.shuffle_aftermath;
        } else if(theme.equalsIgnoreCase("FlowerPetal")) {
            return R.drawable.shuffle_flower_petal;
        }
        else {
            return R.drawable.shuffle_ocean_marina;
        }
    }

    public int getCurrentRepeatIcon()
    {
        if (theme.equalsIgnoreCase("ParadiseNightfall")) {
            return R.drawable.repeat_paradise_nightfall;
        } else if (theme.equalsIgnoreCase("AquaArmy")) {
            return R.drawable.repeat_aqua_army;
        } else if(theme.equalsIgnoreCase("Aftermath")) {
            return R.drawable.repeat_aftermath;
        } else if(theme.equalsIgnoreCase("FlowerPetal")) {
            return R.drawable.repeat_flower_petal;
        }
        else {
            return R.drawable.repeat_ocean_marina;
        }
    }

    public int getCurrentSoundSpoutTheme() {
        if (theme.equalsIgnoreCase("ParadiseNightfall")) {
            return R.drawable.soundspout_paradise_nightfall;
        } else if (theme.equalsIgnoreCase("AquaArmy")) {
            return R.drawable.soundspout_aqua_army;
        } else if(theme.equalsIgnoreCase("Aftermath")) {
            return R.drawable.soundspout_aftermath;
        } else if(theme.equalsIgnoreCase("FlowerPetal")) {
            return R.drawable.soundspout_flower_petal;
        }
        else {
            return R.drawable.soundspout_ocean_marina;
        }
    }

}
