package thenotoriousrog.tornadoplayer.Backend;

import android.content.Context;
import android.os.Environment;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/*
    This class is in charge of converting the playlists to M3U files
    Code inspired by Shuttle Music Player. Tom Malseed is awesome!
    https://github.com/timusus/Shuttle/blob/ef32ed8774ec45f684be755cff927125b7033e9f/app/src/main/java/com/simplecity/amp_library/utils/PlaylistUtils.java
 */
public class M3uCreater implements Runnable  {

    private Playlist playlist; // the playlist we want to convert.
    private Context context;
    private Thread backgroundThread; // the background thread that we are working with.
    private Runnable uiUpdater; // displays the toast message.
    private View view; // this is needed in order to post the UI update.

    public M3uCreater(Playlist playlist, Context context, View view)
    {
        this.playlist = playlist;
        this.context = context;
        this.backgroundThread = new Thread(this);

        this.view = view;
    }

    // starts the background thread!
    public void create()
    {
        backgroundThread.start();
    }

    // converts the playlist to an m3u file.
    private void showToast(String filePath)
    {

        // show the toast that the file was saved and created correctly in the user's phone.

    }

    private void initiateUIUpdater(final String filePath)
    {
        uiUpdater = new Runnable() {
            @Override
            public void run() {
                if(filePath == null)
                {
                    Toast.makeText(context, "I don't have permission to create this file " + ("\ud83d\ude0f"), Toast.LENGTH_LONG).show(); // smirking face
                }
                else
                {
                    Toast.makeText(context, "Saved in " + filePath + "!!", Toast.LENGTH_LONG).show();
                }

            }
        };
    }

    private void postUIUpdate()
    {
        view.post(uiUpdater); // shows the toast message.
    }

    private File getExternalSdCardDir()
    {
        File removableStoragePath = null;
        File fileList[] = new File("/storage/").listFiles();
        for (File file : fileList)
        {
            if(!file.getAbsolutePath().equalsIgnoreCase(Environment.getExternalStorageDirectory().getAbsolutePath()) && file.isDirectory() && file.canRead())
            {
                removableStoragePath = file;
            }
        }

        return removableStoragePath;
    }

    @Override
    public void run()
    {
        File playlistFile = null;
        File externalStorage = getExternalSdCardDir();

        if(Environment.getExternalStorageDirectory().canWrite()) // if we have the ability to write to to the storage directory, do it.
       // if(externalStorage.canWrite())
        {
            File root = new File(Environment.getExternalStorageDirectory(), "/TornadoMusicPlayer/Playlists"); // creates a file in a new directory.
            //File root = new File(externalStorage, "TornadoMusicPlayer/Playlists"); // creates a file in a new directory.
            if(!root.exists())
            {
                root.mkdirs(); // make directories if they don't exist in the file system.
            }

            File noMediaFile = new File(root, ".nomedia"); // the file extension
            if(!noMediaFile.exists())
            {
                // try to create the file if at all possible.
                try
                {
                    noMediaFile.createNewFile();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            String playlistName = playlist.name().replaceAll("[^a-zA-Z0-9.-]", "_");

            playlistFile = new File(root, playlistName + ".m3u"); // create the m3u playlist file name.

            // if multiple names of the playlist exists add an additional one after it.
            int i = 0;
            while(playlistFile.exists())
            {
                i++;
                playlistFile = new File(root, playlistName + i + ".m3u");
            }

            // try to write the file to main memory now.
            try {
                FileWriter fileWriter = new FileWriter(playlistFile);
                StringBuilder body = new StringBuilder();
                body.append("#EXTM3U\n");

                for (SongInfo song : playlist.songs()) {
                    body.append("#EXTINF:")
                            .append(song.getSongDuration())
                            .append(",")
                            .append(song.getSongName())
                            .append(" - ")
                            .append(song.getArtistName())
                            .append("\n")
                            .append(song.getSongPath())
                            .append("\n");
                }
                fileWriter.append(body);
                fileWriter.flush();
                fileWriter.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

            // create toast to tell the users.
            initiateUIUpdater(playlistFile.getPath());
            postUIUpdate(); // update the ui
        }
        else // we cannot write to the directory.
        {
            initiateUIUpdater(null);
            postUIUpdate();
        }


    }
}
