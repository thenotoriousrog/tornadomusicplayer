package thenotoriousrog.tornadoplayer.Backend;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import thenotoriousrog.tornadoplayer.R;

/**
 * Created by Roger on 12/2/17.
 * This class simply holds bitmap generation tools that can be used anywhere in the application for rendering, generating, and resizing bitmaps to an optimal size.
 * The methods in this class are static to allow for quick grabbing anywhere within the program.
 */

public class BitmapWorkshop {


    private BitmapWorkshop() {} // does nothing, simple constructor.

    // calculates the optimum size for images.
    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // recently converted these to doubles to try to get the best optimization of my image!
            final double halfHeight = height / 2;
            final double halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    // takes in a resource, imageID, sizes, and returns an optimum size for the image.
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        //options.inPreferredConfig = Bitmap.Config.RGB_565; not working
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeResource(res, resId, options);
    }

    // Converts a bitmap to a Uri to be used with picasso!
    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Album Art", null);
        return Uri.parse(path);
    }

    public static Bitmap extractAlbumArtForSongList(Context context, SongInfo currSong, int width, int height)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        MediaMetadataRetriever mmr = new MediaMetadataRetriever(); // create a new MediaMetaDataRetriever to extract the album art from the current song if one exists.

        try
        {
            mmr.setDataSource(currSong.getSongPath()); // send in the song path to attempt to extract the song data items.

            byte[] albumArt = mmr.getEmbeddedPicture();
            if(albumArt != null)
            {

                InputStream stream = new ByteArrayInputStream(mmr.getEmbeddedPicture());
                Bitmap albumArtToReturn = BitmapFactory.decodeStream(stream);
                mmr.release(); // release the mmr to free up resources on the device.
                // return art; // return the bitmap.
                return albumArtToReturn;
            }
            else
            {
                mmr.release();
                return null; // this will tell the system to use the default songIcon.
            }

        } catch (RuntimeException ex) { // this exception catch is needed and for some reason occures on certain songs.
            mmr.release();
            return null;
        }

    }

    public static Bitmap extractAlbumArtForNotifications(Context context, SongInfo currSong, int width, int height)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        MediaMetadataRetriever mmr = new MediaMetadataRetriever(); // create a new MediaMetaDataRetriever to extract the album art from the current song if one exists.
        mmr.setDataSource(currSong.getSongPath()); // send in the song path to attempt to extract the song data items.
        byte[] albumArt = mmr.getEmbeddedPicture();
        if(albumArt != null)
        {

            InputStream stream = new ByteArrayInputStream(mmr.getEmbeddedPicture());
            BitmapFactory.Options opts = new BitmapFactory.Options(); // options for the bitmap factory.
            opts.inSampleSize = 4; // this tells the Bitmap factory to return the bitmap 1/4 the size of the original image.
            Bitmap albumArtToReturn = BitmapFactory.decodeStream(stream);
            mmr.release(); // release the mmr to free up resources on the device.

            opts.inSampleSize = 1; // return the bitmap factory instructions to normal.

            return albumArtToReturn;
        }
        else // the album art is null, return the Bitmap with the default tornado image!
        {

            //currentAlbumArt = BitmapWorkshop.decodeSampledBitmapFromResource(currentActivity.getResources(), R.drawable.applogo, 200, 200);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context); // gets the default shared preferences
            String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.

            Bitmap bm = decodeDefaultAlbumArtBitmap(context.getResources(), ThemeUtils.getInstance().getCurrentSoundSpoutTheme(), width, height); // gets the soundspout based on the theme that we are using.
//            if(theme.equalsIgnoreCase("OceanMarina"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(context.getResources(), R.drawable.soundspout_ocean_marina, width, height);
//            }
//            else if(theme.equalsIgnoreCase("ParadiseNightfall"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(context.getResources(), R.drawable.soundspout_paradise_nightfall, width, height);
//            }
//            else if(theme.equalsIgnoreCase("AquaArmy"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(context.getResources(), R.drawable.soundspout_aqua_army, width, height);
//            }
//            else
//            {
//                bm = decodeDefaultAlbumArtBitmap(context.getResources(), R.drawable.soundspout_ocean_marina, width, height);
//            }

            //mmr.release();
            //Bitmap bm = BitmapWorkshop.decodeSampledBitmapFromResource(context.getResources(), R.drawable.soundspout_paradise_nightfall, width, height); // send the bitmap to be processed so that the app will work correctly!
            mmr.release();
            return bm;
            //mmr.release(); // release the mmr to free up resources on the device.
            //return art; // return the bitmap
        }
    }

    // takes in a resource, imageID, sizes, and returns an optimum size for the image.
    private static Bitmap decodeDefaultAlbumArtBitmap(Resources res, int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888; //not working
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = 1; // it seems that the sample size of 2 is a much more preferred number for the bitmap to get the best size and quality for the bitmap.

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeResource(res, resId, options);
    }

    // grabs the album art to be used with a size set with what is passed in with width and height.
    public static Bitmap extractAlbumArt(Activity currentActivity, SongInfo currSong, int width, int height)
    {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever(); // create a new MediaMetaDataRetriever to extract the album art from the current song if one exists.
        mmr.setDataSource(currSong.getSongPath()); // send in the song path to attempt to extract the song data items.
        byte[] albumArt = mmr.getEmbeddedPicture();
        if(albumArt != null)
        {

            InputStream stream = new ByteArrayInputStream(mmr.getEmbeddedPicture());
            Bitmap albumArtToReturn = BitmapFactory.decodeStream(stream);
            mmr.release(); // release the mmr to free up resources on the device.
            // return art; // return the bitmap.
            return albumArtToReturn;
        }
        else // the album art is null, return the Bitmap with the default tornado image!
        {

            //currentAlbumArt = BitmapWorkshop.decodeSampledBitmapFromResource(currentActivity.getResources(), R.drawable.applogo, 200, 200);
            //mmr.release();

            //BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inPreferredConfig = Bitmap.Config.ARGB_4444;

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(currentActivity.getBaseContext()); // gets the default shared preferences
            String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.
            //currentAlbumArt = BitmapWorkshop.decodeSampledBitmapFromResource(currentActivity.getResources(), R.drawable.applogo, 200, 200);
            //mmr.release();

            Bitmap bm = decodeDefaultAlbumArtBitmap(currentActivity.getResources(), ThemeUtils.getInstance().getCurrentSoundSpoutTheme(), width, height); // gets the soundspout based on the theme that we are using.

//            if(theme.equalsIgnoreCase("OceanMarina"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(currentActivity.getResources(), R.drawable.soundspout_ocean_marina, width, height);
//            }
//            else if(theme.equalsIgnoreCase("ParadiseNightfall"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(currentActivity.getResources(), R.drawable.soundspout_paradise_nightfall, width, height);
//            }
//            else if(theme.equalsIgnoreCase("AquaArmy"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(currentActivity.getResources(), R.drawable.soundspout_aqua_army, width, height);
//            }
//            else
//            {
//                bm = decodeDefaultAlbumArtBitmap(currentActivity.getResources(), R.drawable.soundspout_ocean_marina, width, height);
//            }

            //Bitmap bm = BitmapWorkshop.decodeSampledBitmapFromResource(currentActivity.getResources(), R.drawable.soundspout_paradise_nightfall, width, height); // send the bitmap to be processed so that the app will work correctly!
            mmr.release();
            return bm;
            //mmr.release(); // release the mmr to free up resources on the device.
            //return art; // return the bitmap
        }
    }

    public static Bitmap extractAlbumArtForAndroidWear(Context context, SongInfo currSong)
    {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever(); // create a new MediaMetaDataRetriever to extract the album art from the current song if one exists.
        mmr.setDataSource(currSong.getSongPath()); // send in the song path to attempt to extract the song data items.
        byte[] albumArt = mmr.getEmbeddedPicture();
        if(albumArt != null)
        {

            InputStream stream = new ByteArrayInputStream(mmr.getEmbeddedPicture());
            Bitmap albumArtToReturn = BitmapFactory.decodeStream(stream);
            mmr.release(); // release the mmr to free up resources on the device.
            // return art; // return the bitmap.
            return albumArtToReturn;
        }
        else // the album art is null, return the Bitmap with the default tornado image!
        {

            //currentAlbumArt = BitmapWorkshop.decodeSampledBitmapFromResource(currentActivity.getResources(), R.drawable.applogo, 200, 200);
            //mmr.release();

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context); // gets the default shared preferences
            String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.
            //currentAlbumArt = BitmapWorkshop.decodeSampledBitmapFromResource(currentActivity.getResources(), R.drawable.applogo, 200, 200);
            //mmr.release();

            Bitmap bm = decodeDefaultAlbumArtBitmap(context.getResources(), ThemeUtils.getInstance().getCurrentSoundSpoutTheme(), 200, 200); // gets the soundspout based on the theme that we are using.

//            if(theme.equalsIgnoreCase("OceanMarina"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(context.getResources(), R.drawable.soundspout_ocean_marina, 200, 200);
//            }
//            else if(theme.equalsIgnoreCase("ParadiseNightfall"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(context.getResources(), R.drawable.soundspout_paradise_nightfall, 200, 200);
//            }
//            else if(theme.equalsIgnoreCase("AquaArmy"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(context.getResources(), R.drawable.soundspout_aqua_army, 200, 200);
//            }
//            else
//            {
//                bm = decodeDefaultAlbumArtBitmap(context.getResources(), R.drawable.soundspout_ocean_marina, 200, 200);
//            }

           // Bitmap bm = BitmapWorkshop.decodeSampledBitmapFromResource(context.getResources(), R.drawable.soundspout_paradise_nightfall, 200, 200); // send the bitmap to be processed so that the app will work correctly!
            mmr.release();
            return bm;
            //mmr.release(); // release the mmr to free up resources on the device.
            //return art; // return the bitmap
        }
    }

    // grabs the album art for the song with the default of the app logo being 400x400
    public static Bitmap extractAlbumArt(Activity currentActivity, SongInfo currSong)
    {
        MediaMetadataRetriever mmr = new MediaMetadataRetriever(); // create a new MediaMetaDataRetriever to extract the album art from the current song if one exists.
        mmr.setDataSource(currSong.getSongPath()); // send in the song path to attempt to extract the song data items.
        byte[] albumArt = mmr.getEmbeddedPicture();
        if(albumArt != null)
        {

            InputStream stream = new ByteArrayInputStream(mmr.getEmbeddedPicture());
            Bitmap albumArtToReturn = BitmapFactory.decodeStream(stream);

            System.out.println("THE ALBUM ART HAS DENSITY: " + albumArtToReturn.getDensity());
            System.out.println("THE ALBUM ART HAS BYTE COUNT: " + albumArtToReturn.getByteCount());

            mmr.release(); // release the mmr to free up resources on the device.
            // return art; // return the bitmap.
            return albumArtToReturn;
        }
        else // the album art is null, return the Bitmap with the default tornado image!
        {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(currentActivity.getBaseContext()); // gets the default shared preferences
            String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.
            //currentAlbumArt = BitmapWorkshop.decodeSampledBitmapFromResource(currentActivity.getResources(), R.drawable.applogo, 200, 200);
            //mmr.release();

            Bitmap bm = decodeDefaultAlbumArtBitmap(currentActivity.getResources(), ThemeUtils.getInstance().getCurrentSoundSpoutTheme(), 400, 400); // gets the soundspout based on the theme that we are using.

//            if(theme.equalsIgnoreCase("OceanMarina"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(currentActivity.getResources(), R.drawable.soundspout_ocean_marina, 400, 400);
//            }
//            else if(theme.equalsIgnoreCase("ParadiseNightfall"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(currentActivity.getResources(), R.drawable.soundspout_paradise_nightfall, 400, 400);
//            }
//            else if(theme.equalsIgnoreCase("AquaArmy"))
//            {
//                bm = decodeDefaultAlbumArtBitmap(currentActivity.getResources(), R.drawable.soundspout_aqua_army, 400, 400);
//            }
//            else
//            {
//                bm = decodeDefaultAlbumArtBitmap(currentActivity.getResources(), R.drawable.soundspout_ocean_marina, 400, 400);
//            }

            //Bitmap bm = BitmapWorkshop.decodeSampledBitmapFromResource(currentActivity.getResources(), R.drawable.soundspout_paradise_nightfall, 400, 400); // send the bitmap to be processed so that the app will work correctly!
            //Bitmap bm = decodeDefaultAlbumArtBitmap(currentActivity.getResources(), R.drawable.soundspout_paradise_nightfall, 400, 400);

            mmr.release();

            //System.out.println("THE DEFAULT ALBUM ART HAS DENSITY: " + bm.getDensity());
            //System.out.println("THE DEFAULT ALBUM ART HAS BYTE COUNT: " + bm.getByteCount());

            return bm;
            //mmr.release(); // release the mmr to free up resources on the device.
            //return art; // return the bitmap
        }
    }

}
