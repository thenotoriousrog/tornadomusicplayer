package thenotoriousrog.tornadoplayer.Backend;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import thenotoriousrog.tornadoplayer.R;

/*
    This class is responsible for app level responses such as switching themes in the app.
 */
public class Utils {

    private static int SWITCH_THEME; // holds the value of the theme that we want to switch.
    private final static int OCEAN_MARINA_THEME = R.style.AppTheme_OceanMarina; // the Oceean Marina (default theme)
    private final static int PARADISE_NIGHTFALL_THEME = R.style.AppTheme_ParadiseNightfall; // the paradise nightfall theme.
    private final static int AQUA_ARMY_THEME = R.style.AppTheme_AquaArmy; // the aqua army theme.

    // is in charge of the switching the theme when the app starts
    public static void changeToTheme(Activity activity, int theme)
    {
        SWITCH_THEME = theme; // set the switch theme
        activity.finish(); // finish the current activity.
        activity.startActivity(new Intent(activity, activity.getClass()));
    }

    // sets the theme of the activity according to the configuration.
    public static void onActivityCreateSetTheme(Activity activity)
    {
        switch(SWITCH_THEME)
        {
            default:
            case OCEAN_MARINA_THEME:
                activity.setTheme(OCEAN_MARINA_THEME);
                break;
            case PARADISE_NIGHTFALL_THEME:
                activity.setTheme(PARADISE_NIGHTFALL_THEME);
                break;
            case AQUA_ARMY_THEME:
                activity.setTheme(AQUA_ARMY_THEME);
                break;


        }
    }

    public static int getCurrentTheme(Activity activity)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity.getBaseContext()); // gets the default shared preferences
        String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.

        if(theme.equalsIgnoreCase("OceanMarina"))
        {
            return OCEAN_MARINA_THEME;
        }
        else if(theme.equalsIgnoreCase("ParadiseNightfall"))
        {
            return PARADISE_NIGHTFALL_THEME;
        }
        else if(theme.equalsIgnoreCase("AquaArmy"))
        {
            return AQUA_ARMY_THEME;
        }
        else { // the default theme is Ocean Marina if nothing else show that theme.

            return OCEAN_MARINA_THEME;
        }

    }

}
