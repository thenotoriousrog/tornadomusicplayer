package thenotoriousrog.tornadoplayer.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import thenotoriousrog.tornadoplayer.Backend.BitmapWorkshop;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.BackgroundThreads.SongAlbumArtBackgroundThread;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Listeners.SongPopupMenuClickListener;
import thenotoriousrog.tornadoplayer.R;

import java.util.List;

;

/**
 * Created by thenotoriousrog on 5/27/17.
 * This class is in charge of creating a custom adapter that will allow us to display very specific info into ListView.
 *  This will include Song name, artist name, song duration, etc.
 *
 *  I also may need to change MusicAdapter from holding just a string, it may not be able to do that.
 *  I may have to create a pairing class to allow me to pair specific info and the arrayadapter can use that, not quite sure yet honestly.
 */
public class MusicAdapter extends ArrayAdapter<SongInfo> {

    LayoutInflater layoutInflater;

    static class SongViewHolder {
        TextView songNameText;
        TextView artistNameText;
        TextView songDurationText;
        ImageView optionsMenu; // todo: I need to change this and have the app instantiate it through a menu.
        ImageView songIcon; // song icon.

    }

    private MainUIFragment mainUIFragment; // holds a copy of the MainUIFragment that is necessary for the app to get access to items that are a little better.

    public void setMainUIFragment(MainUIFragment mainUIFragment)
    {
        this.mainUIFragment = mainUIFragment;
    }

    // constructor
    public MusicAdapter(Context context, int textViewResourceID) {
        super(context, textViewResourceID);
    }

    // constructor
    // this constructor will be able to get the list of songs and we can then be able to extract the music content from them and create a new list to display!
    // or we could do it before, whatever helps.
    public MusicAdapter(Context context, int resource, List<SongInfo> songs) {
        super(context, resource, songs);
        layoutInflater = LayoutInflater.from(getContext());
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final SongViewHolder viewHolder;

        //View v = convertView; // view that we are working with.

        View currentFocus = ((Activity) getContext()).getCurrentFocus();
        if(currentFocus != null)
        {
            currentFocus.clearFocus();
        }

        if (convertView == null) {
            //LayoutInflater vi;
           // vi = LayoutInflater.from(getContext());
            //v = vi.inflate(R.layout.songlist, null); // this is what expands the items in the list

            convertView = layoutInflater.inflate(R.layout.songlist, parent, false);

            viewHolder = new SongViewHolder();
            viewHolder.songNameText = (TextView) convertView.findViewById(R.id.SongName); // text view for song name

            // if using dark theme make the songs white.
            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                viewHolder.songNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            }
            else{ // else make the songs black.

                viewHolder.songNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }

            viewHolder.artistNameText = (TextView) convertView.findViewById(R.id.ArtistName); // text view for artist name
            viewHolder.songDurationText = (TextView) convertView.findViewById(R.id.SongDuration); // text view for the song duration, which we will have to have a count down timer for by the way!
            viewHolder.optionsMenu = (ImageView) convertView.findViewById(R.id.songOptionsMenu); // get the options menu image for the song.
            viewHolder.songIcon = (ImageView) convertView.findViewById(R.id.songListIcon); // the icon of the music player.

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (SongViewHolder) convertView.getTag();
        }

        final SongInfo info = getItem(position); // set to final because it is needed for the options menu. If it causes problems remove the final modifier and think of a different way to get this.

        if (info != null) // make sure that the SongInfo item exists after adapter gets the item
        {
            //TextView songNameText = (TextView) v.findViewById(R.id.SongName); // text view for song name
            //TextView artistNameText = (TextView) v.findViewById(R.id.ArtistName); // text view for artist name
            //TextView songDurationText = (TextView) v.findViewById(R.id.SongDuration); // text view for the song duration, which we will have to have a count down timer for by the way!

            // TODO: I need to implement the options menu through an actual menu not as an imageview view. That is causing the scrolling to become incredibly slow. I need to fix this asap!
           // final ImageView optionsMenu = (ImageView) convertView.findViewById(R.id.songOptionsMenu); // get the options menu image for the song.
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext()); // gets the default shared preferences
            String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.

            viewHolder.optionsMenu.setImageResource(ThemeUtils.getInstance().getCurrentOptionsMenu()); // sets the color options menu.

//            if(theme.equalsIgnoreCase("OceanMarina"))
//            {
//                viewHolder.optionsMenu.setImageResource(R.drawable.options_ocean_marina);
//            }
//            else if(theme.equalsIgnoreCase("ParadiseNightfall"))
//            {
//                viewHolder.optionsMenu.setImageResource(R.drawable.options_paradise_nightfall);
//            }
//            else if(theme.equalsIgnoreCase("AquaArmy"))
//            {
//                viewHolder.optionsMenu.setImageResource(R.drawable.options_aqua_army);
//            }


            // Whenever the image is clicked, we want to inflate the options menu and perform specific actions on that specific menu.
            viewHolder.optionsMenu.setOnClickListener(new View.OnClickListener() {

                // When clicked, show the options menu which will be to add song to a playlist or to edit tags so far.
                @Override
                public void onClick(View v)
                {
                    PopupMenu popup = new PopupMenu(getContext(), v); // bind the popup menu with the View that was clicked, which is the imageview on the song itself that was clicked.
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.song_popup_menu, popup.getMenu()); // inflate the menu with all of the options on their
                    SongPopupMenuClickListener songMenuClickListener = new SongPopupMenuClickListener(getContext(), viewHolder.optionsMenu, mainUIFragment, info);
                    popup.setOnMenuItemClickListener(songMenuClickListener); // listener for when a user chooses an option on the popup menu.
                    popup.show(); // display our popup menu.
                }
            });

            if (viewHolder.songNameText != null)
            {
                viewHolder.songNameText.setText(info.getSongName());


//                String text = info.getSongName();
//                String str = "";
//                if( text != null && text.length() >= 27)
//                {
//                   // str = text.substring(0,22);
//                    //viewHolder.songNameText.setText(str + "...");
//                    //viewHolder.songNameText.setEllipsize(TextUtils.TruncateAt.END);
//                }
//                else // text is not too long, we can display the whole thing.
//                {
//                   // viewHolder.songNameText.setEllipsize(TextUtils.TruncateAt.END);
//                    viewHolder.songNameText.setText(info.getSongName());
//                }

            }

            viewHolder.artistNameText.setText(info.getArtistName());

            if (viewHolder.songDurationText != null)
            {
                viewHolder.songDurationText.setText(info.getSongTime()); // this should display the actual time of the song.
                //songDurationText.setText(info.getSongDuration()); removed because we are going to display the actual time of the song now.
            }

        }

        // background thread to update the song art to be that of the album art itself.
        SongAlbumArtBackgroundThread backgroundThread = new SongAlbumArtBackgroundThread(viewHolder.songIcon, info);
        backgroundThread.startBackgroundThread();

       // v.setOnClickListener(new SelectedSongPlayer(info)); // have selected Song Player take the song info and do some work with it.
        return convertView;
    }

} // end of Music adapter class.
