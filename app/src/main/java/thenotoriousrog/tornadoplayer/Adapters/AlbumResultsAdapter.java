package thenotoriousrog.tornadoplayer.Adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import thenotoriousrog.tornadoplayer.Backend.Album;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.BackgroundThreads.AlbumArtBackgroundThread;
import thenotoriousrog.tornadoplayer.R;

public class AlbumResultsAdapter extends ArrayAdapter<Album> {


    public AlbumResultsAdapter(Context context, int textViewResourceID)
    {
        super(context, textViewResourceID);
    }

    public AlbumResultsAdapter(Context context, int resource, List<Album> songs)
    {
        super(context, resource, songs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        if(v == null)
        {
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.albumlist, parent, false);
        }

        Album currAlbum = getItem(position);

        if(currAlbum != null)
        {
            TextView album = v.findViewById(R.id.AlbumName);
            album.setText(currAlbum.getAlbumName());

            // if using dark theme make the songs white.
            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                album.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            }
            else{ // else make the songs black.

                album.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }
        }

        AlbumArtBackgroundThread backgroundThread = new AlbumArtBackgroundThread(v, currAlbum);
        backgroundThread.startBackgroundThread();

        v.setTag("ALBUM");


        return v;
    }
}
