package thenotoriousrog.tornadoplayer.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.R;

/*
    Sets the folder adapter to be used when the user is searching.
 */
public class FolderResultsAdapter extends ArrayAdapter<String> {

    public FolderResultsAdapter(Context context, int textViewResourceID)
    {
        super(context, textViewResourceID);
    }

    public FolderResultsAdapter(Context context, int resource, List<String> songs)
    {
        super(context, resource, songs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        if(v == null)
        {
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.folderlist, null);
        }

        String info = getItem(position);

        TextView folderNameText = v.findViewById(R.id.FolderName);
        ImageView optionsMenu = v.findViewById(R.id.folderOptionsMenu);
        ImageView folderIcon = v.findViewById(R.id.folderListIcon);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext()); // gets the default shared preferences
        String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.

        folderIcon.setImageResource(ThemeUtils.getInstance().getCurrentFolderColor());


            if(info != null)
        {
            folderNameText.setText(info);

            String temp = info;
            String[] arr = temp.split("/"); // split the string by /'s

            int length = arr.length; // get the length of the split string.
            String folderName = arr[length -1]; // get the last split which should hold our folder name.

            folderNameText.setText(folderName);

            // if using dark theme make the songs white.
            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                folderNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            }
            else{ // else make the songs black.

                folderNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }

            optionsMenu.setVisibility(View.GONE);
        }

        v.setTag("FOLDER");

        return v;
    }

}
