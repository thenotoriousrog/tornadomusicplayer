package thenotoriousrog.tornadoplayer.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import thenotoriousrog.tornadoplayer.Backend.Album;
import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.BitmapWorkshop;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.BackgroundThreads.AlbumArtBackgroundThread;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.R;

public class AlbumAdapter extends ArrayAdapter<Album> {

    private MainUIFragment mainUIFragment;

    public AlbumAdapter(Context context, int textViewResourceID)
    {
        super(context, textViewResourceID);
    }

    public AlbumAdapter(Context context, int resource, List<Album> albums)
    {
        super(context, resource, albums);
    }

    public void setMainUIFragment(MainUIFragment mainUIFragment)
    {
        this.mainUIFragment = mainUIFragment;
    }

    // generates the views for the artist.
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        if(v == null)
        {
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.albumlist, parent, false);
        }

        Album currAlbum = null;
        try{
            currAlbum = getItem(position);



        }catch (ClassCastException ex) {
            System.out.println("class cast error when attempting to setup album adapter ");
            ex.printStackTrace();
        }


        if(currAlbum != null)
        {
            TextView artist = v.findViewById(R.id.AlbumName);

            // if using dark theme make the songs white.
            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                artist.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            }
            else{ // else make the songs black.

                artist.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }

            artist.setText(currAlbum.getAlbumName()); // sets the name of the artist itself.

            AlbumArtBackgroundThread backgroundThread = new AlbumArtBackgroundThread(v, currAlbum);
            backgroundThread.startBackgroundThread();

//            ImageView albumArt = v.findViewById(R.id.albumWork); // sets the artwork for the user.
//
//            Random random = new Random();
//           // int num = random.nextInt(currAlbum.getSongs().size());
//
//            MediaMetadataRetriever mmr = new MediaMetadataRetriever(); // create a new MediaMetaDataRetriever to extract the album art from the current song if one exists.
//            mmr.setDataSource(currAlbum.getSongs().get(0).getSongPath()); // send in the song path to attempt to extract the song data items.
//            byte[] art = mmr.getEmbeddedPicture();
//
//            mmr.release();
//            if(art != null)
//            {
//                Glide.with(v).asBitmap()
//                        .load(art)
//                        .into(albumArt);
//            }

            //Bitmap art = BitmapWorkshop.extractAlbumArt(mainUIFragment.getActivity(), currAlbum.getSongs().get(num), 50,50);

            // artistArt.setImageBitmap(art);

        }

        return v; // the view that gets sets in the artist list.
    }

}
