package thenotoriousrog.tornadoplayer.Adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.R;

public class PlaylistResultsAdater extends ArrayAdapter<Playlist> {

    public PlaylistResultsAdater(Context context, int textViewResourceID)
    {
        super(context, textViewResourceID);
    }

    public PlaylistResultsAdater(Context context, int resource, List<Playlist> playlists)
    {
        super(context, resource, playlists);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        if(v == null)
        {
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.playlist_list, null);
        }

        Playlist playlist = getItem(position);

        if(playlist.name() != null && playlist.songs() !=null)
        {
            TextView playlistName = v.findViewById(R.id.playListName);

            if(playlistName != null)
            {
                playlistName.setText(playlist.name());

                // if using dark theme make the songs white.
                if(ThemeUtils.getInstance().usingDarkTheme())
                {
                    playlistName.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                }
                else{ // else make the songs black.

                    playlistName.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
                }

            }

            ImageView optionsMenu = v.findViewById(R.id.playlistOptionsMenu);
            optionsMenu.setVisibility(View.GONE);
        }

        v.setTag("PLAYLIST");

        return v;
    }

}
