package thenotoriousrog.tornadoplayer.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.R;

import static thenotoriousrog.tornadoplayer.Backend.ThemeUtils.getInstance;

/**
 * Created by thenotoriousrog on 3/26/18.
 * This adapter is used to implement the click behavior of playlists rather than a drag and drop scheme.
 */

public class ClickSongAdapter extends ArrayAdapter<SongInfo> {

    private boolean useThemeIcon = true; // tells the click song adapter to show the theme icon for touch.

    public ClickSongAdapter(Context context, int textViewResourceID) {
        super(context, textViewResourceID);
    }

    // constructor
    // this constructor will be able to get the list of songs and we can then be able to extract the music content from them and create a new list to display!
    // or we could do it before, whatever helps.
    public ClickSongAdapter(Context context, int resource, List<SongInfo> songs, boolean useThemeIcon) {
        super(context, resource, songs);
        this.useThemeIcon = useThemeIcon;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView; // view that we are working with.

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.playlist_songlist, null); // inflate the song list with the image of the item that we want to see.
        }

        SongInfo info = getItem(position);

        if (info != null) // make sure that the SongInfo item exists after adapter gets the item
        {
            TextView songNameText = (TextView) v.findViewById(R.id.SongName); // text view for song name
            TextView artistNameText = (TextView) v.findViewById(R.id.ArtistName); // text view for artist name
            TextView songDurationText = (TextView) v.findViewById(R.id.SongDuration); // text view for the song duration, which we will have to have a count down timer for by the way!

            // if using dark theme make the songs white.
            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                songNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                artistNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            }
            else{ // else make the songs black.

                songNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
                artistNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }

            ImageView touchIcon = v.findViewById(R.id.touchIcon);

            if(useThemeIcon)
            {
                int currTouchIcon = ThemeUtils.getInstance().getCurrentTouchIcon();
                touchIcon.setImageResource(currTouchIcon); // sets the touch icon.
            }
            else
            {
                touchIcon.setImageResource(R.drawable.remove_red);
            }

            songNameText.setText(info.getSongName());
//            if (songNameText != null) {
//                String text = info.getSongName();
//                String str = "";
//                if (text != null && text.length() >= 27) {
//                    str = text.substring(0, 22);
//                    songNameText.setText(str + "...");
//                    songNameText.setEllipsize(TextUtils.TruncateAt.END);
//                } else // text is not too long, we can display the whole thing.
//                {
//                    songNameText.setText(info.getSongName());
//                }
//            }

            if (info != null) {
                artistNameText.setText(info.getArtistName());
            }


            if (songDurationText != null) {
                songDurationText.setText(info.getSongTime()); // this should display the actual time of the song.
                //songDurationText.setText(info.getSongDuration()); removed because we are going to display the actual time of the song now.
                songDurationText.setVisibility(View.GONE); // make this invisible, users do not need to see this nor does it actually matter.
            }

        }

        return v;
    }

}
