package thenotoriousrog.tornadoplayer.Adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.BackgroundThreads.ArtistAlbumArtBackgroundThread;
import thenotoriousrog.tornadoplayer.R;

public class ArtistResultsAdapter extends ArrayAdapter<Artist> {

    public ArtistResultsAdapter(Context context, int textViewResourceID)
    {
        super(context, textViewResourceID);
    }

    public ArtistResultsAdapter(Context context, int resource, List<Artist> songs)
    {
        super(context, resource, songs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        if(v == null)
        {
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.artistlist, parent, false);
        }

        Artist currArtist = getItem(position);

        if(currArtist != null)
        {
            TextView artist = v.findViewById(R.id.ArtistName);
            artist.setText(currArtist.getArtistName());

            // if using dark theme make the songs white.
            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                artist.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            }
            else{ // else make the songs black.

                artist.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }
        }

        ArtistAlbumArtBackgroundThread backgroundThread = new ArtistAlbumArtBackgroundThread(v, currArtist);
        backgroundThread.startBackgroundThread(); // start the background thread

        v.setTag("ARTIST");

        return v;
    }

}
