package thenotoriousrog.tornadoplayer.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.R;

import java.util.List;

/**
 * Created by thenotoriousrog on 8/3/17.
 * This class is used to show the songs in the folder that was selected by the user. It is used for the pager in showing these items.
 * This class is a mix between the DragSongAdapter and the FolderSongAdapter.
 */

public class FolderSongAdapter extends ArrayAdapter<SongInfo> {

    static class SongViewHolder {
        TextView songNameText;
        TextView artistNameText;
        TextView songDurationText;
        ImageView optionsMenu; // todo: I need to change this and have the app instantiate it through a menu.
        ImageView touchIcon; // the touch icon of the playlist songlist.
    }

    // default contructor
    public FolderSongAdapter(Context context, int textViewResourceID) {
        super(context, textViewResourceID);
    }

    // this constructor will let us take in an arraylist to be able to apply the information that we need to.
    public FolderSongAdapter(Context context, int resource, List<SongInfo> songs) {
        super(context, resource, songs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        SongViewHolder viewHolder;

        View v = convertView; // view that we are working with.

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.playlist_songlist, null); // this is what expands the items in the list

            viewHolder = new SongViewHolder();
            viewHolder.songNameText = (TextView) v.findViewById(R.id.SongName); // text view for song name
            viewHolder.artistNameText = (TextView) v.findViewById(R.id.ArtistName); // text view for artist name

            // if using dark theme make the songs white.
            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                viewHolder.songNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
                viewHolder.artistNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            }
            else{ // else make the songs black.

                viewHolder.songNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
                viewHolder.artistNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }

            viewHolder.songDurationText = (TextView) v.findViewById(R.id.SongDuration); // text view for the song duration, which we will have to have a count down timer for by the way!
            viewHolder.touchIcon = v.findViewById(R.id.touchIcon); // grab the touch icon.
            //viewHolder.optionsMenu = (ImageView) v.findViewById(R.id.songOptionsMenu); // get the options menu image for the song.

            v.setTag(viewHolder);
        }
        else {
            viewHolder = (SongViewHolder) v.getTag();
        }

        SongInfo info = getItem(position);

        if (info != null) // make sure that the SongInfo item exists after adapter gets the item
        {
           // TextView songNameText = (TextView) v.findViewById(R.id.SongName); // text view for song name
            //TextView artistNameText = (TextView) v.findViewById(R.id.ArtistName); // text view for artist name
            //TextView songDurationText = (TextView) v.findViewById(R.id.SongDuration); // text view for the song duration, which we will have to have a count down timer for by the way!

            int currTouchIcon = ThemeUtils.getInstance().getCurrentTouchIcon();

            viewHolder.touchIcon.setImageResource(currTouchIcon); // set the touch icon for the image that we are working with.

            if (viewHolder.songNameText != null)
            {
                viewHolder.songNameText.setText(info.getSongName());
                String text = info.getSongName();
                String str = "";
                if( text != null && text.length() >= 27)
                {
                   // str = text.substring(0,22);
                   // viewHolder.songNameText.setText(str + "...");
                    viewHolder.songNameText.setEllipsize(TextUtils.TruncateAt.END);
                }
                else // text is not too long, we can display the whole thing.
                {
                    viewHolder.songNameText.setText(info.getSongName());
                }
            }

            if (info != null)
            {
                viewHolder.artistNameText.setText(info.getArtistName());
            }

            if (viewHolder.songDurationText != null)
            {
                viewHolder.songDurationText.setText(info.getSongTime()); // this should display the actual time of the song.
                //songDurationText.setText(info.getSongDuration()); removed because we are going to display the actual time of the song now.
            }

        }

        return v;
    }
}
