package thenotoriousrog.tornadoplayer.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.Layout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.R;

/*
    This class is responsible for determining when an item is clicked and then doing the associated response which is to send a list of one song to the music library and intiate the slide up panel to started.
 */
public class SongResultsAdapter extends ArrayAdapter<SongInfo> {

    public SongResultsAdapter(Context context, int textViewResourceID)
    {
        super(context, textViewResourceID);
    }

    public SongResultsAdapter(Context context, int resource, List<SongInfo> songs)
    {
        super(context, resource, songs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        if(v == null)
        {
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.songlist, parent, false);
        }

        SongInfo info = getItem(position);

        TextView songNameText = v.findViewById(R.id.SongName);
        TextView artistNameText = v.findViewById(R.id.ArtistName);
        TextView songDurationText = v.findViewById(R.id.SongDuration);
        ImageView optionsMenu = v.findViewById(R.id.songOptionsMenu);
        ImageView songIcon = v.findViewById(R.id.songListIcon);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext()); // gets the default shared preferences
        String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.

        songIcon.setImageResource(ThemeUtils.getInstance().getCurrentSongIcon());

        if(info != null)
        {
            // if using dark theme make the songs white.
            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                songNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            }
            else{ // else make the songs black.

                songNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }

            songNameText.setText(info.getSongName());

            String text = info.getSongName();
            String str = "";
            if( text != null && text.length() >= 27)
            {
                str = text.substring(0,22);
                songNameText.setText(str + "...");
                songNameText.setEllipsize(TextUtils.TruncateAt.END);
            }
            else // text is not too long, we can display the whole thing.
            {
                songNameText.setText(info.getSongName());
            }

            artistNameText.setText(info.getArtistName());
            songDurationText.setText(info.getSongTime());
            optionsMenu.setVisibility(View.GONE);
        }

        v.setTag("SONG"); // this tag will let us know that the item is a song.

        return v;
    }
}
