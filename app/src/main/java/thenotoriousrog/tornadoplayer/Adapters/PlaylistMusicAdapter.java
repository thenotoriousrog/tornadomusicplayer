package thenotoriousrog.tornadoplayer.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Listeners.PlaylistSongPopupMenuClickListener;
import thenotoriousrog.tornadoplayer.R;

import java.util.List;

/**
 * Created by thenotoriousrog on 8/17/17.
 * This class is suppose to be designed to behave in the exact same way as the MusicAdapter except that it is supposed to perform different actions for the songs in the playlist itself.
 */

public class PlaylistMusicAdapter extends ArrayAdapter<SongInfo> {

    static class SongViewHolder {
        TextView songNameText;
        TextView artistNameText;
        TextView songDurationText;
        ImageView songIcon;
        ImageView optionsMenu; // todo: I need to change this and have the app instantiate it through a menu.
    }

    private MainUIFragment mainUIFragment; // holds a copy of the MainUIFragment that is necessary for the app to get access to items that are a little better.
    private Playlist selectedPlaylist; // holds the playlist that was selected by this user.

    // constructor
    public PlaylistMusicAdapter(Context context, int textViewResourceID) {
        super(context, textViewResourceID);
    }

    // constructor
    // this constructor will be able to get the list of songs and we can then be able to extract the music content from them and create a new list to display!
    // or we could do it before, whatever helps.
    public PlaylistMusicAdapter(Context context, int resource, List<SongInfo> songs) {
        super(context, resource, songs);
    }

    // sets the MainUIFragment so that the Music adapter can have access to all of the fields that are needed.
    public void setMainUIFragment(MainUIFragment fragment)
    {
        mainUIFragment = fragment;
    }

    // set the playlist that was selected by the user.
    public void setSelectedPlaylist(Playlist playlist)
    {
        selectedPlaylist = playlist;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        final SongViewHolder viewHolder;

        View v = convertView; // view that we are working with.

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.songlist, parent, false); // this is what expands the items in the list

            viewHolder = new SongViewHolder();
            viewHolder.songNameText = (TextView) v.findViewById(R.id.SongName); // text view for song name

            // if using dark theme make the songs white.
            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                viewHolder.songNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            }
            else{ // else make the songs black.

                viewHolder.songNameText.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }

            viewHolder.artistNameText = (TextView) v.findViewById(R.id.ArtistName); // text view for artist name
            viewHolder.songDurationText = (TextView) v.findViewById(R.id.SongDuration); // text view for the song duration, which we will have to have a count down timer for by the way!
            viewHolder.optionsMenu = (ImageView) v.findViewById(R.id.songOptionsMenu); // get the options menu image for the song.
            viewHolder.songIcon = v.findViewById(R.id.songListIcon);

            v.setTag(viewHolder);
        }
        else {
            viewHolder = (SongViewHolder) v.getTag();
        }

        final SongInfo info = getItem(position); // set to final because it is needed for the options menu. If it causes problems remove the final modifier and think of a different way to get this.

        if (info != null) // make sure that the SongInfo item exists after adapter gets the item
        {
            //TextView songNameText = (TextView) v.findViewById(R.id.SongName); // text view for song name
            //TextView artistNameText = (TextView) v.findViewById(R.id.ArtistName); // text view for artist name
            //TextView songDurationText = (TextView) v.findViewById(R.id.SongDuration); // text view for the song duration, which we will have to have a count down timer for by the way!
            //final ImageView optionsMenu = (ImageView) v.findViewById(R.id.songOptionsMenu); // get the options menu image for the song.

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext()); // gets the default shared preferences
            String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.

            viewHolder.songIcon.setImageResource(ThemeUtils.getInstance().getCurrentSongIcon());
            viewHolder.optionsMenu.setImageResource(ThemeUtils.getInstance().getCurrentOptionsMenu());

//            if(theme.equalsIgnoreCase("OceanMarina"))
//            {
//                viewHolder.songIcon.setImageResource(R.drawable.music_note_ocean_marina_24dp);
//                viewHolder.optionsMenu.setImageResource(R.drawable.options_ocean_marina);
//            }
//            else if(theme.equalsIgnoreCase("ParadiseNightfall"))
//            {
//                viewHolder.songIcon.setImageResource(R.drawable.music_note_paradise_nightfall_24dp);
//                viewHolder.optionsMenu.setImageResource(R.drawable.options_paradise_nightfall);
//            }
//            else if(theme.equalsIgnoreCase("AquaArmy"))
//            {
//                viewHolder.songIcon.setImageResource(R.drawable.music_note_aqua_army_24dp);
//                viewHolder.optionsMenu.setImageResource(R.drawable.options_aqua_army);
//            }

            // Whenever the image is clicked, we want to inflate the options menu and perform specific actions on that specific menu.
            viewHolder.optionsMenu.setOnClickListener(new View.OnClickListener() {

                // When clicked, show the options menu which will be to add song to a playlist or to edit tags so far.
                @Override
                public void onClick(View v)
                {
                    PopupMenu popup = new PopupMenu(getContext(), v); // bind the popup menu with the View that was clicked, which is the imageview on the song itself that was clicked.
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.playlist_songs_popup_menu, popup.getMenu()); // inflate the menu with all of the options on their

                    PlaylistSongPopupMenuClickListener playlistSongMenuClickListener = new PlaylistSongPopupMenuClickListener(getContext(), viewHolder.optionsMenu, mainUIFragment, selectedPlaylist, info);
                    popup.setOnMenuItemClickListener(playlistSongMenuClickListener); // listener for when a user chooses an option on the popup menu.
                    popup.show(); // display our popup menu.
                }
            });

            if (viewHolder.songNameText != null)
            {
                String text = info.getSongName();
                String str = "";
                if( text != null && text.length() >= 27)
                {
//                    str = text.substring(0,22);
//                    viewHolder.songNameText.setText(str + "...");
//                    viewHolder.songNameText.setEllipsize(TextUtils.TruncateAt.END);
                    viewHolder.songNameText.setText(info.getSongName());
                }
                else // text is not too long, we can display the whole thing.
                {
                    viewHolder.songNameText.setText(info.getSongName());
                }
            }

            if (info != null)
            {
                viewHolder.artistNameText.setText(info.getArtistName());
            }

            if (viewHolder.songDurationText != null)
            {
                viewHolder.songDurationText.setText(info.getSongTime()); // this should display the actual time of the song.
                //songDurationText.setText(info.getSongDuration()); removed because we are going to display the actual time of the song now.
            }
        }


        // v.setOnClickListener(new SelectedSongPlayer(info)); // have selected Song Player take the song info and do some work with it.
        return v;
    }
}
