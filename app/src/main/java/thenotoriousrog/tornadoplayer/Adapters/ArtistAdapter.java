package thenotoriousrog.tornadoplayer.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.BitmapWorkshop;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.BackgroundThreads.ArtistAlbumArtBackgroundThread;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.R;

public class ArtistAdapter extends ArrayAdapter<Artist>  {

    private MainUIFragment mainUIFragment;
    private ArrayList<Boolean> imageStatus = new ArrayList<>();

    public ArtistAdapter(Context context, int textViewResourceID)
    {
        super(context, textViewResourceID);
    }

    public ArtistAdapter(Context context, int resource, List<Artist> artists)
    {
        super(context, resource, artists);

        // iterate through all of the songs and add false as the starting image status.
        for(int i = 0; i < artists.size(); i++)
        {
            imageStatus.add(false);
        }
    }

    public void setMainUIFragment(MainUIFragment mainUIFragment)
    {
        this.mainUIFragment = mainUIFragment;
    }

    // generates the views for the artist.
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View v = convertView;

        if(v == null)
        {
            LayoutInflater li;
            li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.artistlist, parent, false);
        }

        Artist currArtist = getItem(position);

        if(currArtist != null)
        {
            TextView artist = v.findViewById(R.id.ArtistName);
            artist.setText(currArtist.getArtistName()); // sets the name of the artist itself.

            // if using dark theme make the songs white.
            if(ThemeUtils.getInstance().usingDarkTheme())
            {
                artist.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            }
            else{ // else make the songs black.

                artist.setTextColor(ContextCompat.getColor(getContext(), R.color.black));
            }

            ImageView art = v.findViewById(R.id.artistWork);

            boolean defaultImage = false; // if true, we need to load the album art image.

            ArtistAlbumArtBackgroundThread backgroundThread = new ArtistAlbumArtBackgroundThread(v, currArtist);
            backgroundThread.startBackgroundThread(); // start the background thread

//            // if true, we need run the background thread to show the correct image!
//            if(!imageStatus.get(position))
//            {
//                imageStatus.remove(position);
//                imageStatus.add(position, true);
//
//            }
//            else // status is false, we need to revert the image.
//            {
//                imageStatus.remove(position);
//                imageStatus.add(position, false);
//
//                //art.setImageBitmap(null); // set the image to be null to show nothing.
//            }



//            ImageView artistArt = v.findViewById(R.id.artistWork); // sets the artwork for the user.
//
//            //Random random = new Random();
//            //int num = random.nextInt(currArtist.getSongs().size());
//
//            MediaMetadataRetriever mmr = new MediaMetadataRetriever(); // create a new MediaMetaDataRetriever to extract the album art from the current song if one exists.
//            mmr.setDataSource(currArtist.getSongs().get(0).getSongPath()); // send in the song path to attempt to extract the song data items.
//            byte[] albumArt = mmr.getEmbeddedPicture();
//
//            mmr.release();
//            if(albumArt != null)
//            {
//                Glide.with(v)
//                        .load(albumArt)
//                        .into(artistArt);
//            }




        }
        return v; // the view that gets sets in the artist list.
    }

}
