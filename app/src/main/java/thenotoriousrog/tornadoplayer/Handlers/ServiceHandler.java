package thenotoriousrog.tornadoplayer.Handlers;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.Services.PlayerService;

/**
 * Created by thenotoriousrog on 3/26/18.
 *
 * This class is responsible for communicating to and from the UI to and player service.
 */

public class ServiceHandler extends Handler {

    private static FlyupPanelListener flyupPanelListener; // a static reference to be able to communicate with the flyup panel listeneer
    private PlayerServiceHandler playerServiceHandler; // the player service handler will communicate with the player service directly.
    private UIHandler uiHandler; // the UI handler that we will be communicating through
    private HandlerThread playerServiceThread; // the handler thread allows for easy binding with handler's throughout the application
    private HandlerThread musicPlayerThread; // the handler to handle messages from the Music Player Handlers
    private static ServiceHandler instance; // holds an instance of the service handler

    public ServiceHandler(Looper looper)
    {
        super(looper);

        musicPlayerThread = new HandlerThread(Constants.HANDLER.MUSIC_PLAYER_HANDLER, Process.THREAD_PRIORITY_BACKGROUND);
        musicPlayerThread.start();

        playerServiceThread = new HandlerThread(Constants.HANDLER.PLAYER_SERVICE_HANDLER, Process.THREAD_PRIORITY_BACKGROUND);
        playerServiceThread.start();

        System.out.println("Is the playerservice handler null?" + playerServiceHandler);

        instance = this;
    }

    // gets the instance of the service handler to be used in the application.
    public static ServiceHandler getInstance()
    {
        return instance;
    }



    // sends a message to the PlayerServiceHandler to begin playing a message.
    private void sendPlaySelectedSongMsg(Message msg)
    {
        Bundle data = msg.getData();
        String songPath = data.getString(Constants.ACTION.PLAY_SONG);

        data = new Bundle();
        data.putString(Constants.ACTION.PLAY_SONG, songPath); // send the song path that PlayerServiceHandler needs to setup.
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.PLAY_SELECTED; // tell the player service handler to play the selected song.
        m.setData(data);
        playerServiceHandler.sendMessage(m); // send the message for player Service Handler to begin playing the song.
    }

    private void sendReplayMsg()
    {
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.REPLAY; // tells the player service to replay the song.
        playerServiceHandler.sendMessage(m);
    }

    // sends a pause message to player service as well as the UI Handler.
    private void sendPauseMsg()
    {
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.PAUSE; // tells the player service to pause the song.
        playerServiceHandler.sendMessage(m);
    }

    // sends a message to the player service to initiate a play command.
    private void sendPlayMsg()
    {
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.PLAY; // tells the player service to pause the song.
        playerServiceHandler.sendMessage(m);
    }

    // sends a message to the player service to initiate a skip command
    private void sendSkipMsg()
    {
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.SKIP;
        playerServiceHandler.sendMessage(m);
    }

    private void sendPrevMsg()
    {
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.PREV;
        playerServiceHandler.sendMessage(m);
    }

    // sends the message to turn shuffle on.
    private void sendShuffleOnMsg()
    {
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.SHUFFLE_ON;
        playerServiceHandler.sendMessage(m);
    }

    // sends the message to turn shuffle off.
    private void sendShuffleOffMsg()
    {
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.SHUFFLE_OFF;
        playerServiceHandler.sendMessage(m);
    }

    private void sendRepeatOnMsg()
    {
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.REPEAT_ON;
        playerServiceHandler.sendMessage(m);
    }

    private void sendRepeatOffMsg()
    {
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.REPEAT_OFF;
        playerServiceHandler.sendMessage(m);
    }

    private void sendNewListMsg()
    {
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.NEW_LIST;
        playerServiceHandler.sendMessage(m);
    }

    private void sendSearchSelectedListMsg()
    {
        Message m = playerServiceHandler.obtainMessage();
        m.arg1 = Constants.COMMAND.SEARCH_SELECTED;
        playerServiceHandler.sendMessage(m);

//        Message m2 = playerServiceHandler.obtainMessage();
//        m.arg1 = Constants.COMMAND.SEARCH_SELECTED;
//        uiHandler.sendMessage(m);
    }

    private void sendUIPauseMsg()
    {
        Message msg = uiHandler.obtainMessage();
        msg.arg1 = Constants.UI.UI_PAUSE;
        uiHandler.sendMessage(msg); // send the message to the UI Handler to keep track of the things.
    }

    private void sendUIPlayMsg()
    {
        Message msg = uiHandler.obtainMessage();
        msg.arg1 = Constants.UI.UI_PLAY;
        uiHandler.sendMessage(msg);
    }

    private void sendUIUpdateMsg(Message msg)
    {
        Message newMsg = uiHandler.obtainMessage();
        newMsg.arg1 = Constants.UI.UI_UPDATE_SLIDER_INFO;
        Bundle data = new Bundle();
        data.putString("PrevSongPath", msg.getData().getString("PrevSongPath")); // get the prev song path from the data out of the msg
        data.putString("CurrSongPath", MusicLibrary.getCurrSong().getSongPath());
        data.putString("NextSongPath", MusicLibrary.peekNextSong().getSongPath());
        data.putBoolean("restartTimer", msg.getData().getBoolean("restartTimer"));
        data.putBoolean("flyPanelUp", msg.getData().getBoolean("flyPanelUp", false)); // the default value is false just in case something doesn't get selected correctly.
        newMsg.setData(data);
        uiHandler.sendMessage(newMsg); // send the update info to the UI to have the information updated.
    }

    private void sendStopMsg()
    {
       // if(uiHandler != null)
       // {
            Message msg = uiHandler.obtainMessage();
            msg.arg1 = Constants.UI.UI_STOP;
            uiHandler.sendMessage(msg);
        //}

    }

    @Override
    public void handleMessage(Message msg)
    {
        System.out.println("received message in Service Handler!");
        playerServiceHandler = PlayerServiceHandler.getInstance(); // grabs the instance of the playerservicehandler.
        uiHandler = UIHandler.getInstance(); // get the instance of the UIHandler.

        switch (msg.arg1)
        {
            case Constants.COMMAND.PLAY_SELECTED:
                sendPlaySelectedSongMsg(msg);
                break;

            case Constants.COMMAND.REPLAY:
                sendReplayMsg();
                break;

            case Constants.COMMAND.PAUSE:
                sendPauseMsg();
                break;

            case Constants.COMMAND.PLAY:
                sendPlayMsg();
                break;

            case Constants.COMMAND.SKIP:
                sendSkipMsg();
                break;

            case Constants.COMMAND.PREV:
                sendPrevMsg();
                break;

            case Constants.COMMAND.SHUFFLE_ON:
                sendShuffleOnMsg();
                break;

            case Constants.COMMAND.SHUFFLE_OFF:
                sendShuffleOffMsg();
                break;

            case Constants.COMMAND.REPEAT_ON:
                sendRepeatOnMsg();
                break;

            case Constants.COMMAND.REPEAT_OFF:
                sendRepeatOffMsg();
                break;

            case Constants.COMMAND.NEW_LIST:
                sendNewListMsg();
                break;

            case Constants.COMMAND.SEARCH_SELECTED:
                sendSearchSelectedListMsg();
                break;

            case Constants.UI.UI_PAUSE:
                sendUIPauseMsg();
                break;

            case Constants.UI.UI_PLAY:
                sendUIPlayMsg();
                break;

            case Constants.UI.UI_UPDATE_SLIDER_INFO:
                sendUIUpdateMsg(msg);
                break;

            case Constants.UI.UI_STOP:
                sendStopMsg();
                break;
        }
    }

}
