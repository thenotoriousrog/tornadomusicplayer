package thenotoriousrog.tornadoplayer.Handlers;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.UI.FlyupPanelController;

/**
 * Created by thenotoriousrog on 3/30/18.
 *
 * This Handler communicates with the UI elements such as the flyup panel and ensures that the correct items are being shown if a user decides to interact through the notifications instead of the panel.
 */

public class UIHandler extends Handler {

    private static UIHandler instance = null; // an instance to the UI Handler.
    private FlyupPanelListener flyupPanelListener; // the flyup panel listener being used to communicate
    private Looper looper;


    public UIHandler(FlyupPanelListener flyupPanelListener, Looper looper)
    {
        this.flyupPanelListener = flyupPanelListener;
        this.looper = looper;
        instance = this;
    }

    public static UIHandler getInstance()
    {
        return instance;
    }


    /**
     * This method sends an update to the flyup panel layout to take care of all of the work.
     * Note: this cannot be done in the background because it involves touching all of the UI elements, which can only be done in the MainUIThread
     * @variable currSong: this is the song that's playing. That could be a song that's been on for a while or a song that was recently skipped and just started playing. Either way, it's the song that is playing in media player.
     * @variable prevSong: this is only not going to be null if a prev action was selected, if so, then we need to update the flyup panel accordingly.
     */
    private void updateSliderInfo(Message msg)
    {
        Bundle info = msg.getData();
        String currSongPath = info.getString("CurrSongPath");
        String nextSongPath = info.getString("NextSongPath");
        String prevSongPath = info.getString("PrevSongPath");
        boolean restartTimer = info.getBoolean("restartTimer");
        boolean flyPanelUp = info.getBoolean("flyPanelUp"); // holds the value telling the system to fly the panel up or not.

        SongInfo currSong = new SongInfo();
        currSong.getandSetSongInfo(currSongPath);

        SongInfo nextSong = new SongInfo();
        nextSong.getandSetSongInfo(nextSongPath);

        if(prevSongPath != null) // update flyup panel accordingly
        {
            SongInfo prevSong = new SongInfo();
            prevSong.getandSetSongInfo(prevSongPath);

            flyupPanelListener.updateSliderData(prevSong, nextSong); // prevSng will be shown and played, and the current song that is playing now we be shown to be next.
            flyupPanelListener.refreshSliderLayout(); // force fields to


//            if (!MusicLibrary.isShuffleOn()) // if shuffle is off than we want to make sure that we show the next song when they go back.
//            {
//                flyupPanelListener.updateSliderData(prevSong, nextSong); // prevSng will be shown and played, and the current song that is playing now we be shown to be next.
//                flyupPanelListener.refreshSliderLayout(); // force fields to update.
//            } else // shuffle is on.
//            {
//                //SongInfo nextSong = MusicLibrary.peekNextSong(); // this will allow us to see the next song to be played when on shuffle this is important.
//                flyupPanelListener.updateSliderData(prevSong, nextSong);
//                flyupPanelListener.refreshSliderLayout(); // force fields to update.
//            }
        }
        else // update the flyup panel normally.
        {
            System.out.println("Updating the slider layout now!");
            flyupPanelListener.updateSliderData(currSong, nextSong); // updates the slider's information. Very important!
            flyupPanelListener.refreshSliderLayout();
        }


        if(restartTimer) // some operations do not warrant a restarting of timer for example when turning shuffle on and off, this we need this check to see if the command warrants a restart.
        {
            System.out.println("Should be restarting timer now!");
            flyupPanelListener.restartCountdownTimer(currSong); // restarts the countdowntimer with the now currSong.
        }

        if(!MusicLibrary.isMusicPaused()) // if not paused, ensure the play button is showing.
        {
            flyupPanelListener.play(); // ensures that the pause icon is shown instead of the play itself. Very important!
        }

        if(flyPanelUp) // if search was selected, this value will be true flying the panel up.
        {
            FlyupPanelController.flyPanelUp(); // fly the panel up always.
        }
    }

    // stops the UI by killing all background threads.
    // so far, only the countdown timer is the thread that we need to worry about.
    // fixme: for some reason, the service get's killed now even without killing the thread, decide if it's worth having this extra message being sent to the OS or not.
    private void stopUI()
    {
        flyupPanelListener.cancelAndFinishTimer(); // cancels and finishes the timer.

        // **Note: if we want to store the state of the last song that was played we will literally store songs as we go. This is a big deal. However, right now killing the service hides the panel.
        flyupPanelListener.stopSeekBar(); // stops the seekbar if service is killed.
        FlyupPanelController.flyPanelHiden(); // hides the flyup panel
    }

    // handles message directly through the services.
    @Override
    public void handleMessage(Message msg)
    {
        System.out.println("we have received a message in the UI handler");
        switch(msg.arg1)
        {
            case Constants.UI.UI_PAUSE:
                flyupPanelListener.pause();
                break;

            case Constants.UI.UI_PLAY:
                flyupPanelListener.play();
                break;

            case Constants.UI.UI_UPDATE_SLIDER_INFO:
                updateSliderInfo(msg);
                break;

            case Constants.UI.UI_STOP:
                stopUI();
        }
    }


}
