package thenotoriousrog.tornadoplayer.Handlers;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Services.PlayerService;

/**
 * Created by thenotoriousrog on 3/30/18.
 *
 * This class is in control of handling messages and communicating with the PlayerService itself.
 * This class is a singleton class because this class needs to communciate with the player service and is thus only created by the PlayerService itself
 */

public class PlayerServiceHandler extends Handler {

    private static PlayerServiceHandler instance = null;
    private PlayerService playerService;
    private Looper looper; // the looper used in this PlayerServiceHandler


    public PlayerServiceHandler(PlayerService playerService, Looper looper)
    {
        super(looper);
        this.playerService = playerService;
        this.looper = looper;
        instance = this; // creates an instance to be used statically.
    }

    public static PlayerServiceHandler getInstance()
    {
        return instance;
    }

    // TODO: need to create a new class so that I can access songs and be able to update the notification correctly. This is incredibly important to remove this from the UI elements

    // calls the play command from within PlayerService to play the selected song from within the player service.
    private void playSelectedSong(Message msg)
    {
        System.out.println("Trying to play song in Player Service now");
        Bundle data = msg.getData();
        String songPath = data.getString(Constants.ACTION.PLAY_SONG); // gets the song path from the message and stores it into the list.
        playerService.playSong(songPath); // starts playing the song.
        playerService.updateNotification();
    }

    private void replaySong()
    {
        playerService.replaySong();
    }

    private void pause()
    {
        playerService.pause();
    }

    private void play()
    {
        playerService.play();
    }

    private void skip()
    {
        playerService.skip();
    }

    private void prev()
    {
        playerService.prev();
    }

    private void shuffleOn()
    {
        playerService.startShuffling();
    }

    private void shuffleOff()
    {
        playerService.stopShuffling();
    }

    private void repeatOn()
    {
        playerService.startRepeating();
    }

    private void repeatOff()
    {
        playerService.stopRepeating();
    }

    private void applyNewList()
    {
        playerService.applyNewList(); // applies the new list sent into the Music Library.
    }

    private void searchSelected()
    {
        playerService.searchSelected();
    }

    // Handles messages that have been received inside of the handler.
    @Override
    public void handleMessage(Message msg)
    {
        System.out.println("we are received a msg inside Player Service Handler!");

        switch (msg.arg1)
        {
            case Constants.COMMAND.PLAY_SELECTED:
                playSelectedSong(msg);
                break;

            case Constants.COMMAND.REPLAY:
                replaySong();
                break;

            case Constants.COMMAND.PAUSE:
                pause();
                break;

            case Constants.COMMAND.PLAY:
                play();
                break;

            case Constants.COMMAND.SKIP:
                skip();
                break;

            case Constants.COMMAND.PREV:
                prev();
                break;

            case Constants.COMMAND.SHUFFLE_ON:
                shuffleOn();
                break;

            case Constants.COMMAND.SHUFFLE_OFF:
                shuffleOff();
                break;

            case Constants.COMMAND.REPEAT_ON:
                repeatOn();
                break;

            case Constants.COMMAND.REPEAT_OFF:
                repeatOff();
                break;

            case Constants.COMMAND.NEW_LIST:
                applyNewList();
                break;

            case Constants.COMMAND.SEARCH_SELECTED:
                searchSelected();
                break;
        }
    }

}
