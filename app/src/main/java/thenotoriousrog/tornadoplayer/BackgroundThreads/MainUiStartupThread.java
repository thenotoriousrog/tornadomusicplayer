package thenotoriousrog.tornadoplayer.BackgroundThreads;

import android.os.Bundle;

import java.io.File;
import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.Backend.Album;
import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SerializeObject;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.Sorters;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;

/**
 * Created by thenotoriousrog on 3/10/18.
 */

public class MainUiStartupThread implements Runnable {

    private MainUIFragment mainUIFragment;
    private Runnable uiUpdater;
    private Thread backgroundThread;

    public MainUiStartupThread(MainUIFragment mainUIFragment)
    {
        this.mainUIFragment = mainUIFragment;

        backgroundThread = new Thread(this);
    }

    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    private void initiateUIUpdater()
    {
        uiUpdater = new Runnable()
        {

            @Override
            public void run()
            {
                // update the ui here.
            }
        };
    }

    private void postUIUpdate()
    {
        // post something to update the ui here. Although, nothing may need to happen here.
    }

    public boolean isBackgroundThreadAlive()
    {
        return backgroundThread.isAlive();
    }

    @Override
    public void run()
    {
        // todo: fix this app so that when a user rotates the screen it will just show the music app and not the loading screen.

        //System.out.println("Is saved instance state null? " + savedInstanceState);

        System.out.println("WE ARE IN ONCREATE IN THE MAINUIFRAGMENT! IT IS DOING THE WORK WE NEED IT TO!");

        // For testing if everything is null or not...
        //System.out.println("Is songs in MainUIFragment null: " + songs);
       // System.out.println("Is folders in MainUIFragment null: " + folders);
        //System.out.println("Is the SongInfoList null: " + songInfoList);

        mainUIFragment.extractAndSetSortFilters(); // have the app extract and set the sort filters needed for the app to function properly.

        Bundle args = mainUIFragment.getArguments(); // todo: make sure in MainActivity we actually send the correct arguments otherwise this class will crash.
        ArrayList<String> allSongs = args.getStringArrayList("songs");
        mainUIFragment.setSongs(allSongs); // get the raw song paths from the MainActivity.

        //System.out.println("The number of songs grabbed from InitializerActivity: " + songs.size());

        // TODO: sort folders by their name alphabetically!
        ArrayList<String> folders = args.getStringArrayList("folders");
        mainUIFragment.setFolders(folders); // get the raw folder paths from the MainActivity.
        Sorters.sortFolders(mainUIFragment.getFolders()); // sorts the folders by their names.

        ArrayList<SongInfo> songInfoList = args.getParcelableArrayList("songInfoList"); // get the songInfoList from the mainActivity to begin displaying the songs in the correct order.
        mainUIFragment.setSongInfoList(songInfoList);
        mainUIFragment.sort(songInfoList); // this will sort the songInfoList based on the user's filter preference.

        String musicFolderPath = args.getString("musicFolderPath"); // get the music folder path that the user has chosen.

        // TODO: sort playlists by their name alphabetically.
        mainUIFragment.setPlaylistNames(args.getStringArrayList("playlistNames")); // get the arraylist of all playlists.

        // simply for testing whether or not a playlist was parcelized and sent off correctly or not yet.
        System.out.println("LOOK HERE FOR TESTING TO SEE IF THIS WORKED OUT CORRECTLY!!");
        System.out.println("Did we end up receiving our Playlist arraylist?: " + args.getParcelableArrayList("playlists"));


        ArrayList<Playlist> playlists = args.getParcelableArrayList("playlists"); // retrieve the playlists from InitializerActivity class.
        System.out.println("Is the playlists we retrieved from InitializerActivity null?" + playlists);

        if(playlists != null) // if the playlists exist, then set the new Playlists to be used for the entire class.
        {
            System.out.println("The Playlist Arraylist has been set!!");

            Sorters.sortPlaylistsName(playlists); // sort the playlists that we have received making the list more orderly.
            mainUIFragment.setPlaylists(playlists); // set the playlists that was retrieved from InitializerActivity class.
        }

        ArrayList<Artist> artistList = new ArrayList<>(); // this list holds all of the artists
        ArrayList<Album> albumList = new ArrayList<>(); // this list holds all of the albums

        String artistsSer = SerializeObject.ReadSettings(mainUIFragment.getActivity(), "ArtistsList.dat");
        if(artistsSer != null && !artistsSer.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(artistsSer); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                artistList = (ArrayList<Artist>) obj; // set our songInfo list.
            }
        }
        Sorters.sortArtists(artistList); // sort the artists in alphabetical order.


        String albumsSer = SerializeObject.ReadSettings(mainUIFragment.getActivity(), "AlbumsList.dat");
        if(albumsSer != null && !albumsSer.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(albumsSer); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                albumList = (ArrayList<Album>) obj; // set our songInfo list.
            }
        }
        Sorters.sortAlbums(albumList); // sort the albums in alphabetical order.

        mainUIFragment.setArtists(artistList);
        mainUIFragment.setAlbums(albumList);

        //mainUIFragment.setArtists(args.<Artist>getParcelableArrayList("artists")); // sets the artists list for the mainUIFragment.
        //mainUIFragment.setArtists(args.<Artist>getParcelableArrayList("albums")); // sets the albums list for the mainUIFragment.

        File musicDirectory = new File(musicFolderPath); // convert the music folder into a path.

       // MusicLibrary musicLibrary = new MusicLibrary(allSongs, songInfoList, folders, playlists); // creates our music library to be used throughout the app.

        // todo: somehow implement a loading screen until this the app finishes loading up all of the song information. Important.
        // NOTE: if the app is breaking and not for the reason of artists tab implementation, then uncomment this line and add it back into the mainUI fragment
       // mainUIFragment.evaluateAndCreateLists(musicDirectory); // check songs and folders and make sure it is correct and then create the appropriate lists which will be used in TabLayout later!
        initiateUIUpdater();
        postUIUpdate();
    }
}
