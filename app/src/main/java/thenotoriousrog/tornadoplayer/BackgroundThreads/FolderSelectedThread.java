package thenotoriousrog.tornadoplayer.BackgroundThreads;

import android.content.Intent;
import android.os.Bundle;
import android.widget.AdapterView;

import thenotoriousrog.tornadoplayer.Activities.OpenFolderandSongs;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Listeners.FolderOnClickListener;

/**
 * Created by thenotoriousrog on 3/10/18.
 */

public class FolderSelectedThread implements Runnable {

    private MainUIFragment mainUIFragment;
    public final int OPEN_FOLDER_INTENT = 100; // this is the result code that we want to pass back to the intent that way we know to look for specific intent codes.
    private int position;
    private AdapterView<?> parent;
    private Thread backgroundThread;
    private Runnable uiUpdater;
    private volatile boolean stopped = false; // this boolean allows the thread to continue running. If stopped, we need to stop the thread from running.


    public FolderSelectedThread(MainUIFragment mainUIFragment, int position, AdapterView<?> parent)
    {
        this.mainUIFragment = mainUIFragment;
        this.position = position;
        this.parent = parent;

        this.backgroundThread = new Thread(this);
    }

    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    public boolean isBackgroundThreadAlive()
    {
        return backgroundThread.isAlive();
    }

    // tells the thread to stop if possible
    public void stopThread()
    {
        stopped = true;
    }

    private void initiateUIUpdater()
    {
        uiUpdater = new Runnable() {

            @Override
            public void run()
            {
                // ui update work here.
            }
        };
    }

    private void postUIUpdate()
    {
        // post the ui update here.
    }

    @Override
    public void run()
    {
        Intent folderSelectedIntent = new Intent(mainUIFragment.getActivity(), OpenFolderandSongs.class);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("FoldersList", mainUIFragment.getFolders());
        bundle.putInt("SelectedFolderPosition", position);
        System.out.println("item selected posoition: " + parent.getSelectedItemPosition());
        System.out.println("position of the folder that was selected is: " + position);
        folderSelectedIntent.putExtra("FolderListBundle", bundle);

        // if soppted was called. We need to
        if(!stopped)
        {
            mainUIFragment.startActivityForResult(folderSelectedIntent, OPEN_FOLDER_INTENT); // start the activity.
        }

        // Initiate the ui updater and post the ui update if needed in here.
    }
}
