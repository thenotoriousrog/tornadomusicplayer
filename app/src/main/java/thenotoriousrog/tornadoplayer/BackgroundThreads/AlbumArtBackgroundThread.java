package thenotoriousrog.tornadoplayer.BackgroundThreads;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import thenotoriousrog.tornadoplayer.Backend.Album;
import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.BitmapWorkshop;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.R;

public class AlbumArtBackgroundThread implements Runnable {

    private View viewToUpdate; // this is the view that we want to update the album art in.
    private Album currAlbum;
    private Runnable uiUpdater; // this is the thread in which the ui is updated.
    private Thread backgroundThread; // this thread is what runs the application.

    public AlbumArtBackgroundThread(View viewToUpdate, Album currAlbum)
    {
        this.viewToUpdate = viewToUpdate;
        this.currAlbum = currAlbum;
        this.backgroundThread = new Thread(this); // the thread that will start doing work in the background.
    }

    // starts the background thread.
    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    private void inititiateUIUpdater(final Bitmap art, final ImageView albumArt)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(viewToUpdate.getContext()); // gets the default shared preferences
        final String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.

        uiUpdater = new Runnable() {
            @Override
            public void run()
            {
//                Glide.with(viewToUpdate)
//                        .load(art)
//                        .into(albumArt);
                if(art != null) // if not null apply the album art.
                {
                    albumArt.setAlpha(0.0f); // set invisible.
                    albumArt.setImageBitmap(art); // set the bitmap for the image.
                    albumArt.animate().alpha(1.0f); // fade in album art.
                }
                else // apply a default album art for the user.
                {
                    albumArt.setImageResource(ThemeUtils.getInstance().getCurrentAlbumColor()); // gets the current color of album.
//                    if(theme.equalsIgnoreCase("OceanMarina"))
//                    {
//                        albumArt.setImageResource(R.drawable.album_ocean_marina);
//                    }
//                    else if(theme.equalsIgnoreCase("ParadiseNightfall"))
//                    {
//                        albumArt.setImageResource(R.drawable.album_paradise_nightfall);
//                    }
//                    else if(theme.equalsIgnoreCase("AquaArmy"))
//                    {
//                        albumArt.setImageResource(R.drawable.album_aqua_army);
//                    }
                }


            }
        };
    }

    // posts the ui update to update the ui element.
    private void postUIUpdate()
    {
        viewToUpdate.post(uiUpdater);
    }

    // this is where most of the background work is going to take place.
    @Override
    public void run()
    {
        if(currAlbum != null)
        {
            ImageView albumArt = viewToUpdate.findViewById(R.id.albumWork); // sets the artwork for the user.

            try
            {
                MediaMetadataRetriever mmr = new MediaMetadataRetriever(); // create a new MediaMetaDataRetriever to extract the album art from the current song if one exists.
                mmr.setDataSource(currAlbum.getSongs().get(0).getSongPath()); // send in the song path to attempt to extract the song data items.
                byte[] art = mmr.getEmbeddedPicture();

                mmr.release();
                if(art != null) // initialize the album art only if the album art is not null
                {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(art, 0, art.length); // converts the byte[] to a bitmap.
                    inititiateUIUpdater(bitmap, albumArt); // initialize the ui updater with the correct values.
                }
                else
                {
                    inititiateUIUpdater(null, albumArt);
                }

                if(uiUpdater != null) // only start the ui update if the ui updater has been initialized.
                {
                    postUIUpdate();
                }
            }catch (IllegalArgumentException ex) { // illegal argument exception likely means something is wrong with the file path, use the default album art instead.
                inititiateUIUpdater(null, albumArt);

                if(uiUpdater != null)
                {
                    postUIUpdate();
                }
            } catch(IndexOutOfBoundsException ex) {
                inititiateUIUpdater(null, albumArt);

                if(uiUpdater != null){
                    postUIUpdate();
                }
            }

        }
    }
}
