package thenotoriousrog.tornadoplayer.BackgroundThreads;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Looper;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListView;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import thenotoriousrog.tornadoplayer.Activities.ArtistSelectedSongPlayer;
import thenotoriousrog.tornadoplayer.Activities.InitializerActivity;
import thenotoriousrog.tornadoplayer.Activities.PlaylistSelectedSongPlayer;
import thenotoriousrog.tornadoplayer.Activities.SelectedSongPlayer;
import thenotoriousrog.tornadoplayer.Adapters.AlbumAdapter;
import thenotoriousrog.tornadoplayer.Adapters.ArtistAdapter;
import thenotoriousrog.tornadoplayer.Adapters.FolderAdapter;
import thenotoriousrog.tornadoplayer.Adapters.MainUIPagerAdapter;
import thenotoriousrog.tornadoplayer.Adapters.MusicAdapter;
import thenotoriousrog.tornadoplayer.Adapters.PlayListAdapter;
import thenotoriousrog.tornadoplayer.Adapters.PlaylistMusicAdapter;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.Sorters;
import thenotoriousrog.tornadoplayer.Backend.SoundSpoutSpeech;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Listeners.AddPlaylistButtonClickListener;
import thenotoriousrog.tornadoplayer.Listeners.FolderOnClickListener;
import thenotoriousrog.tornadoplayer.Listeners.OnScrollListener;
import thenotoriousrog.tornadoplayer.R;

/**
 * Created by thenotoriousrog on 3/10/18.
 * In charge of running the UI and performing background tasks and then updating UI once it's complete.
 */

public class MainUiInitializationThread implements Runnable {

    private Thread backgroundThread;
    private ListView songsList;
    private ListView foldersList;
    private ListView folderSongsList;
    private ListView playListList;
    private ListView artistsList; // holds all of the artist "folders" themselves.
    private ListView artistSongList; // holds all of the songs within the artist "folder "
    private ListView albumsList; // holds all of the albums
    private PlayListAdapter playListAdapter;
    private Context context;
    private InitializerActivity initializerActivity;
    private View mainView;
    private MainUIFragment mainUIFragment;
    public final int OPEN_FOLDER_INTENT = 100; // this is the result code that we want to pass back to the intent that way we know to look for specific intent codes.
    private com.github.clans.fab.FloatingActionMenu addPlayListMenu;
    private Runnable uiUpdater;

    public MainUiInitializationThread(MainUIFragment mainUIFragment, com.github.clans.fab.FloatingActionMenu addPlayListMenu, View mainView)
    {
        this.mainUIFragment = mainUIFragment;
        this.addPlayListMenu = addPlayListMenu;
        this.mainView = mainView;

        this.backgroundThread = new Thread(this);
    }

    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    private void initiateUIUpdater()
    {
        uiUpdater = new Runnable()
        {
            @Override
            public void run()
            {
                // TODO: any UI updates that need to happen will occurr in here. This is where the snackbar will go and the initialization with the tablayout. and anything else that modifies the UI
                mainUIFragment.getViewPager().setAdapter(mainUIFragment.getMainUIPagerAdapter());
                mainUIFragment.getTabLayout().setupWithViewPager(mainUIFragment.getViewPager());
                Snackbar.make(mainUIFragment.getFlyupPanelController().getFlyupPanel(), SoundSpoutSpeech.grabWelcomeMessage(), Snackbar.LENGTH_SHORT).show(); // show the snackbar welcome message for a short time.


                InitializerActivity activity = (InitializerActivity) mainUIFragment.getActivity(); // get an instance of the initializer activity.
                if(activity.isBoundToService() && MusicLibrary.isActivelyPlaying())
                {
                    mainUIFragment.getFlyupPanelController().showFlyUpPanel(); // show the flyup panel in it's collapsed form.
                    mainUIFragment.getFlyupPanelListener().resumePlayer(); // resume the music player with the current song information.

                    if(MusicLibrary.isActivelyPlaying()) // if actively playing, we know that this is reset of the activity and thus we need to update the CountdownTimer.
                    {
                        mainUIFragment.getFlyupPanelListener().restartCountdownTimer(MusicLibrary.getCurrSong());
                    }

                    mainUIFragment.applyTheme(); // applies the theme to the main layout.

                }

                mainUIFragment.startMusicControlListeners(mainUIFragment.getFlyupPanelListener()); // start the Music control listeners which will allow the MusicControlListner to behave accurately.

            }
        };
    }

    private void postUIUpdate()
    {
        mainView.post(uiUpdater); // update the UI
    }

    // converts a list of songs and their paths into their SongInfo and returns a new list of SongInfo!
    private ArrayList<SongInfo> convertToSongInfoList(ArrayList<String> songsList)
    {
        //System.out.println("Am I failing here?");
        ArrayList<SongInfo> songsInfo = new ArrayList<SongInfo>(); // holds the songs and their info into this list.

        // iterate through the arraylist of song paths and convert each one into a SongInfo, and add to list of SongInfos.
        for(int i = 0; i < songsList.size(); i++)
        {
            SongInfo newSong = new SongInfo(); // create a new SongInfo object.
            newSong.getandSetSongInfo(songsList.get(i)); // get the song path and send it to SongInfo to be parsed into it's song details.

            if(newSong.getSongName() == null || newSong.getSongName().equalsIgnoreCase("")) {
                // do nothing this item is null and should not be included into the list.
            }
            else {
                songsInfo.add(newSong); // add the new SongInfo into list of SongInfos.
            }
        }

        // System.out.println("Did I finish grabbing the info?");
        return songsInfo; // return this list back to caller. All song information has been parsed successfully.
    }

    @Override
    public void run()
    {
        // check to see if the looper is prepared. If it's not, redo the looper.
        if(Looper.myLooper() == null)
        {
            Looper.prepare();
        }

        // create the new songList and change of the styling of list items.
        songsList = new ListView(mainUIFragment.getActivity()); // recreate the new list item
        songsList.setDivider(null); // remove the divider completely.
       // songsList.setDividerHeight(80); // 80 seems to be the perfect height for the divider it is crucial that we are getting it to perform a little better.
        songsList.setNestedScrollingEnabled(true); // set the list of the items in order to help make sure that the items are working correctly.
        songsList.setSmoothScrollbarEnabled(false);
        songsList.setDrawingCacheEnabled(false);
        songsList.setScrollingCacheEnabled(false);
        songsList.setVerticalScrollBarEnabled(false); // removes scroll bars.
        songsList.setFastScrollEnabled(true);
        //songsList.setOnScrollListener(new OnScrollListener()); // todo: decide if I can remove this as I may not need this anymore!
        mainUIFragment.setSongsList(songsList);

        // todo: if the app is breaking in the normal songs field it is because I have removed this line below and sent in a list that can be changed by the entire list. Change it back to fix it.
        //ArrayList<SongInfo> copy = new ArrayList<>(songInfoList); // a copy to allow it to be manipulated by the SelectedSongPlayer without changing the list of the songs themselves.
        InitializerActivity initializerActivity = (InitializerActivity) mainUIFragment.getActivity(); // somehow casting this to the MainActivity works! The selected song player behaves properly for whatever reason.


        // TODO:  The above fails because we are trying to do a cast from EF5Scan to InitializerActivity which is just not going to work.

        songsList.setOnItemClickListener(new SelectedSongPlayer(mainUIFragment.getSongInfoList(), mainUIFragment.getSongs(), mainUIFragment.getFlyupPanelController(), initializerActivity,
                mainUIFragment.getFlyupPanelListener(), mainUIFragment)); // start a SelectedSongPlayer
        //songsList.setOnItemLongClickListener(new LongSelectedSongListener(copy)); // start a LongSelectedSongListener. Pass in songs list that can be chosen by user.

        // TODO: Sort the folders list alphabetically because it just looks better.

        // create the listener for when a folder is selected.
        //foldersList = (ListView) mainView.findVsuper.onBackPressed();iewById(R.id.foldersListView);
        foldersList = new ListView(mainUIFragment.getActivity()); // set the new listview here.
        foldersList.setDivider(null); // remove the divider.
        foldersList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mainUIFragment.getActivity(), R.anim.folderlistandplaylistlistanim)); // set the layout animation for when we load folders again.
        foldersList.setNestedScrollingEnabled(true);
        foldersList.setSmoothScrollbarEnabled(false);
        foldersList.setDrawingCacheEnabled(false);
        foldersList.setScrollingCacheEnabled(false);
        foldersList.setVerticalScrollBarEnabled(false); // removes scrollbars
        mainUIFragment.setFoldersList(foldersList);
       // foldersList.setFastScrollEnabled(true);
        //foldersList.setDividerHeight(); // we can change the height of the divider here if we choose to do so.

        foldersList.setOnItemClickListener(new FolderOnClickListener(mainUIFragment));


        // folderSongsList = (ListView) mainView.findViewById(R.id.openFolderSongsView); // should NOT need a listener for this.
        folderSongsList = new ListView(mainUIFragment.getActivity()); // the new way we are doing the listview.
        folderSongsList.setNestedScrollingEnabled(true);
        folderSongsList.setDivider(null); // remove the divider.
        //folderSongsList.setDividerHeight(80); // height of the divider.
        folderSongsList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mainUIFragment.getActivity(), R.anim.folderlistandplaylistlistanim));
        //folderSongsList.setFastScrollEnabled(true);
        folderSongsList.setScrollingCacheEnabled(false);
        folderSongsList.setDrawingCacheEnabled(false);
        folderSongsList.setSmoothScrollbarEnabled(false);
        folderSongsList.setVerticalScrollBarEnabled(false); // removes scrollbars
        mainUIFragment.setFolderSongsList(folderSongsList);
        //folderSongsList.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in)); // set the animation for when the user wants to select something in the list.

        //playListList = (ListView) mainView.findViewById(R.id.playlistList); // get the list of playlists.
        playListList = new ListView(mainUIFragment.getActivity());
        playListList.setDivider(null); // remove the divider
        playListList.setNestedScrollingEnabled(true);
        playListList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mainUIFragment.getActivity(), R.anim.folderlistandplaylistlistanim)); // set the animation for when we see the items again on the list. This is very important.
        playListList.setDrawingCacheEnabled(false);
        playListList.setScrollingCacheEnabled(false);
        playListList.setSmoothScrollbarEnabled(false);
        playListList.setVerticalScrollBarEnabled(false); // removes scroll bars.
        mainUIFragment.setPlayListList(playListList);
       // playListList.setFastScrollEnabled(true);

        //playListList.setDividerHeight(80); // removed because they are already spaced correctly.

        // Clicking a playlist will show the songs in the playlist and behaves similarly to when the user clicks on a folder.
        playListList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            // When a playlist is selected simply update the adapter and display the songs like the Folder does except we do not have to grab all of the songs anymore.
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                //ListView playlistSongsList = (ListView) mainView.findViewById(R.id.openPlaylistSongsView); // grab the view of the playlist songs right here.

                // create the playlistSongList and set the style of the listitems themselves.
                ListView playlistSongsList = new ListView(mainUIFragment.getActivity()); // create a new ListView here.
                playlistSongsList.setDivider(null); // remove the divider height.
               // playlistSongsList.setDividerHeight(80); // set the height of the divider between items.
                playlistSongsList.setNestedScrollingEnabled(true);
                playlistSongsList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mainUIFragment.getActivity(), R.anim.folderlistandplaylistlistanim)); // set the animation for when we open a playlist.

                //MusicAdapter musicAdapter = new MusicAdapter(getActivity(), R.layout.songlist, PlayLists.get(position).getSongs()); // set the music adapter old version
                // MusicAdapter musicAdapter = new MusicAdapter(getActivity(), R.layout.songlist, PlayLists.get(position).songs()); // set the music adapter
                PlaylistMusicAdapter playlistMusicAdapter = new PlaylistMusicAdapter(mainUIFragment.getActivity(), R.layout.songlist, mainUIFragment.getPlaylists().get(position).songs());
                playlistMusicAdapter.setMainUIFragment(mainUIFragment);
                playlistMusicAdapter.setSelectedPlaylist(mainUIFragment.getPlaylists().get(position)); // set the playlist that was selected by the user.


                // TODO: In here call a method that will sort the playlists by style that the users want to sort the playlists. This is very important.

                playlistSongsList.setAdapter(playlistMusicAdapter);
                playlistMusicAdapter.notifyDataSetChanged(); // let the music player know that the list has indeed changed.
                mainUIFragment.setPlaylistMusicAdapter(playlistMusicAdapter); // set the music adapter that we are working with here.
                //ArrayList<SongInfo> alphabetical = alphabetizeSongs(PlayLists.get(position).songs()); // send the songs to be alphabetized.
                mainUIFragment.sort(mainUIFragment.getPlaylists().get(position).songs()); // sort the songs based on the list that is being grabbed right now.

                // sending int the playlist that was selected will get us our list that we need to see so desperately.
                //playlistSongsList.setOnItemClickListener(new PlaylistSelectedSongPlayer(mediaPlayer, PlayLists.get(position).getSongs(), flyupPanel, FlyupPanelListener)); old version
                playlistSongsList.setOnItemClickListener(new PlaylistSelectedSongPlayer(mainUIFragment.getPlaylists().get(position).songs(),
                        mainUIFragment.getFlyupPanelController(), mainUIFragment.getFlyupPanelListener())); // TODO: removed using alphabetical so change if this is needed.
                mainUIFragment.openPlaylist(playlistSongsList); // tell the mainUIFragment to open the playlist.
            }
        });

        artistsList = new ListView(mainUIFragment.getActivity());
        artistsList.setDivider(null);
        artistsList.setNestedScrollingEnabled(true);
        artistsList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mainUIFragment.getActivity(), R.anim.folderlistandplaylistlistanim)); // set the animation for when we see the items again on the list. This is very important.
        artistsList.setDrawingCacheEnabled(false);
        artistsList.setScrollingCacheEnabled(false);
        artistsList.setSmoothScrollbarEnabled(false);
        artistsList.setVerticalScrollBarEnabled(false); // removes scroll bars.

        ArtistAdapter artistAdapter = new ArtistAdapter(mainUIFragment.getActivity(), R.layout.artistlist, mainUIFragment.getArtists());
        artistAdapter.notifyDataSetChanged();
        artistAdapter.setMainUIFragment(mainUIFragment);
        artistsList.setAdapter(artistAdapter);

        mainUIFragment.setArtistsList(artistsList);

        // decides the behavior for when a user clicks on an artist "folder"
        artistsList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                ListView artistSongList = new ListView(mainUIFragment.getActivity());
                artistSongList.setDivider(null);
                //artistSongList.setDividerHeight(80);
                artistSongList.setNestedScrollingEnabled(true);
                artistSongList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mainUIFragment.getActivity(), R.anim.folderlistandplaylistlistanim)); // set the animation for when we open a playlist.

                // fixme: this is incredibly slow on the main UI thread and must be done in a background thread!
                ArrayList<SongInfo> artistSongs = new ArrayList<>();
                artistSongs = mainUIFragment.getArtists().get(position).getSongs();

                Set<SongInfo> duplicatesRemoved = new HashSet<>();
                duplicatesRemoved.addAll(artistSongs); // doing this removes all duplicates from the songs.
                artistSongs.clear();
                artistSongs.addAll(duplicatesRemoved); // add all the songs back into the set and remove duplicates.

                Sorters.sortBySongName(artistSongs); // sort the artist songs by name.

                MusicAdapter musicAdapter = new MusicAdapter(mainUIFragment.getActivity(), R.layout.songlist, artistSongs);
                musicAdapter.setMainUIFragment(mainUIFragment);
                artistSongList.setAdapter(musicAdapter);
                mainUIFragment.setArtistSongList(artistSongList);

                artistSongList.setOnItemClickListener(new ArtistSelectedSongPlayer(artistSongs, mainUIFragment.getFlyupPanelController(),
                        mainUIFragment.getFlyupPanelListener()));

                mainUIFragment.openArtist(artistSongList);

            }
        });

        albumsList = new ListView(mainUIFragment.getActivity());
        albumsList.setDivider(null);
        albumsList.setNestedScrollingEnabled(true);
        albumsList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mainUIFragment.getActivity(), R.anim.folderlistandplaylistlistanim)); // set the animation for when we see the items again on the list. This is very important.
        albumsList.setDrawingCacheEnabled(false);
        albumsList.setScrollingCacheEnabled(false);
        albumsList.setSmoothScrollbarEnabled(false);
        albumsList.setVerticalScrollBarEnabled(false); // removes scroll bars.

        AlbumAdapter albumAdapter = new AlbumAdapter(mainUIFragment.getActivity(), R.layout.albumlist, mainUIFragment.getAlbums());
        albumAdapter.notifyDataSetChanged();
        albumAdapter.setMainUIFragment(mainUIFragment);
        albumsList.setAdapter(albumAdapter);

        mainUIFragment.setAlbumsList(albumsList);

        // decides the behavior for when a user clicks on an artist "folder"
        albumsList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                ListView albumSongList = new ListView(mainUIFragment.getActivity());
                albumSongList.setDivider(null);
               // albumSongList.setDividerHeight(80);
                albumSongList.setNestedScrollingEnabled(true);
                albumSongList.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(mainUIFragment.getActivity(), R.anim.folderlistandplaylistlistanim)); // set the animation for when we open a playlist.

                // fixme: this is incredibly slow on the main UI thread and must be done in a background thread!
                ArrayList<SongInfo> albumSongs = new ArrayList<>();
                albumSongs = mainUIFragment.getAlbums().get(position).getSongs();


                Set<SongInfo> duplicatesRemoved = new HashSet<>();
                duplicatesRemoved.addAll(albumSongs); // doing this removes all duplicates from the songs.
                albumSongs.clear();
                albumSongs.addAll(duplicatesRemoved); // add all the songs back into the set and remove duplicates.

                Sorters.sortBySongName(albumSongs); // sort the album songs by name.

                MusicAdapter musicAdapter = new MusicAdapter(mainUIFragment.getActivity(), R.layout.songlist, albumSongs);
                musicAdapter.setMainUIFragment(mainUIFragment);
                albumSongList.setAdapter(musicAdapter);
                mainUIFragment.setAlbumSongList(albumSongList);

                albumSongList.setOnItemClickListener(new ArtistSelectedSongPlayer(albumSongs, mainUIFragment.getFlyupPanelController(),
                        mainUIFragment.getFlyupPanelListener()));

                mainUIFragment.openAlbum(albumSongList);

            }
        });

        // TODO: This is where we are sending in the items that we need for the MusicAdapter to perform a little better. I may have to copy these updates acroll other uses of this adapter!!
        // set the music adapter for the song list view
        MusicAdapter musicAdapter = new MusicAdapter(mainUIFragment.getActivity(), R.layout.songlist, mainUIFragment.getSongInfoList()); // used to just be songInfoList
        musicAdapter.setMainUIFragment(mainUIFragment); // sets the mainuifragment to be used in the application itself.

        songsList.setAdapter(musicAdapter); // set out current view.
        musicAdapter.notifyDataSetChanged(); // let the music adapter know that the list is updated and ready to go.

        // set the folder adapter for the list view.
        FolderAdapter folderAdapter = new FolderAdapter(mainUIFragment.getActivity(), R.layout.folderlist, mainUIFragment.getFolders());
        folderAdapter.setMainUIFragment(mainUIFragment); // set the MainUIFragment that we need for the application.
        foldersList.setAdapter(folderAdapter); // set the new adapter.
        folderAdapter.notifyDataSetChanged();

        // TODO: this must be done inside of on create very important!
        //addPlayListButton = (FloatingActionButton) mainView.findViewById(R.id.addPlaylistButton); // grab in our main activity.

        // todo: this can be removed once we have figured everything out for the fab menu.
        // click listener for add playlist button.
        //AddPlaylistButtonClickListener addPlaylistButtonClickListener = new AddPlaylistButtonClickListener(mainUIFragment, mainUIFragment.getPlaylists(), mainUIFragment.getSongs(),
                //mainUIFragment.getSongInfoList(), mainUIFragment.getFolders());
       // addPlayListMenu.setOnClickListener(addPlaylistButtonClickListener); // set the click listener.


        // Create our own custom playlist adapter to ensure that the songs will all be the same, very important.
        playListAdapter = new PlayListAdapter(mainUIFragment.getActivity(), R.layout.playlist_list, mainUIFragment.getPlaylists());
        playListAdapter.setMainUIFragment(mainUIFragment); // set the mainUIFragment that is needed for this action.
        playListList.setAdapter(playListAdapter);
        playListAdapter.notifyDataSetChanged(); // update the playlist again in case their is some data that needs to be updated for the list to be seen.

        //flyupPanel.addPanelSlideListener(FlyupPanelListener); // set the listener that we are using for the sliding layout. There is only one listener, another will not be created.

        // WARNING: may need to call this method before the flyupPanel.appPanelSlideListener if there are any issues involving any music controls.

        // create the listener for when a song is selected.
        //songsList = (ListView) mainView.findViewById(R.id.songsListView);


        // create the variables needed for the ViewPager
        Vector<ListView> tabLists = new Vector<>(); // this vector will hold of the lists that we will need.
        tabLists.add(songsList); // add the songs listview to the view pager. .
        tabLists.add(foldersList); // add the folders listview to the view pager.
        tabLists.add(playListList); // add the playlist listview to the view pager.
        tabLists.add(artistsList); // add the artists list.
        tabLists.add(albumsList); // add the albums list.


        // Set the MainUIPagerAdapter to ensure that everything is being displayed correctly.
        mainUIFragment.setMainUIPagerAdapter(new MainUIPagerAdapter(tabLists, songsList, foldersList, playListList, artistsList, albumsList)); // create mainUIPagerAdapter that is needed for us to keep track of how the app is working.
        //PlaylistPagerAdapter playlistPagerAdapter = new PlaylistPagerAdapter(getActivity(), tabLists);
        mainUIFragment.getMainUIPagerAdapter().notifyDataSetChanged();


        // set the behavior for when a user changes the pages on the layout
        mainUIFragment.getViewPager().addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
                // System.out.println("we are in position: " + position);
                // have have the button slowly disappear as the user starts to add animations to the list.

            }

            @Override
            public void onPageSelected(int position)
            {
                if(position == 2) //
                {
                    //addPlayListButton.setVisibility(View.VISIBLE); // make the button visible again.

                    addPlayListMenu.showMenuButton(true);
                    addPlayListMenu.showMenu(false);

                    // use object animator to flip image while we are loading.
//                    ObjectAnimator spin = ObjectAnimator.ofFloat(addPlayListButton, "rotationY", 0.0f, 360.0f); // spin the tornado image for two full revolutions.
//                    spin.setDuration(400); // the higher the number the slower the spinning animation
//                    spin.reverse(); // reverse the spinning animation
//                    spin.start(); // start the animation.


                }
                else // have the button appear only when when we are in the playlist position.
                {


                    addPlayListMenu.hideMenu(true);
                    //addPlayListMenu.close(true);
                    addPlayListMenu.hideMenuButton(true);
                    //addPlayListButton.setVisibility(View.GONE); // make the playlistbutton gone.
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {} // do nothing for this method.
        });


        initiateUIUpdater();
        postUIUpdate();
    }
}
