package thenotoriousrog.tornadoplayer.BackgroundThreads;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.provider.DocumentFile;
import android.widget.Toast;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import thenotoriousrog.tornadoplayer.Activities.FolderSelectedSongPlayer;
import thenotoriousrog.tornadoplayer.Activities.InitializerActivity;
import thenotoriousrog.tornadoplayer.Adapters.MusicAdapter;
import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.M3uReader;
import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SerializeObject;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.Sorters;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Listeners.AddPlaylistButtonClickListener;
import thenotoriousrog.tornadoplayer.R;

/**
 * Created by thenotoriousrog on 3/10/18.
 */

public class MainUIActivityResultThread implements Runnable {


    public final int OPEN_FOLDER_INTENT = 100; // this is the result code that we want to pass back to the intent that way we know to look for specific intent codes.
    public final int CREATE_PLAYLIST_INTENT = 200; // result code we want to send to the open playlist intent.
    public final int MODIFY_PLAYLIST_INTENT = 300; // the result we want to receive after modifying one or all playlists.
    private int requestCode;
    private Intent intentResult;
    private MainUIFragment mainUIFragment;
    private Runnable uiUpdater;
    private Thread backgroundThread;
    private volatile boolean stopped = false; // this boolean allows the thread to continue running. If stopped, we need to stop the thread from running.


    public MainUIActivityResultThread(MainUIFragment mainUIFragment, int requestCode, Intent intentResult)
    {
        this.mainUIFragment = mainUIFragment;
        this.requestCode = requestCode;
        this.intentResult = intentResult;

        this.backgroundThread = new Thread(this);
    }

    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    public boolean isBackgroundThreadAlive()
    {
        return backgroundThread.isAlive();
    }


    // tells the thread to stop if possible
    public void stopThread()
    {
        stopped = true;
    }


    private void playlistUIUpdater()
    {
        uiUpdater = new Runnable() {
            @Override
            public void run()
            {
                mainUIFragment.updatePlaylistAdapter(); // update the playlist so users can see their playlists immediately once they have been saved.
                mainUIFragment.updateViewPager(mainUIFragment.getViewPager().getCurrentItem());
                mainUIFragment.closePlaylist(); // This forces the UI to refresh showing the playlists. For whatever reason, without this, the lists show up as empty which is lame!
            }
        };
    }

    private void folderUIUpdater()
    {
        uiUpdater = new Runnable()
        {
            @Override
            public void run()
            {
                mainUIFragment.openFolder(mainUIFragment.getFolderSongsList()); // have the MainUIFragament open the folder and display the items correctly in the view pager.
                //mainUIFragment.updatePlaylistAdapter(); // update the playlist so users can see their playlists immediately once they have been saved.
               mainUIFragment.updateFoldersListAdapter();
                mainUIFragment.updateViewPager(mainUIFragment.getViewPager().getCurrentItem());
               // mainUIFragment.closePlaylist(); // This forces the UI to refresh showing the playlists. For whatever reason, without this, the lists show up as empty which is lame!

            }
        };
    }

    private void importPlaylistUIUpdater(final boolean success)
    {
        uiUpdater = new Runnable() {
            @Override
            public void run()
            {
                if(!success)
                {
                    Toast.makeText(mainUIFragment.getActivity(), "I couldn't import your playlist " + ("\ud83d\ude16"), Toast.LENGTH_LONG).show(); // confounded face
                }
                else // tell the user that we were successful and then update the playlist.
                {
                    mainUIFragment.updatePlaylistAdapter();
                    mainUIFragment.updateViewPager(mainUIFragment.getViewPager().getCurrentItem());
                    mainUIFragment.refreshPlaylistAdapter();
                    Toast.makeText(mainUIFragment.getActivity(), "Playlist successfully imported! " + ("\ud83e\udd17"), Toast.LENGTH_LONG).show(); // hugging face
                }
            }
        };
    }

    private void postUIUpdate()
    {
        mainUIFragment.getMainView().post(uiUpdater);
    }

    // This will read whatever is written into main memory and grab our playlists arraylist.
    private ArrayList<Playlist> readPlaylistsFromMainMemory()
    {
        // READ the playlist data object from main memory if it exists.
        ArrayList<Playlist> playlists = new ArrayList<>();
        String ser1 = SerializeObject.ReadSettings(mainUIFragment.getActivity().getBaseContext(), "playlists.dat"); // attempt to find this in main memory.
        if(ser1 != null && !ser1.equalsIgnoreCase(""))
        {
            Object obj = SerializeObject.stringToObject(ser1); // grab the object that was read in main memory.

            // cast the object to the correct type of arraylist.
            if(obj instanceof ArrayList)
            {
                playlists = (ArrayList<Playlist>) obj; // set our songInfo list.
            }
        }

        Sorters.sortPlaylistsName(playlists); // sorts the playlists by their name to keep the app looking uniform.
        return playlists; // todo: make sure that this is not null
    }

    private void writePlaylistsToMainMemory(ArrayList<Playlist> playlists)
    {
        // WRITE the playlist object to main memory.
        String ser = SerializeObject.objectToString(playlists); // Trying to serialize the entire Playlist Arraylist, Not sure if it is possible or not yet.
        if(ser != null && !ser.equalsIgnoreCase(""))
        {
            String savedPlaylistFileName = "playlists.dat"; // should be something like "Playlist 1.dat"
            SerializeObject.WriteSettings(mainUIFragment.getActivity().getBaseContext(), ser, savedPlaylistFileName); // write the item to main memory.
        }
        else // Writing the obeject failed. Think of a better way to handle this if at all.
        {
            System.out.println("WE DID NOT WRITE THE LIST CORRECTLY, SOMETHING BAD HAPPENED.");
            SerializeObject.WriteSettings(mainUIFragment.getActivity().getBaseContext(), "", "playlists.dat"); // we should be getting this list if we are something bad has happened.
        }
    }

    public ArrayList<SongInfo> convertToSongInfoList(ArrayList<String> songsList)
    {
        //System.out.println("Am I failing here?");
        ArrayList<SongInfo> songsInfo = new ArrayList<SongInfo>(); // holds the songs and their info into this list.

        // iterate through the arraylist of song paths and convert each one into a SongInfo, and add to list of SongInfos.
        for(int i = 0; i < songsList.size(); i++)
        {
            SongInfo newSong = new SongInfo(); // create a new SongInfo object.
            newSong.getandSetSongInfo(songsList.get(i)); // get the song path and send it to SongInfo to be parsed into it's song details.

           // if(newSong.getSongName() == null || newSong.getSongName().equalsIgnoreCase("")) {
                // do nothing this item is null and should not be included into the list.
           // }
           // else {
            // if null, no name, or the duration is less than 45 seconds do not add this song.
            if(newSong.getSongName() == null || newSong.getSongName().equalsIgnoreCase(" ")
                    || newSong.getSongName().isEmpty() || Long.parseLong(newSong.getSongDuration()) < 45000) {
                // do nothing this item is null and should not be included into the list.
            }
            else {

                String newSongName = newSong.getSongName().replaceAll("\\s+", "");

                // after removing spaces if the song name is empty then we do not add the song.
                //if(newSongName.length() > 2);
                //{
                    songsInfo.add(newSong); // add the new SongInfo into list of SongInfos.
                //}
            }

                //songsInfo.add(newSong); // add the new SongInfo into list of SongInfos.
           // }
        }

        // System.out.println("Did I finish grabbing the info?");
        return songsInfo; // return this list back to caller. All song information has been parsed successfully.
    }

    private boolean isSDCardRootDirectoryUri(Uri treeUri) {
        String uriString = treeUri.toString();
        return uriString.endsWith("%3A");
    }

    @Override
    public void run()
    {
        if(requestCode == OPEN_FOLDER_INTENT) // this is the result that we wanted.
        {
            if(Looper.myLooper() == null)
            {
                Looper.prepare();
            }

            System.out.println("We have received the folder that we were trying to work with.");

            // **Note: I have the check if the thread is stopped multiple times for a reason. The thread can be stopped at any time and it's essential that thread can stop at any time.

            if(!stopped)
            {
                // update the list that we have received from openFoldersandSongs class.
                Bundle resultBundle; // = new Bundle();
                resultBundle = intentResult.getBundleExtra("resultBundle"); // get the bundle that holds our updated arraylist of folder songs.

                mainUIFragment.setFolderSongs(resultBundle.getStringArrayList("SongsInFolder")); // get the arraylist of songs in the folder from the openFolderandDisplaySongs activity.

                //songInfoList = convertToSongInfoList(folderSongs);
                // note: the arraylist foldersongs is used instead of songInfoList to ensure that the songInfoList is not messed with in any way shape or form.
                ArrayList<SongInfo> foldersongs = convertToSongInfoList(mainUIFragment.getFolderSongs()); // convert the folder songs into their own arraylist which is created in itself.

                //MusicAdapter musicAdapter = new MusicAdapter(getActivity(), R.layout.songlist, foldersongs); // set the music adapter

                if(!stopped)
                {
                    MusicAdapter musicAdapter = new MusicAdapter(mainUIFragment.getActivity(), R.layout.songlist, foldersongs); // set the music adapter
                    musicAdapter.setMainUIFragment(mainUIFragment); // set the playlists in the class to be used by the popup menu.
                    mainUIFragment.getFolderSongsList().setAdapter(musicAdapter); // set out current view.
                    musicAdapter.notifyDataSetChanged(); // let the music adapter know that the list is updated and ready to go.

                    mainUIFragment.getFolderSongsList().startLayoutAnimation(); // start the animation that we want to see!
                    //InitializerActivity musicPlayer = (InitializerActivity) getActivity();

                    mainUIFragment.sort(foldersongs); // sort the folder songs according to the chosen filter by the user.
                    final ArrayList<SongInfo> copy = new ArrayList<>(foldersongs); // a copy to allow it to be manipulated by the SelectedSongPlayer without changing the list of the songs themselves.

                    InitializerActivity mPlayer = (InitializerActivity) mainUIFragment.getActivity(); // somehow casting this to the MainActivity works! The selected song player behaves properly for whatever reason.


                    // creates a new FolderSelectedSongPlayer to ensure that the songs in that folder is used in the FlyupPanelListener.
                    FolderSelectedSongPlayer folderSelectedSongPlayer = new FolderSelectedSongPlayer(copy, mainUIFragment.getFolderSongs(), mainUIFragment.getFlyupPanelController(),
                            mPlayer, mainUIFragment.getFlyupPanelListener(), mainUIFragment);

                    mainUIFragment.setFolderSelectedSongPlayer(folderSelectedSongPlayer);

                    mainUIFragment.getFolderSongsList().setOnItemClickListener(folderSelectedSongPlayer);
                }


                if(!stopped)
                {
                    folderUIUpdater();
                }
                // Todo: We need to add the behavior for when a user hits the back button so that the list of folders become displayed again.
            }

            //foldersList.setVisibility(View.GONE); // make the list of the folder gone when a user clicks a folder.
            // folderSongsList.setVisibility(View.VISIBLE); // make the songs within the folder itself visible now.


            // TODO: if the app breaks because of this then we need to remove the sort from above and also uncomment the alphabetized section of code.



        }
        else if(requestCode == CREATE_PLAYLIST_INTENT) // user has chosen to create a playlist.
        {
            if(intentResult == null) // if null, likely the user pressed the back button.
            {
                return; // force a return out of this method.
            }

            if(!stopped)
            {
                Bundle resultBundle = intentResult.getExtras();

                ArrayList<Playlist> playlistsReceived = new ArrayList<>(); // list of all the playlists.
                playlistsReceived = readPlaylistsFromMainMemory(); // grab the playlists that were written from main memory.

                // see if the we can send the a parcelable data structure here and see if that helps at all.
                System.out.println("The playlist arraylist received from main memory is: " + playlistsReceived);

                ArrayList<String> playlistNames = resultBundle.getStringArrayList("playlistNames");

                if(!stopped)
                {
                    mainUIFragment.setPlaylists(playlistsReceived); // set the playlists to be used by the application.

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mainUIFragment.getActivity().getBaseContext());
                    Set<String> playListNames = new HashSet<>(playlistNames); // convert all of the playlist names to a set to be saved to main memory.

                    // save details into main memory.
                    SharedPreferences.Editor editor = preferences.edit(); // create an editor to make changes to what is saved to main memory.
                    editor.putStringSet("playlistNames", playListNames); // save the set to main memory.
                    editor.apply(); // commit the changes to the application so that the app can reload properl

                    if(!stopped)
                    {
                        // Reset the the playlist button click listener as it will need updated values.
                        mainUIFragment.getAddPlayListMenu().setOnClickListener(null); // remove the old click listener to prevent the lists from creating brand new ones. Very important!
                        AddPlaylistButtonClickListener addPlaylistButtonClickListener = new AddPlaylistButtonClickListener(mainUIFragment, mainUIFragment.getPlaylists(), mainUIFragment.getSongs(),
                                mainUIFragment.getSongInfoList(), mainUIFragment.getFolders());

                        mainUIFragment.getAddPlayListMenu().setOnClickListener(addPlaylistButtonClickListener); // set the click listener.
                    }

                    if(!stopped)
                    {
                        playlistUIUpdater();
                    }

                }
            }
        }
        else if(requestCode == MODIFY_PLAYLIST_INTENT)
        {
            if(intentResult == null) // if null, likely the user pressed the back button.
            {
                return; // force a return out of this method.
            }

            System.out.println("We are back in MainUIFragment after modifying one or more playlists. ");
            Bundle resultBundle = intentResult.getExtras();

            ArrayList<Playlist> playlistsReceived = new ArrayList<>(); // list of all the playlists.
            playlistsReceived = readPlaylistsFromMainMemory(); // grab the playlists that were written from main memory.

            // see if the we can send the a parcelable data structure here and see if that helps at all.
            /// System.out.println("The playlist arraylist received from main memory is: " + playlistsReceived);
            System.out.println("The size of the number of playlists read from main memory is: " + playlistsReceived.size());

            ArrayList<String> playlistNames = resultBundle.getStringArrayList("playlistNames");

            mainUIFragment.setPlaylists(playlistsReceived); // set the playlists to be used by the application.

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mainUIFragment.getActivity().getBaseContext());
            Set<String> playListNames = new HashSet<>(playlistNames); // convert all of the playlist names to a set to be saved to main memory.

            // save details into main memory.
            SharedPreferences.Editor editor = preferences.edit(); // create an editor to make changes to what is saved to main memory.
            editor.putStringSet("playlistNames", playListNames); // save the set to main memory.
            editor.apply(); // commit the changes to the application so that the app can reload properl

            // Reset the click listener because we are going to need to have updated values.
            mainUIFragment.getAddPlayListMenu().setOnClickListener(null); // remove the old click listener to prevent the lists from creating copies of the playlists.
            AddPlaylistButtonClickListener addPlaylistButtonClickListener = new AddPlaylistButtonClickListener(mainUIFragment, mainUIFragment.getPlaylists(), mainUIFragment.getSongs(),
                    mainUIFragment.getSongInfoList(), mainUIFragment.getFolders());

            mainUIFragment.getAddPlayListMenu().setOnClickListener(addPlaylistButtonClickListener); // set the click listener.
            playlistUIUpdater();

        }
        else if(requestCode == Constants.INTENT.PICK_FILE) // we have received the request code to grab a file very important!
        {
            System.out.println("WE ARE BACK FROM PICKING A FILE");


            if(intentResult == null) { // null means the user did not select a file and thus nothing to worry about.
                return;
            }

            // show a quick toast to tell the user that was are working on importing the playlist now.
            mainUIFragment.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(mainUIFragment.getActivity(), "I'm on it! " + ("\ud83d\udcaa") + ("\ud83c\udf2A"), Toast.LENGTH_SHORT).show(); // flexed biceps and tornado.
                }
            });


            Uri fileURI = intentResult.getData();
            File file = new File(fileURI.toString());
            System.out.println("The path of the file selected is: " + file.getAbsoluteFile().getAbsolutePath());

            String[] dirs = file.getAbsoluteFile().getAbsolutePath().split("%3A");
            String[] splitter = file.getAbsoluteFile().getAbsolutePath().split("%2F"); // split on those weird characters.
            String playlistName = splitter[splitter.length-1]; // get the last character which holds the file name.

            System.out.println("Playlist name = " + playlistName);

            String mimeType = mainUIFragment.getActivity().getContentResolver().getType(fileURI);
            Cursor returnCursor = mainUIFragment.getActivity().getContentResolver().query(fileURI, null, null, null, null);




            try {

                System.out.println("dirs = " + dirs[1]);
                String startDir[] = dirs[1].split("%2F");
                System.out.println("The size of start dir = " + startDir[0]);
                String dir = startDir[0];
                System.out.println("playlist name = " + playlistName);
                String path = "/";
                for(int i = 1; i < startDir.length-1; i++)
                {
                    path += startDir[i] + "/"; // add to the path

                }

                System.out.println("The path is: " + path);

                InputStream inputStream = mainUIFragment.getActivity().getContentResolver().openInputStream(fileURI);

                File m3uFile = new File(Environment.getExternalStorageDirectory() + "/" + dir +"/" + path + playlistName); // need to add the playlist.m3u file here!!!! need to get the name.
                System.out.println("The path of our real file is: " + m3uFile.getAbsoluteFile().getAbsolutePath());

                // todo: the playlist name need to be appended in order to grab the playlist properly, this is very very very important doing that will get our file

//                OutputStream outputStream = new FileOutputStream(m3uFile); // output stream for a file.
                //IOUtils.copy(inputStream, outputStream);
                //outputStream.close();


                M3uReader m3uReader = new M3uReader(mainUIFragment, m3uFile);
                Playlist importedPlaylist = m3uReader.createPlaylist(); // import the playlist that was (hopefully) created.

                ArrayList<Playlist> playlists = mainUIFragment.getPlaylists();


                if(importedPlaylist == null) // null playlist means that the file selected was not an m3u.
                {
                    importPlaylistUIUpdater(false); // false means importing the playlist was not successful.
                }
                else // the playlist selected is an m3u file, continue to add that playlist to our list of playlists. 
                {
                    playlists.add(importedPlaylist);

                    writePlaylistsToMainMemory(playlists);

                    mainUIFragment.setPlaylists(playlists); // set the playlists to be used by the application.
                    ArrayList<String> playlistNames = mainUIFragment.getPlaylistNames();


                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mainUIFragment.getActivity().getBaseContext());
                    Set<String> playListNames = new HashSet<>(playlistNames); // convert all of the playlist names to a set to be saved to main memory.
                    playListNames.add(importedPlaylist.name()); // add the new playlist added to the phone.

                    // save details into main memory.
                    SharedPreferences.Editor editor = preferences.edit(); // create an editor to make changes to what is saved to main memory.
                    editor.putStringSet("playlistNames", playListNames); // save the set to main memory.
                    editor.apply(); // commit the changes to the application so that the app can reload properly
                    importPlaylistUIUpdater(true); // true indicates that the playlist was created correctly.
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException ex) {
                ex.printStackTrace();
            } catch (ArrayIndexOutOfBoundsException ex) { // this exception is usually caused by a file not selected which is representative of an m3u file. Which causes crashes.
                ex.printStackTrace();
            }

        }
        else if(requestCode == Constants.INTENT.GAIN_STORAGE_ACCESS) // need to open up the tree to allow for the user to have write access.
        {
            Uri treeUri = null;
            // Get Uri from Storage Access Framework.
            treeUri = intentResult.getData();
            DocumentFile pickedDir= DocumentFile.fromTreeUri(mainUIFragment.getActivity(), treeUri);

            if (!isSDCardRootDirectoryUri(treeUri)) {
                mainUIFragment.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run()
                    {
                        Toast.makeText(mainUIFragment.getActivity(), "Wrong directory selected. Please select SD Card root directory.", Toast.LENGTH_LONG).show();
                    }
                });

                //createSDCardHintDialog().show();
                return;
            }

            // Persist URI in shared preference so that you can use it later.
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mainUIFragment.getActivity());
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("SD_URI_KEY", treeUri.toString());
            editor.putBoolean("hasStorageAccess", true); // we now have storage access so we can make things official.
            editor.commit();

            // Persist access permissions, so you dont have to ask again
            final int takeFlags = intentResult.getFlags() & (Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            mainUIFragment.getActivity().getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);


            return; // leave the method, no UIUpdater to post!
        }

        postUIUpdate(); // post the update to the UI!
    }
}
