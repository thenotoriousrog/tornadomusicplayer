package thenotoriousrog.tornadoplayer.BackgroundThreads;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.Activities.SearchActivity;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;

public class FolderSearchSelectedThread implements Runnable {

    private SearchActivity searchActivity;
    private String folderPath;
    private Thread backgroundThread;
    private Runnable uiUpdater; // what updates the search activity and causes the activity to be killed.

    public FolderSearchSelectedThread(SearchActivity searchActivity, String folderPath)
    {
        this.searchActivity = searchActivity;
        this.folderPath = folderPath;
        backgroundThread = new Thread(this);
    }

    // starts the background thread when started.
    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    // this method will find songs within the directory passed into it and return with a File[] full of .mp3 files.
    private File[] getSongsInDirectory(File dir)
    {
        // filteredSongFiles holds all of the mp3 files that are found
        File[] filteredSongFiles = dir.listFiles(new FilenameFilter() {

            // This will search for songs and ensure that whatever is retrieved is an mp3 and nothing else.
            @Override
            public boolean accept(File dir, String songName) {
                return songName.contains(".mp3");
            }
        });

        return filteredSongFiles; // returns the File[] of filtered songs. It can return 0 if no songs were found.
    }

    // converts a list of songs and their paths into their SongInfo and returns a new list of SongInfo!
    private ArrayList<SongInfo> convertToSongInfoList(ArrayList<String> songsList)
    {
        //System.out.println("Am I failing here?");
        ArrayList<SongInfo> songsInfo = new ArrayList<SongInfo>(); // holds the songs and their info into this list.

        // iterate through the arraylist of song paths and convert each one into a SongInfo, and add to list of SongInfos.
        for(int i = 0; i < songsList.size(); i++)
        {
            SongInfo newSong = new SongInfo(); // create a new SongInfo object.
            newSong.getandSetSongInfo(songsList.get(i)); // get the song path and send it to SongInfo to be parsed into it's song details.

            if(newSong.getSongName() == null || newSong.getSongName().equalsIgnoreCase("")) {
                // do nothing this item is null and should not be included into the list.
            }
            else {
                songsInfo.add(newSong); // add the new SongInfo into list of SongInfos.
            }
        }

        // System.out.println("Did I finish grabbing the info?");
        return songsInfo; // return this list back to caller. All song information has been parsed successfully.
    }

    // This method will extract the songs out of the Folder and convert them into an Arraylist<SongInfo> to be added to the playlist.
    private ArrayList<SongInfo> extractSongsFromFolder(String folderPath)
    {
        ArrayList<String> rawSongs = new ArrayList<>(); // Holds all of the song filepaths.
        ArrayList<SongInfo> folderSongs; // holds all of the songs after being converted into a list of SongInfo objects.

        File folderDirectory = new File(folderPath); // convert the foldername into a file that we can use to open up and get the songs in that folder.
        File[] songs; // will hold all the .mp3 files in this folder.

        if(folderDirectory.isDirectory()) // check to make sure that this is a valid directory before we go searching through it.
        {
            // get the songs in this directory and add it to the arrayList of folderSongs
            songs = getSongsInDirectory(folderDirectory); // get all of the songs in the folder.

            // TODO: check to make sure that their songs in the folder before trying to add them all or the app will crash. I know it works in mine, but it may not for other users.

            // add songs into the arrayList.
            for(int i = 0; i < songs.length; i++)
            {
                rawSongs.add(songs[i].toString()); // adds all the song names into the arraylist of folder songs.
            }
        }
        else
        {
            // TODO: create a toast to let the user know that selecting this folder has failed. Also check if it exists, if it doesn't tell the user that the folder no longer exists.
        }

        folderSongs = convertToSongInfoList(rawSongs); // convert all of the raw songs into an arraylist of SongInfo objects.
        return  folderSongs; // send the arraylist of songs back to the calling function.
    }

    @Override
    public void run()
    {
        ArrayList<SongInfo> songs = extractSongsFromFolder(folderPath); // get the songs from the folder.

        SongInfo currentSong = songs.get(0);

        MusicLibrary.setCurrSong(currentSong);
        MusicLibrary.setCurrentSongList(songs, currentSong); // set the current song list in the music library
        MusicLibrary.setCurrentSongNumber(0); // set the song position in the music library

        if(MusicLibrary.isShuffleOn())
        {
            System.out.println("SHUFFLE IS ON");
            MusicLibrary.resetQueue();
            MusicLibrary.resetPlayedSongs();
            MusicLibrary.shuffle();
        }

        searchActivity.sendSearchSelectedMessage(); // causes the search activity to send the search selected message.
        searchActivity.finish(); // finish the activity.
    }
}
