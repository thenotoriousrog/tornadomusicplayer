package thenotoriousrog.tornadoplayer.BackgroundThreads;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import thenotoriousrog.tornadoplayer.Backend.BitmapWorkshop;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.R;

/**
 * Created by thenotoriousrog on 3/18/18.
 */

public class UpdateMediaSessionThread implements Runnable {

    private MainUIFragment mainUIFragment;
    private SongInfo currSong;
    private SongInfo nextSong;
    private int songPos;
    private Thread backgroundThread;

    public UpdateMediaSessionThread(MainUIFragment mainUIFragment, SongInfo currSong, SongInfo nextSong, int songPos)
    {
       this.mainUIFragment = mainUIFragment;
       this.currSong = currSong;
       this.nextSong = nextSong;
       this.songPos = songPos;

       backgroundThread = new Thread(this); // sets our background thread.
    }

    // starts the background thread!
    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    @Override
    public void run()
    {
        Bitmap albumArt = BitmapWorkshop.extractAlbumArt(mainUIFragment.getActivity(), currSong, 200, 200); // retrieve the album art from the bitmap workshop with a size of 200x200
        //mainUIFragment.getMediaSession().updateMediaSession(currSong, nextSong, songPos, albumArt); // update the media session
    }
}
