package thenotoriousrog.tornadoplayer.BackgroundThreads;


import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.ShuffleQueue;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.UI.FlyupPanelController;

/**
 * Created by thenotoriousrog on 3/10/18.
 * Background thread for shuffle songs.
 */

public class ShuffleThread implements Runnable  {

    private Thread backgroundThread;
    private ShuffleQueue shuffleQueue;
    private ArrayList<SongInfo> currentSongPaths;
    private FlyupPanelListener flyupPanelListener;
    private Runnable uiUpdater;
    private FlyupPanelController flyupPanelController;

    public ShuffleThread(ShuffleQueue shuffleQueue, ArrayList<SongInfo> currentSongPaths, FlyupPanelListener flyupPanelListener, FlyupPanelController flyupPanelController)
    {
        this.shuffleQueue = shuffleQueue;
        this.currentSongPaths = currentSongPaths;
        this.flyupPanelListener = flyupPanelListener;
        this.flyupPanelController = flyupPanelController;
        this.backgroundThread = new Thread(this); // initiate a background thread to do the work in this class.
    }

    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    private void initiateUIUpdater()
    {
        uiUpdater = new Runnable()
        {
            @Override
            public void run()
            {
                flyupPanelListener.updateSliderData(MusicLibrary.getCurrSong(), MusicLibrary.peekNextSong());
                flyupPanelListener.refreshSliderLayout();
            }
        };
    }


    private void postUIUpdate()
    {
        flyupPanelController.getFlyupPanel().post(uiUpdater);
    }


    @Override
    public void run()
    {
        MusicLibrary.shuffle(); // shuffles the songs in the background
        //shuffleQueue = new ShuffleQueue(currentSongPaths); // shuffles all songs in the background.
        //flyupPanelListener.setShuffleQueue(shuffleQueue); // set the shuffle queue for the flyupPanelListener.
        initiateUIUpdater();
        postUIUpdate();
    }
}
