package thenotoriousrog.tornadoplayer.BackgroundThreads;

import android.view.View;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Fragments.AddSongsToSinglePlaylistFragmentActivity;
import thenotoriousrog.tornadoplayer.Fragments.ModifyAllPlaylistsFragmentActivity;

/*
    This thread is called when the user selects a folder to open up. This takes work away from the UI and updates the items correctly.
 */
public class PlaylistFolderSelectedThread implements Runnable {

    private View viewToUpdate;
    private Runnable uiUpdater;
    private Thread backgroundThread;
    private AddSongsToSinglePlaylistFragmentActivity  fragmentActivity;
    private ModifyAllPlaylistsFragmentActivity fragmentActivity2; // the other fragment activity that we are working with.
    private String folderPath; // the path of the folder that was selected.
    private String folderName; // holds the name of the folder after trimming the name of the folder itself.
    private volatile boolean stopped = false; // this boolean allows the thread to continue running. If stopped, we need to stop the thread from running.

    public PlaylistFolderSelectedThread(View viewToUpdate, AddSongsToSinglePlaylistFragmentActivity addSongsToSinglePlaylistFragmentActivity, String folderPath)
    {
        this.viewToUpdate = viewToUpdate;
        this.fragmentActivity = addSongsToSinglePlaylistFragmentActivity;
        this.folderPath = folderPath;

        this.backgroundThread = new Thread(this);

    }

    public PlaylistFolderSelectedThread(View viewToUpdate, ModifyAllPlaylistsFragmentActivity fragmentActivity2, String folderPath)
    {
        this.viewToUpdate = viewToUpdate;
        this.fragmentActivity2 = fragmentActivity2;
        this.folderPath = folderPath;

        this.backgroundThread = new Thread(this);

    }

    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    // tells whether or not the thread is alive.
    public boolean isBackgroundThreadAlive()
    {
        return backgroundThread.isAlive();
    }

    // tells the thread to stop if possible
    public void stopThread()
    {
        stopped = true;
    }

    // WARNING: this method is extremely inefficient in terms of memory.
    private ArrayList<SongInfo> convertToSongInfoList(ArrayList<String> songsList)
    {
        //System.out.println("Am I failing here?");
        ArrayList<SongInfo> songsInfo = new ArrayList<SongInfo>(); // holds the songs and their info into this list.

        // iterate through the arraylist of song paths and convert each one into a SongInfo, and add to list of SongInfos.
        for(int i = 0; i < songsList.size(); i++)
        {

            SongInfo newSong = new SongInfo(); // create a new SongInfo object.
            newSong.getandSetSongInfo(songsList.get(i)); // get the song path and send it to SongInfo to be parsed into it's song details.
            songsInfo.add(newSong); // add the new SongInfo into list of SongInfos.
        }

        // System.out.println("Did I finish grabbing the info?");
        return songsInfo; // return this list back to caller. All song information has been parsed successfully.
    }

    // this method will find songs within the directory passed into it and return with a File[] full of .mp3 files.
    private File[] getSongsInDirectory(File dir)
    {
        // filteredSongFiles holds all of the mp3 files that are found
        File[] filteredSongFiles = dir.listFiles(new FilenameFilter() {

            // This will search for songs and ensure that whatever is retrieved is an mp3 and nothing else.
            @Override
            public boolean accept(File dir, String songName) {
                return songName.contains(".mp3");
            }
        });

        return filteredSongFiles; // returns the File[] of filtered songs. It can return 0 if no songs were found.
    }

    // grabs all of the songs from the folder and creates the ArrayList of SongInfo's!
    private ArrayList<SongInfo> grabSongsFromFolder()
    {
       // String folderPath = folders.get(position); // gets the path of the selected folder itself.
        File folderDir = new File(folderPath);
        File[] songs; // songs within the file

        ArrayList<String> rawSongs = new ArrayList<String>();
        if(folderDir.isDirectory()) // if it is a directory then we need to extract all of the songs in it and create a new arraylist of songInfos
        {
            songs = getSongsInDirectory(folderDir); // get all the songs in the folder directory

            for(int i = 0; i < songs.length; i++)
            {
                rawSongs.add(songs[i].toString()); // add the songs into the arrayList.
            }
        }

        ArrayList<SongInfo> folderSongs = new ArrayList<SongInfo>();
        folderSongs = convertToSongInfoList(rawSongs); // convert all of the raw songs to the folders songs to SongInfos

        String temp = folderPath; //folders.get(position); // get the folder path that the user has chosen.
        String[] arr = temp.split("/"); // split the string by /'s

        int length = arr.length; // get the length of the split string.
        folderName = arr[length -1]; // get the last split and sets the folder name.

        return folderSongs; // reurn the folder songs to be used in the application.

    }

    private void initiateUIUpdater(final ArrayList<SongInfo> folderSongs)
    {
        uiUpdater = new Runnable() {
            @Override
            public void run()
            {
                // if the thread is not stopped, update the UI.
                if(!stopped)
                {
                    // we are using the AddSongToSinglePlaylistFragmentActivity.
                    if(fragmentActivity != null)
                    {
                        fragmentActivity.openFolderInPager(folderSongs); // opens the songs in the pager.
                        fragmentActivity.setFolderSeperatorText(folderName);
                        fragmentActivity.getSongListSeparator().setText(folderName); // update the songListSeperator text to show the name of the folder that the user has selected.
                    }

                    // we are using the ModifyAllPlaylistsFragmentActivity.
                    if(fragmentActivity2 != null)
                    {
                        fragmentActivity2.openFolderInPager(folderSongs); // opens the songs in the pager.
                        fragmentActivity2.setFolderSeperatorText(folderName);
                        fragmentActivity2.getSongListSeparator().setText(folderName); // update the songListSeperator text to show the name of the folder that the user has selected.
                    }

                }

            }
        };
    }

    // updates the UI for the user to see.
    private void postUIUpdate()
    {
        // if not stopped update the UI.
        if(!stopped)
        {
            viewToUpdate.post(uiUpdater);
        }

    }

    @Override
    public void run()
    {
        // if not stopped, begin parsing the songs from the folder.
        if(!stopped)
        {
            ArrayList<SongInfo> folderSongs = grabSongsFromFolder(); // grabs the songs from the folder.

            // if not stopped begin to update the UI.
            if(!stopped)
            {
                initiateUIUpdater(folderSongs); // initiates the UI updater.
                postUIUpdate(); // updates the UI officially.
            }
        }


    }


}
