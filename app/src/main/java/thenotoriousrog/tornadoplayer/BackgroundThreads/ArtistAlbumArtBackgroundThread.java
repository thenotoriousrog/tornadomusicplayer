package thenotoriousrog.tornadoplayer.BackgroundThreads;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import thenotoriousrog.tornadoplayer.Backend.Artist;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.R;

public class ArtistAlbumArtBackgroundThread implements Runnable {

    private View viewToUpdate; // this is the view that we want to update the album art in.
    private Artist currArtist;
    private Runnable uiUpdater; // this is the thread in which the ui is updated.
    private Thread backgroundThread; // this thread is what runs the application.

    public ArtistAlbumArtBackgroundThread(View viewToUpdate, Artist currArtist)
    {
        this.viewToUpdate = viewToUpdate;
        this.currArtist = currArtist;
        this.backgroundThread = new Thread(this); // the thread that will start doing work in the background.
    }

    // starts the background thread.
    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    private void inititiateUIUpdater(final Bitmap albumArt, final ImageView artistArt)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(viewToUpdate.getContext()); // gets the default shared preferences
        final String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.

        uiUpdater = new Runnable() {
            @Override
            public void run()
            {
                if(albumArt != null) // if not null apply the album art.
                {
                    artistArt.setAlpha(0.0f); // set invisible.
                    artistArt.setImageBitmap(albumArt); // set the bitmap for the image.
                    artistArt.animate().alpha(1.0f); // fade in album art.
                }
                else // apply a default album art for the user.
                {
                    artistArt.setImageResource(ThemeUtils.getInstance().getCurrentAlbumColor());
//                    if(theme.equalsIgnoreCase("OceanMarina"))
//                    {
//                        artistArt.setImageResource(R.drawable.album_ocean_marina);
//                    }
//                    else if(theme.equalsIgnoreCase("ParadiseNightfall"))
//                    {
//                        artistArt.setImageResource(R.drawable.album_paradise_nightfall);
//                    }
//                    else if(theme.equalsIgnoreCase("AquaArmy"))
//                    {
//                        artistArt.setImageResource(R.drawable.album_aqua_army);
//                    }
                }
            }
        };
    }

    // posts the ui update to update the ui element.
    private void postUIUpdate()
    {
        viewToUpdate.post(uiUpdater);
    }

    // this is where most of the background work is going to take place.
    @Override
    public void run()
    {
        if(currArtist != null)
        {
            ImageView artistArt = viewToUpdate.findViewById(R.id.artistWork); // sets the artwork for the user.
            try
            {


                MediaMetadataRetriever mmr = new MediaMetadataRetriever(); // create a new MediaMetaDataRetriever to extract the album art from the current song if one exists.
                mmr.setDataSource(currArtist.getSongs().get(0).getSongPath()); // send in the song path to attempt to extract the song data items.
                byte[] albumArt = mmr.getEmbeddedPicture();

                mmr.release();
                if(albumArt != null) // initialize the album art only if the album art is not null
                {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(albumArt, 0, albumArt.length); // converts the byte[] to a bitmap.
                    inititiateUIUpdater(bitmap, artistArt); // initialize the ui updater with the correct values.
                }
                else // if null, we need to show the default artist art work.
                {
                    inititiateUIUpdater(null, artistArt);
                }

                if(uiUpdater != null) // only start the ui update if the ui updater has been initialized.
                {
                    postUIUpdate();
                }

            } catch (IllegalArgumentException ex) { // an illegal argument exception means something is wrong with the path we are sending, use default album art.

                // initialize uiUpdater and begin to display the default art.
                inititiateUIUpdater(null, artistArt);

                if(uiUpdater != null)
                {
                    postUIUpdate();
                }
            } catch (IndexOutOfBoundsException ex) { // likely what is happening is we are getting artists that do not have any songs in them, from songs with no artist.

                inititiateUIUpdater(null, artistArt);

                if(uiUpdater != null)
                {
                    postUIUpdate();
                }
            }

        }
    }
}
