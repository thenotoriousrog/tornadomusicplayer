package thenotoriousrog.tornadoplayer.BackgroundThreads;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.view.View;
import android.widget.ImageView;

import thenotoriousrog.tornadoplayer.Backend.BitmapWorkshop;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.R;

public class SongAlbumArtBackgroundThread implements Runnable {

    private View viewToUpdate;
    private SongInfo song;
    private Runnable uiUpdater;
    private Thread backgroundThread;

    public SongAlbumArtBackgroundThread(View viewToUpdate, SongInfo song)
    {
        this.viewToUpdate = viewToUpdate;
        this.song = song;
        this.backgroundThread = new Thread(this);
    }


    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    private void initiateUIUpdater(final Bitmap art, final ImageView albumArt)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(viewToUpdate.getContext()); // gets the default shared preferences
        final String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.

        uiUpdater = new Runnable() {
            @Override
            public void run()
            {
                if(art != null)
                {
                    albumArt.setAlpha(0.0f); // make clear
                    albumArt.setImageBitmap(art);
                    albumArt.animate().alpha(1.0f); // make totally visible.
                }
                else // apply the icon based on the theme that we're using.
                {
                    albumArt.setImageResource(ThemeUtils.getInstance().getCurrentSongIcon());
//                    if(theme.equalsIgnoreCase("OceanMarina"))
//                    {
//                        albumArt.setImageResource(R.drawable.music_note_ocean_marina_24dp);
//                    }
//                    else if(theme.equalsIgnoreCase("ParadiseNightfall"))
//                    {
//                        albumArt.setImageResource(R.drawable.music_note_paradise_nightfall_24dp);
//                    }
//                    else if(theme.equalsIgnoreCase("AquaArmy"))
//                    {
//                        albumArt.setImageResource(R.drawable.music_note_aqua_army_24dp);
//                    }
                }
            }
        };
    }


    private void postUIUpdate()
    {
        viewToUpdate.post(uiUpdater);
    }

    @Override
    public void run()
    {
        if(song != null)
        {
            ImageView songListIcon = viewToUpdate.findViewById(R.id.songListIcon); // the song icon that we are working with.
            try
            {


                Bitmap albumArt = BitmapWorkshop.extractAlbumArtForSongList(viewToUpdate.getContext(), song, 65,65);
                initiateUIUpdater(albumArt, songListIcon); // will handle the album art accordingly.

                if(uiUpdater != null)
                {
                    postUIUpdate();
                }
            } catch (IllegalArgumentException ex){
                ex.printStackTrace();
                initiateUIUpdater(null, songListIcon);

                if(uiUpdater != null)
                {
                    postUIUpdate();
                }
            } catch (IndexOutOfBoundsException ex){
                ex.printStackTrace();
                initiateUIUpdater(null, songListIcon);

                if(uiUpdater != null)
                {
                    postUIUpdate();
                }
            }

        }
    }
}
