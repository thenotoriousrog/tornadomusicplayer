package thenotoriousrog.tornadoplayer.BackgroundThreads;

import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.design.widget.BottomSheetBehavior;
import android.view.View;

import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.Activities.InitializerActivity;
import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.Handlers.ServiceHandler;
import thenotoriousrog.tornadoplayer.UI.CountdownTimer;
import thenotoriousrog.tornadoplayer.UI.FlyupPanelController;

/**
 * Created by thenotoriousrog on 3/10/18.
 * Background thread for when a user selects a song in a list of songs and ensures smoothness of the flyup panel.
 */

public class FlyupPanelSongSelectedThread implements Runnable{

    private ArrayList<SongInfo> songList;
    private SongInfo currentSong;
    private int songPosition;
    private FlyupPanelListener flyupPanelListener;
    private CountdownTimer songTimer;
    private FlyupPanelController flyupPanelController;
    private Runnable uiUpdater; // runnable that updates the ui
    private Thread backgroundThread;
    private HandlerThread musicPlayerHandler;
    private ServiceHandler serviceHandler;

    public FlyupPanelSongSelectedThread(ArrayList<SongInfo> songList, SongInfo currentSong, int songPosition, FlyupPanelListener flyupPanelListener, CountdownTimer songTimer,
                                        FlyupPanelController flyupPanelController)
    {
        this.songList = songList;
        this.currentSong = currentSong;
        this.songPosition = songPosition;
        this.flyupPanelListener = flyupPanelListener;
        this.songTimer = songTimer;
        this.flyupPanelController = flyupPanelController;

        System.out.println("The current song number passed into song selected thread = " + songPosition);

        musicPlayerHandler = new HandlerThread(Constants.HANDLER.MUSIC_PLAYER_HANDLER, Process.THREAD_PRIORITY_BACKGROUND);
        musicPlayerHandler.start(); // start the music player handler.

        serviceHandler = new ServiceHandler(musicPlayerHandler.getLooper()); // send in the music player handler looper so Service Handler can listen for messages.

        this.backgroundThread = new Thread(this);
    }

    public boolean isBackgroundThreadAlive()
    {
        return backgroundThread.isAlive();
    }

    // starts running the background thread.
    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    // Gets the UI updater prepared to be ran for updating the UI!
    private void initiateUIUpdater()
    {
        // runnable that is in charge of updating the UI after we have finished working in the background.
        uiUpdater = new Runnable()
        {
            @Override
            public void run()
            {

                // ensure that the pause button is shown instead of the play button.
                flyupPanelListener.getPlayButton().setVisibility(View.GONE);
                flyupPanelListener.getPauseButton().setVisibility(View.VISIBLE);

                flyupPanelListener.updateSliderData(currentSong, MusicLibrary.peekNextSong()); // send it the songInfo of the current song to extract data, and send in the song name of the next song that will be played.

                flyupPanelListener.restartCountdownTimer(currentSong); // start the timer with the current song.
                flyupPanelListener.reportSongChangeToMediaSession();
                flyupPanelListener.refreshSliderLayout(); // refreshes the slider layout (but not the album art portion of it
            }
        };
    }


    // posts an update to the UI thread
    private void postUIUpdate()
    {
        flyupPanelController.getFlyupPanel().post(uiUpdater); // post the UI updater to update the UI as quickly as it can.
    }

    // this is the background thread
    @Override
    public void run()
    {
        // check to see if the looper is prepared. If it's not, redo the looper.
        if(Looper.myLooper() == null)
        {
            Looper.prepare();
        }

        InitializerActivity activity = (InitializerActivity) flyupPanelListener.getMainUIFragment().getActivity();

        // if the activity is not bound to the service i.e. if the app if the notification is swiped away, we need to reinstate the player service.
        if(!activity.isBoundToService())
        {
            activity.startPlayerService();
        }

        if (songTimer != null) // songTimer is set and ready to be changed.
        {
            flyupPanelListener.restartCountdownTimer(currentSong);

            //CountdownTimer t = new CountdownTimer(currentSong.getSongDuration(), flyupPanelController.getFlyupPanel());

            //flyupPanelListener.cancelAndFinishTimer(); // cancel and finish the timer that we are using on the panelSlideListener.
            //flyupPanelListener.setTimer(t); // set the new countdownTimer
            //flyupPanelListener.startTimer(); // start the timer.
        }
        else // songTimer is null right here.
        {
            flyupPanelListener.restartCountdownTimer(currentSong);
            //songTimer = new CountdownTimer(currentSong.getSongDuration(), flyupPanelController.getFlyupPanel());

            //flyupPanelListener.setTimer(songTimer); // set the new countdownTimer

        }

        MusicLibrary.setCurrSong(currentSong);
        // Note: only in the song selection threads are we allowed to make calls to the music library. No where else in the code should this happen besides player service.
        MusicLibrary.setCurrentSongList(songList, currentSong); // set the current song list in the music library
        MusicLibrary.setCurrentSongNumber(songPosition); // set the song position in the music library

        if(MusicLibrary.isShuffleOn())
        {
            System.out.println("SHUFFLE IS ON");
            MusicLibrary.resetQueue();
            MusicLibrary.resetPlayedSongs();
            MusicLibrary.shuffle();
        }

        // this message must go second.
        Message msg2 = serviceHandler.obtainMessage();
        msg2.arg1 = Constants.COMMAND.PLAY_SELECTED; // allow for message to send an alert that a song was selected and is ready to be played.
        Bundle song = new Bundle();
        song.putString(Constants.ACTION.PLAY_SONG, currentSong.getSongPath());
        msg2.setData(song); // set the data that we want to receive in service Handler.
        serviceHandler.sendMessage(msg2); // send the message to the service Handler

        while(flyupPanelController.getFlyupPanelState() != BottomSheetBehavior.STATE_EXPANDED) {} // wait for the panel to fly up before updating.

        initiateUIUpdater(); // prepare the uiUpdater to update the UI
        postUIUpdate(); // post the UI updater to update the UI
    }
}
