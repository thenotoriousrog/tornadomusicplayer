package thenotoriousrog.tornadoplayer.NotificationManager;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;

/**
 * Created by thenotoriousrog on 3/18/18.
 * This class takes all of the notification actions and does things.
 */

public class NotificationReceiver extends BroadcastReceiver {

    public static final String NOTIFICATION_ID_STRING = "NotificationId";
    public static final String ACTIONS = "Actions";
    public static final String PAUSE = "Pause";
    public static final String PLAY = "Play";
    public static final String SKIP = "Skip";
    public static final String PREV = "Prev";

    private FlyupPanelListener flyupPanelListener; // a copy of the flyup panel listener to force actions to take place
    private Handler handler; // handler to communicate with the UI.



    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {

            System.out.println("receving wear notifications");

            int notificationId = intent.getIntExtra(NOTIFICATION_ID_STRING, 0);
            //NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
           // manager.cancel(notificationId);

            System.out.println("action from intent = " + intent.getAction());

            String action = intent.getAction();

            switch (action)
            {
                case PAUSE:
                    flyupPanelListener.clickPause();
                    //Message msg = new Message();
                    //Bundle bundle = new Bundle();
                    //bundle.putString("ACTION", "Pause");
                   // msg.setData(bundle);
                   // handler.dispatchMessage(msg); // send the message to the handler
                    //handler.sendMessage(msg);
                    break;

                case PLAY:
                    flyupPanelListener.clickPlay();
                    break;

                case SKIP:
                    System.out.println("Skip was clicked!");
                    flyupPanelListener.clickSkip();
                    break;

                case PREV:
                    flyupPanelListener.clickPrev();
                    break;

            }
        }

    }
}
