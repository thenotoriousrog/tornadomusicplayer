package thenotoriousrog.tornadoplayer.NotificationManager;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by thenotoriousrog on 3/11/18.
 *
 * This is in charge of sending notifications to smart watches that are connected on the user's device!
 */

public class WearActionReceiver extends BroadcastReceiver {

    public static final String NOTIFICATION_ID_STRING = "NotificationId";
    public static final String WEAR_ACTION = "WearAction";
    public static final int SNOOZE_NOTIFICATION = 1;
    public static final int CANCEL_TICKET = 2;

    @Override
    public void onReceive (Context context, Intent intent) {

        if (intent != null) {

            System.out.println("receving wear notifications");

            int notificationId = intent.getIntExtra(NOTIFICATION_ID_STRING, 0);
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            manager.cancel(notificationId);

            int action = intent.getIntExtra(WEAR_ACTION, 0);

            switch (action)
            {
                /// I need to add options to skip the song when done so on the smart watch very important!
                case SNOOZE_NOTIFICATION:
                    //Code for notification snooze
                    System.out.println("test");
                    break;
                case CANCEL_TICKET:
                    //code for removing the user from the queue
                    System.out.println("test2");
                    break;
                default:
                    break;

            }
        }
    }

}
