package thenotoriousrog.tornadoplayer.NotificationManager;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

import com.github.clans.fab.FloatingActionButton;

import thenotoriousrog.tornadoplayer.Activities.InitializerActivity;
import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.Services.PlayerService;

/**
 * Created by thenotoriousrog on 6/16/17.
 *
 * This class will start our handler for the Notification listener and will tell the the SelectedSongPlayer to perform certain actions that the user chooses to do.
 */

public class NotificationHandler extends Handler implements Runnable
{
    private NotificationService notificationService = null; // a copy of the notificationService so that we can set the handler to receive messages from.
    private Handler songPlayerHandler = null; // this is the handler that will recieve messages from the NotificationService class.
    private InitializerActivity initializerActivity = null; // a copy of the music player activity to speak between them
    private boolean listening = true; // this tells us whether or not our program is listening to actions from the NotificationService.
    private ServiceConnection servConn; // a copy of the service connection.
    private Intent notificationIntent;

    public NotificationHandler(InitializerActivity activity)
    {
        // set our fields to do the work that we need.
        initializerActivity = activity;
    }

    // stops the service.
    public void stopService()
    {
        initializerActivity.stopService(notificationIntent); // kills the service
    }

    public void unbindService()
    {
        initializerActivity.unbindService(servConn); // unbinds the service.
    }

    // This method is only called once, it will create the service connection which also sets our handler. Very important.
    public void setServiceConnection()
    {

        // create a service connection which will allow us to set our handler.
         servConn = new ServiceConnection() {

            PlayerService notifyMe = null;

            @Override
            public void onServiceConnected(ComponentName name, IBinder service)
            {
                System.out.println("are we in service connected?");
                //notifyMe = ((PlayerService.LocalBinder) service).getInstance(); // get the instance of the notification
                notifyMe.setMsgHandler(songPlayerHandler); // set the handler that is used to receive messages.
            }

            @Override
            public void onServiceDisconnected(ComponentName name)
            {
                // todo: make sure to have this kill the music playing when the user wants to.
            }
        };

        Intent bindIntent = new Intent(initializerActivity, PlayerService.class);
        //initializerActivity.bindService(bindIntent, servConn, Context.BIND_AUTO_CREATE); // bind the actual service now.
    }


    // This method will be in charge of listening to the messages being sent from the NotificationService class.
    public void startListening()
    {
        // TODO: the only reason that I would need this would
        listening = true; // set it true because we are now listening for a new song.

        System.out.println("we have started the run method");

//        // may need to change SelectedSongPlayer.this to mainActivity, but maybe not
//        notificationIntent = new Intent(initializerActivity, PlayerService.class);
////        notificationIntent.putExtra("songName", song);
////        notificationIntent.putExtra("artistName", artist);
////        notificationIntent.putExtra("nextSong", "Up Next: " + next);
////        System.out.println("song path that we are putting into the intent is: " + songPath);
////        notificationIntent.putExtra("songPath", songPath);
//        notificationIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) // only in oreo can we start a foreground service this way.
//        {
//            initializerActivity.startForegroundService(notificationIntent); // starts the actual foreground service.
//        }
//
//        initializerActivity.startService(notificationIntent); // start the service.

        setServiceConnection();
        songPlayerHandler = this;
        //songPlayerHandler = new Handler(Looper.getMainLooper());

    }

    // this method should be in control of recieving messages that will be sent by the NotificationService.
    @Override
    public void handleMessage(Message msg)
    {
        System.out.println("We have received a message from the NotificationService");
        Bundle notifBundle = msg.getData();
        String action = notifBundle.getString("msg");

        if(action.equalsIgnoreCase("Pause"))
        {
            //FlyupPanelListener.clickPause(); // no return because we want to keep this Thread alive and listening for actions.
            return;
        }
        else if(action.equalsIgnoreCase("Play"))
        {
            System.out.println("Play was received, we need to do some work for play!");
            //FlyupPanelListener.clickPlay(); // no return because we want to keep this Thread alive and listening for actions.
            return;
        }
        else if(action.equalsIgnoreCase("Skip"))
        {
            System.out.println("Skip was clicked!");
            //ssp.printMessage("we have detected a skip action.");
            //ssp.printMessage("Song path that we want to use is: " + songPath);
            //FlyupPanelListener.clickSkip(); // skip to the next song.
            return; // forces a return, stopping the Thread
        }
        else if(action.equalsIgnoreCase("Replay"))
        {
           // FlyupPanelListener.clickPrev(); // single pressing prev issues a restart for the current song playing.
            return; // force a return, stopping the thread.
        }
        else if(action.equalsIgnoreCase("Prev"))
        {
            System.out.println("Something happened pushing prev on smartwatch!!!");
            //FlyupPanelListener.longClickPrev(); // go back to previous song.
            return; // forces a return, stopping the Thread
        }
        else if(action.equalsIgnoreCase("Stop"))
        {
           // FlyupPanelListener.stop(); // quit the music player here.
            return; // force a return stopping the Thread
        }
    }

    @Override
    public void run() {
    }
}