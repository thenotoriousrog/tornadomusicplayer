package thenotoriousrog.tornadoplayer.Listeners;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Layout;
import android.transition.ArcMotion;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.rengwuxian.materialedittext.MaterialEditText;

import cimi.com.easeinterpolator.EaseBackInInterpolator;
import cimi.com.easeinterpolator.EaseBounceInInterpolator;
import cimi.com.easeinterpolator.EaseBounceInOutInterpolator;
import cimi.com.easeinterpolator.EaseBreathInterpolator;
import cimi.com.easeinterpolator.EaseCircularInInterpolator;
import cimi.com.easeinterpolator.EaseCubicInInterpolator;
import cimi.com.easeinterpolator.EaseElasticInInterpolator;
import cimi.com.easeinterpolator.EaseExponentialInInterpolator;
import cimi.com.easeinterpolator.EaseQuintInInterpolator;
import cimi.com.easeinterpolator.EaseSineInInterpolator;
import io.codetail.animation.arcanimator.ArcAnimator;
import io.codetail.animation.arcanimator.Side;
import thenotoriousrog.tornadoplayer.Activities.InitializerActivity;
import thenotoriousrog.tornadoplayer.Backend.BitmapWorkshop;
import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Fragments.AddSongsToSinglePlaylistFragmentActivity;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Fragments.ModifyAllPlaylistsFragmentActivity;
import thenotoriousrog.tornadoplayer.R;

import java.util.ArrayList;

/**
 * Created by thenotoriousrog on 8/21/17.
 * This class is in control of creating responding to click events on the playlist button and also starts the behavior for the new playlist
 */

public class AddPlaylistButtonClickListener implements FloatingActionButton.OnClickListener {

    private MainUIFragment mainUIFragment; // holds the mainUIFragment that is being used to ensure that the behavior of the class is working correctly.
    private ArrayList<Playlist> PlayLists; // holds all of the Playlists that we are working with.
    private ArrayList<String> rawSongs; // holds a list of all the raw songs that the playlist fragment will be using.
    private ArrayList<SongInfo> songs; // holds all of the Songs in the form of songInfo
    private ArrayList<String> folders; // holds all of the names of the folders that we are working with.

    public final int CREATE_PLAYLIST_INTENT = 200; // result code we want to send to the open playlist intent.
    private float startingX; // holds the x position of the starting fab button.
    private float startingY; // holds the y position of the starting fab.

    private boolean createPlaylist = false; // tells the system that the user is starting the playlist and that we should start that activity.
    private String playlistString = ""; // the name of the playlist that we are using.


    // todo: this may need to be changed since we are going to want to do the arc animation for the button to show the dialog after the user selects the item.
    private com.github.clans.fab.FloatingActionMenu fab; // the floating action button that was clicked.

    public AddPlaylistButtonClickListener(MainUIFragment fragment, ArrayList<Playlist> playlists, ArrayList<String> songPaths, ArrayList<SongInfo> songInfos, ArrayList<String> Folders)
    {
        mainUIFragment = fragment;
        PlayLists = playlists;
        rawSongs = songPaths;
        songs = songInfos;
        folders = Folders;

        fab = mainUIFragment.getAddPlayListMenu();
    }

    private void showDiag() {

        //dialogDisplayed = true; // the dialog has now been displayed.

        final View dialogView = View.inflate(mainUIFragment.getActivity(), R.layout.new_playlist_dialog,null);

        final Dialog dialog = new Dialog(mainUIFragment.getActivity(), R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                revealShow(dialogView, true, dialog);
            }
        });

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK){

                    revealShow(dialogView, false, dialog);
                    return true;
                }

                return false;
            }
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }

    private void revealCloseAnimation(final View view, final Dialog dialog, int endRadius, final String str)
    {
        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, (int)startingX, (int)startingY, endRadius, 0);

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                dialog.dismiss();
                //dialog.cancel();
                view.setVisibility(View.INVISIBLE);

                if(createPlaylist)
                {
                    startPlaylistActivity(str);
                }
            }
        });

        anim.setDuration(400);
        anim.start();
    }

    private void startPlaylistActivity(String str)
    {
        Intent addSongsToPlaylistActivity = new Intent(mainUIFragment.getActivity().getApplicationContext(), AddSongsToSinglePlaylistFragmentActivity.class); // activity for adding songs to a single playlist.
        Bundle playlistIntentBundle = new Bundle();
        playlistIntentBundle.putStringArrayList("songs", rawSongs); // send in the songs to be used by the modifyAllPlaylistsFragmentActivity
        playlistIntentBundle.putStringArrayList("playlistNames", mainUIFragment.getPlaylistNames()); // send in the playlist names.
        playlistIntentBundle.putParcelableArrayList("songInfos", songs); // send in the songInfoList
        playlistIntentBundle.putStringArrayList("folders", folders); // send in the folders.
        playlistIntentBundle.putParcelableArrayList("playlists", PlayLists); // send in the playlists NOTE: remove this if we cannot do it this way as it could be causing problems.
        // playlistIntentBundle.putInt("PlaylistPosition", PlayLists.size()-1); // send in the last playlist because that is the one we just added muahaha
        playlistIntentBundle.putString("ChosenPlaylistName", str); // send in the name of the playlist that was just created.

        System.out.println("The name of the playlist that the user created is: " + str);
        addSongsToPlaylistActivity.putExtra("args", playlistIntentBundle); // send the arguments to the fragmentActivity.
        mainUIFragment.startActivityForResult(addSongsToPlaylistActivity, CREATE_PLAYLIST_INTENT); // start the activity for the result of the songs in the playlist.
    }

    private void revealShow(View dialogView, boolean b, final Dialog dialog) {

        final View view = dialogView.findViewById(R.id.newPlaylistDialog);

        System.out.println("is the dialog null upon creation? " + dialog);
        int w = view.getWidth();
        int h = view.getHeight();

        int endRadius = (int) Math.hypot(w, h);

        // todo: somehow get the animation to show on the actual position of the view itself.

        int cx = (int) (fab.getX() + (fab.getWidth()/2));
        int cy = (int) (fab.getY())+ fab.getHeight() + 56;

        final ImageView musicNote1 = view.findViewById(R.id.musicNote1);

        final ImageView musicNote2 = view.findViewById(R.id.musicNote2);
        final ImageView musicNote3 = view.findViewById(R.id.musicNote3);

        if(b){
            Animator revealAnimator = ViewAnimationUtils.createCircularReveal(view, (int)startingX, (int)startingY, 0, endRadius);

            revealAnimator.addListener(new AnimatorListenerAdapter() {

                // listens to the end animation.
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    super.onAnimationEnd(animation);

                    ObjectAnimator animator1 = ObjectAnimator.ofFloat(musicNote1, "translationX", 0, 0, 1230);
                    Interpolator interpolator = new OvershootInterpolator();
                    animator1.setInterpolator(interpolator);
                    animator1.setDuration(800);
                    animator1.start(); // start the animation.

                    ObjectAnimator animator2 = ObjectAnimator.ofFloat(musicNote2, "translationX", 0, 0, 615);
                    Interpolator interpolator2 = new OvershootInterpolator();
                    animator2.setInterpolator(interpolator2);
                    animator2.setDuration(800);
                    animator2.start(); // start the animation.

                    ObjectAnimator animator3 = ObjectAnimator.ofFloat(musicNote3, "translationX", 0, 0, 30);
                    Interpolator interpolator3 = new OvershootInterpolator();
                    animator3.setInterpolator(interpolator3);
                    animator3.setDuration(800);
                    animator3.start(); // start the animation.

                    //ObjectAnimator animator1 = ObjectAnimator.ofFloat(musicNote1, "translationX", 0, 25, 0);
                    //ObjectAnimator animator1 = ObjectAnimator.ofFloat(musicNote1, "translationX", 0, 25, 0);


                }
            });

            view.setVisibility(View.VISIBLE);
            revealAnimator.setDuration(400);
            revealAnimator.start();

        } else {

            Animator anim =
                    ViewAnimationUtils.createCircularReveal(view, (int)startingX, (int)startingY, endRadius, 0);

            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    System.out.println("Is the dialog null? " + dialog);

                    dialog.dismiss();
                    view.setVisibility(View.INVISIBLE);

                    if(createPlaylist)
                    {
                       // startPlaylistActivity(playlistString);
                    }
                }
            });

            anim.setDuration(400);
            anim.start();
        }

        final EditText enterPlaylistNameField = view.findViewById(R.id.playlistEnterField); // set the field that the user will enter their playlist name.

        Button playlistCancelButton = view.findViewById(R.id.playlistCancelButton); // cancel button that cancels everything else.
        playlistCancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int w = view.getWidth();
                int h = view.getHeight();
                int endRadius = (int) Math.hypot(w, h);
                createPlaylist = false; // playlist was not created.
                revealCloseAnimation(view, dialog, endRadius, "");
            }
        });

        Button playListConfirmButton = (Button) view.findViewById(R.id.playlistConfirmButton); // Confirm button for when a user Finishes entering the name of their playlist.
        playListConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                System.out.println("Yes was selected, we need to add songs to this playlist with the view that we want to have.");
                //addSongsDialog.dismiss(); // close the dialog after user says yes.

                createPlaylist = true; // the user has selected to create the playlist thus we must do so

                int w = view.getWidth();
                int h = view.getHeight();

                int endRadius = (int) Math.hypot(w, h);

                String str = enterPlaylistNameField.getText().toString(); // grab what the user enters for their playlist.
                System.out.println("The name in the playlist field is... " + enterPlaylistNameField.getText().toString());

                if (str.length() == 0) {
                    // TODO: decide if it is important to force a user to enter a name or let the app create one on its own. A small detail but an important one nonetheless.
                    str = "New Playlist"; // name the generic playlist for the user.
                }

                playlistString = str;
                mainUIFragment.setPlayListName(str); // set the playlist name chosen by the user.
                mainUIFragment.addEmptyPlaylist();

                mainUIFragment.refreshPlaylistAdapter();
                mainUIFragment.updatePlaylistAdapter(); // this forces a sort making the list behave much better.

                // Warning: if issues arise just uncomment the line below and pass in the viewPager via the constructor of this class from the mainUIFragment.
                // mainUIFragment.updateViewPager(viewPager.getCurrentItem()); // update the view pager after the playlist was updated.

                System.out.println("Is dialog null? " + dialog);

                mainUIFragment.updateViewPager(mainUIFragment.getCurrentPageItem()); // this should work if not see the warning comment 3 lines up.

                revealCloseAnimation(view, dialog, endRadius, str);
            }
        });

    }

    private Rect locateView(View view) {
        Rect loc = new Rect();
        int[] location = new int[2];
        if (view == null) {
            return loc;
        }
        view.getLocationOnScreen(location);

        loc.left = location[0];
        loc.top = location[1];
        loc.right = loc.left + view.getWidth();
        loc.bottom = loc.top + view.getHeight();
        return loc;
    }

    // Controls the behavior for when the plus button is pressed on in the playlists tab.
    @Override
    public void onClick(View v)
    {
        Rect viewRect = locateView(v);
        startingX = viewRect.left + ((viewRect.right - viewRect.left) / 2);
        startingY = ((viewRect.top + viewRect.bottom) / 2);

        // TODO: create the new playlist_plusbutton_dialog here and ask users for their response.
        final Dialog playlistButtonDialog = new Dialog(v.getContext()); // this is the very first dialog when the button is pushed.
        playlistButtonDialog.setContentView(R.layout.new_playlist_dialog);
        showDiag(); // the dialog using the reveal animation.

    }
}
