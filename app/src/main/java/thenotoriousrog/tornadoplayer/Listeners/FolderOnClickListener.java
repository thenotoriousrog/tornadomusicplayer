package thenotoriousrog.tornadoplayer.Listeners;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import thenotoriousrog.tornadoplayer.Activities.OpenFolderandSongs;
import thenotoriousrog.tornadoplayer.BackgroundThreads.FolderSelectedThread;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;

/**
 * Created by thenotoriousrog on 3/10/18.
 */

public class FolderOnClickListener implements AdapterView.OnItemClickListener {

    public final int OPEN_FOLDER_INTENT = 100; // this is the result code that we want to pass back to the intent that way we know to look for specific intent codes.
    private MainUIFragment mainUIFragment;

    public FolderOnClickListener(MainUIFragment mainUIFragment)
    {
        this.mainUIFragment = mainUIFragment;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long l)
    {

        // Note: if we need to do some ui update work, we need to pass in the view so we can post updates to that view and call the initiations in the thread.
        //FolderSelectedThread folderSelectedThread = new FolderSelectedThread(mainUIFragment, position, parent);
        //folderSelectedThread.startBackgroundThread();


        // This needs to go in a background thread.
        Intent folderSelectedIntent = new Intent(mainUIFragment.getActivity(), OpenFolderandSongs.class);
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("FoldersList", mainUIFragment.getFolders());
        bundle.putInt("SelectedFolderPosition", position);
        System.out.println("item selected posoition: " + parent.getSelectedItemPosition());
        System.out.println("position of the folder that was selected is: " + position);
        folderSelectedIntent.putExtra("FolderListBundle", bundle);
        mainUIFragment.startActivityForResult(folderSelectedIntent, OPEN_FOLDER_INTENT); // start the activity.

    }
}
