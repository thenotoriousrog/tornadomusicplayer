package thenotoriousrog.tornadoplayer.Listeners;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SerializeObject;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.R;

import org.cmc.music.common.ID3WriteException;
import org.cmc.music.metadata.IMusicMetadata;
import org.cmc.music.metadata.MusicMetadata;
import org.cmc.music.metadata.MusicMetadataSet;
import org.cmc.music.myid3.MyID3;
import org.cmc.music.myid3.MyID3v2Write;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.generic.AudioFileReader;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

;

/**
 * Created by thenotoriousrog on 8/15/17.
 * This class is in charge of controlling what happens when a user selects one of the options from the popup menu for a specific song.
 */

public class SongPopupMenuClickListener implements PopupMenu.OnMenuItemClickListener {

    private Context context; // the context that is used for pinning popup menu items to views.
    private ImageView songOptionsMenuView; // holds the three dot image that we can bind other pop messages too.
    private MainUIFragment mainUIFragment; // the main ui fragment needed in order to get the appropriate data that our users are going to need.
    private SongInfo song; // the song that was chosen by the user.

    private float startingX; // holds the x position of the starting fab button.
    private float startingY; // holds the y position of the starting fab.

    // This constructor may have to take more arguments in order to get all of the options to work correctly.
    public SongPopupMenuClickListener(Context c, ImageView optionsView, MainUIFragment fragment, SongInfo songChosen)
    {
        context = c;
        songOptionsMenuView = optionsView;
        mainUIFragment = fragment;
        song = songChosen;
    }

    // Writes the playlists to MainMemory
    public void writePlaylistsToMainMemory(View view, ArrayList<Playlist> playlists)
    {
        // WRITE the playlist object to main memory.
        String ser = SerializeObject.objectToString(playlists); // Trying to serialize the entire Playlist Arraylist, Not sure if it is possible or not yet.
        if(ser != null && !ser.equalsIgnoreCase(""))
        {
            String savedPlaylistFileName = "playlists.dat"; // should be something like "Playlist 1.dat"
            SerializeObject.WriteSettings(view.getContext(), ser, savedPlaylistFileName); // write the item to main memory.
        }
        else // Writing the obeject failed. Think of a better way to handle this if at all.
        {
            System.out.println("WE DID NOT WRITE THE LIST CORRECTLY, SOMETHING BAD HAPPENED.");
            SerializeObject.WriteSettings(view.getContext(), "", "playlists.dat"); // we should be getting this list if we are something bad has happened.
        }

    }

    // This method sets the listener for when a user chooses the playlist item that they wanted to add the song to, very important!
    public void listenForSelectedPlaylist(PopupMenu playlistToPickMenu)
    {
        System.out.println("Setting click listeners now");
        playlistToPickMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {


            @Override
            public boolean onMenuItemClick(MenuItem item)
            {
                System.out.println("A playlist item was clicked");
                String playlistName = item.getTitle().toString(); // get the name of the playlist that was chosen.

                ArrayList<SongInfo> songs = null; // holds all of the songs of the playlist that we want to add to.
                ArrayList<Playlist> updatedList = mainUIFragment.getPlaylists(); // the list playlist that we are going to update once the user finds the songs that they are looking for.
                int playlistPos = 0; // holds the position of the playlist to ensure that when updated, the playlist is modified in the correct order and saved in the correct order.

                // loop through the playlists and find the playlist with the name of the playlist that we are trying to work on.
                for(int i = 0; i < mainUIFragment.getPlaylists().size(); i++)
                {
                    System.out.println("looping...");
                    if(mainUIFragment.getPlaylists().get(i).name().equals(playlistName)) // we have found the playlist that we want to work with.
                    {
                        System.out.println("the name of the playlist that the user chose is: " + playlistName);
                        songs = mainUIFragment.getPlaylists().get(i).songs(); // set the songs that we are going to add to this playlist.
                        songs.add(song); // add the song that the user has chosen to add to the playlist.
                        playlistPos = i; // the position of the playlist that is being updated.
                    }
                }

                // TODO: have a test to make sure that the songs is not null and if it is null we need to return and quit this action and tell the user why for whatever reason.

                Playlist updatedPlaylist = new Playlist(playlistName, songs); // update the playlist.

                updatedList.remove(playlistPos); // remove the previous version of the playlist in this position.
                updatedList.add(playlistPos, updatedPlaylist); // readd the playlist in the same position but with the song added this time.

                writePlaylistsToMainMemory(mainUIFragment.getView(), updatedList); // write the updated playlists to main memory again!

                return true; // tell the user now that everything is correct! todo: display a snackbar message to alert the user that their song has been added successfully to their playlist.
            }
        });
    }

    // This message makes sure that all of the items in the fields are filled out with something before changing tags so users do not mess up their songs.
    public boolean isReadyToChange(String str1, String str2, String str3)
    {
        if( (str1.length() >= 1) && (str2.length() >= 1) && (str3.length() >= 1) ) // all fields are filled out and ready to be saved.
        {
            return true; // user has entered something in all three fields.
        }
        else // one or more of the fields is blank, must fix.
        {
            return false; // inform user to add something into the fields.
        }
    }

    private void showDiag() {

        //dialogDisplayed = true; // the dialog has now been displayed.

        final View dialogView = View.inflate(mainUIFragment.getActivity(), R.layout.edit_tags_layout,null);

        final Dialog dialog = new Dialog(mainUIFragment.getActivity(), R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                revealShow(dialogView, true, dialog);
            }
        });

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK){

                    revealShow(dialogView, false, dialog);
                    return true;
                }

                return false;
            }
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }

    private void revealCloseAnimation(final View view, final Dialog dialog, int endRadius, final String str)
    {
        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, (int)startingX, (int)startingY, endRadius, 0);

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                dialog.dismiss();
                //dialog.cancel();
                view.setVisibility(View.INVISIBLE);
            }
        });

        anim.setDuration(400);
        anim.start();
    }

    private void revealShow(View dialogView, boolean b, final Dialog dialog) {

        final View view = dialogView.findViewById(R.id.editTagsLayout);

        System.out.println("is the dialog null upon creation? " + dialog);
        int w = view.getWidth();
        int h = view.getHeight();

        int endRadius = (int) Math.hypot(w, h);

        // todo: somehow get the animation to show on the actual position of the view itself.

        int cx = (int) (songOptionsMenuView.getX() + (songOptionsMenuView.getWidth()/2));
        int cy = (int) (songOptionsMenuView.getY())+ songOptionsMenuView.getHeight() + 56;

        final ImageView musicNote1 = view.findViewById(R.id.musicNote1);

        final ImageView musicNote2 = view.findViewById(R.id.musicNote2);
        final ImageView musicNote3 = view.findViewById(R.id.musicNote3);

        if(b){
            Animator revealAnimator = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, endRadius);

            revealAnimator.addListener(new AnimatorListenerAdapter() {

                // listens to the end animation.
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    super.onAnimationEnd(animation);

//                    ObjectAnimator animator1 = ObjectAnimator.ofFloat(musicNote1, "translationX", 0, 0, 1230);
//                    Interpolator interpolator = new OvershootInterpolator();
//                    animator1.setInterpolator(interpolator);
//                    animator1.setDuration(800);
//                    animator1.start(); // start the animation.
//
//                    ObjectAnimator animator2 = ObjectAnimator.ofFloat(musicNote2, "translationX", 0, 0, 615);
//                    Interpolator interpolator2 = new OvershootInterpolator();
//                    animator2.setInterpolator(interpolator2);
//                    animator2.setDuration(800);
//                    animator2.start(); // start the animation.
//
//                    ObjectAnimator animator3 = ObjectAnimator.ofFloat(musicNote3, "translationX", 0, 0, 30);
//                    Interpolator interpolator3 = new OvershootInterpolator();
//                    animator3.setInterpolator(interpolator3);
//                    animator3.setDuration(800);
//                    animator3.start(); // start the animation.
//
//                    //ObjectAnimator animator1 = ObjectAnimator.ofFloat(musicNote1, "translationX", 0, 25, 0);
//                    //ObjectAnimator animator1 = ObjectAnimator.ofFloat(musicNote1, "translationX", 0, 25, 0);


                }
            });

            view.setVisibility(View.VISIBLE);
            revealAnimator.setDuration(400);
            revealAnimator.start();

        } else {

            Animator anim =
                    ViewAnimationUtils.createCircularReveal(view, (int)startingX, (int)startingY, endRadius, 0);

            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    System.out.println("Is the dialog null? " + dialog);

                    dialog.dismiss();
                    view.setVisibility(View.INVISIBLE);

                }
            });

            anim.setDuration(400);
            anim.start();
        }

        //final EditText enterPlaylistNameField = view.findViewById(R.id.playlistEnterField); // set the field that the user will enter their playlist name.

        Button editCancelButton = view.findViewById(R.id.editCancelButton); // cancel button that cancels everything else.
        editCancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int w = view.getWidth();
                int h = view.getHeight();
                int endRadius = (int) Math.hypot(w, h);
                //createPlaylist = false; // playlist was not created.
                revealCloseAnimation(view, dialog, endRadius, "");
            }
        });

        Button editSaveButton = (Button) view.findViewById(R.id.editSaveButton); // Confirm button for when a user Finishes entering the name of their playlist.
        editSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                System.out.println("Yes was selected, we need to add songs to this playlist with the view that we want to have.");
                //addSongsDialog.dismiss(); // close the dialog after user says yes.

               // createPlaylist = true; // the user has selected to create the playlist thus we must do so

                int w = view.getWidth();
                int h = view.getHeight();

                int endRadius = (int) Math.hypot(w, h);

                final EditText songTitle = (EditText) dialog.findViewById(R.id.editSongTitle); // song title edit field.
                final EditText songArtist = (EditText) dialog.findViewById(R.id.editSongArtist); // song artist edit field.
                final EditText songAlbum = (EditText) dialog.findViewById(R.id.editSongAlbum); // song album edit field.

                // pull data from the entered fields.
                String enteredTitle = songTitle.getText().toString(); // grab the text that the user entered.
                String enteredArtist = songArtist.getText().toString(); // grab the artist name that the user entered.
                String enteredAlbum = songAlbum.getText().toString(); // grab the album name that the user entered.

                if(isReadyToChange(enteredTitle, enteredArtist, enteredAlbum) == true) // all fields are entered and ready to be updated.
                {
                    File chosenSong = new File(song.getSongPath()); // convert the song that was chosen to the songPath to be edited by the app.
                    //File chosenSong = new File(songsToPick.get(position).getSongPath()); // convert the song path to be edited.

                    System.out.println("Song I am trying to write: " + song.getSongPath());

                    try {
                        AudioFile audioFile = AudioFileIO.read(chosenSong.getAbsoluteFile());
                        Tag tag = audioFile.getTag();
                        tag.setField(FieldKey.TITLE, enteredTitle);
                        tag.setField(FieldKey.ARTIST, enteredArtist);
                        tag.setField(FieldKey.ALBUM, enteredAlbum);
                        audioFile.setTag(tag);
                        AudioFileIO.write(audioFile);
                        //audioFile.commit();

                    } catch (CannotReadException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (TagException e) {
                        Toast.makeText(v.getContext(), "I could not understand what you wrote so I couldn't update your song :(", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
                        e.printStackTrace();
                    } catch (ReadOnlyFileException e) {
                        Toast.makeText(v.getContext(), "I don't have permission to change this song :/", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
                        e.printStackTrace();
                    } catch (InvalidAudioFrameException e) {
                        e.printStackTrace();
                    } catch (CannotWriteException e) {
                        Toast.makeText(v.getContext(), "I could not edit your song :(", Toast.LENGTH_SHORT).show(); // let the user know that something failed
                        e.printStackTrace();
                    }
//
//                    try // attempt to set the source of the song file to be edited.
//                    {
//                        srcSet = new MyID3().read(chosenSong); // set the source to be edited.
//                    }
//                    catch (IOException ex) {
//                        System.out.println("received error while editing song tags: " + ex.getMessage());
//                        // may need to print stack trace if error becomes too difficult to understand.
//                    }
//
//                    if(srcSet == null) // if null, there is a problem, or something failed.
//                    {
//                        Toast.makeText(v.getContext(), "I could find your song :(", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
//                        System.out.println("srcSet is null for some reason, figure out why.");
//                    }
//                    else // nothing went wrong, we can edit the tags of the song.
//                    {
//                        // change the song information to what the user entered.
//                        MusicMetadata songMetaData = new MusicMetadata("name");
//                        songMetaData.setSongTitle(enteredTitle); // change the title of the song.
//                        songMetaData.setArtist(enteredArtist); // change the artist of the song.
//                        songMetaData.setAlbum(enteredAlbum); // change the album name of the song.
//
//                        // Overwrite the song in the same location that it was grabbed from.
//                        try
//                        {
//                            new MyID3().write(chosenSong, chosenSong.getAbsoluteFile(), srcSet, songMetaData); // overwrite the song that the user has chosen to overwrite.
//                            //new MyID3v2Write(chosenSong);
//                            Toast.makeText(v.getContext(), "Once I restart your song will be changed! :D", Toast.LENGTH_SHORT).show(); // let the user know that the changes went through and when they should see it.
//
//
//                        } catch (UnsupportedEncodingException ueex) {
//                            System.out.println("Caught an UnsupportedEncodingException: " + ueex.getMessage());
//                            Toast.makeText(v.getContext(), "I could not understand what you wrote so I couldn't update your song :(", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
//                        } catch (ID3WriteException id3wex) {
//                            System.out.println("Caught ID3WriteException: " + id3wex.getMessage());
//                            Toast.makeText(v.getContext(), "Something happened when I was changing your song, please try again!", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
//                        } catch (IOException ex) {
//                            System.out.println("Caught IOException while writing song: " + ex.getMessage());
//                            Toast.makeText(v.getContext(), "I could not edit your song :(", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
//                        }
//
//                        //editDialog.dismiss(); // close edit tags dialog.
//
//                    }
                }
                else // user did not enter all fields.
                {
                    Toast.makeText(v.getContext(), "You have to fill out all of the fields silly :P", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
                }

                System.out.println("Is dialog null? " + dialog);

                revealCloseAnimation(view, dialog, endRadius, "");
            }
        });

    }

    private Rect locateView(View view) {
        Rect loc = new Rect();
        int[] location = new int[2];
        if (view == null) {
            return loc;
        }
        view.getLocationOnScreen(location);

        loc.left = location[0];
        loc.top = location[1];
        loc.right = loc.left + view.getWidth();
        loc.bottom = loc.top + view.getHeight();
        return loc;
    }

    // This method decides what action to take place when the user clicks the option that they want to do.
    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        if(item.getTitle().toString().equalsIgnoreCase("Add to playlist...")) // user has chosen to add this specific song to a playlist.
        {
            // generate another popup menu on the options view that lists the playlists.
            PopupMenu playlistToPickMenu = new PopupMenu(context, songOptionsMenuView); // bind the new options menu to the same menu view.

            // Loop through every playlist and add each playlist name to the new popup playlist item.
            for(int i = 0; i < mainUIFragment.getPlaylists().size(); i++)
            {
                playlistToPickMenu.getMenu().add(mainUIFragment.getPlaylists().get(i).name()); // add the name of the playlist to the menu
            }


            // replace the popup with the popup of the list menu itself.
            MenuInflater inflater = playlistToPickMenu.getMenuInflater();
            inflater.inflate(R.menu.playliststoaddfrom_popup_menu, playlistToPickMenu.getMenu()); // get the menu
            listenForSelectedPlaylist(playlistToPickMenu); // listens for a user to add songs to their playlist and then does that.
            playlistToPickMenu.show(); // show this menu now very important.

            // todo:  create a snackbar message telling the user that the option they wanted to do was successful.
        }
        else if(item.getTitle().toString().equalsIgnoreCase("Edit tags")) // user chose to edit the tags on this song.
        {
            // todo: I NEED TO GET THIS WORKING! I DISABLED IT FOR NOW BUT NEEDS TO BE RELEASED QUICKLY!

            Toast.makeText(mainUIFragment.getContext(), "Coming soon to an app near you!", Toast.LENGTH_SHORT).show();

            // todo: the user needs to show access to the storage access framework otherwise we cannot make edits into the song. Very important to get this working.
//            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context); // gets the default shared preferences
//            boolean hasStorageAccess = sharedPreferences.getBoolean("hasStorageAccess", false); // default is false.
//
//            if(hasStorageAccess)
//            {
//                Rect viewRect = locateView(item.getActionView());
//                startingX = viewRect.left + ((viewRect.right - viewRect.left) / 2);
//                startingY = ((viewRect.top + viewRect.bottom) / 2);
//
//                // TODO: create the new playlist_plusbutton_dialog here and ask users for their response.
//                //final Dialog playlistButtonDialog = new Dialog(songOptionsMenuView.getContext()); // this is the very first dialog when the button is pushed.
//                //playlistButtonDialog.setContentView(R.layout.new_playlist_dialog);
//                //showDiag(); // the dialog using the reveal animation.
//
//                // create an additional dialog box that will let the user enter the song fields, and then we want to modify the song based on that.
//                final Dialog editDialog = new Dialog(context); // create the dialog for the user to be able to edit the tags of the song.
//                editDialog.setContentView(R.layout.edit_tags_layout); // set the layout for when users want to change the tags of the song itself.
//                //editDialog.setTitle("Fix it up!");
//
//                // todo: I NEED TO GET THIS WORKING! I DISABLED IT FOR NOW BUT NEEDS TO BE RELEASED QUICKLY!
//                //showDiag();
//            }
//            else // we need to tell the user what the problem is and send that activity for the main ui fragment to handle.
//            {
//
//                Intent storageAccessIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
//                mainUIFragment.startActivityForResult(storageAccessIntent, Constants.INTENT.GAIN_STORAGE_ACCESS); // this will allow us to gain storage access from the document tree!
//            }


        }


        return true;
    }
}

