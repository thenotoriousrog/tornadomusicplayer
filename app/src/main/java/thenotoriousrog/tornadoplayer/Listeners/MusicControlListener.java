package thenotoriousrog.tornadoplayer.Listeners;

import android.animation.ObjectAnimator;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Message;
import android.os.Process;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.ImageView;

import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Backend.SoundSpoutSpeech;
import thenotoriousrog.tornadoplayer.Backend.ThemeUtils;
import thenotoriousrog.tornadoplayer.Handlers.ServiceHandler;
import thenotoriousrog.tornadoplayer.R;
import thenotoriousrog.tornadoplayer.UI.CountdownTimer;

import java.io.IOException;
import java.util.Random;

/**
 * Created by thenotoriousrog on 5/29/17.
 *
 * Controls the behavior when a music control is selected. implements both normal click and a long click.
 */

public class MusicControlListener implements ImageView.OnClickListener, View.OnLongClickListener {

    private ImageView action; // controls an action that we need to perform.
    private String actionCode; // tells us what action needs to be performed by this item
    //private MediaPlayer mediaPlayer; // allows us to control the music based on an action.
    private View panel; // the main view that we need in order to change up the images.
    private FlyupPanelListener flyupPanelListener; // holds the FlyupPanelListener we are using to help keep the data from each users action working smoothely and correctly.
    //private SelectedSongPlayer ssp; // selected song player that we can use to get some information.
    private String prevSongPath = ""; // holds the previous song path.
    private CountdownTimer songTimer; // songTimer that is used between SelectedSongPlayer and MusicControlListener

    private ObjectAnimator spinEffect; // this is the animation for whenever an item is selected on the control layout.

    private int clickCounter = 0; // the click counter that's needed.
    private long clickDuration = 0; // the duration since the last click.
    private long firstClickTime = 0; // the system time since the very first click.
    private final int ONE_SECOND = 1000; // one thousand milliseconds = 1 second.
    private ServiceHandler serviceHandler; // holds a copy of the service handler
    private HandlerThread handlerThread;

    ImageView skip; // the skip button
    ImageView replay; // the replay/prev button.

    // constructor to have some of the important things needed for us to make this click listener.
    public MusicControlListener(ImageView selectedAction, String code, View p, FlyupPanelListener fpl, ServiceHandler serviceHandler) {
        action = selectedAction;
        actionCode = code;
        //mediaPlayer = mp;
        panel = p;
        flyupPanelListener = fpl;

        //handlerThread = new HandlerThread(Constants.HANDLER.MUSIC_PLAYER_HANDLER, Process.THREAD_PRIORITY_BACKGROUND);
        //handlerThread.start();

        this.serviceHandler = serviceHandler;
        //serviceHandler = new ServiceHandler(handlerThread.getLooper()); // create our copy of the service handler to be used throughout the application.

        if(MusicLibrary.isShuffleOn()) // if true, we need to ensure that the state of the shuffle button is showing, only true if the service is running and restarting the activities.
        {
            ImageView shuffleOff = (ImageView) panel.findViewById(R.id.shuffleOff); // get a copy of the shuffle off button.
            shuffleOff.setVisibility(View.INVISIBLE);
            ImageView shuffleOn = (ImageView) panel.findViewById(R.id.shuffleOn);
            shuffleOn.setVisibility(View.VISIBLE); // make the shuffle button visible to match state of the UI.

            shuffleOn.setImageResource(ThemeUtils.getInstance().getCurrentShuffleIcon());

        }

        skip = panel.findViewById(R.id.skipButton);
        replay = panel.findViewById(R.id.prevButton);

        // Set the action for the action when
        spinEffect = ObjectAnimator.ofFloat(action, "rotationY", 0.0f, 720.0f); // spin the icon whenever the list is moving, very important to get this working correctly.
        spinEffect.setDuration(800); // the higher the number the slower the spinning animation
        //spin.setRepeatCount(ObjectAnimator.); // repeat constantly
        spinEffect.reverse(); // reverse the spinning animation
    }

    // starts the spinning animation for the icon that is clicked by the user.
    public void startSpinAnimation()
    {
        spinEffect.start();
    }


    // tells the system to replay the current song
    private void replay()
    {
        // make sure that play and pause are in their correct order. After skipping, flip the play button to be pause.
        ImageView pause = (ImageView) panel.findViewById(R.id.pauseButton);
        pause.setVisibility(View.VISIBLE);

        ImageView play = (ImageView) panel.findViewById(R.id.playButton);
        play.setVisibility(View.INVISIBLE);

        // sends a replay message to the service handler.
        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = Constants.COMMAND.REPLAY; // allow for message to send an alert that a song was selected and is ready to be played.
        serviceHandler.sendMessage(msg); // send the message to the service Handler

        // reset countdown timer
        SongInfo currSong = MusicLibrary.getCurrSong();
        CountdownTimer t = new CountdownTimer(currSong.getSongDuration(), panel); // get current song timer
        flyupPanelListener.cancelAndFinishTimer();
        flyupPanelListener.setTimer(t);
        flyupPanelListener.startTimer();
    }

    // tells the system to go back to the previous song.
    private void prev()
    {
        System.out.println("Previous button was selected (long click).");

        Message msg = serviceHandler.obtainMessage();
        msg.arg1 = Constants.COMMAND.PREV; // allow for message to send an alert that a song was selected and is ready to be played.
        serviceHandler.sendMessage(msg); // send the message to the service Handler
    }

    // controls the behavior of an action when the user selects an action.
    @Override
    public void onClick(View v)
    {
        // todo: fix this so that the FlyupPanelListener accurately pauses.
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(v.getContext()); // gets the default shared preferences
        String theme = sharedPreferences.getString("Theme", "OceanMarina"); // ocean marina is the default theme that needs to be used unless the user specifies otherwise.

        // define the user actions.
        if(actionCode.equalsIgnoreCase("pause")) // a specific problem for when an action is clicked. ONLY listener is on Pause, create new one for Play!
        {
            // TODO: need to send a msg to pause.
           // mediaPlayer.pause(); // pause the song.


            action.setVisibility(View.INVISIBLE);

            //songTimer.cancel(); // todo: investigate what happends when we press pause and see how the timer reacts.
            // todo: we want to pause the timer when the song is paused. We can likely create this function in the CountdownTimer class, DO THAT! ALSO MAKE THE PAUSED TIME FLASH!!!

            flyupPanelListener.pauseTimer(); // simply cancel the timer.

            ImageView play = (ImageView) panel.findViewById(R.id.playButton);
            play.setVisibility(View.VISIBLE);

            //flyupPanelListener.getMainUIFragment().getMediaSession().pauseMediaSession(); // forces the notification to show the pause button when pressing play from within the app itself.

            Message msg = serviceHandler.obtainMessage();
            msg.arg1 = Constants.COMMAND.PAUSE;
            serviceHandler.sendMessage(msg);


            ObjectAnimator spinEffect = ObjectAnimator.ofFloat(play, "rotationY", 0.0f, 720.0f); // spin the icon whenever the list is moving, very important to get this working correctly.
            spinEffect.setDuration(800); // the higher the number the slower the spinning animation
            //spin.setRepeatCount(ObjectAnimator.); // repeat constantly
            spinEffect.reverse(); // reverse the spinning animation
            spinEffect.start();
        }
        else if(actionCode.equalsIgnoreCase("play")) // this should not be needed anymore we can remove this.
        {
            flyupPanelListener.resumeTimer(); // resume the timer.

            action.setVisibility(View.INVISIBLE); // make the pause button disappear.

            ImageView pause = (ImageView) panel.findViewById(R.id.pauseButton);
            pause.setVisibility(View.VISIBLE); // make the pause button visible again.

            Message msg = serviceHandler.obtainMessage();
            msg.arg1 = Constants.COMMAND.PLAY;
            serviceHandler.sendMessage(msg);

            ObjectAnimator spinEffect = ObjectAnimator.ofFloat(pause, "rotationY", 0.0f, 720.0f); // spin the icon whenever the list is moving, very important to get this working correctly.
            spinEffect.setDuration(800); // the higher the number the slower the spinning animation
            //spin.setRepeatCount(ObjectAnimator.); // repeat constantly
            spinEffect.reverse(); // reverse the spinning animation
            spinEffect.start();

        }
        else if(actionCode.equalsIgnoreCase("skip"))
        {
            // make sure that play and pause are in their correct order. After skipping, flip the play button to be pause.
            // todo: change the pause and play flipping to only occur after a check to see if it is visible when it shouldn't be then make it invisible.
            ImageView pause = (ImageView) panel.findViewById(R.id.pauseButton);
            pause.setVisibility(View.VISIBLE);

            ImageView play = (ImageView) panel.findViewById(R.id.playButton);
            play.setVisibility(View.INVISIBLE);

            //ImageView skip = panel.findViewById(R.id.skipButton);

            System.out.println("skip button is being initiated!");
            Message msg = serviceHandler.obtainMessage();
            msg.arg1 = Constants.COMMAND.SKIP; // allow for message to send an alert that a song was selected and is ready to be played.
            serviceHandler.sendMessage(msg); // send the message to the service Handler


//            ObjectAnimator spinEffect = ObjectAnimator.ofFloat(skip, "rotationY", 0.0f, 360.0f); // spin the icon whenever the list is moving, very important to get this working correctly.
//            spinEffect.setDuration(800); // the higher the number the slower the spinning animation
//            //spin.setRepeatCount(ObjectAnimator.); // repeat constantly
//            spinEffect.reverse(); // reverse the spinning animation
//            spinEffect.start();
        }
        else if(actionCode.equalsIgnoreCase("replay")) // user pressed the back button once, causing the current song to be replayed.
        {
           // todo: update the song an album titles in music controls whenever we go to the previous song.

            System.out.println("Replay was pressed (previous) with media duration: " + MusicLibrary.getMediaDuration());

            //ImageView replay = panel.findV

            // if the song is less than 2 seconds in just go back to the last song.
            if(MusicLibrary.getCurrentSongTimePosition() < 2000)
            {
                prev();
            }
            else {
                replay();
            }

//            ObjectAnimator spinEffect = ObjectAnimator.ofFloat(replay, "rotationY", 0.0f, 360.0f); // spin the icon whenever the list is moving, very important to get this working correctly.
//            spinEffect.setDuration(800); // the higher the number the slower the spinning animation
//            //spin.setRepeatCount(ObjectAnimator.); // repeat constantly
//            spinEffect.reverse(); // reverse the spinning animation
//            spinEffect.start();
        }
        else if(actionCode.equalsIgnoreCase("shuffleOff")) // turning on shuffle, this stuff below seems to be working fine.
        {

            // WARNING** this appears to be working correctly now, do not modify unless sure that this is the problem!!!

            // todo: may be able to have FlyupPanelListener take care of this! Decide if we need to!
            action.setVisibility(View.INVISIBLE); // make the black shuffle button invisible.
            ImageView shuffleOn = (ImageView) panel.findViewById(R.id.shuffleOn);
            shuffleOn.setVisibility(View.VISIBLE);

            shuffleOn.setImageResource(ThemeUtils.getInstance().getCurrentShuffleIcon());

            // todo: send a message to start shuffling
            Message shuffleOnMsg = serviceHandler.obtainMessage();
            shuffleOnMsg.arg1 = Constants.COMMAND.SHUFFLE_ON;
            serviceHandler.sendMessage(shuffleOnMsg);

            // TODO: keep the lines below for now, but we have to create some other class for all the different types of sayings here real real soon.
            CoordinatorLayout snackBarLocation = (CoordinatorLayout) panel.findViewById(R.id.snackbarlocation); // grab the snackbarlocation we wish to use.
            Snackbar.make(snackBarLocation, SoundSpoutSpeech.getShuffleOnMessage(), Snackbar.LENGTH_SHORT).show(); // shuffle is turned on, display appropriate message.

        }
        else if(actionCode.equalsIgnoreCase("shuffleOn")) // turning shuffle off.
        {
            // todo: may be able to have FlyupPanelListener take care of this! Decide if we need to!
            action.setVisibility(View.INVISIBLE); // make the blue shuffle button invisible
            ImageView shuffleOff = (ImageView) panel.findViewById(R.id.shuffleOff);
            shuffleOff.setVisibility(View.VISIBLE); // make the black shuffle button invisible

            Message shuffleOffMsg = serviceHandler.obtainMessage();
            shuffleOffMsg.arg1 = Constants.COMMAND.SHUFFLE_OFF;
            serviceHandler.sendMessage(shuffleOffMsg);

            // TODO: We can keep the lines below as they are until we have a new class to get the SoundSpout messages.
            CoordinatorLayout snackBarLocation = (CoordinatorLayout) panel.findViewById(R.id.snackbarlocation); // grab the snackbarlocation we wish to use.
            Snackbar.make(snackBarLocation, SoundSpoutSpeech.getShuffleOffMessage(), Snackbar.LENGTH_SHORT).show(); // shuffle is turned off, display appropriate message
        }
        else if(actionCode.equalsIgnoreCase("repeatOff")) // turning on repeat
        {
            // todo: may be able to have FlyupPanelListener take care of this! Decide if we need to!
            action.setVisibility(View.INVISIBLE); // make this turn blue.
            ImageView repeatOn = (ImageView) panel.findViewById(R.id.repeatOn);
            repeatOn.setVisibility(View.VISIBLE); // make the blue repeat button turn on.

            repeatOn.setImageResource(ThemeUtils.getInstance().getCurrentRepeatIcon());


           // mediaPlayer.setLooping(true); // start looping the that is playing.
            Message repeatOnMsg = serviceHandler.obtainMessage();
            repeatOnMsg.arg1 = Constants.COMMAND.REPEAT_ON;
            serviceHandler.sendMessage(repeatOnMsg);


            CoordinatorLayout snackBarLocation = (CoordinatorLayout) panel.findViewById(R.id.snackbarlocation); // grab the snackbarlocation we wish to use.
            Snackbar.make(snackBarLocation, SoundSpoutSpeech.getRepeatOnMessage(), Snackbar.LENGTH_SHORT).show(); // repeat is turned on, show appropriate message.

        }
        else if(actionCode.equalsIgnoreCase("repeatOn")) // turning off repeat
        {
            // todo: may be able to have FlyupPanelListener take care of this! Decide if we need to!
            action.setVisibility(View.INVISIBLE); // make this turn black.
            ImageView repeatOff = (ImageView) panel.findViewById(R.id.repeatOff);
            repeatOff.setVisibility(View.VISIBLE); // make the blue repeat button turn on.

            //mediaPlayer.setLooping(false); // stop looping the song.
            Message repeatOffMsg = serviceHandler.obtainMessage();
            repeatOffMsg.arg1 = Constants.COMMAND.REPEAT_OFF;
            serviceHandler.sendMessage(repeatOffMsg);

            CoordinatorLayout snackBarLocation = (CoordinatorLayout) panel.findViewById(R.id.snackbarlocation); // grab the snackbarlocation we wish to use.
            Snackbar.make(snackBarLocation, SoundSpoutSpeech.getRepeatOffMessage(), Snackbar.LENGTH_SHORT).show(); // repeat is turned off, show appropriate message.
        }
        else {} // do nothing, it isn't an action we can do.
    }

    // This option means that the user has decided to long click the previous button. This will cause the previous song to be played.
    // todo: decide if it's safe to remove this method or not. I believe so, but want to be sure.
    @Override
    public boolean onLongClick(View v)
    {
        if(actionCode.equalsIgnoreCase("prev")) // previous button was selected.
        {
            SongInfo currentSong = MusicLibrary.getCurrSong(); // get the current song and hold it to show the users what the next song is going to be.
            SongInfo prevSong = MusicLibrary.getPrevSong(); // get the previous song in the list.

            System.out.println("Previous button was selected (long click).");

            //startSpinAnimation(); // start the spinning icon after the user chooses to select the previous icon.

            // NOTE: prevSong, is going to be the current song, and the currentSong is going to be the song that we are going to show next.
            if(prevSong != null)
            {
                MusicLibrary.setCurrentSongNumber(MusicLibrary.getCurrentSongNumber() - 1); // remove 1 from the current song number since we are going back a song.

                if(MusicLibrary.isShuffleOn() == false) // if shuffle is off than we want to make sure that we show the next song when they go back.
                {
                    flyupPanelListener.updateSliderData(prevSong, currentSong); // prevSng will be shown and played, and the current song that is playing now we be shown to be next.
                    flyupPanelListener.refreshSliderLayout(); // force fields to update.
                }
                else // shuffle is on.
                {
                    SongInfo nextSong = MusicLibrary.peekNextSong(); // this will allow us to see the next song to be played when on shuffle this is important.
                    flyupPanelListener.updateSliderData(prevSong, nextSong);
                    flyupPanelListener.refreshSliderLayout(); // force fields to update.
                }

//                try
//                {
//                    // stop current song and begin to play the next song.
//                    mediaPlayer.stop();
//                    mediaPlayer.reset();
//                    mediaPlayer.setDataSource(prevSong.getSongPath());
//                    mediaPlayer.prepare();
//
//                    mediaPlayer.start();
//
//                } catch (IOException e) {
//                    System.out.println("Error while trying to play previous song.");
//                    e.printStackTrace();
//                }

            }
            else // prevSong is null, we are at the start of the list.
            {
                // TODO: let the user know that they cannot go back because they are at the beginning of the list.
                System.out.println("We are at the start of the list, we cannot go back to anything.");
            }
        }

        return false;
    }
}
