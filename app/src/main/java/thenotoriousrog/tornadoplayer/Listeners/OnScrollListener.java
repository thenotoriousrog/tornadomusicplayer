package thenotoriousrog.tornadoplayer.Listeners;

import android.widget.AbsListView;

/**
 * Created by thenotoriousrog on 3/10/18.
 *
 * A custom scroll listener to make scrolling much faster.
 */

public class OnScrollListener implements AbsListView.OnScrollListener{

    private int previousFirstVisibleItem = 0;
    private long previousEventTime = 0;
    private double speed = 0;

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {

    }

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount)
    {
        if (previousFirstVisibleItem != firstVisibleItem){
            long currTime = System.currentTimeMillis();
            long timeToScrollOneElement = currTime - previousEventTime;
            speed = ((double)1/timeToScrollOneElement)*10;

            previousFirstVisibleItem = firstVisibleItem;
            previousEventTime = currTime;

           // Log.d("DBG", "Speed: " +speed + " elements/second");
        }
    }
}
