package thenotoriousrog.tornadoplayer.Listeners;


import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Stack;

import thenotoriousrog.tornadoplayer.Activities.InitializerActivity;
import thenotoriousrog.tornadoplayer.Backend.BitmapWorkshop;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.ShuffleQueue;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.BackgroundThreads.FlyupPanelSongSelectedThread;
import thenotoriousrog.tornadoplayer.BackgroundThreads.ShuffleThread;
import thenotoriousrog.tornadoplayer.BackgroundThreads.UpdateMediaSessionThread;
import thenotoriousrog.tornadoplayer.BluetoothSupport.BluetoothControlCenter;
import thenotoriousrog.tornadoplayer.BluetoothSupport.MediaButtonIntentReceiver;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Handlers.UIHandler;
import thenotoriousrog.tornadoplayer.NotificationManager.NotificationReceiver;
import thenotoriousrog.tornadoplayer.R;
import thenotoriousrog.tornadoplayer.UI.CountdownTimer;
import thenotoriousrog.tornadoplayer.UI.FlyupPanelController;
import thenotoriousrog.tornadoplayer.NotificationManager.NotificationHandler;

/**
 * Created by thenotoriousrog on 7/12/17.
 * This class is in charge of controlling what happens when the panel is sliding. When a song is changed, it is reflected on the sliding up panel layout from whichever listener sets the song to it.
 * It will also show which song is playing next and will show if Shuffle and/or repeat is engaged on the app itself. This is important because we do not want shuffle/repeat to be turned off randomly.
 * Potential problems: This class is not thread safe. Multiple lists may try to change the song at the same time wreaking havoc on the data that is shown to the user.
 *
 */

public class FlyupPanelListener {

    // Variables for the sliding up panel layout views.
    private TextView nowPlayingText; // the view that actually shows the Now Playing... message
    private TextView songText; // the view in the slide that actually shows the song title.
    private TextView artistText; // the view in the slide that shows the artist of the song.
    private ImageView albumArt; // album art per song.
    private TextView upNext; // controls the up next functionality.
    private TextView timerText; // controls the timer of the text of the song being played.
    private ImageView albumArtHeader; // controls the album art header of the item that we are looking at.

    // Variables for the current song that is playing.
    private static String currentSongTitle = ""; // holds the name of the song currently playing
    private static String currentArtistName = ""; // holds the name of the artist of the song currently playing.
    private static Bitmap currentAlbumArt = null; // holds the album art of the current song Playing.
    private static String upNextText = ""; // holds the name of the song that will play next.
    private static CountdownTimer timer; // holds the Countdowntimer of the song that needs to be played. This is used so that we may pause and restart the timer as needed.
    private static SongInfo nextSong = null; // holds the actual next Song that will be played. This can be changed by the SelectedSongPlayer(either Songs or folderSongs version can).
    private int currentSongNumber = 0; // this tells us which song was grabbed in the list. This is just like the position in the list that is being grabbed from the SelectedSongPlayer.
    private SongInfo prevSong = null; // holds the prev SongInfo
   // private SongInfo currSong = null; // holds the current SongInfo that is playing.
    private CountdownTimer songTimer; // a copy of the countdown timer for the current song.

    // Other variables to make the FlyupPanelListener run better.
    private boolean isExpanded = false; // tell other classes whether the panel sliding listener is expanded or not.
    private boolean shuffleState = false; // tells MusicControlListener if the user has activated shuffle or not.
    private static ArrayList<SongInfo> currentSongPaths = null; // holds the song Paths that we are using currently. This is called in one of the SelectedSongPlayers
    private ShuffleQueue shuffleQueue; // this queue will hold the the shuffle songs and allow us to continue to show the user the next song in the queue.
    private final Activity currentActivity; // holds the activity that was called on. This is important for us to be able to extract the drawable resources and display the defaule album art.
    private NotificationHandler notificationHandler = null; // a single instance of the msgHandler that is used to start the notification for when a song is played.
    //private MediaPlayer mediaPlayer = null; // The copy of this media player used to help us to determine if songs are playing or not and modify the U.I. as needed.
    private FlyupPanelController flyupPanelController; // the flyupPanel controller to control the flyup Panel
    private Stack<SongInfo> playedSongs = new Stack<>(); // this stack holds the items in the list to allow for LIFO operation
    private static SeekBar seekBar; // This is the seek bar that will allow users to be able to control their songs and be able to seek to a specific position.
    static Handler handler = new Handler(); // handler to help us be able to control what it is that we are doing in the list
    private static MainUIFragment mainUIFragment; // the mainUIFragment
    private int tempTime = 0; // holds the temp time of the seekbar to hold position when a song is paused.

//    private BluetoothControlCenter bluetoothControlCenter; // controls app through bluetooth controls
//    private MediaButtonIntentReceiver intentReceiver = new MediaButtonIntentReceiver();

    // Image buttons.
    private ImageView pauseButton;
    private ImageView playButton;
    private ImageView skipButton;
    private ImageView prevButton;

    private UIHandler uiHandler; // the UI handler that we are working with.

    // Constructor to set the correct Views that we want to be manipulating. If we send the wrong Views, we will not updating them correctly. It is crucial we get this working correctly.
    public FlyupPanelListener(TextView nptext, TextView sText, TextView aText, ImageView aArt, TextView upnext, TextView t, Activity activity,
                              SeekBar seekbar, FlyupPanelController flyupPanelController, MainUIFragment mainUIFragment)
    {
        nowPlayingText = nptext;
        songText = sText;
        artistText = aText;
        albumArt = aArt;
        upNext = upnext;
        timerText = t; // don't need this right now.
        currentActivity = activity;

        uiHandler = new UIHandler(this, Looper.getMainLooper());

        seekBar = seekbar;
        this.flyupPanelController = flyupPanelController;
        this.mainUIFragment = mainUIFragment;

        pauseButton = flyupPanelController.getFlyupPanel().findViewById(R.id.pauseButton);
        playButton = flyupPanelController.getFlyupPanel().findViewById(R.id.playButton);
        skipButton = flyupPanelController.getFlyupPanel().findViewById(R.id.skipButton);
        prevButton = flyupPanelController.getFlyupPanel().findViewById(R.id.prevButton);

        // TODO: set the seekbar listener in here because we want to make sure that we are able to create it to behave in very specific way.
        SeekBarListener seekBarListener = new SeekBarListener(flyupPanelController.getFlyupPanel(), FlyupPanelListener.this); // create a seekbar listener to send to the sliding layout to allow for the list to be updated properly.
        seekBar.setOnSeekBarChangeListener(seekBarListener); // set the seekbar listener to allow for the mediaplayer to be changed when a user chooses to do so
    }

    public MainUIFragment getMainUIFragment()
    {
        return mainUIFragment;
    }

    // controls and starts a background thread very important!
    public void loadInBackground(final ArrayList<SongInfo> list, final SongInfo currentSong, final int position)
    {
        FlyupPanelSongSelectedThread backgroundThread = new FlyupPanelSongSelectedThread(list, currentSong, position, this, songTimer, flyupPanelController);
        backgroundThread.startBackgroundThread();
        while(!backgroundThread.isBackgroundThreadAlive()) {} // do nothing while the background thread is alive
     }

    public ImageView getPlayButton()
    {
        return playButton;
    }

    public ImageView getPauseButton()
    {
        return pauseButton;
    }

    // triggers a pause which converts pause button to play button.
    public void pause()
    {
        pauseButton.setVisibility(View.INVISIBLE);
        playButton.setVisibility(View.VISIBLE);
    }

    public void play()
    {
        playButton.setVisibility(View.INVISIBLE);
        pauseButton.setVisibility(View.VISIBLE);
    }

    // reports a change to the media session when it comes to a new song comes on.
    public void reportSongChangeToMediaSession()
    {
        System.out.println("Reporting change to media session");
        //UpdateMediaSessionThread updateMediaSessionThread = new UpdateMediaSessionThread(getMainUIFragment(), MusicLibrary.getCurrSong(), MusicLibrary.peekNextSong(), MusicLibrary.getCurrentSongNumber());
        //updateMediaSessionThread.startBackgroundThread(); // start the background thread!
    }

    // User selected to skip the song on the notification display.
    public void clickPause()
    {
        pauseButton.performClick(); // programmatically press the pause button.
        //mainUIFragment.getMediaSession().pauseMediaSession(); // pause the media session.
    }

    // User selected to skip the song on the notification display.
    public void clickSkip()
    {
        System.out.println("Sending click action to music controls in fly up panel listener");
        skipButton.performClick(); // programmatically press the skip button.
    }

    // User selected to press play again.
    public void clickPlay()
    {
        //mainUIFragment.getMediaSession().playMediaSession();
        playButton.performClick();

    }


    public void clickPrev()
    {
        //mainUIFragment.getMediaSession().playMediaSession();
        prevButton.performClick();
    }

    public void longClickPrev()
    {
        //mainUIFragment.getMediaSession().playMediaSession();
        prevButton.performLongClick(); // this will make the song go back.
    }

    // This method will stop any songs that are playing at the current moment. This will also stop the music player completely.
    public void stop()
    {
        // TODO: this is very important, if a user hits quit, we must stop the music player and switch the icon to show the stop icon, this is the only time this icon will show up!
        playButton.setVisibility(View.VISIBLE); // make the play button invisible, however, this should be replaced with the stop button.
        pauseButton.setVisibility(View.INVISIBLE); // make the pause button invisible here because no song is playing.

    }

    public void stopSeekBar()
    {
        handler.removeCallbacks(tickSeekBar);
    }

    // This is a background thread for the seekbar to tick properly.
    private Runnable tickSeekBar = new Runnable() {
        public void run() {

            seekBar.setProgress((int) MusicLibrary.getCurrentSongTimePosition()); // sets the seek bar to match taht of the progress bar.
            // Running this thread after 100 milliseconds
            handler.postDelayed(this, 1000);
        }
    };

    // This method will be in control of restarting the seekbar with the correct information.
    private void restartSeekBar()
    {

        int songDuration = Integer.parseInt(MusicLibrary.getCurrSong().getSongDuration()); // extracts the song duration and sets for the users.

        seekBar.setMax(songDuration); // get the duration of the song loaded in the music player.
        handler.postDelayed(tickSeekBar, 0); // begin updating the seekbar immediately
    }

    // restarts the countdown timer
    public void restartCountdownTimer(SongInfo newSong)
    {
        if(MusicLibrary.getCurrentSongTimePosition() != -1) // if not -1, we do not have an error and can display the countdown timer.
        {
            System.out.println("current song time: " + MusicLibrary.getCurrentSongTimePosition());
            System.out.println("song duration: " + newSong.getSongDuration());

            long actualTime = Long.parseLong(newSong.getSongDuration()) - MusicLibrary.getCurrentSongTimePosition(); // song duration - the position the current song is at.
            String timeToCountFrom = Long.toString(actualTime);

            CountdownTimer t = new CountdownTimer(timeToCountFrom, mainUIFragment.getView()); // need to get the view from the MainUIFragment to help set the timer.

            if(timer != null) // if the timer is not null, then we need to cancel the current timer.
            {
                cancelAndFinishTimer();
            }

            setTimer(t);
            startTimer(); // starts the timer for the song itself.
        }

    }

    // this method is called by the two SelectedSongPlayers (Songs and FolderSongs) and updates the data that the panel uses when expanded and collapsed.
    public void updateSliderData(SongInfo currentSongPlaying, SongInfo next)
    {
        //System.out.println("The current song playing has the title of: " + currentSongPlaying.getSongName());
        //playedSongs.push(currSong); // add the current song that is playing into the stack.

        if(MusicLibrary.isMusicPaused())
        {
            timer.cancel();
            timer.onFinish();
        }

        MusicLibrary.setCurrSong(currentSongPlaying); // TODO: this should be removed and taken care of inside of the service.
        //currSong = currentSongPlaying; // set the current song that is playing.
        currentSongTitle = currentSongPlaying.getSongName();
        currentArtistName = currentSongPlaying.getArtistName();
        //extractAlbumArt(currentSongPlaying); // send the current song playing to be updated, which also updates the current album art!
        currentAlbumArt = BitmapWorkshop.extractAlbumArt(mainUIFragment.getActivity(), MusicLibrary.getCurrSong()); // get the album for this song.

        //System.out.println("The density of the current album art = " + currentAlbumArt.getDensity());
        //System.out.println("The byte count of the current album art = " + currentAlbumArt.getByteCount());

        if(next == null) // if null, the shuffleQueue is still loading.
        {
            upNextText = "End of music list...";
            nextSong = null;
        }
        else {
            upNextText = "Up next: " + next.getSongName();
            nextSong = next; // set the next song.
        }

        restartSeekBar(); // simply restart the seekbar for the new song
       // reportSongChangeToMediaSession();
    }

    // this method is in control of starting and setting the timer that is
    public static void setTimer(CountdownTimer currSongTimer)
    {
        System.out.println("Timer we are setting in FlyupPanelListener is: " + timer);

        timer = currSongTimer;
    }

    // this method is in control of starting the timer. It is called from other classes in the app.
    // NOTE: This will only work if the Timer has been set using setTimer when called from other apps.
    public static void startTimer()
    {
        // may be able to get rid of this if check if it is necessary.
        if(timer != null) {
            timer.start(); // start the timer for the song that is currently playing.
        }
        else {
            System.err.println("THE TIMER RECEIVED IN PANELSLIDINGLISTENER IS NULL IT HAS NOT BEEN SET.");
        }
    }

    // this method just cancels the songTimer, mainly used for pausing a song and used by the MusicControlListener.
    public void pauseTimer()
    {
        timer.cancel();
    }

    // this will resume the timer once the use has selected to do so.
    public void resumeTimer()
    {
        long remainingTime = timer.getRemainingTime(); // get remaining time of the timer.
        String str = Long.toString(remainingTime); // convert the time to a String.
        timer = new CountdownTimer(str, flyupPanelController.getFlyupPanel()); // create a new timer.
        timer.start(); // start the timer with the time being resumed.
    }

    // this method is in charge of stopping and finishing the current timer that is being played.
    // Note: a timer has to be set using the setTimer method otherwise we will have a null exception.
    public static void cancelAndFinishTimer()
    {
        timer.cancel();
        timer.onFinish();
    }


    // sets the now playing text that we are looking for!
    public void setNowPlayingText(String text)
    {
        nowPlayingText.setText(text);
    }

    // sets the header of the album art that we need to display.
    public void setAlbumArtHeader(Bitmap albumArtHeader)
    {

    }

    // returns the album art for the current song.
    public Bitmap getCurrAlbumArt()
    {
        return currentAlbumArt;
    }

    public void resumePlayer()
    {
        updateSliderData(MusicLibrary.getCurrSong(), MusicLibrary.peekNextSong());
        refreshSliderLayout(); // refresh the slider information.
    }

    // this method refreshes the data of the sliding up layout. This is only called from other classes in order to force a refresh on the sliding layout.
    // NOTE: This only works if UpdateSliderData has been called first.
    public void refreshSliderLayout()
    {
        // simply resets the text of all of the views.

        System.out.println("Current song title after refreshing the layout: " + currentSongTitle);

        // TODO: this needs to make use of the MusicLibrary class and nothing else, very important!
        songText.setText(currentSongTitle); // shows the name of current song playing.
        artistText.setText(currentArtistName); // shows the name of the artist of the current song playing.

        albumArt.setImageBitmap(currentAlbumArt); // shows the album art of the current song playing.
        upNext.setText(upNextText); // shows what song is going to be playing next in the list.

        flyupPanelController.updatePanelHeader(); // updates the panel handler
    }
}
