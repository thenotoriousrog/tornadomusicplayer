package thenotoriousrog.tornadoplayer.Listeners;

import android.content.Intent;
import android.view.View;

import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;

public class ImportPlaylistClickListener implements View.OnClickListener {

    private MainUIFragment mainUIFragment;

    public ImportPlaylistClickListener(MainUIFragment mainUIFragment)
    {
        this.mainUIFragment = mainUIFragment;
    }

    @Override
    public void onClick(View view)
    {
        Intent openFileManagerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        openFileManagerIntent.setType("*/*"); // allow a file of any type. todo: change this to only be m3u
        mainUIFragment.startActivityForResult(openFileManagerIntent, Constants.INTENT.PICK_FILE);
    }
}
