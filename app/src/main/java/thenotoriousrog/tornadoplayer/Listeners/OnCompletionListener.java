package thenotoriousrog.tornadoplayer.Listeners;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Message;

import java.io.IOException;

import thenotoriousrog.tornadoplayer.Backend.Constants;
import thenotoriousrog.tornadoplayer.Backend.MusicLibrary;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Handlers.ServiceHandler;
import thenotoriousrog.tornadoplayer.Listeners.FlyupPanelListener;
import thenotoriousrog.tornadoplayer.Services.PlayerService;
import thenotoriousrog.tornadoplayer.UI.CountdownTimer;
import thenotoriousrog.tornadoplayer.UI.FlyupPanelController;

/**
 * Created by thenotoriousrog on 3/4/18.
 * Controls the behavior for when a song is completed.
 */

public class OnCompletionListener implements MediaPlayer.OnCompletionListener  {

    //private SongInfo newSong; // the new song that needs to be played.
    //private SongInfo nextSong; // the next song that is going to be played.
    private MediaPlayer mediaPlayer; // a copy of the media player that is being used
    private FlyupPanelController flyupPanelController; // a copy of the flyup controller to control the flyup panel with.
    private FlyupPanelListener flyupPanelListener; // a copy of the flyup panel listener we need to send relative data back with.
    private ServiceHandler serviceHandler; // service handler to send a message back to the uiThread to update slider layout when a song finishes.
    private PlayerService playerService; // player service to handle events such as updating the notification and media settings.

    public OnCompletionListener(MediaPlayer mediaPlayer, ServiceHandler serviceHandler, PlayerService playerService)
    {
       // this.newSong = newSong;
        //this.nextSong = nextSong;
        this.mediaPlayer = mediaPlayer;
        this.serviceHandler = serviceHandler;
        this.playerService = playerService;
    }

    // Defines the behavior for the class after the song is completed.
    @Override
    public void onCompletion(MediaPlayer mediaPlayer)
    {
        try
        {
            SongInfo newSong = MusicLibrary.getNextSong(); // grab the current song that is playing.

            MusicLibrary.setCurrSong(newSong); // the current song is the new song now

            if(playerService.isSpeakingWithUI()) // if speaking with the UI, send update to UI.
            {
                Message msg = new Message();
                msg.arg1 = Constants.UI.UI_UPDATE_SLIDER_INFO;
                Bundle updateInfo = new Bundle();
                updateInfo.putString("CurrSongPath", newSong.getSongPath()); // in this case, the new song is the current song we want to play.
                updateInfo.putString("NextSongPath", MusicLibrary.peekNextSong().getSongPath());
                updateInfo.putBoolean("restartTimer", true); // timer needs to be restarted.
                msg.setData(updateInfo); // set the update information
                serviceHandler.sendMessage(msg);
            }


            if(mediaPlayer != null)
            {
                mediaPlayer.release();
                mediaPlayer = null; // nullify the media player.
                mediaPlayer = new MediaPlayer(); // create a new media player.
                MusicLibrary.setMediaPlayer(mediaPlayer); // reset the new media player in music library.
            }

            mediaPlayer.reset();
            mediaPlayer.setDataSource(newSong.getSongPath()); // pass in the song path for when this thing has finished correctly.
            mediaPlayer.prepare();

            //CountdownTimer t = new CountdownTimer(nextSong.getSongDuration(), flyupPanelController.getFlyupPanel());

            //flyupPanelListener.cancelAndFinishTimer(); // finish the current timer.
            //flyupPanelListener.setTimer(t); // set the new timer.
            //flyupPanelListener.startTimer(); // start the new timer.

            mediaPlayer.start(); // start playing the song.
            playerService.updateNotification(); // update the notification once the song starts playing.

            //flyupPanelListener.updateSliderData(newSong, nextSong);
            //flyupPanelListener.refreshSliderLayout(); // force the app to refresh when the next song is playing.
            //flyupPanelListener.reportSongChangeToMediaSession(); // report a song change to the media session when the song ends.
        }
        catch (IOException  ex)
        {
            System.out.println("Something happened during the on completion listener");
            ex.printStackTrace();
        }
    }
}