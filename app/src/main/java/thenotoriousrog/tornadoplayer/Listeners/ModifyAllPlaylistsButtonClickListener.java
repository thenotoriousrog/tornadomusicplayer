package thenotoriousrog.tornadoplayer.Listeners;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.Fragments.ModifyAllPlaylistsFragmentActivity;

public class ModifyAllPlaylistsButtonClickListener implements View.OnClickListener  {

    private MainUIFragment mainUIFragment;
    private ArrayList<Playlist> playlists;
    private ArrayList<String> songPaths;
    private ArrayList<SongInfo> songInfos;
    private ArrayList<String> folders;

    public final int CREATE_PLAYLIST_INTENT = 200; // result code we want to send to the open playlist intent.


    public ModifyAllPlaylistsButtonClickListener(MainUIFragment fragment, ArrayList<Playlist> playlists, ArrayList<String> songPaths, ArrayList<SongInfo> songInfos, ArrayList<String> Folders)
    {
        this.mainUIFragment = fragment;
        this.playlists = playlists;
        this.songPaths = songPaths;
        this.songInfos = songInfos;
        this.folders = Folders;
    }

    @Override
    public void onClick(View view)
    {
        // a fix for when the user tries to modify zero playlists.
        if(mainUIFragment.getPlaylists().isEmpty())
        {
            Toast.makeText(mainUIFragment.getActivity(), "No playlists to modify", Toast.LENGTH_SHORT).show();
            return;
        }

        // This should call the activity that will allow users to modify all of their playlists at once.
        Intent addPlaylistActivity = new Intent(mainUIFragment.getActivity(), ModifyAllPlaylistsFragmentActivity.class); // create the activity to allow us to have the activity do what it is supposed to.
        Bundle playlistIntentBundle = new Bundle();
        playlistIntentBundle.putStringArrayList("songs", songPaths); // send in the songs to be used by the modifyAllPlaylistsFragmentActivity
        playlistIntentBundle.putStringArrayList("playlistNames", mainUIFragment.getPlaylistNames()); // send in the playlist names.
        playlistIntentBundle.putParcelableArrayList("songInfos", songInfos); // send in the songInfoList
        playlistIntentBundle.putStringArrayList("folders", folders); // send in the folders.
        playlistIntentBundle.putParcelableArrayList("playlists", mainUIFragment.getPlaylists()); // send in the playlists NOTE: remove this if we cannot do it this way as it could be causing problems.

        addPlaylistActivity.putExtra("args", playlistIntentBundle); // send in the intent to grab the items for the activity.
        mainUIFragment.startActivityForResult(addPlaylistActivity, CREATE_PLAYLIST_INTENT); // start the activity for the result of the songs in the playlist.

        mainUIFragment.refreshPlaylistAdapter();

        // Warning: if issues arise just uncomment the line below and pass in the viewPager via the constructor of this class from the mainUIFragment.
        // mainUIFragment.updateViewPager(viewPager.getCurrentItem()); // update the view pager after the playlist was updated.

        mainUIFragment.updateViewPager(mainUIFragment.getCurrentPageItem()); // this should work if not see the warning comment 3 lines up.
    }
}
