package thenotoriousrog.tornadoplayer.Listeners;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import thenotoriousrog.tornadoplayer.Backend.Playlist;
import thenotoriousrog.tornadoplayer.Backend.SerializeObject;
import thenotoriousrog.tornadoplayer.Backend.SongInfo;
import thenotoriousrog.tornadoplayer.Fragments.MainUIFragment;
import thenotoriousrog.tornadoplayer.R;

import org.cmc.music.common.ID3WriteException;
import org.cmc.music.metadata.MusicMetadata;
import org.cmc.music.metadata.MusicMetadataSet;
import org.cmc.music.myid3.MyID3;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by thenotoriousrog on 8/17/17.
 * This class controls the behavior of the popup for the songs that are in a specific playlist.
 */

public class PlaylistSongPopupMenuClickListener implements PopupMenu.OnMenuItemClickListener{

    private Context context; // the context that is used for pinning popup menu items to views.
    private ImageView songOptionsMenuView; // holds the three dot image that we can bind other pop messages too.
    private MainUIFragment mainUIFragment; // the main ui fragment needed in order to get the appropriate data that our users are going to need.
    private Playlist currentPlaylist; // the current playlist that we are working with.
    private SongInfo song; // the song that was chosen by the user.
    private float startingX; // holds the x position of the starting fab button.
    private float startingY; // holds the y position of the starting fab.

    // This constructor may have to take more arguments in order to get all of the options to work correctly.
    public PlaylistSongPopupMenuClickListener(Context c, ImageView optionsView, MainUIFragment fragment, Playlist playlist, SongInfo songChosen)
    {
        context = c;
        songOptionsMenuView = optionsView;
        mainUIFragment = fragment;
        currentPlaylist = playlist;
        song = songChosen;
    }

    // Writes the playlists to MainMemory
    public void writePlaylistsToMainMemory(View view, ArrayList<Playlist> playlists)
    {
        // WRITE the playlist object to main memory.
        String ser = SerializeObject.objectToString(playlists); // Trying to serialize the entire Playlist Arraylist, Not sure if it is possible or not yet.
        if(ser != null && !ser.equalsIgnoreCase(""))
        {
            String savedPlaylistFileName = "playlists.dat"; // should be something like "Playlist 1.dat"
            SerializeObject.WriteSettings(view.getContext(), ser, savedPlaylistFileName); // write the item to main memory.
        }
        else // Writing the obeject failed. Think of a better way to handle this if at all.
        {
            System.out.println("WE DID NOT WRITE THE LIST CORRECTLY, SOMETHING BAD HAPPENED.");
            SerializeObject.WriteSettings(view.getContext(), "", "playlists.dat"); // we should be getting this list if we are something bad has happened.
        }

    }

    // This message makes sure that all of the items in the fields are filled out with something before changing tags so users do not mess up their songs.
    public boolean isReadyToChange(String str1, String str2, String str3)
    {
        if( (str1.length() >= 1) && (str2.length() >= 1) && (str3.length() >= 1) ) // all fields are filled out and ready to be saved.
        {
            return true; // user has entered something in all three fields.
        }
        else // one or more of the fields is blank, must fix.
        {
            return false; // inform user to add something into the fields.
        }
    }


    private void showDiag() {

        //dialogDisplayed = true; // the dialog has now been displayed.

        final View dialogView = View.inflate(mainUIFragment.getActivity(), R.layout.edit_tags_layout,null);

        final Dialog dialog = new Dialog(mainUIFragment.getActivity(), R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                revealShow(dialogView, true, dialog);
            }
        });

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK){

                    revealShow(dialogView, false, dialog);
                    return true;
                }

                return false;
            }
        });


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.show();
    }

    private void revealCloseAnimation(final View view, final Dialog dialog, int endRadius, final String str)
    {
        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, (int)startingX, (int)startingY, endRadius, 0);

        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                dialog.dismiss();
                //dialog.cancel();
                view.setVisibility(View.INVISIBLE);
            }
        });

        anim.setDuration(400);
        anim.start();
    }

    private void revealShow(View dialogView, boolean b, final Dialog dialog) {

        final View view = dialogView.findViewById(R.id.editTagsLayout);

        System.out.println("is the dialog null upon creation? " + dialog);
        int w = view.getWidth();
        int h = view.getHeight();

        int endRadius = (int) Math.hypot(w, h);

        // todo: somehow get the animation to show on the actual position of the view itself.

        int cx = (int) (songOptionsMenuView.getX() + (songOptionsMenuView.getWidth()/2));
        int cy = (int) (songOptionsMenuView.getY())+ songOptionsMenuView.getHeight() + 56;

        final ImageView musicNote1 = view.findViewById(R.id.musicNote1);

        final ImageView musicNote2 = view.findViewById(R.id.musicNote2);
        final ImageView musicNote3 = view.findViewById(R.id.musicNote3);

        if(b){
            Animator revealAnimator = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, endRadius);

            revealAnimator.addListener(new AnimatorListenerAdapter() {

                // listens to the end animation.
                @Override
                public void onAnimationEnd(Animator animation)
                {
                    super.onAnimationEnd(animation);

//                    ObjectAnimator animator1 = ObjectAnimator.ofFloat(musicNote1, "translationX", 0, 0, 1230);
//                    Interpolator interpolator = new OvershootInterpolator();
//                    animator1.setInterpolator(interpolator);
//                    animator1.setDuration(800);
//                    animator1.start(); // start the animation.
//
//                    ObjectAnimator animator2 = ObjectAnimator.ofFloat(musicNote2, "translationX", 0, 0, 615);
//                    Interpolator interpolator2 = new OvershootInterpolator();
//                    animator2.setInterpolator(interpolator2);
//                    animator2.setDuration(800);
//                    animator2.start(); // start the animation.
//
//                    ObjectAnimator animator3 = ObjectAnimator.ofFloat(musicNote3, "translationX", 0, 0, 30);
//                    Interpolator interpolator3 = new OvershootInterpolator();
//                    animator3.setInterpolator(interpolator3);
//                    animator3.setDuration(800);
//                    animator3.start(); // start the animation.
//
//                    //ObjectAnimator animator1 = ObjectAnimator.ofFloat(musicNote1, "translationX", 0, 25, 0);
//                    //ObjectAnimator animator1 = ObjectAnimator.ofFloat(musicNote1, "translationX", 0, 25, 0);


                }
            });

            view.setVisibility(View.VISIBLE);
            revealAnimator.setDuration(400);
            revealAnimator.start();

        } else {

            Animator anim =
                    ViewAnimationUtils.createCircularReveal(view, (int)startingX, (int)startingY, endRadius, 0);

            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);

                    System.out.println("Is the dialog null? " + dialog);

                    dialog.dismiss();
                    view.setVisibility(View.INVISIBLE);

                }
            });

            anim.setDuration(400);
            anim.start();
        }

        //final EditText enterPlaylistNameField = view.findViewById(R.id.playlistEnterField); // set the field that the user will enter their playlist name.

        Button editCancelButton = view.findViewById(R.id.editCancelButton); // cancel button that cancels everything else.
        editCancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                int w = view.getWidth();
                int h = view.getHeight();
                int endRadius = (int) Math.hypot(w, h);
                //createPlaylist = false; // playlist was not created.
                revealCloseAnimation(view, dialog, endRadius, "");
            }
        });

        Button editSaveButton = (Button) view.findViewById(R.id.editSaveButton); // Confirm button for when a user Finishes entering the name of their playlist.
        editSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                System.out.println("Yes was selected, we need to add songs to this playlist with the view that we want to have.");
                //addSongsDialog.dismiss(); // close the dialog after user says yes.

                // createPlaylist = true; // the user has selected to create the playlist thus we must do so

                int w = view.getWidth();
                int h = view.getHeight();

                int endRadius = (int) Math.hypot(w, h);

                final EditText songTitle = (EditText) dialog.findViewById(R.id.editSongTitle); // song title edit field.
                final EditText songArtist = (EditText) dialog.findViewById(R.id.editSongArtist); // song artist edit field.
                final EditText songAlbum = (EditText) dialog.findViewById(R.id.editSongAlbum); // song album edit field.

                // pull data from the entered fields.
                String enteredTitle = songTitle.getText().toString(); // grab the text that the user entered.
                String enteredArtist = songArtist.getText().toString(); // grab the artist name that the user entered.
                String enteredAlbum = songAlbum.getText().toString(); // grab the album name that the user entered.

                if(isReadyToChange(enteredTitle, enteredArtist, enteredAlbum) == true) // all fields are entered and ready to be updated.
                {
                    File chosenSong = new File(song.getSongPath()); // convert the song that was chosen to the songPath to be edited by the app.
                    //File chosenSong = new File(songsToPick.get(position).getSongPath()); // convert the song path to be edited.
                    MusicMetadataSet srcSet = null;

                    try // attempt to set the source of the song file to be edited.
                    {
                        srcSet = new MyID3().read(chosenSong); // set the source to be edited.
                    }
                    catch (IOException ex) {
                        System.out.println("received error while editing song tags: " + ex.getMessage());
                        // may need to print stack trace if error becomes too difficult to understand.
                    }

                    if(srcSet == null) // if null, there is a problem, or something failed.
                    {
                        Toast.makeText(v.getContext(), "I could find your song :(", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
                        System.out.println("srcSet is null for some reason, figure out why.");
                    }
                    else // nothing went wrong, we can edit the tags of the song.
                    {
                        // change the song information to what the user entered.
                        MusicMetadata songMetaData = new MusicMetadata("name");
                        songMetaData.setSongTitle(enteredTitle); // change the title of the song.
                        songMetaData.setArtist(enteredArtist); // change the artist of the song.
                        songMetaData.setAlbum(enteredAlbum); // change the album name of the song.

                        // Overwrite the song in the same location that it was grabbed from.
                        try
                        {
                            new MyID3().update(chosenSong, srcSet, songMetaData); // overwrite the song that the user has chosen to overwrite.

                        } catch (UnsupportedEncodingException ueex) {
                            System.out.println("Caught an UnsupportedEncodingException: " + ueex.getMessage());
                            Toast.makeText(v.getContext(), "I could not understand what you wrote so I couldn't update your song :(", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
                        } catch (ID3WriteException id3wex) {
                            System.out.println("Caught ID3WriteException: " + id3wex.getMessage());
                            Toast.makeText(v.getContext(), "Something happened when I was changing your song, please try again!", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
                        } catch (IOException ex) {
                            System.out.println("Caught IOException while writing song: " + ex.getMessage());
                            Toast.makeText(v.getContext(), "I could not edit your song :(", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
                        }

                        //editDialog.dismiss(); // close edit tags dialog.

                        Toast.makeText(v.getContext(), "Once I restart your song will be changed! :D", Toast.LENGTH_SHORT).show(); // let the user know that the changes went through and when they should see it.
                    }
                }
                else // user did not enter all fields.
                {
                    Toast.makeText(v.getContext(), "You have to fill out all of the fields silly :P", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
                }

                System.out.println("Is dialog null? " + dialog);

                revealCloseAnimation(view, dialog, endRadius, "");
            }
        });

    }

    private Rect locateView(View view) {
        Rect loc = new Rect();
        int[] location = new int[2];
        if (view == null) {
            return loc;
        }
        view.getLocationOnScreen(location);

        loc.left = location[0];
        loc.top = location[1];
        loc.right = loc.left + view.getWidth();
        loc.bottom = loc.top + view.getHeight();
        return loc;
    }

    // This method controls the actions when a user selects an option from one of the items in the popup menu.
    @Override
    public boolean onMenuItemClick(MenuItem item)
    {
        if(item.getTitle().toString().equalsIgnoreCase("Remove from playlist")) // user wants to remove this song from the playlist. Very important!
        {
            ArrayList<SongInfo> playlistSongs = currentPlaylist.songs(); // get the songs in the current playlist.

            // loop through all of the songs in the list and find the song that we are working with.
            for(int i = 0; i < playlistSongs.size(); i++)
            {
                if(playlistSongs.get(i) == song) // if the SongInfo's match each other than we know that we have found the song within the list that we want to remove from the playlist.
                {
                    playlistSongs.remove(i); // remove the song from this index because this song is no longer needed in the playlist.
                }
            }

            ArrayList<Playlist> playlists = mainUIFragment.getPlaylists(); // get the playlists that we are working with.

            // loop through the playlists and grab the correct playlist that we want to update here.
            for(int i = 0; i < playlists.size(); i++)
            {
                if(playlists.get(i).name().equals(currentPlaylist.name())) // if the playlist names are correct, we need to remove that playlist and update the correct one.
                {
                    playlists.remove(i); // remove this playlist as it has the old playlist with the songs in it not being current.
                    Playlist updatedPlaylist = new Playlist(currentPlaylist.name(), playlistSongs); // create a new Playlist with the updated information.

                    playlists.add(i, updatedPlaylist); // readd the playlist in the correct spot in the list and have it print the way that it should.
                }
            }

            writePlaylistsToMainMemory(mainUIFragment.getView(), playlists); // send the updated playlists to be written to main memory.

            // update the data that we are working with for the user to see the updates being made in real time.
            mainUIFragment.setPlaylists(playlists); // set the playlists that we are working with.
            mainUIFragment.refreshPlaylistAdapter(); // refresh the playlist adapter that we are working with.
            mainUIFragment.refreshPlaylistMusicAdapter(); // refresh the adapter of the songs that we are working with.

        }
        else if(item.getTitle().toString().equalsIgnoreCase("Edit tags"))
        {
            // create an additional dialog box that will let the user enter the song fields, and then we want to modify the song based on that.
//            final Dialog editDialog = new Dialog(context); // create the dialog for the user to be able to edit the tags of the song.
//            editDialog.setContentView(R.layout.edit_tags_layout); // set the layout for when users want to change the tags of the song itself.
//            editDialog.setTitle("Fix it up!");

            // todo: this needs to be implemented for the app in the future! Very very very important!
            Toast.makeText(mainUIFragment.getActivity(), "Coming soon to an app near you!", Toast.LENGTH_SHORT).show();


//            Rect viewRect = locateView(item.getActionView());
//            startingX = viewRect.left + ((viewRect.right - viewRect.left) / 2);
//            startingY = ((viewRect.top + viewRect.bottom) / 2);
//
//            final Dialog editDialog = new Dialog(context); // create the dialog for the user to be able to edit the tags of the song.
//            editDialog.setContentView(R.layout.edit_tags_layout); // set the layout for when users want to change the tags of the song itself.
//            showDiag();

//            Button saveButton = (Button) editDialog.findViewById(R.id.editSaveButton); // save button that will change the tags of the song based on what is selected.
//            Button cancelButton = (Button) editDialog.findViewById(R.id.editCancelButton); // cancel button that will let people
//            final EditText songTitle = (EditText) editDialog.findViewById(R.id.editSongTitle); // song title edit field.
//            final EditText songArtist = (EditText) editDialog.findViewById(R.id.editSongArtist); // song artist edit field.
//            final EditText songAlbum = (EditText) editDialog.findViewById(R.id.editSongAlbum); // song album edit field.
//
//            editDialog.show(); // show the edit dialog.
//
//            // set the behavior for when the user selects save. This is when we will pull the data from the edit fields and will change the tags of the song itself.
//            saveButton.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v)
//                {
//                    System.out.println("Save the fields was selected.");
//
//                    // pull data from the entered fields.
//                    String enteredTitle = songTitle.getText().toString(); // grab the text that the user entered.
//                    String enteredArtist = songArtist.getText().toString(); // grab the artist name that the user entered.
//                    String enteredAlbum = songAlbum.getText().toString(); // grab the album name that the user entered.
//
//                    if(isReadyToChange(enteredTitle, enteredArtist, enteredAlbum) == true) // all fields are entered and ready to be updated.
//                    {
//                        File chosenSong = new File(song.getSongPath()); // convert the song that was chosen to the songPath to be edited by the app.
//                        //File chosenSong = new File(songsToPick.get(position).getSongPath()); // convert the song path to be edited.
//                        MusicMetadataSet srcSet = null;
//
//                        try // attempt to set the source of the song file to be edited.
//                        {
//                            srcSet = new MyID3().read(chosenSong); // set the source to be edited.
//                        }
//                        catch (IOException ex) {
//                            System.out.println("received error while editing song tags: " + ex.getMessage());
//                            // may need to print stack trace if error becomes too difficult to understand.
//                        }
//
//                        if(srcSet == null) // if null, there is a problem, or something failed.
//                        {
//                            Toast.makeText(v.getContext(), "I could find your song :(", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
//                            System.out.println("srcSet is null for some reason, figure out why.");
//                        }
//                        else // nothing went wrong, we can edit the tags of the song.
//                        {
//                            // change the song information to what the user entered.
//                            MusicMetadata songMetaData = new MusicMetadata("name");
//                            songMetaData.setSongTitle(enteredTitle); // change the title of the song.
//                            songMetaData.setArtist(enteredArtist); // change the artist of the song.
//                            songMetaData.setAlbum(enteredAlbum); // change the album name of the song.
//
//                            // Overwrite the song in the same location that it was grabbed from.
//                            try
//                            {
//                                new MyID3().update(chosenSong, srcSet, songMetaData); // overwrite the song that the user has chosen to overwrite.
//
//                            } catch (UnsupportedEncodingException ueex) {
//                                System.out.println("Caught an UnsupportedEncodingException: " + ueex.getMessage());
//                                Toast.makeText(v.getContext(), "I could not understand what you wrote so I couldn't update your song :(", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
//                            } catch (ID3WriteException id3wex) {
//                                System.out.println("Caught ID3WriteException: " + id3wex.getMessage());
//                                Toast.makeText(v.getContext(), "Something happened when I was changing your song, please try again!", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
//                            } catch (IOException ex) {
//                                System.out.println("Caught IOException while writing song: " + ex.getMessage());
//                                Toast.makeText(v.getContext(), "I could not edit your song :(", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
//                            }
//
//                            editDialog.dismiss(); // close edit tags dialog.
//
//                            Toast.makeText(v.getContext(), "Once I restart your song will be changed! :D", Toast.LENGTH_SHORT).show(); // let the user know that the changes went through and when they should see it.
//                        }
//                    }
//                    else // user did not enter all fields.
//                    {
//                        Toast.makeText(v.getContext(), "You have to fill out all of the fields silly :P", Toast.LENGTH_SHORT).show(); // let the user know that something failed.
//                    }
//                }
//            });
//
//            // When the user presses the cancel button, close the dialog box.
//            cancelButton.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View v)
//                {
//                    editDialog.dismiss(); // close the dialog.
//                }
//            });
        }

        return true;
    }
}
